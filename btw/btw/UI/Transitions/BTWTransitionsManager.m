//
//  BTWTransitionsManager.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 23.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWTransitionsManager.h"
#import "BTWTabBarController.h"
#import "BTWUserProfileViewController.h"
#import "BTWRouteContainerViewController.h"
#import "BTWImageEditorViewController.h"
#import "BTWDialogViewController.h"
#import "BTWMatchFoundViewController.h"
#import "BTWRouteStateChangedViewController.h"
#import "BTWPhoneNumberViewController.h"
#import "BTWAgreementPopupViewController.h"
#import "BTWUserService.h"

#import "ScaleSegue.h"
#import "PopupSegue.h"
#import "PopupUnwindSegue.h"
#import "UIActionSheet+Blocks.h"

#import <MessageUI/MessageUI.h>

@interface UINavigationController (Rotations)
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
@end

@implementation UINavigationController (Rotations)

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end

@interface BTWTransitionsManager () <UIImagePickerControllerDelegate, BTWImageEditorViewControllerDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate>
@property (nonatomic, weak) UIViewController *authViewController;
@property (nonatomic, weak) BTWTabBarController *tabBarController;
@property (nonatomic, weak) UIViewController *currentPopupViewController;
@property (nonatomic, copy) BTWDataBlock imagePickerCompletionBlock;
@property (nonatomic, assign) BOOL showEditorForImagePicker;
@end

@implementation BTWTransitionsManager

#pragma mark - Initialization
static BTWTransitionsManager *_sharedInstance = nil;
+ (BTWTransitionsManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWTransitionsManager new];
    });
    return _sharedInstance;
}

#pragma mark - Entry points
- (UIViewController *)authorizationEntryPointVC
{
    return [self viewControllerOfType:BTWViewControllerTypeAuthorization];
}

- (UIViewController *)appEntryPointVC
{
    return [self viewControllerOfType:BTWViewControllerTypeTabBarController];
}

- (UIViewController *)greetingVC
{
    return [self viewControllerOfType:BTWViewControllerTypeGreeting];
}

#pragma mark - Transitions
- (void)showAuthorization
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    UIViewController *authVC = [self authorizationEntryPointVC];
    authVC.view.top = keyWindow.frameHeight;
    [keyWindow addSubview:authVC.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        authVC.view.top = 0.0f;
    } completion:^(BOOL finished){
        for (UIView *view in keyWindow.subviews) {
            [view removeFromSuperview];
        }
        keyWindow.rootViewController = authVC;
    }];
}

- (void)processAuthorization
{
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    
    UIViewController *tabVC = [self appEntryPointVC];
    UIViewController *authVC = keyWindow.rootViewController;
    [keyWindow insertSubview:tabVC.view belowSubview:authVC.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        authVC.view.top = authVC.view.frameHeight;
    } completion:^(BOOL finished){
        for (UIView *view in keyWindow.subviews) {
            [view removeFromSuperview];
        }
        keyWindow.rootViewController = tabVC;
    }];
    
    if ([BTWSettingsService isFirstAuthorizationForUser:[BTWUserService sharedInstance].authorizedUser]) {
        [_tabBarController view];
        _tabBarController.selectedIndex = kProfileViewControllerIndex;
    }
}

#pragma mark - Common
- (UINavigationController *)rootNavigationController
{
    UINavigationController *navigationController = nil;
    if (_authViewController) {
        navigationController = (UINavigationController *)_authViewController;
    } else if (_tabBarController) {
        navigationController = _tabBarController.navigationController;
    }
    return navigationController;
}

- (void)pushViewController:(BTWViewControllerType)type intoNavigation:(UINavigationController *)navigationController
{
    UIViewController *viewController = [self viewControllerOfType:type];
    [navigationController pushViewController:viewController animated:YES];
}

- (void)showModalViewController:(UIViewController *)viewController
{
    [self showModalViewController:viewController animated:YES];
}

- (void)showModalViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[self rootNavigationController] presentViewController:viewController animated:animated completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }];
}

- (void)pushViewController:(UIViewController *)viewController
{
    [[self rootNavigationController] pushViewController:viewController animated:YES];
}

- (UIViewController *)viewControllerOfType:(BTWViewControllerType)type
{
    UIViewController *resultVC = nil;

    NSString *restorationID = nil;
    switch (type) {
        case BTWViewControllerTypeRoute:
            restorationID = @"routeViewController";
            break;
        case BTWViewControllerTypeMyProfile:
            restorationID = @"myProfileViewController";
            break;
        case BTWViewControllerTypeUserProfile:
            restorationID = @"userProfileViewController";
            break;
        case BTWViewControllerTypeAuthorization:
            restorationID = @"authViewController";
            break;
        case BTWViewControllerTypeTabBarController:
            restorationID = @"tabBarController";
            break;
        case BTWViewControllerTypePresent:
            restorationID = @"presentViewController";
            break;
        case BTWViewControllerTypeMatching:
            restorationID = @"matchingViewController";
            break;
        case BTWViewControllerTypeActiveRoutes:
            restorationID = @"activeRoutesViewController";
            break;
        case BTWViewControllerTypeSettings:
            restorationID = @"settingsViewController";
            break;
        case BTWViewControllerTypeSettingsFilters:
            restorationID = @"settingsFiltersViewController";
            break;
        case BTWViewControllerTypeMail:
            restorationID = @"mailViewController";
            break;
        case BTWViewControllerTypeGreeting:
            restorationID = @"greetingViewController";
            break;
        default:
            break;
    }
    resultVC = [self viewControllerWithIdentifier:restorationID];
    
    if (type == BTWViewControllerTypeTabBarController) {
        self.tabBarController = (BTWTabBarController *)((UINavigationController *)resultVC).topViewController;
    } else if (type == BTWViewControllerTypeAuthorization) {
        self.authViewController = resultVC;
    }

    return resultVC;
}

- (UIViewController *)viewControllerWithIdentifier:(NSString *)storyboardID
{
    UIViewController *resultVC = nil;
    if ([storyboardID isValidString]) {
        NSString *storyboardName = [BTWUIUtils isIpad] ? @"Main_iPad" : @"Main_iPhone";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
        resultVC = [storyboard instantiateViewControllerWithIdentifier:storyboardID];;
    }
    return resultVC;
}

#pragma mark - Specific
- (void)showImagePickerAndEditorWithCompletion:(BTWDataBlock)completionBlock
{
    [completionBlock copy];
    
    __weak typeof(self) wself = self;
    void (^showCameraBlock)(void) = [^{
        [wself showImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera completionHandler:completionBlock];
        self.showEditorForImagePicker = YES;
    } copy];
    
    void (^showLibraryBlock)(void) = [^{
        [wself showImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary completionHandler:completionBlock];
        self.showEditorForImagePicker = YES;
    } copy];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        RIButtonItem *pickerItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Take photo", nil) action:^{
            showCameraBlock();
        }];
        RIButtonItem *selectItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Choose from gallery", nil) action:^{
            showLibraryBlock();
        }];
        RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Cancel", nil)];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:cancelItem destructiveButtonItem:nil otherButtonItems:pickerItem, selectItem, nil];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    } else {
        showLibraryBlock();
    }
}

- (void)showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType completionHandler:(BTWDataBlock)completionBlock
{
    self.showEditorForImagePicker = NO;
    self.imagePickerCompletionBlock = completionBlock;
    
    UIImagePickerController *imagePicker = nil;
    if (sourceType == UIImagePickerControllerSourceTypeCamera) {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        imagePicker.showsCameraControls = YES;
    } else {
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [[BTWTransitionsManager sharedInstance] showModalViewController:imagePicker];
}

- (void)showDialog:(Dialog *)dialog
{
    UIViewController *topController = _tabBarController.navigationController.topViewController;
    if ( !([topController isKindOfClass:[BTWDialogViewController class]] &&
           [((BTWDialogViewController *)topController).dialog.backend_id isEqualToString:dialog.backend_id]))
    {
        [_tabBarController.navigationController popToRootViewControllerAnimated:NO];
        BTWDialogViewController *dialogViewController = [[BTWDialogViewController alloc] initWithDialog:dialog];
        [_tabBarController.navigationController pushViewController:dialogViewController animated:YES];
    }
}

- (void)showMatchFoundScreenForUser:(User *)user andTopicId:(NSString *)topicId
{
    BTWMatchFoundViewController *matchFoundViewController = [[BTWMatchFoundViewController alloc] initWithMatch:user andTopicId:topicId];
    ScaleSegue *segue = [[ScaleSegue alloc] initWithIdentifier:nil source:[BTWTransitionsManager sharedInstance].tabBarController destination:matchFoundViewController];
    [segue perform];
}

- (void)showMatchFoundScreenForDialog:(Dialog *)dialog
{
    BTWMatchFoundViewController *matchFoundViewController = [[BTWMatchFoundViewController alloc] initWithDialog:dialog];
    ScaleSegue *segue = [[ScaleSegue alloc] initWithIdentifier:nil source:[BTWTransitionsManager sharedInstance].tabBarController destination:matchFoundViewController];
    [segue perform];
}

- (void)showMailComposerWithSubject:(NSString *)subject body:(NSString *)body recepient:(NSString *)recepient
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposeController = [[MFMailComposeViewController alloc] init];
        mailComposeController.navigationBar.tintColor = [UIColor whiteColor];
        mailComposeController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
        mailComposeController.navigationBar.translucent = NO;
        mailComposeController.mailComposeDelegate = self;
        mailComposeController.delegate = self;
        [mailComposeController setToRecipients:@[recepient]];
        [mailComposeController setSubject:subject];
        [mailComposeController setMessageBody:body isHTML:NO];
        [[BTWTransitionsManager sharedInstance] showModalViewController:mailComposeController];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Plesae setup Mail account for sending messages", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)showPopupForEvent:(Event *)event
{
    BTWRouteStateChangedViewController *viewController = [[BTWRouteStateChangedViewController alloc] initWithNibName:nil bundle:nil];
    viewController.event = event;
    [self showPopup:viewController];
    self.currentPopupViewController = viewController;
}

- (BTWPhoneNumberViewController *)showPhoneNumberPopup
{
    BTWPhoneNumberViewController *viewController = [[BTWPhoneNumberViewController alloc] initWithNibName:nil bundle:nil];
    [self showPopup:viewController];
    return viewController;
}

- (void)showAgreementPopupWithCompletionHandler:(BTWVoidBlock)completionHandler
{
    BTWAgreementPopupViewController *viewController = [[BTWAgreementPopupViewController alloc] initWithNibName:nil bundle:nil];
    viewController.completionHandler = completionHandler;
    [self showPopup:viewController];
}

- (void)showPopup:(UIViewController *)popupViewController
{
    [self dismissPopup];
    PopupSegue *segue = [[PopupSegue alloc] initWithIdentifier:@"popupSegue" source:self.tabBarController destination:popupViewController];
    [segue perform];
    self.currentPopupViewController = popupViewController;
}

- (void)dismissPopup
{
    if (_currentPopupViewController) {
        PopupUnwindSegue *unwindSegue = [[PopupUnwindSegue alloc] initWithIdentifier:@"popupUnwindSegue" source:_currentPopupViewController destination:[UIApplication sharedApplication].keyWindow.rootViewController];
        [unwindSegue perform];
    }
}

#pragma mark - UIImagePickerController delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (originalImage) {
        if (_showEditorForImagePicker) {
            BTWImageEditorViewController *controller = [[BTWImageEditorViewController alloc] initWithImage:originalImage];
            controller.delegate = self;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
            [picker dismissViewControllerAnimated:YES completion:^{
                [self showModalViewController:navigationController];
            }];
        } else {
            [picker dismissViewControllerAnimated:YES completion:nil];
            if (_imagePickerCompletionBlock) {
                _imagePickerCompletionBlock(originalImage);
                self.imagePickerCompletionBlock = nil;
            }
        }
    } else {
        if (_imagePickerCompletionBlock) {
            _imagePickerCompletionBlock(nil);
            self.imagePickerCompletionBlock = nil;
        }
    }
}

#pragma mark - UINavigationController delegate & status bar appearence
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - MFMailComoserViewController delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - BTWImageEditorViewController delegate
- (void)imageEditor:(BTWImageEditorViewController *)controller didFinishWithOutputImage:(UIImage *)outputImage
{
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    if (_imagePickerCompletionBlock) {
        _imagePickerCompletionBlock(outputImage);
        self.imagePickerCompletionBlock = nil;
    }
}

@end
