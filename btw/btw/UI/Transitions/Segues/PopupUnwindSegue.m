//
//  PopupUnwindSegue.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "PopupUnwindSegue.h"

@implementation PopupUnwindSegue

- (void)perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    
    [UIView animateWithDuration:0.3 animations:^{
        sourceViewController.view.alpha = 0.0f;
    } completion:^(BOOL finished){
        [sourceViewController.view removeFromSuperview];
        [sourceViewController removeFromParentViewController];
    }];
}

@end
