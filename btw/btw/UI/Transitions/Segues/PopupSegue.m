//
//  PopupSegue.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "PopupSegue.h"

@implementation PopupSegue

- (void)perform
{
    UIViewController *sourceController = self.sourceViewController;
    UIViewController *destinationController = self.destinationViewController;
    destinationController.view.frameHeight = sourceController.view.frameHeight;
    
    [sourceController.view addSubview:destinationController.view];
    [sourceController addChildViewController:destinationController];
    [destinationController didMoveToParentViewController:sourceController];
    
    destinationController.view.alpha = 0.0;
    [UIView animateWithDuration:0.1 animations:^{
        destinationController.view.alpha = 1.0;
    }];
}

@end
