//
//  ScaleUnwindSegue.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "ScaleUnwindSegue.h"
#import "pop.h"

@implementation ScaleUnwindSegue

- (void)perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    
    POPSpringAnimation *scaleXY = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleXY.toValue = [NSValue valueWithCGPoint:(CGPoint){0.7, 0.7}];
    scaleXY.springSpeed = 15.0f;
    [sourceViewController.view pop_addAnimation:scaleXY forKey:@"scaleXY"];

    [UIView animateWithDuration:0.2 delay:0.1 options:0 animations:^{
        sourceViewController.view.alpha = 0.0f;
    } completion:^(BOOL finished){
        [sourceViewController dismissViewControllerAnimated:NO completion:nil];
    }];
}

@end
