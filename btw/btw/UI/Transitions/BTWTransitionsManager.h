 //
//  BTWTransitionsManager.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 23.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWTabBarController.h"

typedef NS_ENUM(NSUInteger, BTWViewControllerType) {
    BTWViewControllerTypeAuthorization,
    BTWViewControllerTypeTabBarController,
    BTWViewControllerTypePresent,
    BTWViewControllerTypeMyProfile,
    BTWViewControllerTypeUserProfile,
    BTWViewControllerTypeRoute,
    BTWViewControllerTypeMatching,
    BTWViewControllerTypeActiveRoutes,
    BTWViewControllerTypeSettings,
    BTWViewControllerTypeSettingsFilters,
    BTWViewControllerTypeMail,
    BTWViewControllerTypeGreeting
};

@class BTWPhoneNumberViewController;
@interface BTWTransitionsManager : NSObject

@property (nonatomic, weak, readonly) BTWTabBarController *tabBarController;

+ (BTWTransitionsManager *)sharedInstance;

- (UIViewController *)authorizationEntryPointVC;
- (UIViewController *)appEntryPointVC;
- (UIViewController *)greetingVC;
- (void)showAuthorization;
- (void)processAuthorization;
- (void)showProfile;
- (void)showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType completionHandler:(BTWDataBlock)completionBlock;
- (void)showImagePickerAndEditorWithCompletion:(BTWDataBlock)completionBlock;
- (void)showDialog:(Dialog *)dialog;
- (void)showMatchFoundScreenForUser:(User *)user andTopicId:(NSString *)topicId;
- (void)showMatchFoundScreenForDialog:(Dialog *)dialog;
- (void)showMailComposerWithSubject:(NSString *)subject body:(NSString *)body recepient:(NSString *)recepient;
- (void)showPopupForEvent:(Event *)event;
- (BTWPhoneNumberViewController *)showPhoneNumberPopup;
- (void)showAgreementPopupWithCompletionHandler:(BTWVoidBlock)completionHandler;

- (UIViewController *)viewControllerWithIdentifier:(NSString *)storyboardID;
- (UIViewController *)viewControllerOfType:(BTWViewControllerType)type;
- (void)pushViewController:(BTWViewControllerType)type intoNavigation:(UINavigationController *)navigationController;

- (UINavigationController *)rootNavigationController;
- (void)pushViewController:(UIViewController *)viewController;
- (void)showModalViewController:(UIViewController *)viewController;
- (void)showModalViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end
