//
//  MBProgressHUD+CompletionIcon.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 31.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (CompletionIcon)

- (void)handleCompletionForSuccess:(BOOL)success;
- (void)handleCompletionForSuccess:(BOOL)success hideAfterDelay:(NSTimeInterval)delay;

@end
