//
//  PopoverTable.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "WYPopoverController.h"

@interface PopoverTable : UIViewController

@property (nonatomic, strong, readonly) WYPopoverController *popover;
@property (nonatomic, strong) NSArray *itemsArray;
@property (nonatomic, strong) id selectedItem;
@property (nonatomic, assign) BOOL addEmptyItem;

@property (nonatomic, copy) void (^selectItemBlock)(id item);
@property (nonatomic, copy) NSString * (^cellTitleBlock)(id item);
@property (nonatomic, copy) NSString * (^cellSubtitleBlock)(id item);
@property (nonatomic, copy) BOOL (^shouldHighlightBlock)(id item);

- (void)presentFromView:(UIView *)aView;

@end
