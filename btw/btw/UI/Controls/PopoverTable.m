//
//  PopoverTable.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

// TODO: Поправить логику генерации модели 

#import "PopoverTable.h"
#import "KeyboardListener.h"

static CGFloat kTextHorizontalPadding = 15.0f;
static CGFloat kTextVerticalPadding = 5.0f;
static CGFloat kSeparatorPadding = 10.0f;
static CGFloat kMinCellHeight = 30.0f;
static CGFloat kMaxCellHeight = 100.0f;
static CGFloat kMaxTableHeight = 300.0f;

#pragma mark - Popover Table Item
@interface PopoverTableItem : NSObject
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, strong) id object;
@property (nonatomic, assign) BOOL shouldHighlight;

+ (instancetype)itemWithTitle:(NSString *)title subtitle:(NSString *)subtitle height:(CGFloat)height;

@end

@implementation PopoverTableItem

+ (instancetype)itemWithTitle:(NSString *)title subtitle:(NSString *)subtitle height:(CGFloat)height
{
    PopoverTableItem *item = [PopoverTableItem new];
    item.title = title;
    item.subtitle = subtitle;
    item.height = height;
    return item;
}

@end

#pragma mark - Popover Table Cell
@interface PopoverTableCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIView *separatorView;
@end

@implementation PopoverTableCell

- (id)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.titleLabel = [UILabel new];
    _titleLabel.numberOfLines = 1;
    [self addSubview:_titleLabel];
    
    self.subtitleLabel = [UILabel new];
    _subtitleLabel.numberOfLines = 1;
    [self addSubview:_subtitleLabel];
    
    self.separatorView = [UIView new];
    _separatorView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    [self addSubview:_separatorView];
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    CGFloat labelWidth = self.frame.size.width - kTextHorizontalPadding * 2;
    if ([_subtitleLabel.text isValidString]) {
        CGFloat labelHeight = floorf((frame.size.height - 3 * kTextVerticalPadding)/2);
        _titleLabel.frame = CGRectMake(kTextHorizontalPadding, kTextVerticalPadding, labelWidth, labelHeight);
        _subtitleLabel.frame = CGRectMake(kTextHorizontalPadding, CGRectGetMaxY(_titleLabel.frame) + kTextVerticalPadding, labelWidth, labelHeight);
    } else {
        _titleLabel.frame = CGRectMake(kTextHorizontalPadding, kTextVerticalPadding, labelWidth, self.frame.size.height - 2 * kTextVerticalPadding);
    }
    _separatorView.frame = CGRectMake(kSeparatorPadding, self.frame.size.height - 1, self.frame.size.width - 2 * kSeparatorPadding, 1);
}

@end

#pragma mark - Popover Table
@interface PopoverTable () <UITableViewDelegate, UITableViewDataSource, WYPopoverControllerDelegate>
@property (nonatomic, strong, readwrite) WYPopoverController *popover;
@property (nonatomic, assign) WYPopoverArrowDirection arrowDirection;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *presentingView;
@property (nonatomic, strong) NSArray *internalItems;
@end

@implementation PopoverTable

#pragma mark - Initialization
- (id)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)commonInitialization
{
    self.tableView = [UITableView new];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIViewController *contentController = [UIViewController new];
    [contentController.view addSubview:_tableView];

    [_tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [contentController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_tableView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(_tableView)]];
    [contentController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_tableView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(_tableView)]];
    [contentController.view layoutIfNeeded];
    
    self.popover = [[WYPopoverController alloc] initWithContentViewController:contentController];
    _popover.delegate = self;
}

#pragma mark - Lifecycle
- (void)presentFromView:(UIView *)aView
{
    self.presentingView = aView;
    if (!_presentingView || ![_itemsArray isValidArray]) {
        return;
    }
    
    CGFloat itemsHeight = 0.0;
    for (PopoverTableItem *item in _internalItems) {
        itemsHeight += item.height;
    }
    
    UIApplication *sharedApplication = [UIApplication sharedApplication];
    UIViewController *rootVC = sharedApplication.keyWindow.rootViewController;
    UIViewController *currentVC = [rootVC isKindOfClass:[UINavigationController class]] ? [((UINavigationController *)rootVC) topViewController] : rootVC;
    CGFloat navBarHeight = currentVC.navigationController.navigationBar.size.height + sharedApplication.statusBarFrame.size.height;
    
    CGRect viewRect = [[UIApplication sharedApplication].keyWindow convertRect:aView.frame fromView:aView.superview];
    CGFloat padding = 5.0f;
    CGFloat upAreaHeight = CGRectGetMinY(viewRect) - padding - navBarHeight;
    CGFloat downAreaHeight = ([KeyboardListener sharedInstance].isKeyboardVisible ? [KeyboardListener sharedInstance].keyboardRect.origin.y : [UIApplication sharedApplication].keyWindow.boundsHeight) - CGRectGetMaxY(viewRect) - (CGFloat)_popover.theme.arrowHeight - padding;
    
    CGFloat areaHeight; WYPopoverArrowDirection arrowDirection;
    if (itemsHeight < downAreaHeight || downAreaHeight > upAreaHeight) {
        areaHeight = MIN(downAreaHeight, kMaxTableHeight);
        arrowDirection = WYPopoverArrowDirectionUp;
    } else {
        areaHeight = MIN(upAreaHeight, kMaxTableHeight);
        arrowDirection = WYPopoverArrowDirectionDown;
    }
    
    _tableView.size = (CGSize){aView.boundsWidth, MIN(itemsHeight, areaHeight)};
    _popover.contentViewController.view.size = _tableView.size;
    _popover.popoverContentSize = _tableView.size;
    if (_selectedItem) {
        NSInteger row = [_itemsArray indexOfObject:_selectedItem];
        if (row != NSNotFound) {
            if (_addEmptyItem) {
                row = row + 1;
            }
            [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        }
    }
    [_tableView reloadData];
    
    if (!_popover.isPopoverVisible || _arrowDirection != arrowDirection) {
        [_popover presentPopoverFromRect:aView.frame inView:aView.superview permittedArrowDirections:arrowDirection animated:YES];
        self.arrowDirection = arrowDirection;
    }
}

- (void)setItemsArray:(NSArray *)itemsArray
{
    _itemsArray = itemsArray;
    [self setupTableModel];
}

- (void)setupTableModel
{
    if ([_itemsArray count] > 0) {
        NSMutableArray *internalItemsMutable = [NSMutableArray array];
        if (_addEmptyItem) {
            [internalItemsMutable addObject:[PopoverTableItem itemWithTitle:NSLocalizedString(@"Leave empty", nil) subtitle:nil height:kMinCellHeight]];
        }
        for (id item in _itemsArray) {
            NSString *title = _cellTitleBlock ? _cellTitleBlock(item) : [NSString stringWithFormat:@"%@", item];
            NSString *subtitle = _cellSubtitleBlock ? _cellSubtitleBlock(item) : nil;
            CGFloat height = [self cellHeightForTitle:title subtitle:subtitle];
            PopoverTableItem *newItem = [PopoverTableItem itemWithTitle:title subtitle:subtitle height:height];
            newItem.object = item;
            if (_shouldHighlightBlock) {
                newItem.shouldHighlight = _shouldHighlightBlock(item);
            } else {
                newItem.shouldHighlight = [item isEqual:_selectedItem];
            }
            [internalItemsMutable addObject:newItem];
        }
        self.internalItems = [NSArray arrayWithArray:internalItemsMutable];
        if (_presentingView) {
            [self presentFromView:_presentingView];
        }
    } else {
        self.internalItems = nil;
        if (_popover.popoverVisible) {
            [_popover dismissPopoverAnimated:YES];
        }
    }
}

- (CGFloat)cellHeightForTitle:(NSString *)title subtitle:(NSString *)subtitle
{
    CGFloat titleHeight = [title boundingRectWithSize:CGSizeMake(1000.0, kMaxCellHeight)
                                              options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:[BTWStylesheet popoverTableCellTitleFont]}
                                              context:nil].size.height;
    CGFloat cellHeight = 2 * kTextVerticalPadding + titleHeight;
    
    if ([subtitle isValidString]) {
        CGFloat subtitleHeight = [title boundingRectWithSize:CGSizeMake(1000.0, kMaxCellHeight)
                                                     options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[BTWStylesheet popoverTableCellSubtitleFont]}
                                                     context:nil].size.height;
        cellHeight += subtitleHeight + kTextVerticalPadding;
    }
    return MAX(cellHeight, kMinCellHeight);
}

#pragma mark - UITableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_internalItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PopoverTableItem *item = _internalItems[indexPath.row];
    return item.height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseID = @"popoverCell";
    PopoverTableCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID];
    if (!cell) {
        cell = [[PopoverTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseID];
    }
    
    PopoverTableItem *item = _internalItems[indexPath.row];
    cell.titleLabel.text = item.title;
    cell.subtitleLabel.text = item.subtitle;
    cell.titleLabel.font = item.shouldHighlight ? [BTWStylesheet popoverTableCellHighlightedTitleFont] : [BTWStylesheet popoverTableCellTitleFont];
    cell.titleLabel.textColor = item.shouldHighlight ? [BTWStylesheet popoverTableCellHighlightedTitleColor] : [BTWStylesheet popoverTableCellTitleColor];
    cell.subtitleLabel.font = [BTWStylesheet popoverTableCellSubtitleFont];
    cell.separatorView.hidden = (indexPath.row == [_internalItems count] - 1);
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectItemBlock) {
        PopoverTableItem *item = _internalItems[indexPath.row];
        _selectItemBlock(item.object);
    }
    [_popover dismissPopoverAnimated:YES];
}

#pragma mark - WYPopoverController delegate
- (BOOL)popoverControllerShouldIgnoreKeyboardBounds:(WYPopoverController *)popoverController
{
    return YES;
}

@end
