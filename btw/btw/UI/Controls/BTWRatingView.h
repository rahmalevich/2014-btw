//
//  BTWRatingView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BTWRatingViewReturnBlock)(CGFloat rating);

typedef NS_ENUM(NSInteger, BTWRatingViewType) {
    BTWRatingViewTypeHalf,
    BTWRatingViewTypeFull,
    BTWRatingViewTypeAccurate
};

@class BTWRatingView;

@protocol BTWRatingViewProtocol <NSObject>
@optional
- (void)starsSelectionChanged:(BTWRatingView *)ratingView rating:(CGFloat)rating;
@end


@interface BTWRatingView : UIControl

// the main one
@property (nonatomic, assign) CGFloat rating;

// customizable properties
@property (nonatomic, strong) UIImage *starImage;
@property (nonatomic, strong) UIImage *starHighlightedImage;
@property (nonatomic, assign) NSInteger minRating;
@property (nonatomic, assign) NSInteger maxRating;
@property (nonatomic, assign) CGFloat horizontalMargin;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) BTWRatingViewType type;
@property (nonatomic, assign) CGFloat halfStarThreshold;

// Action callbacks
@property (nonatomic, weak) IBOutlet id<BTWRatingViewProtocol> delegate;
@property (nonatomic, copy) BTWRatingViewReturnBlock returnBlock;

@end
