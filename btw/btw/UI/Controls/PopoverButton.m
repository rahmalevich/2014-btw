//
//  PopoverButton.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "PopoverButton.h"
#import "PopoverTable.h"

@interface PopoverButton ()
@property (nonatomic, strong) PopoverTable *popoverTable;
@end

@implementation PopoverButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.emptyValuePlaceholder = NSLocalizedString(@"Not selected", nil);
        [self addTarget:self action:@selector(touchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)touchUpInside:(UIButton *)sender
{
    self.popoverTable = [PopoverTable new];
    
    __weak typeof(self) wself = self;
    _popoverTable.selectItemBlock = ^(id item){
        [wself.delegate popoverButton:wself didSelectItem:item];

        NSString *title = [wself titleForItem:item];
        [wself setTitle:title forState:UIControlStateNormal];
    };
    _popoverTable.addEmptyItem = _addEmptyValue;
    _popoverTable.cellTitleBlock = _titleBlock;
    _popoverTable.cellSubtitleBlock = _subtitleBlock;
    _popoverTable.shouldHighlightBlock = _shouldHighlightBlock;
    _popoverTable.itemsArray = _itemsArray;
    _popoverTable.selectedItem = _selectedItem;
    
    [_popoverTable presentFromView:self];
}

- (NSString *)titleForItem:(id)item
{
    NSString *title;
    if (item) {
        title = _titleBlock ? _titleBlock(item) : [NSString stringWithFormat:@"%@", item];
    } else {
        title = _emptyValuePlaceholder;
    }
    return title;
}

- (void)setSelectedItem:(id)selectedItem
{
    _selectedItem = selectedItem;
    NSString *title = [self titleForItem:_selectedItem];
    [self setTitle:title forState:UIControlStateNormal];
}

@end
