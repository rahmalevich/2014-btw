//
//  BTWGalleryButtonsView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 06.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWGalleryButtonsView.h"

static const CGFloat kPadding = 10.0f;
static const CGFloat kDefaultButtonsOffset = 30.0f;
static const CGFloat kDefaultButtonSize = 60.0f;
static const CGFloat kLabelCenterOffsetY = 17.0f;
static const CGFloat kLabelBottomInset = 12.0f;

@interface BTWGalleryButton ()
@property (nonatomic, strong, readwrite) UIImage *image;
@property (nonatomic, copy, readwrite) NSString *title;
@property (nonatomic, copy, readwrite) BTWVoidBlock actionBlock;
@end

@implementation BTWGalleryButton
+ (instancetype)buttonWithImage:(UIImage *)image title:(NSString *)title action:(BTWVoidBlock)actionBlock
{
    BTWGalleryButton *newButton = [BTWGalleryButton new];
    newButton.image = image;
    newButton.title = title;
    newButton.actionBlock = actionBlock;
    return newButton;
}
@end

@interface BTWGalleryButtonsView ()
@property (nonatomic, strong) NSArray *buttons;
@property (nonatomic, strong) NSArray *buttonViews;
@end

@implementation BTWGalleryButtonsView

#pragma mark - Initialization
- (id)initWithButtons:(NSArray *)buttons
{
    if (self = [super init]) {
        self.buttons = buttons;
        
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat buttonsOffset = MIN([buttons count] > 1 ? (screenWidth - 2 * kPadding - kDefaultButtonSize * [buttons count]) / [buttons count] - 1 : 0, kDefaultButtonsOffset);
        
        NSMutableArray *buttonViews = [NSMutableArray array];
        for (NSInteger i = 0; i < [buttons count]; i++) {
            BTWGalleryButton *galleryButton = buttons[i];

            UIButton *newButton = [UIButton buttonWithType:UIButtonTypeCustom];
            newButton.frame = CGRectMake(kPadding + i * (kDefaultButtonSize + buttonsOffset), kPadding, kDefaultButtonSize, kDefaultButtonSize);
            newButton.clipsToBounds = YES;
            newButton.tag = i;
//            newButton.layer.cornerRadius = floorf(kDefaultButtonSize/2);
            [newButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.5] size:(CGSize){kDefaultButtonSize, kDefaultButtonSize}] forState:UIControlStateNormal];
            [newButton setImage:galleryButton.image forState:UIControlStateNormal];
            [newButton addTarget:self action:@selector(handleButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([galleryButton.title isValidString]) {
                UILabel *titleLabel = [UILabel new];
                titleLabel.font = [BTWStylesheet commonFontOfSize:9.0];
                titleLabel.textColor = [UIColor whiteColor];
                titleLabel.text = galleryButton.title;
                titleLabel.textAlignment = NSTextAlignmentCenter;
                [titleLabel sizeToFit];
                titleLabel.center = CGPointMake(floorf(kDefaultButtonSize/2), floorf(kDefaultButtonSize/2) + kLabelCenterOffsetY);
                [newButton addSubview:titleLabel];
                newButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, kLabelBottomInset, 0);
            }
            
            [buttonViews addObject:newButton];
            [self addSubview:newButton];
        }
        self.buttonViews = [NSArray arrayWithArray:buttonViews];
        
    }
    return self;
}

#pragma mark - View lifecycle
- (CGSize)sizeThatFits:(CGSize)size
{
    CGFloat maxX = 0;
    for (UIButton *button in _buttonViews) {
        CGFloat buttonMaxX = CGRectGetMaxX(button.frame);
        if (buttonMaxX > maxX) {
            maxX = buttonMaxX;
        }
    }
    
    return CGSizeMake(maxX + kPadding, kDefaultButtonSize + kPadding * 2);
}

- (void)handleButtonTouch:(UIButton *)sender
{
    BTWGalleryButton *galleryButton = _buttons[sender.tag];
    BTWVoidBlock actionBlock = galleryButton.actionBlock;
    if (actionBlock) {
        actionBlock();
    }
}

@end
