//
//  BTWRatingView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWRatingView.h"

static const CGFloat kDefaultHalfstarThreshold = 0.6f;

@implementation BTWRatingView

#pragma mark - Initialization & Memory Management

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.backgroundColor = [UIColor clearColor];
    
    // default star images, can be customized via properties
    _starImage = [UIImage imageNamed:@"icon_star_gray"];
    _starHighlightedImage = [UIImage imageNamed:@"icon_star_orange"];
    
    // default values
    _editable = NO;
    _minRating = 0.0f;
    _maxRating = 5.0;
    _rating = 0.0;
    _horizontalMargin = 0;
    _type = BTWRatingViewTypeFull;
    _halfStarThreshold = kDefaultHalfstarThreshold;
}

#pragma mark - Custom Getters & Setters

- (void)setReturnBlock:(BTWRatingViewReturnBlock)retBlock {
    _returnBlock = [retBlock copy];
    _delegate = nil;
}

- (void)setDelegate:(id<BTWRatingViewProtocol>)delegate {
    _delegate = delegate;
    _returnBlock = nil;
}

- (void)setRating:(CGFloat)rating {
    _rating = rating;
    [self setNeedsDisplay];
}

- (void)setType:(BTWRatingViewType)type {
    _type = type;
    [self setNeedsDisplay];
}

- (void)setEditable:(BOOL)editable {
    _editable = editable;
    self.userInteractionEnabled = editable;
}

#pragma mark - Drawing

- (CGRect)rectOfStarAtPosition:(NSInteger)position highlighted:(BOOL)highlighted {

    CGSize starImageSize = highlighted ? self.starHighlightedImage.size : self.starImage.size;
    CGFloat starSpaceWidth = self.bounds.size.width - 2 * self.horizontalMargin;

    CGFloat imageAspect = starImageSize.width / starImageSize.height;
    CGFloat starWidth = MIN(starSpaceWidth / _maxRating, self.bounds.size.height / imageAspect);
    CGFloat starHeight = starWidth * imageAspect;
    
    NSInteger offset = 0;
    offset = (_maxRating - 1 > 0) ? (starSpaceWidth - _maxRating * starWidth) / (_maxRating - 1) : 0;
    offset = MAX(0.0f, offset);
    
    CGFloat x = _horizontalMargin + starWidth * position;
    if (position > 0) {
        x += offset * position;
    }
    CGFloat y = (self.bounds.size.height - starHeight) / 2.0;
    CGRect resultRect = CGRectMake(x, y, starWidth, starHeight);
    
    return resultRect;
}

- (void)drawImage:(UIImage *)image atPosition:(NSInteger)position {
    [image drawInRect:[self rectOfStarAtPosition:position highlighted:YES]];
}

- (void)drawRect:(CGRect)rect {
    CGRect bounds = self.bounds;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Fill background color
    CGContextSetFillColorWithColor(ctx, self.backgroundColor.CGColor);
    CGContextFillRect(ctx, bounds);
    
    // Draw rating Images
    CGSize starSize = self.starHighlightedImage.size;
    for (NSInteger i = 0; i < self.maxRating; i++) {
        CGContextSaveGState(ctx);
        [self drawImage:self.starImage atPosition:i];
        
        if (i < self.rating) {
            if (i < self.rating && (self.rating < i + 1)) {
                CGPoint starPoint = [self rectOfStarAtPosition:i highlighted:NO].origin;
                CGFloat difference = self.rating - i;
                CGRect rectClip;
                rectClip.origin = starPoint;
                rectClip.size = starSize;
                if (self.type == BTWRatingViewTypeHalf && difference < self.halfStarThreshold) {
                    rectClip.size.width /= 2.0;
                } else if (self.type == BTWRatingViewTypeAccurate) {
                    rectClip.size.width *= difference;
                } else {
                    rectClip.size.width = 0;
                }
                
                if (rectClip.size.width > 0) {
                    CGContextClipToRect( ctx, rectClip);
                }
            }
            
            [self drawImage:self.starHighlightedImage atPosition:i];
        }
        
        CGContextRestoreGState(ctx);
    }
}

#pragma mark - User Interaction

- (CGFloat)ratingForPoint:(CGPoint)point {
    CGFloat rating = 0;
    for (NSInteger i = 0; i < self.maxRating; i++) {
        CGPoint p =[self rectOfStarAtPosition:i highlighted:NO].origin;
        if (point.x > p.x) {
            CGFloat increment = 1.0f;
            
            if (self.type == BTWRatingViewTypeHalf) {
                CGFloat difference = (point.x - p.x) / self.starImage.size.width;
                if (difference < self.halfStarThreshold) {
                    increment = 0.5f;
                }
            }
            rating += increment;
        }
    }
    
    rating = MAX(_minRating, rating);
    
    return rating;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self handleTouchesStarted:touches];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesMoved:touches withEvent:event];
    [self handleTouchesStarted:touches];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    [self handleTouchesEnded:touches];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    [self handleTouchesEnded:touches];
}

- (void)handleTouchesStarted:(NSSet *)touches {
    if (!self.editable) {
        return;
    }
    
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    self.rating = [self ratingForPoint:touchLocation];
    [self setNeedsDisplay];
}

- (void)handleTouchesEnded:(NSSet *)touches {
    if (!self.editable) {
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(starsSelectionChanged:rating:)]) {
        [self.delegate starsSelectionChanged:self rating:self.rating];
    }
    
    if (self.returnBlock) {
        self.returnBlock(self.rating);
    }
}

@end