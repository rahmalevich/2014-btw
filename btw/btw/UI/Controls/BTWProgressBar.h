//
//  BTWProgressBar.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 27.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWProgressBar : UIView

@property (nonatomic, strong) UIImage *glowImage;
@property (nonatomic, assign) CGFloat progress;

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated;

@end
