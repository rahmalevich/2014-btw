//
//  PopoverButton.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@class PopoverButton;
@protocol PopoverButtonDelegate <NSObject>
- (void)popoverButton:(PopoverButton *)popoverButton didSelectItem:(id)item;
@end

@interface PopoverButton : UIButton

@property (nonatomic, weak) IBOutlet id<PopoverButtonDelegate> delegate;
@property (nonatomic, strong) NSArray *itemsArray;
@property (nonatomic, strong) id selectedItem;
@property (nonatomic, copy) NSString *emptyValuePlaceholder;
@property (nonatomic, assign) BOOL addEmptyValue;

@property (nonatomic, copy) NSString * (^titleBlock)(id item);
@property (nonatomic, copy) NSString * (^subtitleBlock)(id item);
@property (nonatomic, copy) BOOL (^shouldHighlightBlock)(id item);

@end
