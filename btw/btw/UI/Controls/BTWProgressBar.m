//
//  BTWProgressBar.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 27.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProgressBar.h"
#import "pop.h"

static CGFloat const kBarWidth = 1.5f;

@interface BTWProgressBar ()
@property (nonatomic, strong) CAShapeLayer *backgroundLayer;
@property (nonatomic, strong) CAShapeLayer *progressLayer;
@property (nonatomic, strong) UIImageView *glowImageView;
@end

@implementation BTWProgressBar

#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonSetup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonSetup];
    }
    return self;
}

- (void)commonSetup
{
    self.progress = 0.0f;
    
    self.backgroundLayer = [[CAShapeLayer alloc] init];
    _backgroundLayer.fillColor = [UIColor clearColor].CGColor;
    _backgroundLayer.strokeColor = [UIColor whiteColor].CGColor;
    _backgroundLayer.lineWidth = kBarWidth;
    [self.layer addSublayer:_backgroundLayer];
    
    self.progressLayer = [[CAShapeLayer alloc] init];
    _progressLayer.fillColor = [UIColor clearColor].CGColor;
    _progressLayer.strokeColor = [UIColor orangeColor].CGColor;
    _progressLayer.lineWidth = kBarWidth;
    _progressLayer.strokeEnd = 0.0f;
    [self.layer addSublayer:_progressLayer];
    
    self.glowImageView = [[UIImageView alloc] init];
    _glowImageView.contentMode = UIViewContentModeTop;
    [self addSubview:_glowImageView];
    
    [self setupWithFrame:self.frame];
    
    self.glowImage = [UIImage imageNamed:@"glow"];
    [self setProgress:0.7 animated:YES];
}

- (void)setupWithFrame:(CGRect)frame
{
    CGFloat progressWidth = MIN(frame.size.width, frame.size.height);
    if (((NSInteger)progressWidth % 2) > 0) {
        progressWidth = progressWidth - 1.0f;
    }
    
    _backgroundLayer.frame = CGRectMake(0, 0, progressWidth, progressWidth);
    _progressLayer.frame = _backgroundLayer.frame;
    
    CGFloat radius = 0.5 * progressWidth;
    UIBezierPath *circlePath = [UIBezierPath bezierPath];
    [circlePath moveToPoint:CGPointMake(radius, 0)];
    [circlePath addArcWithCenter:CGPointMake(radius, radius) radius:radius startAngle:-0.5 * M_PI endAngle:1.5 * M_PI clockwise:YES];
    [circlePath closePath];
    
    _backgroundLayer.path = circlePath.CGPath;
    _progressLayer.path = circlePath.CGPath;
    
    _glowImageView.frame = [self frameForGlowImage:_glowImage];
    _glowImageView.transform = CGAffineTransformMakeRotation(_progress * 2 * M_PI);
}

- (CGRect)frameForGlowImage:(UIImage *)glowImage
{
    CGFloat radius = _progressLayer.frame.size.width/2;
    CGFloat glowImageWidth = glowImage.size.width;
    CGFloat glowImageHalfWidth = floor(glowImageWidth/2);
    CGRect resultFrame = CGRectMake(radius - glowImageHalfWidth, -glowImageHalfWidth, glowImageWidth, 2 * radius + glowImageWidth);
    return resultFrame;
}

#pragma mark - Lifecycle
- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self setupWithFrame:frame];
}

- (void)setGlowImage:(UIImage *)glowImage
{
    _glowImage = glowImage;
    _glowImageView.image = glowImage;
    _glowImageView.frame = [self frameForGlowImage:glowImage];
}

- (void)setProgress:(CGFloat)progress
{
    [self setProgress:progress animated:NO];
}

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated
{
    if (animated) {
        POPSpringAnimation *stroke = [POPSpringAnimation animation];
        stroke.property = [POPAnimatableProperty propertyWithName:kPOPShapeLayerStrokeEnd];
        stroke.toValue = @(progress);
        [_progressLayer pop_addAnimation:stroke forKey:@"strokeEnd"];
        
        POPSpringAnimation *rotation = [POPSpringAnimation animation];
        rotation.property = [POPAnimatableProperty propertyWithName:kPOPLayerRotation];
        rotation.fromValue = @(_progress * 2 * M_PI);
        rotation.toValue = @(progress * 2 * M_PI);
        [_glowImageView.layer pop_addAnimation:rotation forKey:@"rotation"];
    } else {
        _progressLayer.strokeEnd = progress;
        _glowImageView.transform = CGAffineTransformMakeRotation(progress * 2 * M_PI);
    }
    
    _progress = progress;
}

@end
