//
//  HMSegmentedControlWithArrow.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "HMSegmentedControlWithArrow.h"

@interface HMSegmentedControl (Protected)
- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated notify:(BOOL)notify;
@end

@interface HMSegmentedControlWithArrow ()
@property (nonatomic, strong) UIImageView *arrowImageView;
@property (nonatomic, strong) NSArray *separatorImageViewsArray;
@end

@implementation HMSegmentedControlWithArrow

#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.layer.shadowOpacity = 0.5f;
}

#pragma mark - Custom setters
- (void)setArrowImage:(UIImage *)arrowImage
{
    if (![_arrowImage isEqual:arrowImage]) {
        _arrowImage = arrowImage;
        
        if (!_arrowImageView) {
            self.arrowImageView = [UIImageView new];
            [self addSubview:_arrowImageView];
        }
        _arrowImageView.image = arrowImage;
        _arrowImageView.frame = [self calculateArrowFrame];
    }
}

- (void)setSeparatorImage:(UIImage *)separatorImage
{
    if (![_separatorImage isEqual:separatorImage]) {
        _separatorImage = separatorImage;
        if (self.sectionTitles.count > 0) {
            [self updateSeparators];
        }
    }
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];

    _arrowImageView.frame = [self calculateArrowFrame];
    for (NSInteger i = 0; i < [_separatorImageViewsArray count]; i++) {
        UIImageView *separatorImageView = _separatorImageViewsArray[i];
        separatorImageView.frame = [self calculateSeparatorFrameForIndex:i];
    }
}

#pragma mark - Overriden methods
- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated notify:(BOOL)notify
{
    [super setSelectedSegmentIndex:index animated:animated notify:notify];
    
    CGRect arrowFrame = [self calculateArrowFrame];
    if (animated) {
        [UIView animateWithDuration:0.15f animations:^{
            _arrowImageView.frame = arrowFrame;
        }];
    } else {
        _arrowImageView.frame = arrowFrame;
    }
}

- (void)setSectionTitles:(NSArray *)sectionTitles
{
    [super setSectionTitles:sectionTitles];

    _arrowImageView.frame = [self calculateArrowFrame];
    [self updateSeparators];
}

#pragma mark - Utils
- (CGRect)calculateArrowFrame
{
    CGRect resultRect = CGRectZero;
    if (_arrowImage && [self.sectionTitles count] > 0) {
        CGFloat sectionWidth = self.boundsWidth / [self.sectionTitles count];
        CGFloat arrowWidth = floorf(_arrowImage.size.width / _arrowImage.scale);
        CGFloat arrowHeight = floorf(_arrowImage.size.height / _arrowImage.scale);
        CGFloat arrowPosition = floorf((sectionWidth * self.selectedSegmentIndex) + (sectionWidth - arrowWidth)/2);
        resultRect = CGRectMake(arrowPosition, self.boundsHeight, arrowWidth, arrowHeight);
    }
    return resultRect;
}

- (CGRect)calculateSeparatorFrameForIndex:(NSInteger)index
{
    CGRect resultRect = CGRectZero;
    if (_separatorImage) {
        CGFloat sectionWidth = self.boundsWidth / [self.sectionTitles count];
        CGFloat separatorWidth = _separatorImage.size.width; //floorf(_separatorImage.size.width / _separatorImage.scale);
        CGFloat separatorHeight = _separatorImage.size.height; //floorf(_separatorImage.size.height / _separatorImage.scale);
        CGFloat separatorPosition = floorf(sectionWidth * (index + 1) - separatorWidth/2);
        resultRect = CGRectMake(separatorPosition, self.boundsHeight - separatorHeight, separatorWidth, separatorHeight);
    }
    return resultRect;
}

- (void)updateSeparators
{
    if (_separatorImage) {
        if (!_separatorImageViewsArray) {
            NSMutableArray *separatorsArray = [NSMutableArray array];
            for (NSInteger i = 0; i < [self.sectionTitles count] - 1; i++) {
                UIImageView *separatorImageView = [[UIImageView alloc] initWithImage:_separatorImage];
                separatorImageView.frame = [self calculateSeparatorFrameForIndex:i];
                [separatorsArray addObject:separatorImageView];
                [self addSubview:separatorImageView];
            }
            self.separatorImageViewsArray = [NSArray arrayWithArray:separatorsArray];
        } else {
            for (UIImageView *separatorImageView in _separatorImageViewsArray) {
                separatorImageView.image = _separatorImage;
            }
        }
    }
}

@end
