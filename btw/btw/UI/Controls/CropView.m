//
//  CropView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 30.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "CropView.h"

static const CGFloat kPadding = 0.0f;
static const CGFloat kMinWidth = 150.0f;
static const CGFloat kPinSize = 15.0f;
static const CGFloat kPinViewSize = 40.0f;
static const CGFloat kDefaultCropSize = 200.0f;

@interface CropView () <UIGestureRecognizerDelegate>
@property (nonatomic, assign) BOOL initialized;
@property (nonatomic, assign) CGPoint panPreviousPosition;
@property (nonatomic, strong) NSArray *pinsArray;
@end

@implementation CropView

#pragma mark - View lifecycle
- (void)layoutSubviews
{
    [self setupView];
}

- (void)setupView
{
    if (!_initialized) {
        NSMutableArray *pinsArray = [NSMutableArray array];
        for (int i = 0; i < 4; i++) {
            CGRect pinContainerFrame;
            pinContainerFrame.origin.x = self.center.x + ((i == 0 || i == 3) ? -1 : 1) * floorf(kDefaultCropSize/2) - floorf(kPinViewSize/2);
            pinContainerFrame.origin.y = self.center.y + ((i == 0 || i == 1) ? -1 : 1) * floorf(kDefaultCropSize/2) - floorf(kPinViewSize/2);
            pinContainerFrame.size = (CGSize){kPinViewSize, kPinViewSize};
            
            UIView *pinContainer = [[UIView alloc] initWithFrame:pinContainerFrame];
            pinContainer.tag = i;
            pinContainer.backgroundColor = [UIColor clearColor];
            
            CGRect pinFrame;
            pinFrame.origin.x = floorf((kPinViewSize - kPinSize)/2);
            pinFrame.origin.y = floorf((kPinViewSize - kPinSize)/2);
            pinFrame.size = (CGSize){kPinSize, kPinSize};
            
            UIView *pinView = [[UIView alloc] initWithFrame:pinFrame];
            pinView.backgroundColor = [UIColor whiteColor];
            pinView.layer.cornerRadius = floorf(kPinSize/2);
            [pinContainer addSubview:pinView];
            
            UIPanGestureRecognizer *pinPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pinPanRecognized:)];
            [pinContainer addGestureRecognizer:pinPanRecognizer];
            
            [pinsArray addObject:pinContainer];
            [self addSubview:pinContainer];
        }
        self.pinsArray = [NSArray arrayWithArray:pinsArray];
        
        UIPanGestureRecognizer *viewPanRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewPanRecognized:)];
        viewPanRecognizer.delegate = self;
        [self addGestureRecognizer:viewPanRecognizer];
        
        self.initialized = YES;
    }
}

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *currentPath = [self currentPath];
    CGRect currentPathRect = currentPath.bounds;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithWhite:1.0 alpha:0.5].CGColor);
    CGContextSetLineWidth(context, 0.5);
    
    CGFloat linesWidth = CGRectGetWidth(currentPathRect);
    CGFloat linesOffset = floorf(CGRectGetWidth(currentPathRect) / 3);
    UIBezierPath *linesPath = [UIBezierPath bezierPath];
    [linesPath moveToPoint:(CGPoint){CGRectGetMinX(currentPathRect), CGRectGetMinY(currentPathRect) + linesOffset}];
    [linesPath addLineToPoint:(CGPoint){CGRectGetMinX(currentPathRect) + linesWidth, CGRectGetMinY(currentPathRect) + linesOffset}];
    [linesPath moveToPoint:(CGPoint){CGRectGetMinX(currentPathRect), CGRectGetMinY(currentPathRect) + 2 * linesOffset}];
    [linesPath addLineToPoint:(CGPoint){CGRectGetMinX(currentPathRect) + linesWidth, CGRectGetMinY(currentPathRect) + 2 * linesOffset}];
    [linesPath moveToPoint:(CGPoint){CGRectGetMinX(currentPathRect) + linesOffset, CGRectGetMinY(currentPathRect)}];
    [linesPath addLineToPoint:(CGPoint){CGRectGetMinX(currentPathRect) + linesOffset, CGRectGetMinY(currentPathRect) + linesWidth}];
    [linesPath moveToPoint:(CGPoint){CGRectGetMinX(currentPathRect) + 2 * linesOffset, CGRectGetMinY(currentPathRect)}];
    [linesPath addLineToPoint:(CGPoint){CGRectGetMinX(currentPathRect) + 2 * linesOffset, CGRectGetMinY(currentPathRect) + linesWidth}];
    [linesPath stroke];

    CGFloat dashArray[] = {5,2};
    CGContextSetLineDash(context, 3, dashArray, 2);
    CGContextSetLineWidth(context, 3.0);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextAddPath(context, currentPath.CGPath);
    CGContextStrokePath(context);
    
    CGMutablePathRef overlayPath = CGPathCreateMutable();
    CGPathAddRect(overlayPath, NULL, self.bounds);
    CGPathAddPath(overlayPath, NULL, currentPath.CGPath);
    CGContextAddPath(context, overlayPath);
    CGContextSetFillColorWithColor(context, [UIColor colorWithWhite:0.0 alpha:0.7].CGColor);
    CGContextDrawPath(context, kCGPathEOFill);
    CFRelease(overlayPath);
}

#pragma mark - Crop area
- (UIBezierPath *)currentPath
{
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:((UIView *)_pinsArray[0]).center];
    [bezierPath addLineToPoint:((UIView *)_pinsArray[1]).center];
    [bezierPath addLineToPoint:((UIView *)_pinsArray[2]).center];
    [bezierPath addLineToPoint:((UIView *)_pinsArray[3]).center];
    [bezierPath closePath];

    return bezierPath;
}

- (CGRect)currentCropArea
{
    CGRect cropArea = [self currentPath].bounds;
    return cropArea;
}

#pragma mark - Recognizing gestures
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    BOOL result = NO;
    CGRect pathRect = [self currentPath].bounds;
    CGPoint touchLocation = [touch locationInView:self];
    if (CGRectContainsPoint(pathRect, touchLocation)) {
        result = YES;
    }
    return result;
}

- (void)viewPanRecognized:(UIPanGestureRecognizer *)panRecognizer
{
    CGPoint currentPosition = [panRecognizer locationInView:self];
    if (panRecognizer.state == UIGestureRecognizerStateChanged)
    {
        CGFloat xOffset = currentPosition.x - _panPreviousPosition.x;
        CGFloat yOffset = currentPosition.y - _panPreviousPosition.y;
        
        CGRect pathRect = [self currentPath].bounds;
        xOffset = MAX(kPadding - CGRectGetMinX(pathRect), xOffset);
        xOffset = MIN(self.bounds.size.width - kPadding - CGRectGetMaxX(pathRect), xOffset);
        yOffset = MAX(kPadding - CGRectGetMinY(pathRect), yOffset);
        yOffset = MIN(self.bounds.size.height - kPadding - CGRectGetMaxY(pathRect), yOffset);
        
        for (UIView *pinView in _pinsArray) {
            CGPoint center = pinView.center;
            center.x += xOffset;
            center.y += yOffset;
            pinView.center = center;
        }
        
        [self setNeedsDisplay];
    }
    self.panPreviousPosition = currentPosition;
}

- (void)pinPanRecognized:(UIPanGestureRecognizer *)panRecognizer
{
    CGPoint currentPosition = [panRecognizer locationInView:self];
    if (panRecognizer.state == UIGestureRecognizerStateChanged)
    {
        UIView *pinView = panRecognizer.view;
        
        CGRect pathRect = [self currentPath].bounds;
        CGFloat xOffset = currentPosition.x - _panPreviousPosition.x;
        CGFloat yOffset = currentPosition.y - _panPreviousPosition.y;

        BOOL isLeft = pinView.center.x < CGRectGetMidX(pathRect);
        BOOL isTop = pinView.center.y < CGRectGetMidY(pathRect);
        NSInteger multiplier = (isLeft ? 1 : -1) * (isTop ? 1 : -1);
        if (fabsf(xOffset) > fabsf(yOffset)) {
            xOffset = MAX((isLeft ? kPadding : CGRectGetMinX(pathRect) + kMinWidth) - pinView.center.x, xOffset);
            xOffset = MIN((isLeft ? CGRectGetMaxX(pathRect) - kMinWidth : self.bounds.size.width - kPadding) - pinView.center.x, xOffset);
            xOffset = MAX((isTop ? kPadding : CGRectGetMinY(pathRect) + kMinWidth) - pinView.center.y, multiplier * xOffset) / multiplier;
            xOffset = MIN((isTop ? CGRectGetMaxY(pathRect) - kMinWidth : self.bounds.size.height - kPadding) - pinView.center.y, multiplier * xOffset) / multiplier;
            yOffset = multiplier * xOffset;
        } else {
            yOffset = MAX((isTop ? kPadding : CGRectGetMinY(pathRect) + kMinWidth) - pinView.center.y, yOffset);
            yOffset = MIN((isTop ? CGRectGetMaxY(pathRect) - kMinWidth : self.bounds.size.height - kPadding) - pinView.center.y, yOffset);
            yOffset = MAX((isLeft ? kPadding : CGRectGetMinX(pathRect) + kMinWidth) - pinView.center.x, multiplier * yOffset) / multiplier;
            yOffset = MIN((isLeft ? CGRectGetMaxX(pathRect) - kMinWidth : (self.bounds.size.width - kPadding)) - pinView.center.x, multiplier * yOffset) / multiplier;
            xOffset = multiplier * yOffset;
        }
        pinView.center = CGPointMake(pinView.center.x + xOffset, pinView.center.y + yOffset);
        
        UIView *prevPinView = pinView.tag == 0 ? _pinsArray[3] : _pinsArray[pinView.tag - 1];
        UIView *nextPinView = pinView.tag == 3 ? _pinsArray[0] : _pinsArray[pinView.tag + 1];
        CGPoint prevPinCenter = prevPinView.center;
        CGPoint nextPinCenter = nextPinView.center;
        if (pinView.tag == 0 || pinView.tag == 2) {
            prevPinCenter.x = pinView.center.x;
            nextPinCenter.y = pinView.center.y;
        } else {
            prevPinCenter.y = pinView.center.y;
            nextPinCenter.x = pinView.center.x;
        }
        prevPinView.center = prevPinCenter;
        nextPinView.center = nextPinCenter;

        [self setNeedsDisplay];
    }
    self.panPreviousPosition = currentPosition;
}

@end
