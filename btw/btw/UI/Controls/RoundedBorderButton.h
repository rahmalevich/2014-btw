//
//  RoundedBorderButton.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 21.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoundedBorderButton : UIButton

@property (nonatomic, assign) CGFloat cornerRadius;

@end
