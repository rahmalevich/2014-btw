//
//  BTWProfileContainerViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "RKSwipeBetweenViewControllers.h"
#import "HMSegmentedControlWithArrow.h"

@interface BTWSwipeBetweenViewControllers : RKSwipeBetweenViewControllers

@property (nonatomic, strong, readonly) UISegmentedControl *segmentedControl;
@property (nonatomic, assign) BOOL hideHeader;
@property (nonatomic, assign) BOOL disableScrolling;

- (void)setSectionTitles:(NSArray *)sectionTitles;
- (void)setCurrentPageIndex:(NSInteger)currentPageIndex;
- (void)setLeftIndicatorVisible:(BOOL)visible;
- (void)setRightIndicatorVisible:(BOOL)visible;

@end
