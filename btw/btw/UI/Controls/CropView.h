//
//  CropView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 30.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CropView : UIView

- (CGRect)currentCropArea;

@end
