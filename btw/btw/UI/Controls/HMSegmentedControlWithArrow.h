//
//  HMSegmentedControlWithArrow.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "HMSegmentedControl.h"

@interface HMSegmentedControlWithArrow : HMSegmentedControl

@property (nonatomic, strong) UIImage *arrowImage;
@property (nonatomic, strong) UIImage *separatorImage;

@end
