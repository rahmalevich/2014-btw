//
//  BTWProfileContainerViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSwipeBetweenViewControllers.h"
#import "BTWTransitionsManager.h"

static CGFloat kHeaderHeight = 40.0f;
static CGFloat kSegmentControlHeight = 28.0f;
static CGFloat kSegmentControlWidth = 240.0f;
static CGFloat kIndicatorViewSize = 10.0f;
static CGFloat kIndicatorViewOffset = 10.0f;

@interface RKSwipeBetweenViewControllers (Protected)

@property (nonatomic, assign) NSInteger currentPageIndex;

- (void)setupSegmentButtons;
- (void)setupPageViewController;
- (void)animateToIndex:(NSInteger)index;

@end

@interface BTWSwipeBetweenViewControllers ()
@property (nonatomic, assign) BOOL didSetup;
@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) UIView *leftIndicatorView;
@property (strong, nonatomic) UIView *rightIndicatorView;
@property (strong, nonatomic, readwrite) UISegmentedControl *segmentedControl;
@end

@implementation BTWSwipeBetweenViewControllers

#pragma mark - Initialization
- (id)init
{
    UIPageViewController *pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    if (self = [super initWithRootViewController:pageController]) {}
    return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNavigationBarHidden:YES animated:NO];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kHeaderHeight)];
    headerView.backgroundColor = [BTWStylesheet commonHeaderColor];
    [self.view insertSubview:headerView belowSubview:self.navigationBar];
    self.headerView = headerView;
    
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithFrame:CGRectMake((self.view.frameWidth - kSegmentControlWidth)/2, (kHeaderHeight - kSegmentControlHeight)/2, kSegmentControlWidth, kSegmentControlHeight)];
    segmentedControl.tintColor = [BTWStylesheet commonBlueColor];
    [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    [headerView addSubview:segmentedControl];
    self.segmentedControl = segmentedControl;
    
    UIView *leftIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(segmentedControl.frame) - kIndicatorViewOffset - kIndicatorViewSize, floorf(0.5 * (CGRectGetHeight(headerView.frame) - kIndicatorViewSize)), kIndicatorViewSize, kIndicatorViewSize)];
    leftIndicatorView.backgroundColor = [BTWStylesheet commonBlueColor];
    leftIndicatorView.layer.cornerRadius = floorf(kIndicatorViewSize/2);
    leftIndicatorView.alpha = 0.0f;
    [headerView addSubview:leftIndicatorView];
    self.leftIndicatorView = leftIndicatorView;
    
    UIView *rightIndicatorView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(segmentedControl.frame) + kIndicatorViewOffset, floorf(0.5 * (CGRectGetHeight(headerView.frame) - kIndicatorViewSize)), kIndicatorViewSize, kIndicatorViewSize)];
    rightIndicatorView.backgroundColor = [BTWStylesheet commonBlueColor];
    rightIndicatorView.layer.cornerRadius = floorf(kIndicatorViewSize/2);
    rightIndicatorView.alpha = 0.0f;
    [headerView addSubview:rightIndicatorView];
    self.rightIndicatorView = rightIndicatorView;
}

#pragma mark - Public
- (void)setSectionTitles:(NSArray *)sectionTitles
{
    [_segmentedControl removeAllSegments];
    for (NSInteger i = 0; i < [sectionTitles count]; i++) {
        [_segmentedControl insertSegmentWithTitle:sectionTitles[i] atIndex:i animated:NO];
    }
}

- (void)setLeftIndicatorVisible:(BOOL)visible
{
    [UIView animateWithDuration:0.3 animations:^{
        _leftIndicatorView.alpha = visible ? 1.0f : 0.0f;
    }];
}

- (void)setRightIndicatorVisible:(BOOL)visible
{
    [UIView animateWithDuration:0.3 animations:^{
        _rightIndicatorView.alpha = visible ? 1.0f : 0.0f;
    }];
}

#pragma mark - Custom setters
- (void)setHideHeader:(BOOL)hideHeader
{
    _hideHeader = hideHeader;
    _headerView.hidden = hideHeader;
}

- (void)setDiableScrolling:(BOOL)disableScrolling
{
    if (_disableScrolling != disableScrolling) {
        _disableScrolling = disableScrolling;
        self.pageController.dataSource = _disableScrolling ? nil : self;
    }
}

#pragma mark - Overriden methods
- (void)setupSegmentButtons {}

- (void)setupPageViewController
{
    if (!_didSetup) {
        self.didSetup = YES;
        [super setupPageViewController];
        if (_disableScrolling) {
            self.pageController.dataSource = nil;
        }
        [self setCurrentPageIndex:self.currentPageIndex];
    }
}

- (void)setCurrentPageIndex:(NSInteger)currentPageIndex
{
    NSInteger previousIndex = self.currentPageIndex;
    [super setCurrentPageIndex:currentPageIndex];
    
    if (_segmentedControl.selectedSegmentIndex != currentPageIndex) {
        _segmentedControl.selectedSegmentIndex = currentPageIndex;
    }
    
    [self animateToIndex:self.currentPageIndex];
    
    [self.pageController setViewControllers:@[self.viewControllerArray[self.currentPageIndex]] direction:(self.currentPageIndex > previousIndex ? UIPageViewControllerNavigationDirectionForward : UIPageViewControllerNavigationDirectionReverse) animated:YES completion:nil];
}

- (void)segmentedControlValueChanged:(UISegmentedControl *)segmentedControl
{
    self.currentPageIndex = segmentedControl.selectedSegmentIndex;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    [super pageViewController:pageViewController didFinishAnimating:finished previousViewControllers:previousViewControllers transitionCompleted:completed];
    
    if (completed) {
        self.segmentedControl.selectedSegmentIndex = self.currentPageIndex;
    }
}

@end
