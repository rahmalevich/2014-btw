//
//  NMRangeSliderWithLabels.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "NMRangeSlider.h"

@interface BTWRangeSliderWithLabels : UIView

@property (nonatomic, strong, readonly) NMRangeSlider *slider;
@property (nonatomic, strong, readonly) UILabel *lowerLabel;
@property (nonatomic, strong, readonly) UILabel *upperLabel;

@property (nonatomic, copy) void(^sliderValueChangedBlock)(NMRangeSlider *);

@end
