//
//  BTWGalleryButtonsView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 06.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWGalleryButton : NSObject
@property (nonatomic, strong, readonly) UIImage *image;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) BTWVoidBlock actionBlock;
+ (instancetype)buttonWithImage:(UIImage *)image title:(NSString *)title action:(BTWVoidBlock)actionBlock;
@end

@interface BTWGalleryButtonsView : UIView
- (id)initWithButtons:(NSArray *)buttons;
@end
