//
//  MBProgressHUD+CompletionIcon.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 31.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MBProgressHUD+CompletionIcon.h"

static NSTimeInterval const kDefaultDelay = 2.0f;

@implementation MBProgressHUD (CompletionIcon)

- (void)handleCompletionForSuccess:(BOOL)success
{
    [self handleCompletionForSuccess:success hideAfterDelay:kDefaultDelay];
}

- (void)handleCompletionForSuccess:(BOOL)success hideAfterDelay:(NSTimeInterval)delay
{
    UIImage *hudImage = success ? [UIImage imageNamed:@"icon_success"] : [UIImage imageNamed:@"icon_error"];
    UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
    hudImageView.size = hudImage.size;
    self.mode = MBProgressHUDModeCustomView;
    self.customView = hudImageView;
    [self hide:YES afterDelay:delay];
}

@end
