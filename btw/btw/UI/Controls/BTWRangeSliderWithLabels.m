//
//  NMRangeSliderWithLabels.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWRangeSliderWithLabels.h"
#import <objc/runtime.h>

const CGFloat kDefaultSliderHeight = 35.0f;
const CGFloat kDefaultFontSize = 12.0f;
const CGSize  kDefaultLabelSize = (CGSize){40.0f, 22.0f};

#pragma mark - NMRangeSlider
@protocol NMRangeSliderObserver <NSObject>
- (void)addObserversForSlider:(NMRangeSlider *)slider;
@end

@interface NMRangeSlider (Protected)
@property (retain, nonatomic) UIImageView* lowerHandle;
@property (retain, nonatomic) UIImageView* upperHandle;
@property (nonatomic, unsafe_unretained) id<NMRangeSliderObserver> sliderObserver;
@end

@implementation NMRangeSlider (Protected)
@dynamic lowerHandle;
@dynamic upperHandle;
@dynamic sliderObserver;

- (void)setSliderObserver:(id<NMRangeSliderObserver>)sliderObserver
{
    objc_setAssociatedObject(self, @selector(sliderObserver), sliderObserver, OBJC_ASSOCIATION_ASSIGN);
}

- (id<NMRangeSliderObserver>)sliderObserver
{
    return objc_getAssociatedObject(self, @selector(sliderObserver));
}

- (void)didAddSubview:(UIView *)subview
{
    [super didAddSubview:subview];
    
    if (self.lowerHandle && self.upperHandle) {
        [self.sliderObserver addObserversForSlider:self];
    }
}

@end

#pragma mark - BTWRangeSliderWithLabels
@interface BTWRangeSliderWithLabels () <NMRangeSliderObserver>
@property (nonatomic, assign) BOOL didAddObserversForSlider;
@property (nonatomic, strong, readwrite) NMRangeSlider *slider;
@property (nonatomic, strong, readwrite) UILabel *lowerLabel;
@property (nonatomic, strong, readwrite) UILabel *upperLabel;
@end

@implementation BTWRangeSliderWithLabels

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.slider = [[NMRangeSlider alloc] initWithFrame:CGRectMake(0.0f, self.frameHeight - kDefaultSliderHeight, self.frameWidth, kDefaultSliderHeight)];
    _slider.sliderObserver = self;
    [_slider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:_slider];
    
    self.lowerLabel = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero, kDefaultLabelSize}];
    _lowerLabel.backgroundColor = [UIColor clearColor];
    _lowerLabel.font = [BTWStylesheet commonLightFontOfSize:kDefaultFontSize];
    _lowerLabel.textColor = [BTWStylesheet settingsTitleColor];
    [self addSubview:_lowerLabel];
    
    self.upperLabel = [[UILabel alloc] initWithFrame:(CGRect){CGPointZero, kDefaultLabelSize}];
    _upperLabel.backgroundColor = [UIColor clearColor];
    _upperLabel.font = [BTWStylesheet commonLightFontOfSize:kDefaultFontSize];
    _upperLabel.textColor = [BTWStylesheet settingsTitleColor];
    [self addSubview:_upperLabel];
}

- (void)dealloc
{
    if (_didAddObserversForSlider) {
        [_slider.lowerHandle removeObserver:self forKeyPath:@"frame"];
        [_slider.upperHandle removeObserver:self forKeyPath:@"frame"];
    }
}

#pragma mark - Lifecycle
- (void)updateRectForLabel:(UILabel *)label
{
    CGRect resultRect = label.frame;
    resultRect.origin.y = _slider.y - kDefaultLabelSize.height;
    if ([label isEqual:_lowerLabel]) {
        resultRect.origin.x = self.x + _slider.lowerCenter.x - floorf(resultRect.size.width/2);
    } else {
        resultRect.origin.x = self.x + _slider.upperCenter.x - floorf(resultRect.size.width/2);
    }
    label.frame = resultRect;
}

- (void)addObserversForSlider:(NMRangeSlider *)slider
{
    if (!_didAddObserversForSlider) {
        self.didAddObserversForSlider = YES;
        
        [slider.lowerHandle addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
        [self updateRectForLabel:_lowerLabel];
        
        [slider.upperHandle addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
        [self updateRectForLabel:_upperLabel];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:_slider.lowerHandle]) {
        _lowerLabel.text = [NSString stringWithFormat:@"%d", (int)_slider.lowerValue];
        [self updateRectForLabel:_lowerLabel];
    } else {
        _upperLabel.text = [NSString stringWithFormat:@"%d", (int)_slider.upperValue];
        [self updateRectForLabel:_upperLabel];
    }
}

- (void)sliderValueChanged:(NMRangeSlider *)slider
{
    if (_sliderValueChangedBlock) {
        _sliderValueChangedBlock(slider);
    }
}

@end
