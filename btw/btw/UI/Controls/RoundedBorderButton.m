//
//  RoundedBorderButton.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 21.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "RoundedBorderButton.h"

CGFloat kDefaultCornerRaduis = 15.0f;

@implementation RoundedBorderButton

#pragma mark - Initialization
- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    [self setTitleColor:[self tintColor] forState:UIControlStateNormal];
    [self setTitleColor:[[self tintColor] colorWithAlphaComponent:0.8]
 forState:UIControlStateHighlighted];
    [self setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
    
    self.layer.cornerRadius = kDefaultCornerRaduis;
    self.layer.borderWidth = 1.0;
    
    [self refreshBorderColor];
}

#pragma mark - Overriden methods
- (void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
}

- (void)setTintColor:(UIColor *)tintColor
{
    [super setTintColor:tintColor];
    [self setTitleColor:tintColor forState:UIControlStateNormal];
    [self refreshBorderColor];
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    [self refreshBorderColor];
}

- (void)refreshBorderColor
{
    UIColor *resultColor = nil;
    if ([self isHighlighted]) {
        resultColor = [[self tintColor] colorWithAlphaComponent:0.5];
    } else if ([self isEnabled]) {
        resultColor = self.tintColor;
    } else {
        resultColor = [UIColor lightGrayColor];
    }
    self.layer.borderColor = resultColor.CGColor;
}

- (void)setHighlighted:(BOOL)highlighted
{
    // Only change if necessary.
    if ( highlighted == super.highlighted ) {
        return;
    }
    
    [super setHighlighted:highlighted];

    [self refreshBorderColor];
    
    [self setNeedsDisplay];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize org = [super sizeThatFits:self.bounds.size];
    return CGSizeMake(org.width + 20, org.height - 2);
}

@end
