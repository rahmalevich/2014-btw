//
//  BTWMailContainerViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMailContainerViewController.h"
#import "BTWSwipeBetweenViewControllers.h"
#import "BTWMailViewController.h"
#import "BTWNotificationsViewController.h"
#import "BTWMessagingService.h"
#import "BTWNotificationsService.h"

@interface BTWMailContainerViewController ()
@property (nonatomic, strong) BTWSwipeBetweenViewControllers *swipeControllersContainer;
@end

@implementation BTWMailContainerViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.swipeControllersContainer = [BTWSwipeBetweenViewControllers new];
    
    BTWMailViewController *mailViewController = [[BTWMailViewController alloc] initWithNibName:nil bundle:nil];
    mailViewController.containerController = self;
    
    BTWNotificationsViewController *notificationsViewController = [[BTWNotificationsViewController alloc] initWithNibName:nil bundle:nil];
    notificationsViewController.containerController = self;
    
    _swipeControllersContainer.viewControllerArray = [@[mailViewController, notificationsViewController] mutableCopy];
    
    [[BTWMessagingService sharedInstance] addObserver:self forKeyPath:@"unconfirmedMessagesCount" options:NSKeyValueObservingOptionNew context:nil];
    [[BTWNotificationsService sharedInstance] addObserver:self forKeyPath:@"unconfirmedNotificationsCount" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc
{
    [[BTWMessagingService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedMessagesCount"];
    [[BTWNotificationsService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedNotificationsCount"];
}

#pragma mark - Observing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"unconfirmedMessagesCount"] || [keyPath isEqualToString:@"unconfirmedNotificationsCount"]) {
        [self updateIndicators];
    }
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Inbox", nil);
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    _swipeControllersContainer.view.frame = self.view.bounds;
    _swipeControllersContainer.sectionTitles = @[NSLocalizedString(@"Chats", nil), NSLocalizedString(@"Notifications", nil)];
    
    [self.view addSubview:_swipeControllersContainer.view];
    [self addChildViewController:_swipeControllersContainer];
    [_swipeControllersContainer didMoveToParentViewController:self];
    
    [self updateIndicators];
}

- (void)updateIndicators
{
    [_swipeControllersContainer setLeftIndicatorVisible:([BTWMessagingService sharedInstance].unconfirmedMessagesCount > 0)];
    [_swipeControllersContainer setRightIndicatorVisible:([BTWNotificationsService sharedInstance].unconfirmedNotificationsCount > 0)];
}

- (void)showContacts
{
    [_swipeControllersContainer setCurrentPageIndex:0];
}

- (void)showNotifications
{
    [_swipeControllersContainer setCurrentPageIndex:1];
}

@end
