//
//  BTWNotificationsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWNotificationsViewController.h"
#import "BTWNotificationsService.h"
#import "BTWUserProfileViewController.h"
#import "BTWTransitionsManager.h"
#import "BTWNotificationCell.h"
#import "BTWMapAttachmentViewController.h"
#import "BTWUserService.h"
#import "BTWPhoneNumberViewController.h"

#import "MBProgressHUD+CompletionIcon.h"

static NSString * const kNotificationCellReuseId = @"kNotificationCellReuseId";

@interface BTWNotificationsViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, BTWNotificationCellDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSFetchedResultsController *dataController;
@end

@implementation BTWNotificationsViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    [_tableView registerNib:[UINib nibWithNibName:@"BTWNotificationCell" bundle:nil] forCellReuseIdentifier:kNotificationCellReuseId];
    _tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, kCentralButtonOffset, 0.0f);
    
    self.dataController = [Notification MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"created" ascending:NO delegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak typeof(self) wself = self;
    [[BTWNotificationsService sharedInstance] updateNotificationsWithCompletionHandler:^(NSError *error){
        if (!error) {
            [[BTWNotificationsService sharedInstance] confirmNotificationsWithCompletionHandler:nil];
        }
        
        // Делаем задержку для корректной последовательности анимаций
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [wself updateAppearence];
        });
    }];
    [self updateAppearence];
}

- (void)updateAppearence
{
    if ([_dataController.fetchedObjects count] == 0) {
        if ([BTWNotificationsService sharedInstance].isLoadingData) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _tableView.alpha = 1.0f;
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

#pragma mark - UITableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataController.fetchedObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Notification *notification = _dataController.fetchedObjects[indexPath.row];
    return [BTWNotificationCell cellHeightForNotification:notification];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTWNotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:kNotificationCellReuseId forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    BTWNotificationCell *notificationCell = (BTWNotificationCell *)cell;
    Notification *notification = _dataController.fetchedObjects[indexPath.row];
    notificationCell.notification = notification;
    notificationCell.delegate = self;
}

#pragma mark - BTWNotificationCell delegate
- (void)avatarDidTouchForCell:(BTWNotificationCell *)cell
{
    BTWUserProfileViewController *profileController = (BTWUserProfileViewController *)[[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeUserProfile];
    profileController.user = cell.notification.user;
    [_containerController.navigationController pushViewController:profileController animated:YES];
}

- (void)makeActionForNotification:(Notification *)notification accept:(BOOL)accept showScreen:(BOOL)showScreen
{
    if (accept && ![[BTWUserService sharedInstance].authorizedUser.phone_verified boolValue]) {
        __weak typeof(self) wself = self;
        BTWPhoneNumberViewController *viewController = [[BTWTransitionsManager sharedInstance] showPhoneNumberPopup];
        viewController.completionBlock = ^{
            [wself makeActionForNotification:notification accept:accept showScreen:showScreen];
        };
    } else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        BTWNetworkResponseBlock completionHanlder = ^(NSDictionary *response, NSError *error){
            [hud handleCompletionForSuccess:(error ? NO : YES)];
        };
        if (accept) {
            [[BTWNotificationsService sharedInstance] acceptNotification:notification withCompletionHadnler:completionHanlder];
        } else {
            [[BTWNotificationsService sharedInstance] declineNotification:notification withCompletionHadnler:completionHanlder];
        }
    }
}

- (void)acceptButtonDidTouchForCell:(BTWNotificationCell *)cell
{
    BOOL showScreen = [cell.notification.type isEqualToString:kNotificationTypeLike];
    [self makeActionForNotification:cell.notification accept:YES showScreen:showScreen];
}

- (void)declineButtonDidTouchForCell:(BTWNotificationCell *)cell
{
    [self makeActionForNotification:cell.notification accept:NO showScreen:NO];
}

- (void)routeButtonDidTouchForCell:(BTWNotificationCell *)cell
{
    if ([cell.notification.travel_polyline isValidString]) {
        BTWMapAttachmentViewController *viewController = [[BTWMapAttachmentViewController alloc] initWithPolyline:cell.notification.travel_polyline andDate:cell.notification.travel_date];
        [[BTWTransitionsManager sharedInstance] pushViewController:viewController];
    }
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [_tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];

    [self updateAppearence];
}

@end
