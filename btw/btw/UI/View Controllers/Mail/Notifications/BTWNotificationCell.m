//
//  BTWNotificaitonCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWNotificationCell.h"

#import "SmartName.h"
#import "UIButton+WebCache.h"
#import "TTTAttributedLabel.h"

static NSString *kUserNameLink = @"kUserNameLink";

@interface BTWNotificationCell () <TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (weak, nonatomic) IBOutlet UIButton *routeButton;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *actionIconImageView;
@property (weak, nonatomic) IBOutlet UIView *titleContainerView;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@property (strong, nonatomic) TTTAttributedLabel *titleLabel;
@end

@implementation BTWNotificationCell

#pragma mark - Initialization
+ (CGFloat)cellHeightForNotification:(Notification *)notification
{
    CGFloat resultHeight = 0.0f;
    if ([notification.type isEqualToString:kNotificationTypeLike]) {
        resultHeight = 90.0f;
    } else if ([notification.type isEqualToString:kNotificationTypeRide]) {
        resultHeight = 105.0f;
    }
    return resultHeight;
}

- (void)awakeFromNib
{
    _avatarButton.layer.cornerRadius = floorf(_avatarButton.frameWidth/2);
    [_separatorImageView setImage:[UIImage imageWithColor:[BTWStylesheet separatorColor] size:_separatorImageView.size]];
    
    for (UIButton *button in _buttonsCollection) {
        [button setBackgroundImage:[UIImage imageWithColor:button.backgroundColor size:button.size] forState:UIControlStateNormal];
    }
    _buttonsContainer.layer.cornerRadius = 5.0f;
    
    self.titleLabel = [[TTTAttributedLabel alloc] initWithFrame:_titleContainerView.bounds];
    _titleLabel.numberOfLines = 0;
    _titleLabel.delegate = self;
    [_titleContainerView addSubview:_titleLabel];

    [_titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_titleLabel]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(_titleLabel)]];
    [_titleContainerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_titleLabel]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(_titleLabel)]];
}

- (void)setNotification:(Notification *)notification
{
    if (![_notification isEqual:notification]) {
        _notification = notification;
        
        [_avatarButton sd_setBackgroundImageWithURL:[NSURL URLWithString:notification.user.photo_mid] forState:UIControlStateNormal placeholderImage:[notification.user midPlaceholder]];
        
        NSString *nameToDisplay = @"";
        NSString *titleString = @"";
        UIImage *actionIcon = nil;
        if ([_notification.type isEqualToString:kNotificationTypeLike]) {
            _routeButton.hidden = YES;
            if ([BTWUIUtils isRussianLocale]) {
                SmartPol pol = ([notification.user.gender integerValue] == BTWUserGenderFemale) ? SmartPolJenskij : SmartPolMujskoj;
                NSString *firstName = [[SmartName sharedInstance] processName:notification.user.name pol:pol padezh:SmartPadezhDatelnij];
                NSString *lastName = [[SmartName sharedInstance] processName:notification.user.surname pol:pol padezh:SmartPadezhDatelnij];
                nameToDisplay = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
            } else {
                nameToDisplay = _notification.user.fullname;
            }
            titleString = [NSString stringWithFormat:NSLocalizedString(@"%@ likes you", nil), nameToDisplay];
            actionIcon = [UIImage imageNamed:@"icon_notifications_like"];
        } else if ([_notification.type isEqualToString:kNotificationTypeRide]) {
            _routeButton.hidden = NO;
            nameToDisplay = _notification.user.fullname;
            titleString = [NSString stringWithFormat:NSLocalizedString(@"%@ offers you the trip", nil), nameToDisplay];
            actionIcon = [_notification.travel_role isEqualToString:kUserRoleDriver] ? [UIImage imageNamed:@"icon_car"] : [UIImage imageNamed:@"icon_walker"];
        }
        NSRange nameRange = [titleString rangeOfString:nameToDisplay];
        
        NSDictionary *nameAttributes = @{ NSFontAttributeName : [BTWStylesheet commonBoldFontOfSize:15.0],
                                          NSForegroundColorAttributeName : [BTWStylesheet commonTextColor] };
        NSDictionary *highlightedNameAttributes = @{ NSFontAttributeName : [BTWStylesheet commonBoldFontOfSize:15.0],
                                                     NSForegroundColorAttributeName : [[BTWStylesheet commonTextColor] colorWithAlphaComponent:0.5f] };
        NSDictionary *regulartAttributes = @{ NSFontAttributeName : [BTWStylesheet commonFontOfSize:15.0],
                                              NSForegroundColorAttributeName : [BTWStylesheet commonTextColor] };
        
        _titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleString attributes:regulartAttributes];
        _titleLabel.linkAttributes = nameAttributes;
        _titleLabel.activeLinkAttributes = highlightedNameAttributes;
        [_titleLabel addLinkToURL:[NSURL URLWithString:kUserNameLink] withRange:nameRange];
        _actionIconImageView.image = actionIcon;
    }
}

#pragma mark - Actions
- (IBAction)avatarButtonTouch:(UIButton *)sender
{
    [_delegate avatarDidTouchForCell:self];
}

- (IBAction)actionAccept:(UIButton *)sender
{
    [_delegate acceptButtonDidTouchForCell:self];
}

- (IBAction)actionReject:(UIButton *)sender
{
    [_delegate declineButtonDidTouchForCell:self];
}

- (IBAction)actionRoute:(UIButton *)sender
{
    [_delegate routeButtonDidTouchForCell:self];
}

#pragma mark - TTTAttributedLabel delegate
- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [_delegate avatarDidTouchForCell:self];
}

@end
