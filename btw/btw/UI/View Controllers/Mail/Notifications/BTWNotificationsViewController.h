//
//  BTWNotificationsViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWMailContainerViewController.h"

@interface BTWNotificationsViewController : BTWViewController

@property (nonatomic, weak) BTWMailContainerViewController *containerController;

@end
