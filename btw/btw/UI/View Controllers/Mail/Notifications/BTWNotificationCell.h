//
//  BTWNotificaitonCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

@class BTWNotificationCell;
@protocol BTWNotificationCellDelegate <NSObject>
- (void)avatarDidTouchForCell:(BTWNotificationCell *)cell;
- (void)acceptButtonDidTouchForCell:(BTWNotificationCell *)cell;
- (void)declineButtonDidTouchForCell:(BTWNotificationCell *)cell;
- (void)routeButtonDidTouchForCell:(BTWNotificationCell *)cell;
@end

@interface BTWNotificationCell : UITableViewCell

@property (nonatomic, weak) id<BTWNotificationCellDelegate> delegate;
@property (nonatomic, strong) Notification *notification;

+ (CGFloat)cellHeightForNotification:(Notification *)notification;

@end
