//
//  BTWDialogHeaderView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

@class BTWDialogHeaderView;
@protocol BTWDialogHeaderViewDelegate <NSObject>
- (void)didTouchAvatarForHeader:(BTWDialogHeaderView *)header;
- (void)didTouchTitleForHeader:(BTWDialogHeaderView *)header;
@end

@interface BTWDialogHeaderView : UIView

@property (weak, nonatomic) id<BTWDialogHeaderViewDelegate> delegate;
@property (weak, nonatomic, readonly) UIButton *avatarButton;
@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) UILabel *subtitleLabel;

@end
