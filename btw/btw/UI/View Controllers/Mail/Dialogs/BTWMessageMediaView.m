//
//  BTWMessageMediaView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMessageMediaView.h"

@interface BTWMessageMediaView ()
@property (weak, nonatomic) IBOutlet UIButton *viewButton;
@property (weak, nonatomic, readwrite) UIView *contentView;
@end

@implementation BTWMessageMediaView

#pragma mark - Initialization
+ (BTWMessageMediaView *)viewWithContentView:(UIView *)contentView
{
    BTWMessageMediaView *mediaView = [[[NSBundle mainBundle] loadNibNamed:@"BTWMessageMediaView" owner:self options:nil] firstObject];
    mediaView.size = contentView.size;
    [mediaView insertSubview:contentView atIndex:0];
    mediaView.contentView = contentView;
    return mediaView;
}

#pragma mark - Actions
- (IBAction)buttonAction:(UIButton *)sender
{
    if (_actionBlock) {
        _actionBlock();
    }
}

@end
