//
//  BTWDialogViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "JSQMessages.h"

@interface BTWDialogViewController : JSQMessagesViewController

@property (nonatomic, strong, readonly) Dialog *dialog;

- (id)initWithDialog:(Dialog *)dialog;

@end
