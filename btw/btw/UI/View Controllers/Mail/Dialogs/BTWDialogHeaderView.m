//
//  BTWDialogHeaderView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWDialogHeaderView.h"

@interface BTWDialogHeaderView ()
@property (weak, nonatomic) IBOutlet UIButton *avatarButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@end

@implementation BTWDialogHeaderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _avatarButton.layer.cornerRadius = floorf(_avatarButton.frameWidth/2);
}

- (IBAction)avatarButtonTouch:(id)sender
{
    [_delegate didTouchAvatarForHeader:self];
}

- (IBAction)tapGestureRecognized:(id)sender
{
    [_delegate didTouchTitleForHeader:self];
}

@end
