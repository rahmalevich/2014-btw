//
//  BTWMapAttachmentViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMapViewController.h"

@interface BTWMapAttachmentViewController : BTWMapViewController

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;
- (id)initWithPolyline:(NSString *)polyline andDate:(NSDate *)date;

@end
