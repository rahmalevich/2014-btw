//
//  BTWDialogViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDialogViewController.h"
#import "BTWDialogDataController.h"
#import "BTWDialogHeaderView.h"
#import "BTWMessagingService.h"
#import "BTWUserService.h"
#import "BTWRouteService.h"
#import "BTWLocationManager.h"
#import "BTWFormattingService.h"
#import "BTWTransitionsManager.h"
#import "RoundedBorderButton.h"
#import "BTWUserProfileViewController.h"

#import "AFNetworkReachabilityManager.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "UIActionSheet+Blocks.h"
#import "IDMPhotoBrowser.h"
#import "MBProgressHUD.h"

static NSString *kSystemMessageCellId = @"kSystemMessageCellId";
static CGFloat kCellTopLabelHeight = 20.0f;
static CGFloat kAvatarSize = 40.0f;

#pragma mark - BTWDialogViewController
@interface BTWDialogViewController () <UIActionSheetDelegate, BTWDialogDataControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, BTWDialogHeaderViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) Dialog *dialog;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) BTWDialogDataController *dataController;
@property (nonatomic, strong) BTWDialogHeaderView *headerView;
@property (nonatomic, strong) JSQMessagesBubbleImage *incomingBubbleImageData;
@property (nonatomic, strong) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (nonatomic, strong) JSQMessagesBubbleImage *errorBubbleImageData;
@end

@implementation BTWDialogViewController

#pragma mark - Initialization & Memory managment
- (id)initWithDialog:(Dialog *)dialog
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.dialog = dialog;
        [[AFNetworkReachabilityManager sharedManager] addObserver:self forKeyPath:@"reachable" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [_dataController stopTimers];
    [[AFNetworkReachabilityManager sharedManager] removeObserver:self forKeyPath:@"reachable"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.senderId = [BTWUserService sharedInstance].authorizedUser.backend_id;
    self.senderDisplayName = [BTWUserService sharedInstance].authorizedUser.fullname;
    
    self.collectionView.backgroundColor = [BTWStylesheet dialogBackgroundColor];
    self.collectionView.collectionViewLayout.messageBubbleFont = [BTWStylesheet commonFontOfSize:14.0f];
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeMake(kAvatarSize, kAvatarSize);
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeMake(kAvatarSize, kAvatarSize);
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [BTWDialogDataController sharedBubbleImageFactory];
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[BTWStylesheet outgoingMessageBubbleColor]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[BTWStylesheet incomingMessageBubbleColor]];
    self.errorBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[BTWStylesheet errorBubbleColor]];
    
    // Индикация загрузки
    self.placeholderLabel.text = NSLocalizedString(@"Dialog is empty", nil);
    self.placeholderLabel.hidden = YES;
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityIndicator.hidesWhenStopped = YES;
    [_activityIndicator setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addSubview:_activityIndicator];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_activityIndicator attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_activityIndicator attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [self.view setNeedsLayout];
    
    // Заголовок
    self.headerView = [[[NSBundle mainBundle] loadNibNamed:@"BTWDialogHeaderView" owner:self options:nil] firstObject];
    _headerView.delegate = self;
//    [_headerView.avatarButton sd_setBackgroundImageWithURL:[NSURL URLWithString:_dialog.user.photo_mid] forState:UIControlStateNormal placeholderImage:[_dialog.user midPlaceholder]];
    _headerView.titleLabel.text = _dialog.user.fullname;
    [self updateHeaderSubtitle];
    self.navigationBar.topItem.titleView = _headerView;

    UIImage *backButtonImage = [[UIImage imageNamed:@"btn_back"] imageByColorizingWithColor:[UIColor whiteColor]];
    self.navigationBar.topItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStylePlain target:self action:@selector(actionBack:)];
    self.navigationBar.topItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_block"] style:UIBarButtonItemStylePlain target:self action:@selector(actionBlock:)];
    
    // Тулбар
    UIButton *sendButton = self.inputToolbar.contentView.rightBarButtonItem;
    [sendButton setTitleColor:[BTWStylesheet composerControlsColor] forState:UIControlStateNormal];
    [sendButton setTitleColor:[[BTWStylesheet commonBlueColor] colorWithAlphaComponent:0.75f] forState:UIControlStateHighlighted];
    sendButton.titleLabel.font = [BTWStylesheet commonBoldFontOfSize:17.0];
    self.inputToolbar.tintColor = [BTWStylesheet commonBlueColor];
    self.inputToolbar.backgroundColor = [BTWStylesheet composerBackgroundColor];

    BOOL reachable = [AFNetworkReachabilityManager sharedManager].reachable;
    self.inputToolbar.contentView.leftBarButtonItem.enabled = reachable;
    self.inputToolbar.contentView.rightBarButtonItem.enabled = reachable;
    
    // Timestamp
    JSQMessagesTimestampFormatter *formatter = [JSQMessagesTimestampFormatter sharedFormatter];
    NSMutableDictionary *textAttributes = [formatter.dateTextAttributes mutableCopy];
    textAttributes[NSFontAttributeName] = [BTWStylesheet commonFontOfSize:10.0];
    textAttributes[NSForegroundColorAttributeName] = [BTWStylesheet commonGrayColor];
    formatter.dateTextAttributes = textAttributes;
    formatter.timeTextAttributes = textAttributes;
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapRecognized:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    self.dataController = [[BTWDialogDataController alloc] initWithDialog:_dialog delegate:self];
    self.showLoadEarlierMessagesHeader = _dataController.hasEarlierMessages;
    
    [self updateAppearence];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.automaticallyScrollsToMostRecentMessage = YES;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - Recognizing gestures
- (void)backgroundTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    JSQMessagesComposerTextView *textView = self.inputToolbar.contentView.textView;
    if ([textView isFirstResponder]) {
        [textView resignFirstResponder];
    }
}

#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"reachable"]) {
        BOOL reachable = [AFNetworkReachabilityManager sharedManager].reachable;
        self.inputToolbar.contentView.rightBarButtonItem.enabled = reachable;
        self.inputToolbar.contentView.leftBarButtonItem.enabled = reachable;
    }
}

#pragma mark - Utils
- (void)updateAppearence
{
    if ([_dataController messagesCount] == 0) {
        if (_dataController.isLoadingData) {
            [_activityIndicator startAnimating];
            if (!self.placeholderLabel.hidden) {
                [UIView animateWithDuration:0.3 animations:^{
                    self.placeholderLabel.alpha = 0.0f;
                } completion:^(BOOL finished){
                    self.placeholderLabel.hidden = YES;
                }];
            }
        } else {
            [_activityIndicator stopAnimating];
            if (self.placeholderLabel.hidden) {
                self.placeholderLabel.alpha = 0.0f;
                self.placeholderLabel.hidden = NO;
                [UIView animateWithDuration:0.3 animations:^{
                    self.placeholderLabel.alpha = 1.0f;
                }];
            }
        }
    } else {
        [_activityIndicator stopAnimating];
        if (!self.placeholderLabel.hidden) {
            [UIView animateWithDuration:0.3 animations:^{
                self.placeholderLabel.alpha = 0.0f;
            } completion:^(BOOL finished){
                self.placeholderLabel.hidden = YES;
            }];
        }
    }
}

- (void)updateHeaderSubtitle
{
    NSString *subtitle = [_dialog.user.online boolValue] ? NSLocalizedString(@"online", nil) : [NSString stringWithFormat:@"%@ %@ %@ %@", NSLocalizedString(@"was", nil), [[[BTWFormattingService shortDateFormatter] stringFromDate:_dialog.user.last_activity] lowercaseString], NSLocalizedString(@"at", nil), [[BTWFormattingService shortTimeFormatter] stringFromDate:_dialog.user.last_activity]];
    [self setHeaderText:subtitle animated:YES];
}

- (void)setHeaderText:(NSString *)text animated:(BOOL)animated
{
    if (animated) {
        CATransition *animation = [CATransition animation];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.type = kCATransitionFade;
        animation.duration = 0.15;
        [_headerView.subtitleLabel.layer addAnimation:animation forKey:@"kCATransitionFade"];
    }
    
    _headerView.subtitleLabel.text = text;
}

- (void)showEarlierMessages
{
    [_dataController showEarlierMessages];

    CGFloat oldOffsetY = self.collectionView.contentOffset.y;
    CGFloat oldContentHeight = self.collectionView.contentSize.height;
    
    if (self.showLoadEarlierMessagesHeader == _dataController.hasEarlierMessages) {
        [self.collectionView.collectionViewLayout invalidateLayoutWithContext:[JSQMessagesCollectionViewFlowLayoutInvalidationContext context]];
        [self.collectionView reloadData];
    } else {
        // При смене этого свойства collectionView перегружается в JSQMessagesViewController
        self.showLoadEarlierMessagesHeader = _dataController.hasEarlierMessages;
    }
    [self.collectionView layoutIfNeeded];

    CGFloat newContentHeight = self.collectionView.contentSize.height;
    CGFloat newContentOffsetY = oldOffsetY + (newContentHeight - oldContentHeight);
    self.collectionView.contentOffset = (CGPoint){0.0, newContentOffsetY};
}

- (void)resendMessage:(DialogMessage *)message
{
    RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Cancel", nil)];
    RIButtonItem *resendItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Resend message", nil) action:^{
        [[BTWMessagingService sharedInstance] resendMessage:message];
    }];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:cancelItem destructiveButtonItem:nil otherButtonItems:resendItem, nil];
    [sheet showFromToolbar:self.inputToolbar];
}

#pragma mark - Actions
- (void)actionBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionBlock:(id)sender
{
    __weak typeof(self) wself = self;
    RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Cancel", nil)];
    
    RIButtonItem *blockItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Delete user", nil) action:^{
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
        [[BTWMessagingService sharedInstance] removeDialog:wself.dialog withCompletionHandler:^(NSError *error)
        {
            UIImage *hudImage = error ? [UIImage imageNamed:@"icon_error"] : [UIImage imageNamed:@"icon_success"];
            UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
            hudImageView.size = hudImage.size;
            hud.mode = MBProgressHUDModeCustomView;
            hud.customView = hudImageView;
            [hud hide:YES afterDelay:1.0];

            if (!error) {
                [wself.navigationController popViewControllerAnimated:YES];
            }
        }];
    }];
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:cancelItem destructiveButtonItem:blockItem otherButtonItems:nil];
    [sheet showFromToolbar:self.inputToolbar];
}

#pragma mark - BTWDialogHeaderView delegate
- (void)didTouchTitleForHeader:(BTWDialogHeaderView *)header
{
    [self openProfile];
}

- (void)didTouchAvatarForHeader:(BTWDialogHeaderView *)header
{
    [self openProfile];
}

- (void)openProfile
{
    BTWUserProfileViewController *profileController = (BTWUserProfileViewController *)[[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeUserProfile];
    profileController.user = _dialog.user;
    [[BTWTransitionsManager sharedInstance] pushViewController:profileController];
}

#pragma mark - BTWDialogDataController delegate
- (void)dataController:(BTWDialogDataController *)controller didEndLoadingDataWithError:(NSError *)error
{
    self.showLoadEarlierMessagesHeader = controller.hasEarlierMessages;
    [self updateAppearence];
    [self finishReceivingMessageAnimated:YES];
}

- (void)userDidStartTypingMessageForDataController:(BTWDialogDataController *)controller
{
    [self setHeaderText:NSLocalizedString(@"printing...", nil) animated:YES];
}

- (void)userDidFinishTypingMessageForDataController:(BTWDialogDataController *)controller
{
    [self updateHeaderSubtitle];
}

- (void)dataControllerDidReceiveOnlineMessage:(BTWDialogDataController *)controller
{
    [self updateHeaderSubtitle];
}

- (void)dataControllerDidReceiveOfflineMessage:(BTWDialogDataController *)controller
{
    [self updateHeaderSubtitle];
}

#pragma mark - JSQMessagesViewController method overrides
- (void)textViewDidChange:(UITextView *)textView
{
    if ([AFNetworkReachabilityManager sharedManager].reachable) {
        [super textViewDidChange:textView];
        if ([textView.text isValidString]) {
            [[BTWMessagingService sharedInstance] sendTypingMessageToDialog:self.dialog];
        }
    }
}

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date
{
//TODO: sounds? [JSQSystemSoundPlayer jsq_playMessageSentSound];
    [[BTWMessagingService sharedInstance] sendMessageWithText:text toDialog:_dialog];
    [self finishSendingMessageAnimated:YES];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    __weak typeof(self) wself = self;
    RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Cancel", nil)];
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil cancelButtonItem:cancelItem destructiveButtonItem:nil otherButtonItems:nil];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        RIButtonItem *makePhotoItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Make photo", nil) action:^{
            [[BTWTransitionsManager sharedInstance] showImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera completionHandler:^(UIImage *photo){
                if (photo) {
                    [[BTWMessagingService sharedInstance] sendPhoto:photo toDialog:wself.dialog withProgressHandler:nil completionHandler:nil];
                    [wself finishSendingMessageAnimated:YES];
                }
            }];
        }];
        [sheet addButtonItem:makePhotoItem];
    }
    
    RIButtonItem *choosePhotoItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Choose photo", nil) action:^{
        [[BTWTransitionsManager sharedInstance] showImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary completionHandler:^(UIImage *photo){
            if (photo) {
                [[BTWMessagingService sharedInstance] sendPhoto:photo toDialog:wself.dialog withProgressHandler:nil completionHandler:nil];
                [wself finishSendingMessageAnimated:YES];
            }
        }];
    }];
    [sheet addButtonItem:choosePhotoItem];
    
    if ([BTWLocationManager sharedInstance].currentLocation) {
        RIButtonItem *locationItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"My location", nil) action:^{
            [[BTWMessagingService sharedInstance] sendCurrentLocationToDialog:wself.dialog];
            [wself finishSendingMessageAnimated:YES];
        }];
        [sheet addButtonItem:locationItem];
    }
    
    if ([BTWRouteService sharedInstance].currentRoute) {
        RIButtonItem *routeItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Route", nil) action:^{
            [[BTWMessagingService sharedInstance] sendCurrentRouteToDialog:wself.dialog];
            [wself finishSendingMessageAnimated:YES];
        }];
        [sheet addButtonItem:routeItem];
    }
    
    [sheet showFromToolbar:self.inputToolbar];
}

#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_dataController messageAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id<JSQMessageBubbleImageDataSource> result = nil;
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    if (message.incoming) {
        result = _incomingBubbleImageData;
    } else {
        if ([message.status integerValue] == BTWDialogMessageStatusUnsent) {
            result = _errorBubbleImageData;
        } else {
            result = _outgoingBubbleImageData;
        }
    }
    return result;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    NSAttributedString *timestamp = nil;
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    DialogMessage *previousMessage = indexPath.item > 0 ? [_dataController messageAtIndex:indexPath.item - 1] : nil;
    if (!previousMessage || ![BTWCommonUtils date:message.date isSameToDate:previousMessage.date]) {
        timestamp = [[NSMutableAttributedString alloc] initWithString:[[JSQMessagesTimestampFormatter sharedFormatter] relativeDateForDate:message.date] attributes:[JSQMessagesTimestampFormatter sharedFormatter].dateTextAttributes];
    }
    return timestamp;
}

- (UIEdgeInsets)collectionView:(JSQMessagesCollectionView *)collectionView edgeInsetsForTopCellLineAtIndexPath:(NSIndexPath *)indexPath
{
    UIEdgeInsets result = UIEdgeInsetsZero;
    
    NSAttributedString *text = [collectionView.dataSource collectionView:collectionView attributedTextForCellTopLabelAtIndexPath:indexPath];
    if (text) {
        CGFloat padding = 5.0f, labelPadding = 5.0f;
        result.left = text.size.width + padding + labelPadding;
        result.right = padding;
    }
    return result;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat resultHeight = 0.0f;
    NSAttributedString *attributedString = [self collectionView:collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:indexPath];
    if (attributedString) {
        CGFloat padding = 10.f;
        CGRect rect = [attributedString boundingRectWithSize:CGSizeMake(self.view.frameWidth - 2 * padding, 10000.0f) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
        resultHeight = rect.size.height + 7.0f; // TODO: Определить причину обрезки лэйбла
    }
    return resultHeight;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    NSAttributedString *resultString = nil;
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    if ([message isSystemMessage]) {
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        [paragraphStyle setAlignment:NSTextAlignmentCenter];
        
        NSDictionary *attributes = @{ NSFontAttributeName : [BTWStylesheet commonFontOfSize:12.0],
                                      NSForegroundColorAttributeName : [UIColor darkGrayColor],
                                      NSParagraphStyleAttributeName : paragraphStyle };
        resultString = [[NSAttributedString alloc] initWithString:message.text attributes:attributes];
    }
    return resultString;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_dataController messagesCount];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView = [super collectionView:collectionView viewForSupplementaryElementOfKind:kind atIndexPath:indexPath];
    if ([reusableView isKindOfClass:[JSQMessagesLoadEarlierHeaderView class]]) {
        JSQMessagesLoadEarlierHeaderView *header = (JSQMessagesLoadEarlierHeaderView *)reusableView;
        header.loadButton.tintColor = [BTWStylesheet commonGrayColor];
        ((RoundedBorderButton *)header.loadButton).cornerRadius = 5.0f;
    }
    return reusableView;
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    cell.cellTopLabelLineView.backgroundColor = [UIColor lightGrayColor];
    
    if ([message isSystemMessage]) {
        cell.avatarContainerView.hidden = YES;
    } else {
        cell.avatarContainerView.hidden = NO;
        NSString *avatarImageString = message.incoming ? _dialog.user.photo_mid : [BTWUserService sharedInstance].authorizedUser.photo_mid;
        [cell.avatarImageView sd_setImageWithURL:[NSURL URLWithString:avatarImageString] placeholderImage:[_dialog.user midPlaceholder]];
        cell.avatarImageView.clipsToBounds = YES;
        cell.avatarImageView.layer.cornerRadius = floorf(kAvatarSize/2);
    }
    
    cell.messageBubbleContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    cell.messageBubbleContainerView.layer.shadowRadius = 1.0f;
    cell.messageBubbleContainerView.layer.shadowOpacity = 0.3f;
    cell.messageBubbleContainerView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    
    cell.timeLabel.text = [[BTWFormattingService shortTimeFormatter] stringFromDate:message.date];
    
    UIImage *deliveryStatusImage = nil;
    switch ([message.status integerValue]) {
        case BTWDialogMessageStatusBlank:
            deliveryStatusImage = [UIImage imageNamed:@"icon_chat_clock"];
            break;
        case BTWDialogMessageStatusSent:
            deliveryStatusImage = [UIImage imageNamed:@"icon_chat_sent"];
            break;
        case BTWDialogMessageStatusDelivered:
            deliveryStatusImage = [UIImage imageNamed:@"icon_chat_delivered"];
            break;
        case BTWDialogMessageStatusUnsent:
            deliveryStatusImage = [UIImage imageNamed:@"icon_chat_error"];
            break;
        default:
            break;
    }
    
    if (!message.isMediaMessage) {
        cell.deliveryStatusImageView.image = deliveryStatusImage;
        cell.timeLabel.textColor = message.incoming ? [BTWStylesheet incomingTextColor] : [BTWStylesheet outgoingTextColor];
        cell.timeLabel.textAlignment = message.incoming ? NSTextAlignmentLeft : NSTextAlignmentRight;
        cell.textView.textColor = message.incoming ? [BTWStylesheet incomingTextColor] : [BTWStylesheet outgoingTextColor];
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    } else {
        BOOL messageIsUnsent = ([message.status integerValue] == BTWDialogMessageStatusUnsent);
        cell.timeLabel.textColor = messageIsUnsent ? [BTWStylesheet commonGrayColor] : [UIColor whiteColor];
        cell.timeLabel.textAlignment = NSTextAlignmentRight;
        cell.deliveryStatusImageView.image = messageIsUnsent ? deliveryStatusImage : [deliveryStatusImage imageByColorizingWithColor:[UIColor whiteColor]];
        
        if (!message.mediaItem.mediaView) {
            [message.mediaItem updateMediaViewWithCompletionHandler:^(NSError *error){
                if (!error) {
                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
                }
            }];
        } else {
            BTWMessageMediaView *mediaView = message.mediaItem.mediaView;
            mediaView.actionBlock = ^{
                if (![message.mediaItem isKindOfClass:[MessageMediaItemPhoto class]]) {
                    self.automaticallyScrollsToMostRecentMessage = NO;
                }
                [message.mediaItem openAttachment];
            };
            
            // немного магии
            UIEdgeInsets buttonInsets = mediaView.viewButton.contentEdgeInsets;
            buttonInsets.left = message.incoming ? 26.0f : 20.0f;
            mediaView.viewButton.contentEdgeInsets = buttonInsets;
        }
    }
    
    return cell;
}



#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    NSAttributedString *text = [collectionView.dataSource collectionView:collectionView attributedTextForCellTopLabelAtIndexPath:indexPath];
    if (text) {
        return kCellTopLabelHeight;
    }
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    [self showEarlierMessages];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    if (message.incoming) {
        [self openProfile];
    }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    if ([message.status integerValue] == BTWDialogMessageStatusUnsent) {
        [self resendMessage:message];
    } else if (message.isMediaMessage) {
        if (message.mediaItem.mediaView.actionBlock) {
            message.mediaItem.mediaView.actionBlock();
        }
    }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    DialogMessage *message = [_dataController messageAtIndex:indexPath.item];
    if ([message.status integerValue] == BTWDialogMessageStatusUnsent) {
        [self resendMessage:message];
    } else {
        JSQMessagesComposerTextView *textView = self.inputToolbar.contentView.textView;
        if ([textView isFirstResponder]) {
            [textView resignFirstResponder];
        }
    }
}

@end
