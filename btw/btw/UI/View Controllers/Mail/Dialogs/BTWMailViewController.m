//
//  BTWMailViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMailViewController.h"
#import "BTWMailTableCell.h"
#import "BTWDialogViewController.h"
#import "BTWTransitionsManager.h"
#import "BTWMessagingService.h"

static NSString *kMailCellReuseID = @"kMailCellReuseID";

@interface BTWMailViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) NSFetchedResultsController *dialogsController;
@end

@implementation BTWMailViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Chats", nil);
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];

    [_tableView registerNib:[UINib nibWithNibName:@"BTWMailTableCell" bundle:nil] forCellReuseIdentifier:kMailCellReuseID];
    _tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, kCentralButtonOffset, 0.0f);
    
    self.dialogsController = [Dialog MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:@"last_message_date" ascending:NO delegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak typeof(self) wself = self;
    [[BTWMessagingService sharedInstance] updateDialogsWithCompletionHandler:^(NSError *error){
        // Делаем задержку для корректной последовательности анимаций
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [wself updateAppearence];
        });
    }];
    [self updateAppearence];
}

- (void)updateAppearence
{
    if ([_dialogsController.fetchedObjects count] == 0) {
        if ([BTWMessagingService sharedInstance].isLoadingDialogs) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BTWMailTableCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dialogsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTWMailTableCell *cell = [_tableView dequeueReusableCellWithIdentifier:kMailCellReuseID forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    Dialog *dialog = [_dialogsController objectAtIndexPath:indexPath];
    [[BTWTransitionsManager sharedInstance] showDialog:dialog];
}

- (void)configureCell:(BTWMailTableCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Dialog *dialog = [_dialogsController objectAtIndexPath:indexPath];
    cell.dialog = dialog;
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(BTWMailTableCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self updateAppearence];
}

@end
