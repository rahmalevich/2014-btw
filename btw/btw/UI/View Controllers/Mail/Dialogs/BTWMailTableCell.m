//
//  BTWMailTableCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMailTableCell.h"
#import "BTWFormattingService.h"

#import "UIImageView+WebCache.h"
#import "CustomBadge.h"

@interface BTWMailTableCell ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) CustomBadge *badge;
@end

@implementation BTWMailTableCell

+ (CGFloat)cellHeight
{
    return 70.0;
}

- (void)awakeFromNib
{
    _avatarImageView.layer.cornerRadius = floorf(_avatarImageView.boundsWidth/2);
    [_separatorImageView setImage:[UIImage imageWithColor:[BTWStylesheet separatorColor] size:_separatorImageView.size]];
    
    BadgeStyle *badgeStyle = [BadgeStyle defaultStyle];
    badgeStyle.badgeFrameColor = [UIColor whiteColor];
    badgeStyle.badgeInsetColor = [BTWStylesheet badgeColor];
    self.badge = [CustomBadge customBadgeWithString:nil withStyle:badgeStyle];
    _badge.x = floorf(_avatarImageView.centerX + cos(M_PI_4)*(_avatarImageView.frameWidth/2) - _badge.frameWidth/2);
    _badge.y = floorf(_avatarImageView.centerY + sin(M_PI_4)*(_avatarImageView.frameWidth/2) - _badge.frameWidth/2);
    
    [self addSubview:_badge];
}

- (void)setDialog:(Dialog *)dialog
{
    if (![_dialog isEqual:dialog]) {
        _dialog = dialog;
    }
    
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:_dialog.user.photo_mid] placeholderImage:[_dialog.user midPlaceholder]];
    _nameLabel.text = [_dialog.user fullname];
    
    if ([_dialog.unconfirmed_messages_count integerValue] > 0) {
        _badge.hidden = NO;
        _badge.badgeText = [_dialog.unconfirmed_messages_count stringValue];
    } else {
        _badge.hidden = YES;
    }
    
    DialogMessage *lastMessage = [_dialog.messages lastObject];
    if (lastMessage) {
        NSDateFormatter *formatter = [BTWCommonUtils dateIsToday:lastMessage.date] ? [BTWFormattingService shortTimeFormatter] : [BTWFormattingService shortDateFormatter];
        _timeLabel.text = [formatter stringFromDate:lastMessage.date];
        
        NSString *lastMessageText = nil;
        if (lastMessage.mediaItem) {
            if ([lastMessage.mediaItem isKindOfClass:[MessageMediaItemPhoto class]]) {
                lastMessageText = NSLocalizedString(@"Photo", nil);
            } else if ([lastMessage.mediaItem isKindOfClass:[MessageMediaItemLocation class]]) {
                lastMessageText = NSLocalizedString(@"Location", nil);
            } else if ([lastMessage.mediaItem isKindOfClass:[MessageMediaItemRoute class]]) {
                lastMessageText = NSLocalizedString(@"Route", nil);
            }
            lastMessageText = [lastMessageText lowercaseString];
        } else {
            lastMessageText = lastMessage.text;
        }
        
        _lastMessageLabel.text = lastMessageText;
    } else {
        _timeLabel.text = @"";
        _lastMessageLabel.text = @"";
    }
}

@end
