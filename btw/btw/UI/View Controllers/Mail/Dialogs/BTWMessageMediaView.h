//
//  BTWMessageMediaView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

@interface BTWMessageMediaView : UIView

@property (nonatomic, copy) BTWVoidBlock actionBlock;
@property (weak, nonatomic, readonly) UIView *contentView;
@property (weak, nonatomic, readonly) UIButton *viewButton;

+ (BTWMessageMediaView *)viewWithContentView:(UIView *)contentView;

@end
