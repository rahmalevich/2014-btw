//
//  BTWMailTableCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWMailTableCell : UITableViewCell

@property (nonatomic, strong) Dialog *dialog;

+ (CGFloat)cellHeight;

@end
