//
//  BTWMailViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWMailContainerViewController.h"

@interface BTWMailViewController : BTWViewController

@property (nonatomic, weak) BTWMailContainerViewController *containerController;

@end
