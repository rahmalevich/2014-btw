//
//  BTWMapAttachmentViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMapAttachmentViewController.h"
#import "BTWMapConstants.h"
#import "BTWLocationManager.h"
#import "BTWFormattingService.h"

#import "GoogleMaps.h"

@interface BTWMapAttachmentViewController ()
@property (nonatomic, assign) CLLocationCoordinate2D cooridnate;
@property (nonatomic, copy) NSString *polyline;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) GMSPolyline *routePolyline;
@property (nonatomic, strong) NSMutableSet *markersSet;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation BTWMapAttachmentViewController

#pragma mark - Initialization
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.cooridnate = coordinate;
    }
    return self;
}

- (id)initWithPolyline:(NSString *)polyline andDate:(NSDate *)date
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.polyline = polyline;
        self.date = date;
    }
    return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = [_polyline isValidString] ? NSLocalizedString(@"Route", nil) : NSLocalizedString(@"Location", nil);
    
    self.markersSet = [NSMutableSet set];
    
    CLLocationCoordinate2D coordinate = [BTWLocationManager sharedInstance].currentLocation.coordinate;
    self.mapView.camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:kDefaultZoomLevel];
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.rotateGestures = NO;
    self.mapView.settings.tiltGestures = NO;
    
    if ([_polyline isValidString]) {
        if (_date) {
            _timeView.hidden = NO;
            _timeLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Travel date", nil), [[BTWFormattingService shortDateTimeFormatter] stringFromDate:_date]];
        } else {
            _timeView.hidden = YES;
        }
        
        GMSPath *path = [GMSPath pathFromEncodedPath:_polyline];
        self.routePolyline = [GMSPolyline polylineWithPath:path];
        _routePolyline.strokeWidth = 4.f;
        _routePolyline.strokeColor = [BTWStylesheet routeColor];
        _routePolyline.map = self.mapView;
    
        GMSMarker *markerA = [GMSMarker markerWithPosition:[path coordinateAtIndex:0]];
        markerA.icon = [UIImage imageNamed:@"pin_location"];
        markerA.appearAnimation = kGMSMarkerAnimationPop;
        markerA.map = self.mapView;
        [_markersSet addObject:markerA];
        
        GMSMarker *markerB = [GMSMarker markerWithPosition:[path coordinateAtIndex:[path count] - 1]];
        markerB.groundAnchor = kFinishMarkerGroundAnchor;
        markerB.icon = [UIImage imageNamed:@"pin_finish"];
        markerB.appearAnimation = kGMSMarkerAnimationPop;
        markerB.map = self.mapView;
        [_markersSet addObject:markerB];
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];
        [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:kMapBoundsPadding]];
    } else if (CLLocationCoordinate2DIsValid(_cooridnate)) {
        _timeView.hidden = YES;
        
        GMSMarker *marker = [GMSMarker markerWithPosition:_cooridnate];
        marker.icon = [UIImage imageNamed:@"pin_location"];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.map = self.mapView;
        [_markersSet addObject:marker];
        
        [self.mapView animateToLocation:_cooridnate];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

@end
