//
//  BTWMailContainerViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWMailContainerViewController : BTWViewController

- (void)showContacts;
- (void)showNotifications;

@end
