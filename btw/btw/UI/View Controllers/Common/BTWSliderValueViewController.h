//
//  BTWSliderValueViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWSliderValueViewController : BTWViewController

@property (weak, nonatomic, readonly) UILabel *titleLabel;

@end
