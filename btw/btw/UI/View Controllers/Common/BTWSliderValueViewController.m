//
//  BTWSliderValueViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWSliderValueViewController.h"

@interface BTWSliderValueViewController ()
@property (weak, nonatomic, readwrite) IBOutlet UILabel *titleLabel;
@end

@implementation BTWSliderValueViewController

@end
