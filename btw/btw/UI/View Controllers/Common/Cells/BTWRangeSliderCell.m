//
//  BTWRangeSliderCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRangeSliderCell.h"
#import "BTWRangeSliderWithLabels.h"

#pragma mark - BTWRangeSliderCell
@interface BTWRangeSliderCell ()
@property (weak, nonatomic, readwrite) IBOutlet UIView *separatorView;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic, readwrite) IBOutlet BTWRangeSliderWithLabels *sliderView;
@end

@implementation BTWRangeSliderCell

#pragma mark - Height
+ (CGFloat)cellHeight
{
    return 95.0f;
}

@end
