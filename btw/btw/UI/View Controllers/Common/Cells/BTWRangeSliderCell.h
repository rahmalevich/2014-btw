//
//  BTWRangeSliderCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRangeSliderWithLabels.h"

@interface BTWRangeSliderCell : UITableViewCell

@property (weak, nonatomic, readonly) UIView *separatorView;
@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) BTWRangeSliderWithLabels *sliderView;

+ (CGFloat)cellHeight;

@end
