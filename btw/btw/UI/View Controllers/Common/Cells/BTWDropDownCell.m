//
//  BTWDropDownCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDropDownCell.h"

@interface BTWDropDownCell ()
@property (weak, nonatomic, readwrite) IBOutlet UIView *separatorView;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic, readwrite) IBOutlet PopoverButton *popoverButton;
@end

@implementation BTWDropDownCell

+ (CGFloat)cellHeight
{
    return 45.0f;
}

@end
