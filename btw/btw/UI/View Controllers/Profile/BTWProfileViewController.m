//
//  BTWProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWProfileViewController.h"
#import "BTWMyProfileAboutViewController.h"
#import "BTWMyProfileGalleryViewController.h"
#import "BTWMyProfileDetailsViewController.h"
#import "BTWProfileFeedbacksViewController.h"
#import "BTWProfileAboutView.h"

#import "BTWTransitionsManager.h"
#import "BTWQuestionsService.h"
#import "BTWUserService.h"

#import "KeyboardHelper.h"
#import "MBProgressHUD.h"

static const CGFloat kFloatingHeaderHeight = 50.0f;
static const CGFloat kSegmentedControlHeight = 30.0f;
static const CGFloat kSegmentedControlPadding = 20.0f;

@interface BTWProfileViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, assign) BOOL didSetup;
@property (nonatomic, assign) CGFloat containerHeight;

@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) UILabel *titleLabel;

@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) NSArray *controllersArray;
@property (nonatomic, weak) UIViewController *currentChildController;
@property (nonatomic, strong) KeyboardHelper *keyboardHelper;
@property (nonatomic, strong) UIView *headerView;
@end

@implementation BTWProfileViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = [BTWStylesheet commonFontOfSize:18.0];
    titleLabel.textColor = [BTWStylesheet navigationTitleColor];
    self.titleLabel = titleLabel;
    
    self.navigationItem.titleView = titleLabel;
    self.navigationController.navigationBar.translucent = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    
    UINavigationBar *navigationBar = self.navigationController.navigationBar;
    navigationBar.layer.shadowOffset = (CGSize){0.0f, 1.0f};
    navigationBar.layer.shadowOpacity = 0.5f;
    navigationBar.layer.shadowColor = [UIColor blackColor].CGColor;

    self.keyboardHelper = [[KeyboardHelper alloc] initWithTargetView:self.view scrollView:self.tableView];
    
    // segmented control
    self.segmentedControl = [[UISegmentedControl alloc] init];
    _segmentedControl.tintColor = [BTWStylesheet tabBarColor];
    _segmentedControl.backgroundColor = [BTWStylesheet commonBackgroundColor];
    _segmentedControl.autoresizingMask = UIViewAutoresizingNone;
    _segmentedControl.frame = CGRectMake(kSegmentedControlPadding, kSegmentedControlPadding, self.tableView.boundsWidth - 2 * kSegmentedControlPadding, kSegmentedControlHeight);
    [_segmentedControl addTarget:self action:@selector(onSegmentedControlChange:) forControlEvents:UIControlEventValueChanged];
    
    // Profile Header
    BTWProfileAboutView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"BTWProfileAboutView" owner:self options:nil] firstObject];
    headerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.tableView.tableHeaderView = headerView;
    
    // table header view
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, kFloatingHeaderHeight)];
    tableHeaderView.backgroundColor = [BTWStylesheet commonBackgroundColor];
    tableHeaderView.autoresizingMask = UIViewAutoresizingNone;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, kFloatingHeaderHeight - 1, self.tableView.boundsWidth, 1)];
    lineView.backgroundColor = [BTWStylesheet separatorColor];
    
    self.segmentedControl.center = tableHeaderView.center;
    [tableHeaderView addSubview:self.segmentedControl];
    [tableHeaderView addSubview:lineView];
    self.headerView = tableHeaderView;
    
    // Bottom inset
//    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, kCentralButtonOffset, 0);
}

- (void)viewWillAppear:(BOOL)animated
{
    // не вызываем [super viewWillAppear:] чтобы предотвратить ресайз UITableViewController'a при отображении клавиатуры
    
    if (![BTWQuestionsService sharedInstance].questionsUpdated) {
        [[BTWQuestionsService sharedInstance] updateQuestions];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

    if (!_didSetup) {
        self.didSetup = YES;
        [self onSegmentedControlChange:_segmentedControl];
    }
}

#pragma mark - Custom setters
- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    
    _titleLabel.text = title;
    [_titleLabel sizeToFit];
}

- (void)setUser:(User *)user
{
    if (![user isEqual:_user]) {
        _user = user;
        
        BTWProfileAboutView *headerView = (BTWProfileAboutView *)self.tableView.tableHeaderView;
        headerView.user = user;
    }
}

#pragma mark - UIScrolView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    BTWProfileAboutView *aboutView = (BTWProfileAboutView *)self.tableView.tableHeaderView;
    
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat backgroundHeight = offsetY < 0 ? aboutView.frameHeight + fabs(offsetY) : aboutView.frameHeight;
    [aboutView setBackgroundHeight:backgroundHeight];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return kFloatingHeaderHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return _containerHeight;
}

#pragma mark - Segmented Control
- (void)onSegmentedControlChange:(UISegmentedControl *)control
{
    BTWViewController<BTWProfileChildViewController> *childToSwap = self.controllersArray[control.selectedSegmentIndex];
    [self swapCurrentChildViewControllerWith:childToSwap];
}

#pragma mark - Child Controllers
- (void)presentChildViewController:(UIViewController *)childVC
{
    [childVC view];
    
    if (self.currentChildController) {
        [self removeCurrentChildViewController];
    }
    
    [self addChildViewController:childVC];
    
    [self.containerView addSubview:childVC.view];
    self.currentChildController = childVC;
    
    [childVC didMoveToParentViewController:self];
}

- (void)removeCurrentChildViewController
{
    [self.currentChildController willMoveToParentViewController:nil];
    [self.currentChildController.view removeFromSuperview];
    [self.currentChildController removeFromParentViewController];
}

- (void)swapCurrentChildViewControllerWith:(BTWViewController<BTWProfileChildViewController> *)viewController
{
    viewController.profileViewController = self;
    [viewController prepareForParentController];

    CGFloat headerHeight = [self tableView:self.tableView heightForHeaderInSection:0];
    self.containerHeight = MAX(viewController.view.boundsHeight, self.view.frameHeight - headerHeight);
    self.containerView.frameHeight = _containerHeight;

    [self.currentChildController willMoveToParentViewController:nil];
    [self addChildViewController:viewController];
    [self.containerView addSubview:viewController.view];

    UIView *contentView = viewController.view;
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(contentView)]];
    [self.containerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contentView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(contentView)]];

    viewController.view.alpha = 0.0f;
    [UIView animateWithDuration:0.15f animations:^{
        viewController.view.alpha = 1.0f;
        self.currentChildController.view.alpha = 0.0f;
    } completion:^(BOOL finished){
        [self.currentChildController.view removeFromSuperview];
        [self.currentChildController removeFromParentViewController];
        self.currentChildController = viewController;
        [self.currentChildController didMoveToParentViewController:self];
        [self.tableView reloadData];
    }];
}

- (void)childViewController:(UIViewController *)childController didUpdateHeight:(CGFloat)height
{
    self.containerHeight = MAX(height, self.view.frameHeight - kFloatingHeaderHeight);
    self.containerView.frameHeight = _containerHeight;
    [self.tableView reloadData];
}

@end
