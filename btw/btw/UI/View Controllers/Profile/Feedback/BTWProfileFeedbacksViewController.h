//
//  BTWProfileFeedbacksViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWProfileViewController.h"

@interface BTWProfileFeedbacksViewController : BTWViewController <BTWProfileChildViewController>

@property (nonatomic, strong) User *user;

@end
