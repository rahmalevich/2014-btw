//
//  BTWProfileFeedbackCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 31.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWProfileFeedbackCell.h"
#import "BTWRatingView.h"
#import "BTWFormattingService.h"

#import "UIImageView+WebCache.h"

@interface BTWProfileFeedbackCell ()
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedbackTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet BTWRatingView *markView;
@end

@implementation BTWProfileFeedbackCell

+ (CGFloat)cellHeight
{
    return 90.0f;
}

- (void)awakeFromNib
{
    self.backgroundColor = [BTWStylesheet commonBackgroundColor];
    _photoImageView.layer.cornerRadius = floorf(_photoImageView.boundsWidth/2);
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    _separatorView.backgroundColor = [BTWStylesheet separatorColor];
}

- (void)setFeedback:(Feedback *)feedback
{
    _feedback = feedback;
    
    [_photoImageView sd_setImageWithURL:[NSURL URLWithString:_feedback.author.photo_url] placeholderImage:[_feedback.author bigPlaceholder]];
    _nameLabel.text = [_feedback.author fullname];
    _feedbackTextLabel.text = _feedback.comment;
    _markView.rating = [_feedback.points floatValue];
    _dateLabel.text = [[BTWFormattingService shortDateTimeFormatter] stringFromDate:_feedback.date];
}

@end
