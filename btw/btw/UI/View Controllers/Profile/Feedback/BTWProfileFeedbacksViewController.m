//
//  BTWProfileFeedbacksViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWProfileFeedbacksViewController.h"
#import "BTWProfileFeedbackCell.h"
#import "BTWProfileFeedbacksDataController.h"

static NSString * const kFeedbackCell = @"kFeedbackCell";

@interface BTWProfileFeedbacksViewController () <UITableViewDelegate, UITableViewDataSource, BTWDataControllerDelegate>
@property (nonatomic, strong) BTWProfileFeedbacksDataController *dataController;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation BTWProfileFeedbacksViewController
@synthesize profileViewController;

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    [_tableView registerNib:[UINib nibWithNibName:@"BTWProfileFeedbackCell" bundle:nil] forCellReuseIdentifier:kFeedbackCell];
    
    self.dataController = [[BTWProfileFeedbacksDataController alloc] initWithDelegate:self];
    _dataController.user = _user;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_dataController reloadData];
}

- (void)setUser:(User *)user
{
    _user = user;
    _dataController.user = user;
}

- (void)updateAppearance
{
    if ([_dataController.feedbacksArray count] == 0) {
        if (_dataController.isLoadingData) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _placeholderLabel.alpha = 0.0f;
        }];
    }
    
    [_tableView reloadData];
}

#pragma mark - Child view controller methods
- (void)prepareForParentController
{
    [self.view layoutIfNeeded];
    self.view.frameHeight = self.tableView.contentSize.height;
    self.tableView.scrollEnabled = NO;
    self.view.origin = CGPointZero;
}

#pragma mark - Data controller delegate
- (void)dataController:(BTWDataController *)controller didEndLoadingDataWithError:(NSError *)error
{
    [self updateAppearance];
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BTWProfileFeedbackCell cellHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataController.feedbacksArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTWProfileFeedbackCell *cell = [tableView dequeueReusableCellWithIdentifier:kFeedbackCell forIndexPath:indexPath];
    cell.feedback = _dataController.feedbacksArray[indexPath.row];
    return cell;
}

@end
