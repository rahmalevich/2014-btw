//
//  BTWProfileFeedbackCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 31.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTWProfileFeedbackCell : UITableViewCell

@property (nonatomic, strong) Feedback *feedback;

+ (CGFloat)cellHeight;

@end
