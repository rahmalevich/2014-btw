//
//  BTWProfileViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BTWProfileParentViewController <NSObject>
- (void)childViewController:(UIViewController *)childController didUpdateHeight:(CGFloat)height;
@end

@protocol BTWProfileChildViewController <NSObject>
@property (nonatomic, weak) id<BTWProfileParentViewController> profileViewController;
@end

@interface BTWProfileViewController : UITableViewController <BTWProfileParentViewController>

@property (nonatomic, strong) User *user;

@end
