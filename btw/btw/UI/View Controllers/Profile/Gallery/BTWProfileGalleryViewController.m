//
//  BTWProfileGalleryViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileGalleryViewController.h"
#import "BTWProfileGalleryCell.h"
#import "BTWGalleryButtonsView.h"
#import "BTWProfileGalleryDataController.h"
#import "BTWTransitionsManager.h"

#import "IDMPhotoBrowser.h"

static NSString *const kGalleryCellID = @"kGalleryCellID";

@interface BTWProfileGalleryViewController () <BTWDataControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorLeftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *activityIndicatorTopConstraint;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) BTWProfileGalleryDataController *dataController;
@property (nonatomic, weak) IDMPhotoBrowser *photoBrowser;
@property (nonatomic, strong) BTWGalleryButtonsView *buttonsView;
@end

@implementation BTWProfileGalleryViewController
@synthesize profileViewController;

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.dataController = [[BTWProfileGalleryDataController alloc] initWithDelegate:self];
}

#pragma mark - Setters
- (void)setUser:(User *)user
{
    if (![_user isEqual:user]) {
        _user = user;
        _dataController.user = user;
    }
}

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.buttonsView = [self createButtonsView];
    
    [_collectionView registerNib:[UINib nibWithNibName:@"BTWProfileGalleryCell" bundle:nil] forCellWithReuseIdentifier:kGalleryCellID];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [_dataController updatePhotos];
    if ([_dataController.photosArray count] == 0) {
        [_activityIndicator startAnimating];
    }
}

- (BTWGalleryButtonsView *)createButtonsView
{
    // override in subclasses
    return nil;
}

#pragma mark - Utils
- (void)prepareForParentController {
    [self.view layoutIfNeeded];
    self.view.frame = CGRectMake(0, 0, self.view.boundsWidth, self.collectionView.collectionViewLayout.collectionViewContentSize.height);
    self.collectionView.scrollEnabled = NO;
    self.view.origin = CGPointZero;
}

#pragma mark - BTWDataController delegate
- (void)dataController:(BTWProfileGalleryDataController *)controller didEndLoadingDataWithError:(NSError *)error
{
    [_activityIndicator stopAnimating];

    [_collectionView reloadData];
    [self.profileViewController childViewController:self didUpdateHeight:_collectionView.collectionViewLayout.collectionViewContentSize.height];
    
    if ([_collectionView numberOfItemsInSection:0] > 0) {
        _placeholderLabel.hidden = YES;
    } else {
        if (_placeholderLabel.hidden) {
            _placeholderLabel.alpha = 0.0;
            _placeholderLabel.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0;
            }];
        }
    }
}

#pragma mark - UICollectionView delegate & datasource
- (GalleryImage *)galleryImageForIndexPath:(NSIndexPath *)indexPath
{
    return _dataController.photosArray[indexPath.row];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_dataController.photosArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTWProfileGalleryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kGalleryCellID forIndexPath:indexPath];
    cell.galleryImage = [self galleryImageForIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BTWProfileGalleryCell *cell = (BTWProfileGalleryCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    NSMutableArray *photosArray = [NSMutableArray array];
    for (GalleryImage *galleryImage in _dataController.photosArray) {
        [photosArray addObject:[IDMPhoto photoWithURL:[NSURL URLWithString:galleryImage.url]]];
    }
    
    IDMPhotoBrowser *photoBrowser = [[IDMPhotoBrowser alloc] initWithPhotos:photosArray animatedFromView:cell];
    [photoBrowser setInitialPageIndex:[_dataController.photosArray indexOfObject:cell.galleryImage]];
    photoBrowser.scaleImage = cell.imageView.image;
    photoBrowser.buttonsView.size = _buttonsView.size;
    [photoBrowser.buttonsView addSubview:_buttonsView];
    
    CGFloat doneButtonSize = 40.0f;
    photoBrowser.doneButton.size = CGSizeMake(doneButtonSize, doneButtonSize);
    photoBrowser.doneButton.layer.cornerRadius = floorf(doneButtonSize/2);
    photoBrowser.doneButton.clipsToBounds = YES;
    [photoBrowser.doneButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.5] size:CGSizeMake(doneButtonSize, doneButtonSize)] forState:UIControlStateNormal];
    [photoBrowser.doneButton setImage:[UIImage imageNamed:@"icon_gallery_close"] forState:UIControlStateNormal];
    
    [[BTWTransitionsManager sharedInstance] showModalViewController:photoBrowser];
    
    self.photoBrowser = photoBrowser;
}

@end
