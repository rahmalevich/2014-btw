//
//  BTWProfileGalleryCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileGalleryCell.h"

#import "UIImageView+WebCache.h"

@interface BTWProfileGalleryCell ()
@property (weak, nonatomic, readwrite) IBOutlet UIImageView *imageView;
@end

@implementation BTWProfileGalleryCell

#pragma mark - Setup
- (void)setGalleryImage:(GalleryImage *)galleryImage
{
    if (![galleryImage isEqual:_galleryImage]) {
        _galleryImage = galleryImage;
        _imageView.alpha = 0.0f;
        
        typeof(self) wself = self;
        [_imageView sd_setImageWithURL:[NSURL URLWithString:_galleryImage.thumb_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
        {
            if (image) {
                if (cacheType == SDImageCacheTypeNone) {
                    [UIView animateWithDuration:0.3 animations:^{
                        wself.imageView.alpha = 1.0f;
                    }];
                } else {
                    wself.imageView.alpha = 1.0f;
                }
            }
        }];
    }
}

@end
