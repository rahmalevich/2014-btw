//
//  BTWProfileGalleryViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWProfileViewController.h"

@interface BTWProfileGalleryViewController : BTWViewController <BTWProfileChildViewController>

@property (nonatomic, readonly) User *user;

@end
