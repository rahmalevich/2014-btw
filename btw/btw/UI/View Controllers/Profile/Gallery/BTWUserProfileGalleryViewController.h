//
//  BTWUserProfileGalleryViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileGalleryViewController.h"

@interface BTWUserProfileGalleryViewController : BTWProfileGalleryViewController

- (void)setUser:(User *)user;

@end
