//
//  BTWProfileGalleryAddCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileGalleryAddCell.h"
#import "UIImage+ImageWithColor.h"

@interface BTWProfileGalleryAddCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@end

@implementation BTWProfileGalleryAddCell

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];

    UIColor *color = highlighted ? [UIColor darkGrayColor] : [UIColor lightGrayColor];
    _labelTitle.textColor = color;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 5.0);
    CGContextSetStrokeColorWithColor(context, [UIColor lightGrayColor].CGColor);

    UIBezierPath *borderPath = [UIBezierPath bezierPathWithRect:self.bounds];
    CGContextAddPath(context, borderPath.CGPath);

    CGContextStrokePath(context);
}

@end
