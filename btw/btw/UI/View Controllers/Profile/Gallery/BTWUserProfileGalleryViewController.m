//
//  BTWUserProfileGalleryViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWUserProfileGalleryViewController.h"
#import "BTWGalleryButtonsView.h"
#import "BTWUserService.h"
#import "BTWProfileGalleryDataController.h"

#import "IDMPhotoBrowser.h"
#import "MBProgressHUD.h"

@interface BTWProfileGalleryViewController (Protected)

@property (nonatomic, strong) User *user;
@property (nonatomic, weak) IDMPhotoBrowser *photoBrowser;
@property (nonatomic, strong) BTWProfileGalleryDataController *dataController;

- (void)commonInitialization;

@end

@interface BTWUserProfileGalleryViewController ()

@end

@implementation BTWUserProfileGalleryViewController

#pragma mark - Setup
- (void)setUser:(User *)user
{
    [super setUser:user];
}

- (BTWGalleryButtonsView *)createButtonsView
{
    __weak typeof(self) wself = self;
    BTWGalleryButton *complainButton = [BTWGalleryButton buttonWithImage:[UIImage imageNamed:@"icon_gallery_complain"] title:nil action:^{
        NSUInteger photoIndex = wself.photoBrowser.currentPageIndex;
        [wself.photoBrowser hideBrowserWithCompletionHandler:^{
            [wself complainOnImageAtIndex:photoIndex];
        }];
    }];
    BTWGalleryButtonsView *resultView = [[BTWGalleryButtonsView alloc] initWithButtons:@[/*likeButton,*/ complainButton]];
    [resultView sizeToFit];
    
    return resultView;
}

#pragma mark - Actions
- (void)complainOnImageAtIndex:(NSUInteger)photoIndex
{
    NSArray *galleryImages = self.dataController.photosArray;
    GalleryImage *currentImage = galleryImages.count > photoIndex ? galleryImages[photoIndex] : nil;
    if (currentImage) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.dataController complainOnPhoto:currentImage completionHandler:^(NSError *error){
            UIImage *hudImage = error ? [UIImage imageNamed:@"icon_error"] : [UIImage imageNamed:@"icon_success"];
            UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
            hudImageView.size = hudImage.size;
            hud.mode = MBProgressHUDModeCustomView;
            hud.customView = hudImageView;
            hud.labelText = error ? nil : NSLocalizedString(@"Complain sent", nil);
            [hud hide:YES afterDelay:1.0];
        }];
    }
}

@end
