//
//  BTWMyProfileGalleryViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMyProfileGalleryViewController.h"
#import "BTWProfileGalleryDataController.h"
#import "BTWProfileGalleryAddCell.h"
#import "BTWGalleryButtonsView.h"
#import "BTWTransitionsManager.h"
#import "BTWUserService.h"

#import "IDMPhotoBrowser.h"
#import "UIAlertView+Blocks.h"
#import "MBProgressHUD.h"
#import "Masonry.h"

// TODO: Отображать баннер

static NSString *const kGalleryAddCellID = @"kGalleryAddCellID";

@interface BTWProfileGalleryViewController (Protected) <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) BTWProfileGalleryDataController *dataController;
@property (nonatomic, weak) UICollectionView *collectionView;
@property (nonatomic, weak) IDMPhotoBrowser *photoBrowser;
@property (nonatomic, weak) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) NSLayoutConstraint *activityIndicatorLeftConstraint;
@property (nonatomic, weak) NSLayoutConstraint *activityIndicatorTopConstraint;

- (void)commonInitialization;
- (BTWGalleryButtonsView *)createButtonsView;
- (GalleryImage *)galleryImageForIndexPath:(NSIndexPath *)indexPath;

@end

@interface BTWMyProfileGalleryViewController ()

@end

@implementation BTWMyProfileGalleryViewController

#pragma mark - Initialization
- (void)commonInitialization
{
    [super commonInitialization];
    self.user = [BTWUserService sharedInstance].authorizedUser;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:@"BTWProfileGalleryAddCell" bundle:nil] forCellWithReuseIdentifier:kGalleryAddCellID];

    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat indicatorLeft = layout.sectionInset.left + layout.minimumInteritemSpacing + layout.itemSize.width + floorf((layout.itemSize.width - self.activityIndicator.boundsWidth)/2);
    CGFloat indicatorTop = layout.sectionInset.top + floorf((layout.itemSize.height - self.activityIndicator.boundsHeight)/2);
    self.activityIndicatorLeftConstraint.constant = indicatorLeft;
    self.activityIndicatorTopConstraint.constant = indicatorTop;
}

- (BTWGalleryButtonsView *)createButtonsView
{
    __weak typeof(self) wself = self;
    BTWGalleryButton *avatarButton = [BTWGalleryButton buttonWithImage:[UIImage imageNamed:@"icon_gallery_avatar"] title:NSLocalizedString(@"Profile picture", nil) action:^{
        [wself setCurrentPhotoToAvatar];
    }];
    BTWGalleryButton *deleteButton = [BTWGalleryButton buttonWithImage:[UIImage imageNamed:@"icon_gallery_remove"] title:NSLocalizedString(@"Delete", nil) action:^{
        [wself deleteCurrentPhoto];
    }];
    BTWGalleryButtonsView *resultView = [[BTWGalleryButtonsView alloc] initWithButtons:@[avatarButton, deleteButton]];
    [resultView sizeToFit];
    
    return resultView;
}

#pragma mark - UICollectionView delegate & datasource
- (GalleryImage *)galleryImageForIndexPath:(NSIndexPath *)indexPath
{
    return [self.dataController canUploadMorePhotos] ? self.dataController.photosArray[indexPath.row - 1] : self.dataController.photosArray[indexPath.row];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger photosCount = [self.dataController.photosArray count];
    return [self.dataController canUploadMorePhotos] ? photosCount + 1 : photosCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *resultCell = nil;
    if (indexPath.row == 0 && [self.dataController canUploadMorePhotos]) {
        BTWProfileGalleryAddCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kGalleryAddCellID forIndexPath:indexPath];
        resultCell = cell;
    } else {
        resultCell = [super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    }
    return resultCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && [self.dataController canUploadMorePhotos]) {
        [self uploadPhoto];
    } else {
        [super collectionView:collectionView didSelectItemAtIndexPath:indexPath];
    }
}

#pragma mark - Actions
- (void)deleteCurrentPhoto
{
    RIButtonItem *noItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"No", nil)];
    
    RIButtonItem *yesItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Yes", nil) action:^{
        GalleryImage *galleryImage = self.dataController.photosArray[[self.photoBrowser currentPageIndex]];
        [self.photoBrowser hideBrowserWithCompletionHandler:^{
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = NSLocalizedString(@"Deleting photo...", nil);
            [[BTWUserService sharedInstance] removeGalleryPhoto:galleryImage completionHandler:^(NSError *error){
                if (!error) {
                    [self.dataController updatePhotos];
                }
                UIImage *hudImage = error ? [UIImage imageNamed:@"icon_error"] : [UIImage imageNamed:@"icon_success"];
                UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
                hudImageView.size = hudImage.size;
                hud.mode = MBProgressHUDModeCustomView;
                hud.customView = hudImageView;
                [hud hide:YES afterDelay:1.0];
            }];
        }];
    }];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Delete this photo?", nil) cancelButtonItem:noItem otherButtonItems:yesItem, nil];
    [alertView show];
}

- (void)setCurrentPhotoToAvatar
{
    RIButtonItem *noItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"No", nil)];
    
    RIButtonItem *yesItem = [RIButtonItem itemWithLabel:NSLocalizedString(@"Yes", nil) action:^{
        GalleryImage *galleryImage = self.dataController.photosArray[[self.photoBrowser currentPageIndex]];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.photoBrowser.view animated:YES];
        [[BTWUserService sharedInstance] setAvatarWithGalleryPhoto:galleryImage completionHandler:^(NSError *error)
         {
             UIImage *hudImage = error ? [UIImage imageNamed:@"icon_error"] : [UIImage imageNamed:@"icon_success"];
             UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
             hudImageView.size = hudImage.size;
             hud.mode = MBProgressHUDModeCustomView;
             hud.customView = hudImageView;
             [hud hide:YES afterDelay:1.0];
         }];
    }];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"Make this your profile photo?", nil) cancelButtonItem:noItem otherButtonItems:yesItem, nil];
    [alertView show];
}

- (void)uploadPhoto
{
    __weak typeof(self) wself = self;
    [[BTWTransitionsManager sharedInstance] showImagePickerAndEditorWithCompletion:^(UIImage *image)
     {
         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         hud.mode = MBProgressHUDModeDeterminate;
         hud.labelText = NSLocalizedString(@"Uploading photo...", nil);
         
         [[BTWUserService sharedInstance] uploadGalleryPhoto:image name:nil message:nil progressHandler:^(BTWNetworkProgressType type, CGFloat progress){
             if (type == BTWNetworkProgressTypeUpload) {
                 hud.progress = progress;
             }
         } withCompletionHandler:^(NSError *error) {
             if (!error) {
                 [wself.dataController updatePhotos];
             }
             
             UIImage *hudImage = error ? [UIImage imageNamed:@"icon_error"] : [UIImage imageNamed:@"icon_success"];
             UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
             hudImageView.size = hudImage.size;
             
             hud.mode = MBProgressHUDModeCustomView;
             hud.customView = hudImageView;
             hud.labelText = error ? NSLocalizedString(@"Failed to upload", nil) : NSLocalizedString(@"Upload complete", nil);
             hud.detailsLabelText = ([error.domain isEqualToString:kBTWAPIErrorDomain] && error.code == BTWApiErrorCodeStorageRestriction) ? NSLocalizedString(@"Please upgrade your account to upload more photos", nil) : nil;
             [hud hide:YES afterDelay:1.0];
         }];
     }];
}

@end
