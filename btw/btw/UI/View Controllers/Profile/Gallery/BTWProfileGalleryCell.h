//
//  BTWProfileGalleryCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWProfileGalleryCell : UICollectionViewCell

@property (weak, nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, strong) GalleryImage *galleryImage;

@end
