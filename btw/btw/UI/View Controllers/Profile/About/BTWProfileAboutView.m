//
//  BTWProfileAboutView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWProfileAboutView.h"
#import "BTWRatingView.h"
#import "BTWLocationManager.h"
#import "BTWUserService.h"
#import "BTWTransitionsManager.h"

#import "RoundedBorderButton.h"

#import "IDMPhotoBrowser.h"
#import "UIImage+BlurredFrame.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD+CompletionIcon.h"

@interface BTWProfileAboutView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backgroundHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *avatarView;
@property (weak, nonatomic) IBOutlet UIView *bottomGradientContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIButton *editAvatarButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet BTWRatingView *ratingView;

@end

@implementation BTWProfileAboutView

#pragma mark - Initialization

- (void)awakeFromNib
{
    [self commonInitialization];
}

- (void)commonInitialization
{
    _avatarImageView.layer.cornerRadius = floorf(_avatarImageView.frameWidth/2);
    
    _avatarView.layer.cornerRadius = floorf(_avatarView.frameWidth/2);
    _avatarView.layer.shadowOpacity = 0.5f;
    _avatarView.layer.shadowRadius = 5.0f;
    _avatarView.layer.shadowOffset = (CGSize){0.0f, 1.0f};
    _avatarView.layer.shadowColor = [UIColor blackColor].CGColor;
    
    CAGradientLayer *gradient = [CAGradientLayer new];
    gradient.frame = _bottomGradientContainerView.bounds;
    gradient.colors = @[(__bridge id)[UIColor clearColor].CGColor, (__bridge  id)[UIColor colorWithWhite:0.0 alpha:0.15].CGColor];
    [_bottomGradientContainerView.layer addSublayer:gradient];
    
    UITapGestureRecognizer *avatarTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapRecognized:)];
    [_avatarView addGestureRecognizer:avatarTapRecognizer];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup
- (void)setUser:(User *)user
{
    if (![_user isEqual:user]) {
        _user = user;
        [self updatePhotos];
        [self updateProfileFields];
    }
}

- (void)setEditState:(BOOL)editState
{
    if (_editState != editState) {
        _editState = editState;
        
        if (editState) {
            _editAvatarButton.hidden = NO;
            _editAvatarButton.alpha = 0.0f;
            [UIView animateWithDuration:0.3 animations:^{
                _editAvatarButton.alpha = 1.0f;
            }];
        } else {
            [UIView animateWithDuration:0.3 animations:^{
                _editAvatarButton.alpha = 0.0f;
            } completion:^(BOOL finished){
                _editAvatarButton.hidden = YES;
            }];
        }
    }
}

- (void)updatePhotos
{
    __block UIImage *avatarPhoto = nil;
    void (^updatePhotosBlock)(void) = ^{
        if (avatarPhoto) {
            UIImage *processedImage = [avatarPhoto applyBlurWithRadius:10.0 tintColor:[UIColor colorWithWhite:0.5 alpha:0.5] saturationDeltaFactor:1.0 maskImage:nil atFrame:(CGRect){CGPointZero, avatarPhoto.size}];
            _backgroundImageView.image = processedImage;
            _avatarImageView.image = avatarPhoto;
            
            CATransition *transition = [CATransition animation];
            transition.duration = 1.0f;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionFade;
            [_backgroundImageView.layer addAnimation:transition forKey:nil];
            [_avatarImageView.layer addAnimation:transition forKey:nil];
        }
    };
    
    [_user.managedObjectContext refreshObject:_user mergeChanges:YES];
    if (_user.photo_url) {
        NSURL *photoURL = [NSURL URLWithString:_user.photo_url];
        
        // # проверяем наличие фотографий в кэшэ
        SDWebImageManager *imageManager = [SDWebImageManager sharedManager];
        avatarPhoto = [imageManager.imageCache imageFromDiskCacheForKey:[imageManager cacheKeyForURL:photoURL]];
        
        // # устанавливаем плэйсхолдэры и загружаем фотографии, если их нет
        if (avatarPhoto) {
            updatePhotosBlock();
        } else {
            if (!_backgroundImageView.image || !_avatarImageView.image) {
                _backgroundImageView.image = [UIImage imageNamed:@"bg_register"];
                _avatarImageView.image = [_user midPlaceholder];
            }
            
            [imageManager downloadImageWithURL:photoURL options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
             {
                 avatarPhoto = image;
                 updatePhotosBlock();
             }];
        }
    } else {
        _backgroundImageView.image = [UIImage imageNamed:@"bg_register"];
        _avatarImageView.image = [_user midPlaceholder];
    }
}

- (void)updateProfileFields
{
    [_user.managedObjectContext refreshObject:_user mergeChanges:YES];
    
    _titleLabel.text = _user.fullname;
    _subtitleLabel.text = _user.age > 0 ? [BTWCommonUtils ageStringForAge:_user.age] : @"";
    _ratingView.hidden = NO;
    _ratingView.rating = [_user.average_points floatValue];

    if ([_user.backend_id isEqualToString:[BTWUserService sharedInstance].authorizedUser.backend_id]) {
        if ([BTWLocationManager sharedInstance].currentLocation) {
            [self updateSubtitleLabel];
        } else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSetInitialLocation:) name:kLocationManagerDidSetInitialLocationNotification object:nil];
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(avatarChanged:) name:kUserAvatarChangedNotification object:nil];
    }
}

- (void)setBackgroundHeight:(CGFloat)height
{
    _backgroundHeightConstraint.constant = height;
    [self layoutIfNeeded];
}

#pragma mark - Location
- (void)updateSubtitleLabel
{
    __weak typeof(self) wself = self;
    [[BTWLocationManager sharedInstance] geocodeCurrentLocationWithCompletionHandler:^(BTWPlacemark *placemark, NSError *error)
     {
         NSMutableString *labelText = [NSMutableString stringWithString:@""];
         if (_user.age > 0) {
             [labelText appendString:[BTWCommonUtils ageStringForAge:_user.age]];
         }
         if ([placemark.locality isValidString] && [placemark.country isValidString]) {
             if (labelText.length > 0) {
                 [labelText appendString:@", "];
             }
             [labelText appendFormat:@"%@, %@", placemark.locality, placemark.country];
         }
         wself.subtitleLabel.text = labelText;
     }];
}

- (void)didSetInitialLocation:(NSNotification *)notification
{
    [self updateSubtitleLabel];
}

#pragma mark - Avatar
- (void)avatarChanged:(NSNotification *)notification
{
    [self updatePhotos];
}

#pragma mark - Actions
- (void)avatarTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    if (_editState || ![self.user.photo_url isValidString]) {
        [self actionSelectAvatar:nil];
    } else {
        IDMPhotoBrowser *photoBrowser = [[IDMPhotoBrowser alloc] initWithPhotos:@[[IDMPhoto photoWithURL:[NSURL URLWithString:_user.photo_url]]] animatedFromView:nil];
        CGFloat doneButtonSize = 40.0f;
        photoBrowser.doneButton.size = CGSizeMake(doneButtonSize, doneButtonSize);
        photoBrowser.doneButton.layer.cornerRadius = floorf(doneButtonSize/2);
        photoBrowser.doneButton.clipsToBounds = YES;
        [photoBrowser.doneButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.5] size:CGSizeMake(doneButtonSize, doneButtonSize)] forState:UIControlStateNormal];
        [photoBrowser.doneButton setImage:[UIImage imageNamed:@"icon_gallery_close"] forState:UIControlStateNormal];
        [[BTWTransitionsManager sharedInstance] showModalViewController:photoBrowser];
    }
}

- (IBAction)actionSelectAvatar:(UIButton *)sender
{
    [[BTWTransitionsManager sharedInstance] showImagePickerAndEditorWithCompletion:^(UIImage *image)
     {
         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
         hud.mode = MBProgressHUDModeDeterminate;
         hud.labelText = NSLocalizedString(@"Uploading photo...", nil);
         
         [[BTWUserService sharedInstance] uploadAvatar:image progressHandler:^(BTWNetworkProgressType type, CGFloat progress){
             if (type == BTWNetworkProgressTypeUpload) {
                 hud.progress = progress;
             }
         } withCompletionHandler:^(NSError *error) {
             [hud handleCompletionForSuccess:(error ? NO : YES)];
             hud.labelText = error ? NSLocalizedString(@"Failed to upload", nil) : NSLocalizedString(@"Upload complete", nil);
         }];
     }];
}

- (void)cancelEdit
{
    self.editState = NO;
}

@end
