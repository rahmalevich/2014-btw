//
//  BTWProfileAboutView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

@interface BTWProfileAboutView : UIView

@property (nonatomic, strong) User *user;
@property (nonatomic, assign) BOOL editState;

- (void)setBackgroundHeight:(CGFloat)height;
- (void)updateProfileFields;

@end
