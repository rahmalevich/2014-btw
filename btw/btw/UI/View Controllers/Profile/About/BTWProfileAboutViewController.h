//
//  BTWProfileViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWProfileViewController.h"

@interface BTWProfileAboutViewController : BTWViewController <BTWProfileChildViewController>

@property (nonatomic, readonly) User *user;

- (void)updateProfileFields;

@end
