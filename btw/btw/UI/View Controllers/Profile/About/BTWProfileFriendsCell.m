//
//  BTWProfileFriendsCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 13.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileFriendsCell.h"

@interface BTWProfileFriendsCell ()

@end

@implementation BTWProfileFriendsCell

- (void)awakeFromNib
{
    _imageView.layer.cornerRadius = floorf(_imageView.boundsWidth/2);
}

@end
