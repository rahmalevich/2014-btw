//
//  BTWMyProfileEditCarViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 19.08.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMyProfileEditCarViewController.h"
#import "BTWQuestionsService.h"
#import "BTWUserService.h"
#import "KeyboardListener.h"
#import "PopoverButton.h"
#import "InsetTextField.h"

@interface BTWMyProfileEditCarViewController () <PopoverButtonDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet PopoverButton *carBrandButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carModelButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carColorButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carYearButton;
@property (weak, nonatomic) IBOutlet InsetTextField *carNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *surveyFieldsArray;
@end

@implementation BTWMyProfileEditCarViewController
@synthesize profileViewController;

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carsUpdated:) name:kQuestionsServiceDidFinishToUpdateCars object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsUpdated:) name:kQuestionsServiceDidUpdateQuestions object:nil];
    
    [[KeyboardListener sharedInstance] addObserver:self forKeyPath:@"isKeyboardVisible" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[KeyboardListener sharedInstance] removeObserver:self forKeyPath:@"isKeyboardVisible"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UIView *aView in _surveyFieldsArray) {
        aView.layer.cornerRadius = 5.0f;
    }
    _doneButton.layer.cornerRadius = 5.0f;
    [_doneButton setBackgroundImage:[UIImage imageWithColor:[BTWStylesheet commonGreenColor] size:_doneButton.size] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![BTWQuestionsService sharedInstance].carsUpdated) {
        [[BTWQuestionsService sharedInstance] updateCars];
    }
    
    [self setupCarFields];
}

- (void)setupCarFields
{
    // автомобиль
    Car *usersCar = [BTWUserService sharedInstance].authorizedUser.car;
    
    // # производитель автомобиля
    CarBrand *selectedBrand = [CarBrand MR_findFirstByAttribute:@"name" withValue:usersCar.brand];
    _carBrandButton.itemsArray = [CarBrand MR_findAllSortedBy:@"name" ascending:YES];
    _carBrandButton.titleBlock = ^(CarBrand *carBrand){ return carBrand.name; };
    if (selectedBrand) {
        _carBrandButton.selectedItem = selectedBrand;
    } else if ([usersCar.brand isValidString]) {
        [_carBrandButton setTitle:usersCar.brand forState:UIControlStateNormal];
    }
    
    // # модель автомобиля
    _carModelButton.itemsArray = selectedBrand.models;
    _carModelButton.selectedItem = usersCar.model;
    
    // # цвет автомобиля
    Question *colorQuestion = [Question MR_findFirstByAttribute:@"category" withValue:kColorCategoryKey];
    Answer *selectedAnswer = [[colorQuestion.answers filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"text = %@", usersCar.color]] firstObject];
    NSArray *orderedColors = [colorQuestion.answers.array sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sort_order" ascending:YES]]];
    _carColorButton.itemsArray = orderedColors;
    _carColorButton.titleBlock = ^(Answer *item){ return item.text; };
    if (selectedAnswer) {
        _carColorButton.selectedItem = selectedAnswer;
    } else {
        [_carColorButton setTitle:usersCar.color forState:UIControlStateNormal];
    }
    
    // # год автомобиля
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:[NSDate date]];
    NSMutableArray *items = [NSMutableArray array];
    for (NSInteger year = [components year]; year  >= kFilterCarMinYear; year--) {
        [items addObject:[@(year) stringValue]];
    }
    _carYearButton.itemsArray = items;
    _carYearButton.selectedItem = [usersCar.year integerValue] > 0 ? [usersCar.year stringValue] : nil;
    
    // # номер автомобиля
    _carNumberTextField.text = usersCar.number;
    _carNumberTextField.tintColor = [BTWStylesheet commonTextColor];
}

#pragma mark - Notifications handling
- (void)carsUpdated:(NSNotification *)notification
{
    CarBrand *selectedBrand = [CarBrand MR_findFirstByAttribute:@"name" withValue:[BTWUserService sharedInstance].authorizedUser.car.brand];
    _carBrandButton.itemsArray = [CarBrand MR_findAllSortedBy:@"name" ascending:YES];
    _carBrandButton.selectedItem = selectedBrand;
    _carModelButton.itemsArray = selectedBrand.models;
}

- (void)questionsUpdated:(NSNotification *)notification
{
    Question *colorQuestion = [Question MR_findFirstByAttribute:@"category" withValue:kColorCategoryKey];
    Answer *selectedAnswer = [[colorQuestion.answers filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"text = %@", [BTWUserService sharedInstance].authorizedUser.car.color]] firstObject];
    _carColorButton.itemsArray = [NSArray arrayWithArray:colorQuestion.answers.array];
    _carColorButton.selectedItem = selectedAnswer;
}

#pragma mark - Popover button delegate
- (void)popoverButton:(PopoverButton *)popoverButton didSelectItem:(id)item
{
    User *authorizedUser = [BTWUserService sharedInstance].authorizedUser;
    if ([popoverButton isEqual:_carBrandButton]) {
        CarBrand *selectedBrand = (CarBrand *)item;
        _carModelButton.itemsArray = selectedBrand.models;
        authorizedUser.car.brand = selectedBrand.name;
    } else if ([popoverButton isEqual:_carModelButton]) {
        NSString *selectedModel = (NSString *)item;
        authorizedUser.car.model = selectedModel;
    } else if ([popoverButton isEqual:_carColorButton]) {
        Answer *selectedColor = (Answer *)item;
        authorizedUser.car.color = selectedColor.text;
    } else if ([popoverButton isEqual:_carYearButton]) {
        NSString *selectedYear = (NSString *)item;
        authorizedUser.car.year = @([selectedYear integerValue]);
    }
    [[BTWUserService sharedInstance] updateCarInfoWithCompletionHandler:nil];
}

#pragma mark - Text field delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return newString.length <= kCarNumberMaxLength;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.text.length > 0) {
        [BTWUserService sharedInstance].authorizedUser.car.number = textField.text;
        [[BTWUserService sharedInstance] updateCarInfoWithCompletionHandler:nil];
        [textField resignFirstResponder];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Enter your car's number", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
    return YES;
}

#pragma mark - Actions
- (IBAction)actionDone:(UIButton *)sender
{
    if (_doneBlock) {
        _doneBlock();
    }
}

@end
