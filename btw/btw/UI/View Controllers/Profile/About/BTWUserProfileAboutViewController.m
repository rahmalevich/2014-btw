//
//  BTWUserProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWUserProfileAboutViewController.h"
#import "BTWUserService.h"

@interface BTWProfileAboutViewController (Protected)
@property (weak, nonatomic) IBOutlet UIView *controlsContainerView;
@property (weak, nonatomic) IBOutlet UICollectionView *friendsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *interestsCollectionView;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, strong) NSArray *interests;
@end

@interface BTWUserProfileAboutViewController ()
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *controlsContainerHeight;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *facebookView;
@end

@implementation BTWUserProfileAboutViewController

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];    
    [self updateAppearence];
}

- (void)setUser:(User *)user
{
    [super setUser:user];
    [self updateContent];
}

- (void)updateContent
{
    User *authorizedUser = [BTWUserService sharedInstance].authorizedUser;
    if ([authorizedUser.socialNetworkService isReady]) {
        if (authorizedUser.linkedSocialNetwork == self.user.linkedSocialNetwork) {
            _placeholderLabel.hidden = YES;
            [_activityIndicator startAnimating];
            
            __weak typeof(self) wself = self;
            __block BOOL friendsLoaded = NO, interestsLoaded = NO;
            [authorizedUser.socialNetworkService getMutualFriendsForUser:[self.user socialNetworkID] withCompletionHandler:^(NSArray *mutualFriends, NSError *error)
             {
                 wself.friends = mutualFriends;
                 friendsLoaded = YES;
                 if (friendsLoaded && interestsLoaded) {
                     [wself updateAppearence];
                 }
             }];
            
            [authorizedUser.socialNetworkService getMutualInterestsForUser:[self.user socialNetworkID] withCompletionHandler:^(NSArray *mutualInterests, NSError *error)
             {
                 wself.interests = mutualInterests;
                 interestsLoaded = YES;
                 if (friendsLoaded && interestsLoaded) {
                     [wself updateAppearence];
                 }
             }];
        } else {
            [self updateAppearence];
        }
    } else {
        [self updateAppearence];
    }
}

- (void)updateAppearence
{
    [_activityIndicator stopAnimating];
    if ([self.friends count] > 0 || [self.interests count] > 0) {
        _controlsContainerHeight.constant = 255.0f;
        _facebookView.alpha = 0.0f;
        _facebookView.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            _facebookView.alpha = 1.0f;
        }];
    } else {
        _controlsContainerHeight.constant = 150.0f;
        _placeholderLabel.alpha = 0.0f;
        _placeholderLabel.hidden = NO;
        _placeholderLabel.text = [[BTWUserService sharedInstance].authorizedUser.socialNetworkService isReady] ?  NSLocalizedString(@"You don't have mutual friends or interests yet", nil) : NSLocalizedString(@"Connect to social networks to see mutual friends and interests", nil);
        [UIView animateWithDuration:0.3 animations:^{
            _placeholderLabel.alpha = 1.0f;
        }];
    }
}

@end
