//
//  BTWMyProfileEditCarViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 19.08.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWProfileViewController.h"

@interface BTWMyProfileEditCarViewController : BTWViewController <BTWProfileChildViewController>

@property (nonatomic, copy) BTWVoidBlock doneBlock;

@end
