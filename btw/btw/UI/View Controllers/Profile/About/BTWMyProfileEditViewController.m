//
//  BTWProfileEditView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMyProfileEditViewController.h"
#import "BTWFormattingService.h"

#import "InsetTextField.h"
#import "RMDateSelectionViewController.h"

@interface BTWMyProfileEditViewController ()

@property (nonatomic, strong) NSDate *birthdayDate;
@property (nonatomic, strong) NSDate *previousValidDate;
@property (nonatomic, assign) BTWUserGender gender;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *surnameTextField;
@property (weak, nonatomic) IBOutlet UIButton *birthdayButton;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation BTWMyProfileEditViewController
@synthesize profileViewController;

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _nameTextField.tintColor = [BTWStylesheet commonTextColor];
    _surnameTextField.tintColor = [BTWStylesheet commonTextColor];
    _saveButton.layer.cornerRadius = 5.0f;
    [_saveButton setBackgroundImage:[UIImage imageWithColor:_saveButton.backgroundColor] forState:UIControlStateNormal];
    
    [self setupContent];
}

- (void)prepareForParentController
{
    [self.view layoutIfNeeded];
}

#pragma mark - Public interface
- (void)setUser:(User *)user
{
    _user = user;
    [self setupContent];
}

- (void)setupContent
{
    _nameTextField.text = _user.name;
    _surnameTextField.text = _user.surname;
    
    if (_user.birthday) {
        self.birthdayDate = _user.birthday;
        [_birthdayButton setTitle:[[BTWFormattingService shortDateFormatterRelativeFormatting:NO] stringFromDate:_user.birthday] forState:UIControlStateNormal];
    }
    
    self.gender = [_user.gender integerValue];
    [_genderButton setTitle:[BTWCommonUtils stringForGender:_gender] forState:UIControlStateNormal];
}

- (BOOL)updateUserValues
{
    BOOL result = NO;
    if (_nameTextField.text.length > 0 && _surnameTextField.text.length > 0) {
        if (![_user.name isEqualToString:_nameTextField.text]) {
            _user.name = _nameTextField.text;
            result = YES;
        }
        if (![_user.surname isEqualToString:_surnameTextField.text]) {
            _user.surname = _surnameTextField.text;
            result = YES;
        }
        if (_birthdayDate) {
            _user.birthday = _birthdayDate;
            result = YES;
        }
        if ([_user.gender integerValue] != _gender) {
            _user.gender = @(_gender);
            result = YES;
        }
    }
    return result;
}

#pragma mark - Actions
- (IBAction)actionSelectGender:(UIButton *)sender
{
    [RMDateSelectionViewController setLocalizedTitleForCancelButton:NSLocalizedString(@"Cancel", nil)];
    [RMDateSelectionViewController setLocalizedTitleForSelectButton:NSLocalizedString(@"Select", nil)];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController selectionControllerForObjects:@[@(BTWUserGenderMale), @(BTWUserGenderFemale)]];
    dateSelectionController.objectTitleBlock = ^(id object){
        BTWUserGender gender = [object integerValue];
        return [BTWCommonUtils stringForGender:gender];
    };
    
    __weak typeof(self) wself = self;
    [dateSelectionController showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSNumber *gender){
        wself.gender = [gender integerValue];
        [wself.genderButton setTitle:[BTWCommonUtils stringForGender:wself.gender] forState:UIControlStateNormal];
    } andCancelHandler:nil];
}

- (IBAction)actionSelectBirthday:(UIButton *)sender
{
    [RMDateSelectionViewController setLocalizedTitleForCancelButton:NSLocalizedString(@"Cancel", nil)];
    [RMDateSelectionViewController setLocalizedTitleForSelectButton:NSLocalizedString(@"Select", nil)];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController dateSelectionController];
    dateSelectionController.disableBouncingWhenShowing = YES;
    dateSelectionController.hideNowButton = YES;
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionController.datePicker.minimumDate = [[BTWFormattingService dateFormatterWithFormat:@"MM.dd.yyyy"] dateFromString:@"01.01.1900"];

    if (_birthdayDate) {
        dateSelectionController.datePicker.date = _birthdayDate;
    }
    [dateSelectionController.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    __weak typeof(self) wself = self;
    [dateSelectionController showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *date){
        wself.birthdayDate = date;
        [wself.birthdayButton setTitle:[[BTWFormattingService shortDateFormatterRelativeFormatting:NO] stringFromDate:date] forState:UIControlStateNormal];
    } andCancelHandler:nil];
}

- (void)dateChanged:(UIDatePicker *)datePicker
{
    NSDateComponents *components = [NSDateComponents new];
    components.year = -18;
    NSDate *maximumDate = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    if ([maximumDate compare:datePicker.date] == NSOrderedAscending) {
        [datePicker setDate:self.previousValidDate animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You should be 18 years old for using this application", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    } else {
        self.previousValidDate = datePicker.date;
    }
}

- (IBAction)actionSave:(UIButton *)sender
{
    if (_saveBlock) {
        _saveBlock();
    }
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:_nameTextField]) {
        [_surnameTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        if ([textField isEqual:_surnameTextField]) {
            [self actionSelectBirthday:nil];
        }
    }
    return YES;
}

@end
