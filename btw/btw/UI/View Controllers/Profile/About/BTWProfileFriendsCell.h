//
//  BTWProfileFriendsCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 13.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTWProfileFriendsCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
