//
//  BTWMyProfileAboutViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMyProfileAboutViewController.h"
#import "BTWMyProfileViewController.h"
#import "BTWUserService.h"
#import "BTWLocationManager.h"
#import "BTWFacebookService.h"
#import "BTWVkService.h"
#import "BTWTransitionsManager.h"
#import "BTWCommonUtils.h"
#import "BTWFormattingService.h"
#import "BTWSettingsService.h"

#import "InsetTextField.h"
#import "KeyboardHelper.h"

#import "MBProgressHUD+CompletionIcon.h"
#import "UIImageView+WebCache.h"
#import "RMDateSelectionViewController.h"

@interface BTWMyProfileViewController (Protected)
- (void)actionEditCar;
@end

@interface BTWProfileAboutViewController (Protected)
@property (weak, nonatomic) IBOutlet UILabel *friendsPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UILabel *interestsPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *friendsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *interestsCollectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *friendsActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *interestsActivityIndicator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carViewHeightConstraint;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, strong) NSArray *interests;

- (void)avatarTapRecognized:(UITapGestureRecognizer *)gestureRecognizer;

@end

@interface BTWMyProfileAboutViewController () <UITextFieldDelegate>
@property (nonatomic, assign) BOOL didSetup;
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, assign) BOOL editState;
@property (nonatomic, strong) KeyboardHelper *keyboardHelper;

@property (weak, nonatomic) IBOutlet UIView *generalControlsView;
@property (weak, nonatomic) IBOutlet UIView *connectToFBControlsView;
@property (weak, nonatomic) IBOutlet UIView *facebookFriendsView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *facebookActivityIndicator;
@end

@implementation BTWMyProfileAboutViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nil bundle:nil]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.didSetup = NO;
    
    self.localContext = [NSManagedObjectContext MR_context];
    self.user = [User MR_findFirstByAttribute:@"backend_id" withValue:[BTWUserService sharedInstance].authorizedUser.backend_id inContext:_localContext];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionClosed:) name:kFacebookSessionFailedToOpenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionClosed:) name:kFacebookSessionClosedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(friendsLoaded:) name:kFacebookFriendsLoadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(interestsLoaded:) name:kFacebookInterestsLoadedNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionClosed:) name:kVkSessionFailedToOpenNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sessionClosed:) name:kVkSessionClosedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(friendsLoaded:) name:kVkFriendsLoadedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(interestsLoaded:) name:kVkInterestsLoadedNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Lifecycle
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (!_didSetup) {
        [self handleSocialNetworkLinkStatus];
        self.didSetup = YES;
    }
}

#pragma mark - Social networks info
- (void)handleSocialNetworkLinkStatus
{
    [self.user.managedObjectContext refreshObject:self.user mergeChanges:YES];
    
    BTWLinkedSocialNetwork linkedNetwork = [self.user linkedSocialNetwork];
    id<BTWSocialNetworkService> socialNetworkService = [self.user socialNetworkService];
    
    if (linkedNetwork != BTWLinkedSocialNetworkBlank && [socialNetworkService isReady]) {
        [_facebookActivityIndicator stopAnimating];
        if (_facebookFriendsView.hidden) {
            [self updateFriendsAppearence];
            [self updateInterestsAppearence];
            [_generalControlsView bringSubviewToFront:_facebookFriendsView];
            _facebookFriendsView.alpha = 0.0f;
            _facebookFriendsView.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                _facebookFriendsView.alpha = 1.0f;
            } completion:^(BOOL finished){
                _connectToFBControlsView.hidden = YES;
            }];
        }
    } else {
        if ([socialNetworkService isOpeningSession]) {
            [_facebookActivityIndicator startAnimating];
            if (!_connectToFBControlsView.hidden || !_facebookFriendsView.hidden) {
                [UIView animateWithDuration:0.3 animations:^{
                    _connectToFBControlsView.alpha = 0.0f;
                    _facebookFriendsView.alpha = 0.0f;
                } completion:^(BOOL finished){
                    _connectToFBControlsView.hidden = _facebookFriendsView.hidden = YES;
                }];
            }
        } else {
            [_facebookActivityIndicator stopAnimating];
            if (_connectToFBControlsView.hidden) {
                [_generalControlsView bringSubviewToFront:_connectToFBControlsView];
                _connectToFBControlsView.alpha = 0.0f;
                _connectToFBControlsView.hidden = NO;
                [UIView animateWithDuration:0.3 animations:^{
                    _connectToFBControlsView.alpha = 1.0f;
                } completion:^(BOOL finished){
                    _facebookFriendsView.hidden = YES;
                }];
            }
        }
    }
    [self.view setNeedsLayout];
}

- (void)updateFriendsAppearence
{
    id<BTWSocialNetworkService> socialNetworkService = [self.user socialNetworkService];

    self.friends = [socialNetworkService friends];
    if ([self.friends count] == 0) {
        if (socialNetworkService.isLoadingFriends) {
            [self.friendsActivityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                self.friendsPlaceholderLabel.alpha = 0.0f;
            }];
        } else {
            [self.friendsActivityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                self.friendsPlaceholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [self.friendsActivityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            self.friendsPlaceholderLabel.alpha = 0.0f;
        }];
    }
}

- (void)updateInterestsAppearence
{
    id<BTWSocialNetworkService> socialNetworkService = [self.user socialNetworkService];
    
    self.interests = socialNetworkService.interests;
    if ([self.interests count] == 0) {
        if (socialNetworkService.isLoadingInterests) {
            [self.interestsActivityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                self.interestsPlaceholderLabel.alpha = 0.0f;
            }];
        } else {
            [self.interestsActivityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                self.interestsPlaceholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [self.interestsActivityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            self.interestsPlaceholderLabel.alpha = 0.0f;
        }];
    }
}

- (void)sessionClosed:(NSNotification *)notification
{
    [self handleSocialNetworkLinkStatus];
}

- (void)friendsLoaded:(NSNotification *)notification
{
    [self handleSocialNetworkLinkStatus];
    [self updateFriendsAppearence];
}

- (void)interestsLoaded:(NSNotification *)notification
{
    [self handleSocialNetworkLinkStatus];
    [self updateInterestsAppearence];
}

#pragma mark - Actions
- (IBAction)actionFacebook:(UIButton *)sender
{
    [self linkSocialNetwork:BTWLinkedSocialNetworkFacebook];
}

- (IBAction)actionVk:(UIButton *)sender
{
    [self linkSocialNetwork:BTWLinkedSocialNetworkVk];
}

- (void)linkSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork
{
    __weak typeof(self) wself = self;
 
    id<BTWSocialNetworkService> socialNetworkService = nil;
    if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
        socialNetworkService = [BTWFacebookService sharedInstance];
    } else if (socialNetwork == BTWLinkedSocialNetworkVk) {
        socialNetworkService = [BTWVKService sharedInstance];
    }
    
    [socialNetworkService setupWithCompletionHandler:^(BOOL opened, id error)
    {
        BTWErrorBlock errorBlock = [^(NSError *error){
            NSString *message;
            NSString *networkTitle = socialNetwork == BTWLinkedSocialNetworkFacebook ? @"Facebook" : @"VK";
            if ([error.domain isEqualToString:kBTWAPIErrorDomain] && error.code == BTWApiErrorCodeUserAlreadyRegistered) {
                message = [NSString stringWithFormat:NSLocalizedString(@"%@ profile is different from the one you are connected to", nil), networkTitle];
            } else {
                message = [NSString stringWithFormat:NSLocalizedString(@"Cannot connect to %@", nil), networkTitle];
            }
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alertView show];
        } copy];
        
        BTWVoidBlock handleLinkBlock = [^{
            [socialNetworkService updateFriendsAndInterests];
            [wself handleSocialNetworkLinkStatus];
        } copy];

        if ([socialNetworkService isReady]) {
            BOOL alreadyLinked = NO;
            if ( (socialNetwork == BTWLinkedSocialNetworkFacebook && [[BTWUserService sharedInstance].authorizedUser.facebook_id isValidString]) ||
                 (socialNetwork == BTWLinkedSocialNetworkVk && [[BTWUserService sharedInstance].authorizedUser.vkontakte_id isValidString]) )
            {
                alreadyLinked = YES;
            }
            
            if (alreadyLinked) {
                handleLinkBlock();
            } else {
                [[BTWUserService sharedInstance] linkSocialNetwork:socialNetwork userID:[socialNetworkService userID] sessionKey:[socialNetworkService accessToken] completionHandler:^(NSError *error)
                {
                    if (error) {
                        [socialNetworkService closeSession];
                        errorBlock(nil);
                    } else {
                        handleLinkBlock();
                    }
                }];
            }
        } else {
            errorBlock(error);
        }
    }];
}

- (IBAction)actionEditCar:(UITapGestureRecognizer *)sender
{
    [(BTWMyProfileViewController *)self.profileViewController actionEditCar];
}

@end
