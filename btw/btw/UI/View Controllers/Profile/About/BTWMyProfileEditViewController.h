//
//  BTWProfileEditView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWProfileViewController.h"

@interface BTWMyProfileEditViewController : BTWViewController <BTWProfileChildViewController>

@property (nonatomic, copy) BTWVoidBlock saveBlock;
@property (nonatomic, weak) User *user;

- (BOOL)updateUserValues;

@end
