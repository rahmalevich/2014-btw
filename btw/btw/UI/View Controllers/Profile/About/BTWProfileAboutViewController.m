//
//  BTWProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileAboutViewController.h"
#import "BTWProfileFriendsCell.h"
#import "BTWFacebookService.h"
#import "BTWVKService.h"
#import "BTWTransitionsManager.h"
#import "BTWUserProfileViewController.h"

#import "UIImage+BlurredFrame.h"
#import "UIImageView+WebCache.h"
#import "IDMPhotoBrowser.h"

static CGFloat const kCarPanelHeight = 100.0f;
static NSString * const kFriendsCellReuseID = @"friendsCellReuseID";
static NSString * const kInterestsCellReuseID = @"interestsCellReuseID";

@interface BTWProfileAboutViewController () <UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *controlsContainerView;
@property (weak, nonatomic) IBOutlet UILabel *friendsPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UILabel *interestsPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UILabel *carModelLabel;
@property (weak, nonatomic) IBOutlet UILabel *carNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *carColorLabel;
@property (weak, nonatomic) IBOutlet UILabel *carYearLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *friendsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *interestsCollectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *friendsActivityIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *interestsActivityIndicator;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *carViewHeightConstraint;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSArray *friends;
@property (nonatomic, strong) NSArray *interests;
@end

@implementation BTWProfileAboutViewController
@synthesize profileViewController;

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_friendsCollectionView registerNib:[UINib nibWithNibName:@"BTWProfileFriendsCell" bundle:nil] forCellWithReuseIdentifier:kFriendsCellReuseID];
    [_interestsCollectionView registerNib:[UINib nibWithNibName:@"BTWProfileFriendsCell" bundle:nil] forCellWithReuseIdentifier:kInterestsCellReuseID];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    self.carViewHeightConstraint.constant = self.user.car ? kCarPanelHeight : 0.0f;
    [self.view setNeedsLayout];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateProfileFields];
}

#pragma mark - Setup
- (void)setUser:(User *)user
{
    if (![_user isEqual:user]) {
        _user = user;
        
        [self updateProfileFields];
    }
}

- (void)setFriends:(NSArray *)friends
{
    _friends = friends;
    _friendsPlaceholderLabel.hidden = [friends count] > 0;
    [_friendsCollectionView reloadData];
}

- (void)setInterests:(NSArray *)interests
{
    _interests = interests;
    _interestsPlaceholderLabel.hidden = [interests count] > 0;
    [_interestsCollectionView reloadData];
}

- (void)updateProfileFields
{
    [_user.managedObjectContext refreshObject:_user.car mergeChanges:NO];
    
    NSString *carModel;
    if ([_user.car.brand isValidString]) {
        carModel = _user.car.brand;
        if ([_user.car.model isValidString]) {
            carModel = [carModel stringByAppendingFormat:@" %@", _user.car.model];
        }
    } else {
        carModel = NSLocalizedString(@"Model not set", nil);
    }

    _carModelLabel.text = carModel;
    _carNumberLabel.text = [_user.car.number isValidString] ? _user.car.number : NSLocalizedString(@"Number not set", nil);
    _carColorLabel.text = [_user.car.color isValidString] ? _user.car.color : NSLocalizedString(@"Color not set", nil);
    _carYearLabel.text = [_user.car.year integerValue] > 0 ? [_user.car.year stringValue] : NSLocalizedString(@"Not specified", nil);
}

#pragma mark - UICollectionView delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger result = 0;
    if ([collectionView isEqual:_friendsCollectionView]) {
        result = [_friends count];
    } else {
        result = [_interests count];
    }
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isFriendsCollection = [collectionView isEqual:_friendsCollectionView];
    
    NSString *reuseID = isFriendsCollection ? kFriendsCellReuseID : kInterestsCellReuseID;
    BTWProfileFriendsCell *cell = (BTWProfileFriendsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseID forIndexPath:indexPath];
    
    NSDictionary *object = isFriendsCollection ? _friends[indexPath.row] : _interests[indexPath.row];
    NSURL *imageURL = nil;
    NSString *title = [object valueForKey:@"name"];
    if ([collectionView isEqual:_friendsCollectionView]) {
        imageURL = [object valueForKeyPath:@"photo.avatarMid_160x160"];
    } else {
        if ([_user.facebook_id isValidString]) {
            imageURL = [[BTWFacebookService sharedInstance] imageURLForGraphObject:(FBGraphObject *)object];
        } else if ([_user.vkontakte_id isValidString]) {
            imageURL = object[@"photo_100"];
        }
    }
    
    cell.titleLabel.text = title;
    [cell.imageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar_placeholder_male_mid"]];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([collectionView isEqual:_friendsCollectionView]) {
        NSDictionary *object = _friends[indexPath.row];
        NSString *userID = object[@"id"];
        if ([userID isValidString]) {
            BTWUserProfileViewController *profileController = (BTWUserProfileViewController *)[[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeUserProfile];
            profileController.socialNetworkID = userID;
            [self.navigationController pushViewController:profileController animated:YES];
        }
    }
}

@end
