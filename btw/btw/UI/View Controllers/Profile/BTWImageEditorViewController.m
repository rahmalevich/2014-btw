//
//  BTWImageEditorViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 30.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWImageEditorViewController.h"

#import "CropView.h"

@interface BTWImageEditorViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet CropView *cropView;

@property (nonatomic, strong, readwrite) UIImage *image;
@end

@implementation BTWImageEditorViewController

#pragma mark - Initialization
- (id)initWithImage:(UIImage *)image
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.image = image;
    }
    return self;
}

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(actionDone:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil) style:UIBarButtonItemStylePlain target:self action:@selector(actionCancel:)];
    self.title = NSLocalizedString(@"Select area", nil);
    
    _photoImageView.image = _image;
}

- (void)actionDone:(id)sender
{
    CGRect visibleImageRect;
    
    CGFloat imageWidth = _image.size.width;
    CGFloat imageHeight = _image.size.height;
    CGFloat imageAspect = imageWidth / imageHeight;
    CGFloat imageViewAspect = _photoImageView.frameWidth / _photoImageView.frameHeight;
    CGFloat scale;
    if (imageAspect > imageViewAspect) {
        scale = imageHeight / _photoImageView.frameHeight;
        visibleImageRect.size.width = imageHeight * imageViewAspect;
        visibleImageRect.size.height = imageHeight;
        visibleImageRect.origin.x = (imageWidth - visibleImageRect.size.width)/2;
        visibleImageRect.origin.y = 0.0f;
    } else {
        scale = imageWidth / _photoImageView.frameWidth;
        visibleImageRect.size.width = imageWidth;
        visibleImageRect.size.height = imageWidth / imageViewAspect;
        visibleImageRect.origin.x = 0.0;
        visibleImageRect.origin.y = (imageHeight - visibleImageRect.size.height)/2;
    }
    
    CGRect cropRect = [_cropView currentCropArea];
    cropRect.origin.x = floorf(visibleImageRect.origin.x + cropRect.origin.x * scale);
    cropRect.origin.y = floorf(visibleImageRect.origin.y + cropRect.origin.y * scale);
    cropRect.size.width = floorf(cropRect.size.width * scale);
    cropRect.size.height = floorf(cropRect.size.height * scale);
    
    UIGraphicsBeginImageContextWithOptions(cropRect.size, NO, 1);
    [_image drawAtPoint:(CGPoint){-cropRect.origin.x, -cropRect.origin.y}];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [_delegate imageEditor:self didFinishWithOutputImage:croppedImage];
}

- (void)actionCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
