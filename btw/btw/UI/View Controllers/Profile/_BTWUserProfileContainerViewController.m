//
//  BTWUserProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWUserProfileContainerViewController.h"
#import "BTWUserProfileAboutViewController.h"
#import "BTWUserProfileDetailsViewController.h"
#import "BTWUserProfileGalleryViewController.h"
#import "BTWSwipeBetweenViewControllers.h"
#import "BTWTransitionsManager.h"

@interface BTWUserProfileContainerViewController ()
@property (nonatomic, strong) BTWSwipeBetweenViewControllers *swipeControllersContainer;
@end


@implementation BTWUserProfileContainerViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    BTWUserProfileAboutViewController *aboutController = [[BTWUserProfileAboutViewController alloc] initWithNibName:nil bundle:nil];
    BTWUserProfileGalleryViewController *galleryContainer = [[BTWUserProfileGalleryViewController alloc] initWithNibName:@"BTWProfileGalleryViewController" bundle:nil];
    BTWUserProfileDetailsViewController *detailsController = [[BTWUserProfileDetailsViewController alloc] initWithNibName:@"BTWProfileDetailsViewController" bundle:nil];
    
    [aboutController setUser:_user];
    [galleryContainer setUser:_user];
    [detailsController setUser:_user];
    
    self.swipeControllersContainer = [BTWSwipeBetweenViewControllers new];
    _swipeControllersContainer.viewControllerArray = [@[aboutController, galleryContainer, detailsController] mutableCopy];
}

- (void)setUser:(User *)user
{
    if (![user isEqual:_user]) {
        _user = user;
        
        BTWUserProfileAboutViewController *aboutController = _swipeControllersContainer.viewControllerArray[0];
        BTWUserProfileGalleryViewController *galleryController = _swipeControllersContainer.viewControllerArray[1];
        BTWUserProfileDetailsViewController *detailsController = _swipeControllersContainer.viewControllerArray[2];
        [aboutController setUser:_user];
        [galleryController setUser:_user];
        [detailsController setUser:_user];
    }
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = _user.fullname;
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    _swipeControllersContainer.view.frame = self.view.bounds;
    _swipeControllersContainer.segmentedControl.sectionTitles = @[NSLocalizedString(@"About", nil), NSLocalizedString(@"Gallery", nil), NSLocalizedString(@"Details", nil)];
    
    [self.view addSubview:_swipeControllersContainer.view];
    [self addChildViewController:_swipeControllersContainer];
    [_swipeControllersContainer didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

@end
