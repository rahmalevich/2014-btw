//
//  BTWProfileQuestionsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileQuizViewController.h"
#import "BTWProfileQuestionContentView.h"
#import "BTWQuizDataController.h"
#import "BTWQuestionsService.h"

#import "pop.h"

static CGFloat const kStaticHeight = 390.0f;

@interface BTWProfileQuizViewController () <BTWDataControllerDelegate>
@property (nonatomic, strong) BTWQuizDataController *dataController;
@property (strong, nonatomic) BTWProfileQuestionContentView *currentQuestionView;
@property (strong, nonatomic) BTWProfileQuestionContentView *nextQuestionView;
@property (nonatomic, assign) BOOL animationInProgress;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation BTWProfileQuizViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.dataController = [[BTWQuizDataController alloc] initWithDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    self.view.frameHeight = kStaticHeight;
    
    __weak typeof(self) wself = self;
    void (^previousQuestionBlock)(void) = ^{
        [wself.dataController previousQuestion];
        [wself animateQuestionSwitchToLeft:NO];
    };
    void (^nextQuestionBlock)(void) = ^{
        [wself.dataController nextQuestion];
        [wself animateQuestionSwitchToLeft:YES];
    };
    void (^restartQuestionnaireBlock)(void) = ^{
        [wself.dataController restartQuestionnaire];
        [wself animateQuestionSwitchToLeft:NO];
    };
    void (^answerSelectedBlock)(Answer *) = ^(Answer *answer){
        [wself.dataController chooseAnswer:answer];
        [wself.dataController nextQuestion];
        [wself animateQuestionSwitchToLeft:YES];
    };
    
    self.currentQuestionView = [[[NSBundle mainBundle] loadNibNamed:@"BTWProfileQuestionContentView" owner:self options:nil] firstObject];
    _currentQuestionView.previosQuestionBlock = previousQuestionBlock;
    _currentQuestionView.nextQuestionBlock = nextQuestionBlock;
    _currentQuestionView.restartQuestionnaireBlock = restartQuestionnaireBlock;
    _currentQuestionView.answerSelectedBlock = answerSelectedBlock;
    
    self.nextQuestionView = [[[NSBundle mainBundle] loadNibNamed:@"BTWProfileQuestionContentView" owner:self options:nil] firstObject];
    _nextQuestionView.previosQuestionBlock = previousQuestionBlock;
    _nextQuestionView.nextQuestionBlock = nextQuestionBlock;
    _nextQuestionView.restartQuestionnaireBlock = restartQuestionnaireBlock;
    _nextQuestionView.answerSelectedBlock = answerSelectedBlock;
    
    [self setupContent];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsServiceDidUpdateQuestions:) name:kQuestionsServiceDidUpdateQuestions object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsServiceDidFailToUpdateQuestions:) name:kQuestionsServiceDidFailToUpdateQuestions object:nil];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    _currentQuestionView.frame = [self defaultQuestionViewFrame];
}

- (void)setupContent
{
    if ([BTWQuestionsService sharedInstance].questionsAreLoading) {
        [_activityIndicator startAnimating];
        if (_contentView.alpha != 0.0f || _placeholderLabel.alpha != 0.0f) {
            [UIView animateWithDuration:0.3 animations:^{
                _contentView.alpha = 0.0f;
                _placeholderLabel.alpha = 0.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        if ([_dataController numberOfQuestions] > 0) {
            [_currentQuestionView setupWithQuestion:_dataController.currentQuestion atIndex:[_dataController currentQuestionIndex] totalCount:[_dataController numberOfQuestions]];
            [_contentView addSubview:_currentQuestionView];
            [UIView animateWithDuration:0.3 animations:^{
                _contentView.alpha = 1.0f;
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [UIView animateWithDuration:0.3 animations:^{
                _contentView.alpha = 0.0f;
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    }
}

- (CGRect)defaultQuestionViewFrame
{
//    return CGRectMake(0, kContentViewTopOffset, self.view.boundsWidth, self.view.boundsHeight - kContentViewTopOffset);
//  Временно убрал инсет, так как пока используется в профилях
    return self.view.bounds;
}

- (void)animateQuestionSwitchToLeft:(BOOL)toLeft
{
    if (_animationInProgress) {
        return;
    }
    self.animationInProgress = YES;
    
    NSInteger multiplier = toLeft ? 1 : -1;
    
    [_nextQuestionView setupWithQuestion:_dataController.currentQuestion atIndex:_dataController.currentQuestionIndex totalCount:_dataController.numberOfQuestions];
    _nextQuestionView.frame = CGRectOffset([self defaultQuestionViewFrame], multiplier * _contentView.boundsWidth, 0);
    [_contentView addSubview:_nextQuestionView];
    
    POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewFrame];
    animation.toValue = [NSValue valueWithCGRect:CGRectOffset(_contentView.frame, multiplier * -_contentView.boundsWidth, 0)];
    animation.springSpeed = 20.0f;
    animation.removedOnCompletion = YES;
    
    __weak typeof(self) wself = self;
    animation.completionBlock = ^(POPAnimation *oldAnimation, BOOL finished){
        BTWProfileQuestionContentView *nextQuestionView = wself.nextQuestionView;
        nextQuestionView.frame = [wself defaultQuestionViewFrame];
        wself.nextQuestionView = wself.currentQuestionView;
        wself.currentQuestionView = nextQuestionView;
        wself.contentView.frame = CGRectOffset(_contentView.frame, multiplier * _contentView.boundsWidth, 0);
        wself.animationInProgress = NO;
    };
    
    [_contentView pop_addAnimation:animation forKey:@"translation"];
}

- (void)prepareForParentController
{
    self.view.frameHeight = kStaticHeight;
}

#pragma mark - Questions service notifications
- (void)questionsServiceDidUpdateQuestions:(NSNotification *)notification
{
    [self setupContent];
}

- (void)questionsServiceDidFailToUpdateQuestions:(NSNotification *)notification
{
    [self setupContent];
}

#pragma mark - BTWDataController delegate
- (void)dataController:(BTWDataController *)controller didEndLoadingDataWithError:(NSError *)error
{
    [self setupContent];
}

@end
