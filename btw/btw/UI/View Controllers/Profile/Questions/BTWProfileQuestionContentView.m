//
//  BTWProfileQuestionContentView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileQuestionContentView.h"
#import "BTWUserService.h"

static CGFloat const kButtonsOffset = 5.0f;

@interface BTWProfileQuestionContentView ()

@property (nonatomic, strong, readwrite) Question *question;
@property (nonatomic, strong) NSArray *answerButtons;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewHeight;
@property (weak, nonatomic) IBOutlet UIView *completeView;
@property (weak, nonatomic) IBOutlet UIView *questionView;
@property (weak, nonatomic) IBOutlet UIView *questionHeaderView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *prevButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation BTWProfileQuestionContentView

#pragma mark - Initialization
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _questionHeaderView.layer.cornerRadius = 10.0f;
    _questionHeaderView.layer.borderWidth = 1.0f;
    _questionHeaderView.layer.borderColor = [BTWStylesheet commonGrayColor].CGColor;
}

#pragma mark - Setup
- (void)setupWithQuestion:(Question *)question atIndex:(NSUInteger)index totalCount:(NSUInteger)count
{
    self.question = question;
    
    if (question) {
        _questionView.hidden = NO;
        _completeView.hidden = YES;
        
        _progressLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Question %ld from %ld", nil), (long)index + 1, (long)count];
        _titleLabel.text = question.text;
        _prevButton.hidden = (index == 0);
        _nextButton.hidden = ![[BTWUserService sharedInstance].authorizedUser.answers intersectsSet:[_question.answers set]];
        
        for (UIView *subview in _buttonsView.subviews) {
            [subview removeFromSuperview];
        }
        
        NSMutableArray *answerButtons = [NSMutableArray array];
        CGFloat currentOffset = 0.0f;
        for (NSInteger i = 0; i < [_question.answers count]; i++) {
            Answer *answer = _question.answers[i];

            UIButton *answerButton = [UIButton buttonWithType:UIButtonTypeCustom];
            answerButton.tag = i;
            [answerButton addTarget:self action:@selector(actionAnswerSelected:) forControlEvents:UIControlEventTouchUpInside];
            [answerButton setBackgroundImage:[UIImage imageNamed:@"btn_profile_gray"] forState:UIControlStateNormal];
            [answerButton setBackgroundImage:[UIImage imageNamed:@"btn_profile_orange"] forState:UIControlStateHighlighted];
            [answerButton setBackgroundImage:[UIImage imageNamed:@"btn_profile_orange"] forState:UIControlStateSelected];
            [answerButton setTitle:answer.text forState:UIControlStateNormal];
            [answerButton.titleLabel setFont:[BTWStylesheet commonFontOfSize:14.0]];
            [answerButton sizeToFit];
            answerButton.x = floorf(_buttonsView.centerX - answerButton.boundsWidth/2);
            answerButton.y = currentOffset + (i > 0 ? kButtonsOffset : 0.0);
            answerButton.selected = [[BTWUserService sharedInstance].authorizedUser.answers containsObject:answer];
            [answerButtons addObject:answerButton];
            [_buttonsView addSubview:answerButton];
            
            currentOffset = CGRectGetMaxY(answerButton.frame);
        }
        self.answerButtons = [NSArray arrayWithArray:answerButtons];
        
        _buttonsViewHeight.constant = currentOffset;
        _contentViewHeight.constant = MAX(self.frameHeight, _bottomView.frameHeight + _buttonsViewHeight.constant + _topView.frameHeight);
        [self setNeedsLayout];
    } else {
        _questionView.hidden = YES;
        _completeView.hidden = NO;
    }
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    if (_question) {
        _contentViewHeight.constant = MAX(self.frameHeight, _bottomView.frameHeight + _buttonsViewHeight.constant + _topView.frameHeight);
        [self setNeedsLayout];
    }
}

#pragma mark - Actions
- (IBAction)actionAnswerSelected:(UIButton *)sender
{
    for (UIButton *button in _answerButtons) {
        button.selected = NO;
    }
    sender.selected = YES;
    
    if (_answerSelectedBlock) {
        Answer *answer = _question.answers[sender.tag];
        _answerSelectedBlock(answer);
    }
}

- (IBAction)actionLeft:(UIButton *)sender
{
    if (_previosQuestionBlock) {
        _previosQuestionBlock();
    }
}

- (IBAction)actionRight:(UIButton *)sender
{
    if (_nextQuestionBlock) {
        _nextQuestionBlock();
    }
}

- (IBAction)actionRestartQuestionnaire:(UIButton *)sender
{
    if (_restartQuestionnaireBlock) {
        _restartQuestionnaireBlock();
    }
}

@end
