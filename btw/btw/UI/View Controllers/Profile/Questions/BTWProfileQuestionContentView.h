//
//  BTWProfileQuestionContentView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWProfileQuestionContentView : UIView

@property (nonatomic, strong, readonly) Question *question;
@property (nonatomic, copy) void (^answerSelectedBlock)(Answer *);
@property (nonatomic, copy) BTWVoidBlock previosQuestionBlock;
@property (nonatomic, copy) BTWVoidBlock nextQuestionBlock;
@property (nonatomic, copy) BTWVoidBlock restartQuestionnaireBlock;

- (void)setupWithQuestion:(Question *)question atIndex:(NSUInteger)index totalCount:(NSUInteger)count;

@end
