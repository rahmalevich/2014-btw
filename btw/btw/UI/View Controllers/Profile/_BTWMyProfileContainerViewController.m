//
//  BTWProfileContainerViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMyProfileContainerViewController.h"
#import "BTWMyProfileAboutViewController.h"
#import "BTWMyProfileDetailsViewController.h"
#import "BTWMyProfileGalleryViewController.h"
#import "BTWSwipeBetweenViewControllers.h"
#import "BTWTransitionsManager.h"
#import "BTWQuestionsService.h"

@interface BTWMyProfileContainerViewController ()
@property (nonatomic, strong) BTWSwipeBetweenViewControllers *swipeControllersContainer;
@end

@implementation BTWMyProfileContainerViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    BTWMyProfileAboutViewController *aboutController = (BTWMyProfileAboutViewController *)[[BTWTransitionsManager sharedInstance] viewControllerWithIdentifier:@"myProfileAboutViewController"];
    aboutController.containerController = self;
    
    self.swipeControllersContainer = [BTWSwipeBetweenViewControllers new];
    _swipeControllersContainer.viewControllerArray = [@[
                                                        aboutController,
                                                        [[BTWMyProfileGalleryViewController alloc] initWithNibName:@"BTWProfileGalleryViewController" bundle:nil],
                                                        [[BTWMyProfileDetailsViewController alloc] initWithNibName:@"BTWProfileDetailsViewController" bundle:nil],
                                                        [[BTWTransitionsManager sharedInstance] viewControllerWithIdentifier:@"profileQuestionsViewController"]] mutableCopy];
    
    _swipeControllersContainer.segmentedControl.font = [BTWStylesheet segmentedControlFontOfSize:14.0];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Profile", nil);
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    _swipeControllersContainer.view.frame = self.view.bounds;
    _swipeControllersContainer.segmentedControl.sectionTitles = @[NSLocalizedString(@"About", nil), NSLocalizedString(@"Gallery", nil), NSLocalizedString(@"Details", nil), NSLocalizedString(@"Questions", nil)];
    
    [self.view addSubview:_swipeControllersContainer.view];
    [self addChildViewController:_swipeControllersContainer];
    [_swipeControllersContainer didMoveToParentViewController:self];
}

@end
