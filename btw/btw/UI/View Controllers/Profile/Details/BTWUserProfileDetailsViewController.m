//
//  BTWUserProfileDetailsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWUserProfileDetailsViewController.h"

@interface BTWProfileDetailsViewController (Protected)
@property (nonatomic, strong, readwrite) User *user;
@end

@interface BTWUserProfileDetailsViewController ()

@end

@implementation BTWUserProfileDetailsViewController

#pragma mark - Lifecycle
- (void)setUser:(User *)user
{
    [super setUser:user];
}

@end
