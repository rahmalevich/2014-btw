//
//  BTWUserProfileDetailsViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileDetailsViewController.h"

@interface BTWUserProfileDetailsViewController : BTWProfileDetailsViewController

- (void)setUser:(User *)user;

@end
