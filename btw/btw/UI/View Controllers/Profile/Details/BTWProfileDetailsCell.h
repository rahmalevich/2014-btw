//
//  BTWProfileDetailsCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTWProfileDetailsCell : UITableViewCell

@property (weak, nonatomic, readonly) UIView *separatorView;
@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) UILabel *subtitleLabel;

+ (CGFloat)cellHeight;

@end
