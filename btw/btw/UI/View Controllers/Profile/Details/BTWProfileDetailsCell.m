//
//  BTWProfileDetailsCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileDetailsCell.h"

@interface BTWProfileDetailsCell ()
@property (weak, nonatomic, readwrite) IBOutlet UIView *separatorView;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *subtitleLabel;
@end

@implementation BTWProfileDetailsCell

+ (CGFloat)cellHeight
{
    return 45.0f;
}

@end
