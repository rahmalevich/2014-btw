//
//  BTWProfileDetailsViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWProfileViewController.h"

@interface BTWProfileDetailsViewController : BTWViewController <BTWProfileChildViewController>

@property (weak, nonatomic, readonly) UITableView *tableView;
@property (strong, nonatomic, readonly) User *user;

@end
