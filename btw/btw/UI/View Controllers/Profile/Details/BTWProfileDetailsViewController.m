//
//  BTWProfileDetailsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileDetailsViewController.h"
#import "BTWUserService.h"
#import "BTWQuestionsService.h"
#import "BTWProfileDetailsCell.h"
#import "BTWUIUtils.h"

static CGFloat const kFooterHeight = 20.0f;
static NSString * const kDetailCellID = @"detailCellID";

@interface BTWProfileDetailsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) NSArray *tableModel;
@property (strong, nonatomic, readwrite) User *user;
@property (nonatomic, assign) BOOL fullProfileIsLoading;
@end

@implementation BTWProfileDetailsViewController
@synthesize profileViewController;

#pragma mark - Initialization & Memory managment
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"BTWProfileDetailsCell" bundle:nil] forCellReuseIdentifier:kDetailCellID];
    self.tableView.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    UIView *footerView = [UIView viewWithFrame:CGRectMake(0, 0, self.view.boundsWidth, kFooterHeight)];
    footerView.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = footerView;
    
    [self setupContent];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsServiceDidUpdateQuestions:) name:kQuestionsServiceDidUpdateQuestions object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsServiceDidFailToUpdateQuestions:) name:kQuestionsServiceDidFailToUpdateQuestions object:nil];
}

- (void)setUser:(User *)user
{
    if (![user isEqual:_user]) {
        _user = user;
        
        if (_user) {
            if (_user.quiz) {
                [self setupContent];
            } else {
                __weak typeof(self) wself = self;
                self.fullProfileIsLoading = YES;
                [[BTWUserService sharedInstance] getFullProfileForUser:_user withCompletionHander:^(NSError *error){
                    wself.fullProfileIsLoading = NO;
                    [wself setupContent];
                }];
            }
        }
    }
}

- (void)setupContent
{
    if ([BTWQuestionsService sharedInstance].questionsAreLoading || _fullProfileIsLoading) {
        [_activityIndicator startAnimating];
        if (_placeholderLabel.alpha != 0.0f || _tableView.alpha != 0.0f) {
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
                _tableView.alpha = 0.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        self.tableModel = [Question MR_findAllSortedBy:@"sort_order" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"category = %@", kDetailsCategoryKey]];
        if ([_tableModel count] > 0) {
            [_tableView reloadData];
            [UIView animateWithDuration:0.3 animations:^{
                _tableView.alpha = 1.0f;
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [UIView animateWithDuration:0.3 animations:^{
                _tableView.alpha = 0.0f;
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    }
}

#pragma mark - Utils
- (void)prepareForParentController {
    [self.view layoutIfNeeded];
    self.view.frameHeight = self.tableView.contentSize.height;
    self.tableView.scrollEnabled = NO;
    self.view.origin = CGPointZero;
}

#pragma mark - Questions service notifications
- (void)questionsServiceDidUpdateQuestions:(NSNotification *)notification
{
    [self setupContent];
}

- (void)questionsServiceDidFailToUpdateQuestions:(NSNotification *)notification
{
    [self setupContent];
}

#pragma mark - UITableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableModel count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BTWProfileDetailsCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Question *question = _tableModel[indexPath.row];
    
    NSMutableSet *userAnswers = [_user.answers mutableCopy];
    [userAnswers filterUsingPredicate:[NSPredicate predicateWithFormat:@"question.backend_id = %@", question.backend_id]];
    Answer *answer = [userAnswers anyObject];
    
    BTWProfileDetailsCell *cell = (BTWProfileDetailsCell *)[tableView dequeueReusableCellWithIdentifier:kDetailCellID forIndexPath:indexPath];
    cell.titleLabel.text = question.text;
    cell.subtitleLabel.text = answer ? answer.text : NSLocalizedString(@"Not selected", nil);
    return cell;
}

@end
