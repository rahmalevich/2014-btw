//
//  BTWMyProfileDetailsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMyProfileDetailsViewController.h"
#import "BTWUserService.h"
#import "BTWQuestionsService.h"
#import "BTWDropDownCell.h"

#import "PopoverButton.h"

static NSString * const kDropDownCellID = @"dropDownCellID";

@interface BTWProfileDetailsViewController (Protected) <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSArray *tableModel;
@property (strong, nonatomic, readwrite) User *user;
@end

@interface BTWMyProfileDetailsViewController () <PopoverButtonDelegate>
@property (nonatomic, strong) Question *currentQuestion;
@end

@implementation BTWMyProfileDetailsViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"BTWDropDownCell" bundle:nil] forCellReuseIdentifier:kDropDownCellID];
    self.user = [BTWUserService sharedInstance].authorizedUser;
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BTWDropDownCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Question *question = self.tableModel[indexPath.row];
    self.currentQuestion = question;
    
    NSMutableSet *userAnswers = [self.user.answers mutableCopy];
    [userAnswers intersectSet:[question.answers set]];
    Answer *answer = [userAnswers anyObject];
    
    BTWDropDownCell *cell = (BTWDropDownCell *)[tableView dequeueReusableCellWithIdentifier:kDropDownCellID forIndexPath:indexPath];
    cell.titleLabel.text = question.text;
    cell.popoverButton.emptyValuePlaceholder = NSLocalizedString(@"Not selected", nil);
    cell.popoverButton.itemsArray = [question.answers array];
    cell.popoverButton.titleBlock = ^(Answer *item){ return item.text; };
    cell.popoverButton.selectedItem = answer;
    cell.popoverButton.delegate = self;
    cell.popoverButton.addEmptyValue = YES;
    cell.backgroundColor = [BTWStylesheet commonBackgroundColor];
    return cell;
}

#pragma mark - Popover delegate
- (void)popoverButton:(PopoverButton *)popoverButton didSelectItem:(Answer *)answer
{
    NSMutableSet *oldAnswers = [self.user.answers mutableCopy];
    [oldAnswers intersectSet:[answer.question.answers set]];
    [self.user removeAnswers:oldAnswers];
    if (answer) {
        [self.user addAnswersObject:answer];
    }
    [self.user.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    [[BTWQuestionsService sharedInstance] submitAnswers];
}

@end
