//
//  BTWImageEditorViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 30.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"

@class BTWImageEditorViewController;
@protocol BTWImageEditorViewControllerDelegate <NSObject>
- (void)imageEditor:(BTWImageEditorViewController *)controller didFinishWithOutputImage:(UIImage *)outputImage;
@end

@interface BTWImageEditorViewController : BTWViewController

@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, weak) id<BTWImageEditorViewControllerDelegate> delegate;

- (id)initWithImage:(UIImage *)image;

@end
