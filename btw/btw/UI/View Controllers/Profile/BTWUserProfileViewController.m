//
//  BTWUserProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWUserProfileViewController.h"
#import "BTWUserProfileAboutViewController.h"
#import "BTWUserProfileDetailsViewController.h"
#import "BTWUserProfileGalleryViewController.h"
#import "BTWProfileFeedbacksViewController.h"
#import "BTwProfileAboutView.h"
#import "BTWUserService.h"

#import "BTWTransitionsManager.h"

@interface BTWProfileViewController (Protected) <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) NSArray *controllersArray;
@property (nonatomic, strong) UIView *headerView;
@end

@interface BTWUserProfileAboutViewController (Protected)
@property (weak, nonatomic) UILabel *placeholderLabel;
@property (weak, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@interface BTWUserProfileViewController ()
@property (nonatomic, assign) BOOL isLoadingUserData;
@end

@implementation BTWUserProfileViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.controllersArray = @[ [[BTWUserProfileAboutViewController alloc] initWithNibName:nil bundle:nil],
                               [[BTWUserProfileGalleryViewController alloc] initWithNibName:@"BTWProfileGalleryViewController" bundle:nil],
                               [[BTWUserProfileDetailsViewController alloc] initWithNibName:@"BTWProfileDetailsViewController" bundle:nil],
                               [[BTWProfileFeedbacksViewController alloc] initWithNibName:nil bundle:nil] ];
    
    // segmented control
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"About", nil) atIndex:0 animated:NO];
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"Gallery", nil) atIndex:1 animated:NO];
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"Details", nil) atIndex:2 animated:NO];
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"Profile: Feedbacks", nil) atIndex:3 animated:NO];
    self.segmentedControl.selectedSegmentIndex = 0;
        
    [self setupWithSocialNetworkID:_socialNetworkID];
}

- (void)setupWithSocialNetworkID:(NSString *)socialNetworkID
{
    if ([socialNetworkID isValidString]){
        
        // переводим интерфейс в режим загрузки данных
        self.isLoadingUserData = YES;
        [self.tableView reloadData];
        
        __weak typeof(self) wself = self;
        [[BTWUserService sharedInstance] getUserFromSocialNetwork:[[BTWUserService sharedInstance].authorizedUser linkedSocialNetwork] byUserID:_socialNetworkID completionHandler:^(NSDictionary *response, NSError *error)
        {
            if (!error) {
                User *user = [User MR_importFromObject:response[@"payload"]];
                wself.user = user;
            }
            
            wself.isLoadingUserData = NO;
            
            if (!wself.user) {
                BTWUserProfileAboutViewController *aboutController = wself.controllersArray[0];
                [aboutController.activityIndicator stopAnimating];
                aboutController.placeholderLabel.text = NSLocalizedString(@"Data loading error", nil);
            } else {
                wself.headerView.alpha = 0.0f;
                [UIView animateWithDuration:0.15f animations:^{
                    wself.headerView.alpha = 1.0f;
                }];
                
                [wself.tableView beginUpdates];
                [wself.tableView endUpdates];
            }
        }];
    }
}

- (void)setSocialNetworkID:(NSString *)socialNetworkID
{
    _socialNetworkID = [socialNetworkID copy];
    
    if ([self isViewLoaded]) {
        [self setupWithSocialNetworkID:_socialNetworkID];
    }
}

- (void)setUser:(User *)user
{
    [super setUser:user];
    
    self.title = self.user.fullname;
    
    ((BTWUserProfileAboutViewController *)self.controllersArray[0]).user = user;
    ((BTWUserProfileGalleryViewController *)self.controllersArray[1]).user = user;
    ((BTWUserProfileDetailsViewController *)self.controllersArray[2]).user = user;
    ((BTWProfileFeedbacksViewController *)self.controllersArray[3]).user = user;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (_isLoadingUserData || !self.user) {
        return 0.0f;
    } else {
        return [super tableView:tableView heightForHeaderInSection:section];
    }
}

@end