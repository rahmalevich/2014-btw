//
//  BTWUserProfileViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWUserProfileContainerViewController : BTWViewController

@property (nonatomic, strong) User *user;

@end
