//
//  BTWMyProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMyProfileViewController.h"
#import "BTWMyProfileAboutViewController.h"
#import "BTWMyProfileGalleryViewController.h"
#import "BTWMyProfileDetailsViewController.h"
#import "BTWMyProfileEditViewController.h"
#import "BTWMyProfileEditCarViewController.h"
#import "BTWProfileFeedbacksViewController.h"
#import "BTWProfileAboutView.h"

#import "BTWTransitionsManager.h"
#import "BTWUserService.h"

@interface BTWProfileViewController (Protected)
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) NSArray *controllersArray;
@property (nonatomic, strong) UIView *headerView;

- (void)onSegmentedControlChange:(UISegmentedControl *)control;
- (void)swapCurrentChildViewControllerWith:(BTWViewController<BTWProfileChildViewController> *)viewController;
@end

@interface BTWMyProfileViewController () <UITableViewDelegate>
@property (nonatomic, assign) BOOL editState;
@property (nonatomic, assign) BOOL carEditState;
@property (nonatomic, strong) BTWMyProfileEditViewController *editViewController;
@property (nonatomic, strong) BTWMyProfileEditCarViewController *editCarViewController;
@end

@implementation BTWMyProfileViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Profile", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_edit"] style:UIBarButtonItemStylePlain target:self action:@selector(actionEdit:)];
    
    self.user = [BTWUserService sharedInstance].authorizedUser;
    
    BTWProfileFeedbacksViewController *feedbacksViewController = [[BTWProfileFeedbacksViewController alloc] initWithNibName:nil bundle:nil];
    feedbacksViewController.user = self.user;
    
    BTWMyProfileAboutViewController *aboutViewController = [[BTWMyProfileAboutViewController alloc] initWithNibName:nil bundle:nil];
    aboutViewController.profileViewController = self;
    
    self.controllersArray = @[aboutViewController,
                              [[BTWMyProfileGalleryViewController alloc] initWithNibName:@"BTWProfileGalleryViewController" bundle:nil],
                              [[BTWMyProfileDetailsViewController alloc] initWithNibName:@"BTWProfileDetailsViewController" bundle:nil],
                              feedbacksViewController];

    // segmented control
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"About", nil) atIndex:0 animated:NO];
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"Gallery", nil) atIndex:1 animated:NO];
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"Details", nil) atIndex:2 animated:NO];
    [self.segmentedControl insertSegmentWithTitle:NSLocalizedString(@"Profile: Feedbacks", nil) atIndex:3 animated:NO];
    self.segmentedControl.selectedSegmentIndex = 0;
    
    // edit controller
    self.editViewController = [[BTWMyProfileEditViewController alloc] initWithNibName:nil bundle:nil];
    __weak typeof(self) wself = self;
    _editViewController.saveBlock = ^{
        [wself finishEditing:nil];
    };
    
    // edit car controller
    self.editCarViewController = [[BTWMyProfileEditCarViewController alloc] initWithNibName:nil bundle:nil];
    _editCarViewController.doneBlock = ^{
        [wself finishEditing:nil];
    };
}

- (void)actionEdit:(id)sender
{
    self.carEditState = NO;
    self.editState = !_editState;
    [self setEditState:_editState forCar:NO];
}

- (void)actionEditCar
{
    self.editState = NO;
    self.carEditState = !_carEditState;
    [self setEditState:_carEditState forCar:YES];
}

- (void)finishEditing:(id)sender
{
    if (_editState) {
        self.editState = NO;
    }
    if (_carEditState) {
        self.carEditState = NO;
    }
    [self setEditState:NO forCar:NO];
}

- (void)setEditState:(BOOL)editState forCar:(BOOL)forCar
{
    if (editState) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(finishEditing:)];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_edit"] style:UIBarButtonItemStylePlain target:self action:@selector(actionEdit:)];
    }
    
    BTWProfileAboutView *aboutView = (BTWProfileAboutView *)self.tableView.tableHeaderView;
    aboutView.editState = forCar ? NO : editState;
    
    BTWViewController<BTWProfileChildViewController> *childToSwap = nil;
    if (editState) {
        if (forCar) {
            childToSwap = _editCarViewController;
        } else {
            _editViewController.user = self.user;
            childToSwap = _editViewController;
        }
    } else {
        if ([_editViewController updateUserValues]) {
            [self.user.managedObjectContext MR_saveToPersistentStoreAndWait];
            [[BTWUserService sharedInstance] saveProfileWithCompletionHandler:nil];
            [aboutView updateProfileFields];
        }
        childToSwap = self.controllersArray[self.segmentedControl.selectedSegmentIndex];
    }
    [self swapCurrentChildViewControllerWith:childToSwap];
    
    [UIView animateWithDuration:0.15f animations:^{
        self.headerView.alpha = editState ? 0.0f : 1.0f;
    }];
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}

#pragma mark - UITableView
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return _editState || _carEditState ? 0.0f : [super tableView:tableView heightForHeaderInSection:section];
}

@end
