//
//  BTWProfileChildViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

@protocol BTWProfileChildViewController <NSObject>

+ (CGFloat)contentHeight;

@end

