//
//  BTWUserProfileViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWProfileViewController.h"

@interface BTWUserProfileViewController : BTWProfileViewController

@property (nonatomic, copy) NSString *socialNetworkID;

@end
