//
//  BTWGreetingViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 27.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWGreetingViewController.h"
#import "BTWTransitionsManager.h"

static CGFloat kSmallScreenContentHeight = 405.0f;

@interface BTWGreetingViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@end

@implementation BTWGreetingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    _loginButton.layer.cornerRadius = 5.0f;
    [_loginButton setBackgroundImage:[UIImage imageWithColor:_loginButton.backgroundColor] forState:UIControlStateNormal];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if ([BTWUIUtils isSmallScreenDevice]) {
        _contentViewHeight.constant = kSmallScreenContentHeight;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControl.currentPage = (int)((scrollView.contentOffset.x + scrollView.frameWidth/2)/scrollView.frameWidth);
}

- (IBAction)actionLogin:(UIButton *)sender
{
    [[BTWTransitionsManager sharedInstance] showAuthorization];
}

@end
