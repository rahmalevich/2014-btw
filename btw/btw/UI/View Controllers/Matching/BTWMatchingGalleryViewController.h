//
//  BTWMatchingProfileViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWMatchingGalleryViewController : BTWViewController

@property (nonatomic, strong) User *user;

@end
