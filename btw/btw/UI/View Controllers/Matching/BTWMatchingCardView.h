//
//  BTWMatchingCardView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWCardsViewItem.h"

@interface BTWMatchingCardView : UIView <BTWCardsViewItem>

@property (nonatomic, strong) MatchingUser *user;
@property (nonatomic, weak, readonly) UIImageView *photoImageView;

@end
