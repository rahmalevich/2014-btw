//
//  ScaleSegue.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "ScaleSegue.h"
#import "pop.h"

@implementation ScaleSegue

- (void)perform
{
    UIViewController *sourceController = self.sourceViewController;
    UIViewController *destinationController = self.destinationViewController;
    destinationController.view.frameHeight = sourceController.view.frameHeight;
    
    [sourceController.view addSubview:destinationController.view];
    [sourceController addChildViewController:destinationController];
    [destinationController didMoveToParentViewController:sourceController];
    
    destinationController.view.alpha = 0.0;
    [UIView animateWithDuration:0.1 animations:^{
        destinationController.view.alpha = 1.0;
    }];
    
    POPSpringAnimation *scaleXY = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleXY.fromValue = [NSValue valueWithCGPoint:(CGPoint){0.8, 0.8}];
    scaleXY.toValue = [NSValue valueWithCGPoint:(CGPoint){1.0, 1.0}];
    scaleXY.springBounciness = 15.0f;
    scaleXY.springSpeed = 15.0f;
    scaleXY.dynamicsFriction = 25.0f;
    [destinationController.view pop_addAnimation:scaleXY forKey:@"scaleXY"];
}

@end
