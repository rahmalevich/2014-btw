//
//  BTWCardsViewItem.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@protocol BTWCardsViewItem <NSObject>
/**
 @abstract Offset from center of container [-1.0, 1.0]
 */
- (void)itemDidMoveToOffset:(CGFloat)offset;
@end
