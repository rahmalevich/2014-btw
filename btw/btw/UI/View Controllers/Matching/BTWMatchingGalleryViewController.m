//
//  BTWMatchingProfileViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMatchingGalleryViewController.h"
#import "BTWCommonUtils.h"

@interface BTWMatchingGalleryViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation BTWMatchingGalleryViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Совпадения";
    _nameLabel.text = [NSString stringWithFormat:@"%@ %@, %@", _user.name, _user.surname, [BTWCommonUtils ageStringForAge:[_user.age integerValue]]];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
}

#pragma mark - Actions
- (IBAction)actionDecline:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionAccept:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Scroll View delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSInteger pageIndex = floorf((scrollView.contentOffset.x + scrollView.frameWidth * 0.5) / scrollView.frameWidth);
    _pageControl.currentPage = pageIndex;
}

@end
