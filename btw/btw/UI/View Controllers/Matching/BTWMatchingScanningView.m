//
//  BTWMatchingScanningView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMatchingScanningView.h"
#import "BTWUserService.h"

#import "UIImageView+WebCache.h"
#import "pop.h"

@interface BTWMatchingScanningView () <POPAnimationDelegate>
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UIView *restartSearchView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIView *bigCircleView;
@property (weak, nonatomic) IBOutlet UIView *smallCircleView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, assign) BOOL initialized;
@end

@implementation BTWMatchingScanningView

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        // Подписываемся на обновление аватара
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(avatarChanged:) name:kUserAvatarChangedNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!_initialized) {
        _avatarImageView.layer.cornerRadius = floorf(_avatarImageView.boundsWidth/2);
        _avatarImageView.layer.borderColor = [BTWStylesheet commonOrangeColor].CGColor;
        _avatarImageView.layer.borderWidth = 3.0f;
        
        _bigCircleView.layer.cornerRadius = floorf(_bigCircleView.boundsWidth/2);
        _smallCircleView.layer.cornerRadius = floorf(_smallCircleView.boundsWidth/2);
        
        _initialized = YES;
    }

    [self updatePhoto];
}

- (void)setShowRestartScreen:(BOOL)showRestartScreen
{
    if (_showRestartScreen != showRestartScreen) {
        _showRestartScreen = showRestartScreen;
        
        [self.layer removeAllAnimations];
        if (_showRestartScreen) {
            _restartSearchView.hidden = NO;
            _restartSearchView.alpha = 0.0f;
            [UIView animateWithDuration:0.3 animations:^{
                _restartSearchView.alpha = 1.0f;
                _searchView.alpha = 0.0f;
            } completion:^(BOOL finished){
                _searchView.hidden = YES;
            }];
        } else {
            _searchView.hidden = NO;
            _searchView.alpha = 0.0f;
            [UIView animateWithDuration:0.3 animations:^{
                _searchView.alpha = 1.0f;
                _restartSearchView.alpha = 0.0f;
            } completion:^(BOOL finished){
                _restartSearchView.hidden = YES;
            }];
        }
    }
}

- (void)updatePhoto
{
    User *authorizedUser = [BTWUserService sharedInstance].authorizedUser;
    NSURL *photoURL = [NSURL URLWithString:authorizedUser.photo_url];
    [_avatarImageView sd_setImageWithURL:photoURL placeholderImage:[authorizedUser midPlaceholder]];
}

- (void)avatarChanged:(NSNotification *)notification
{
    [self updatePhoto];
}

- (void)startAnimating
{
    CGFloat scale = 1.2;
    CGFloat bounciness = 20.0;
    CGFloat speed = 3.0;
    
    POPSpringAnimation *avatarAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    avatarAnimation.toValue = [NSValue valueWithCGPoint:(CGPoint){scale, scale}];
    avatarAnimation.springBounciness = bounciness;
    avatarAnimation.springSpeed = speed;
    avatarAnimation.autoreverses = YES;
    avatarAnimation.repeatForever = YES;
    [_avatarImageView pop_addAnimation:avatarAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *smallCircleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    smallCircleAnimation.beginTime = CACurrentMediaTime() + 0.2;
    smallCircleAnimation.toValue = [NSValue valueWithCGPoint:(CGPoint){scale, scale}];
    smallCircleAnimation.springBounciness = bounciness;
    smallCircleAnimation.springSpeed = speed;
    smallCircleAnimation.autoreverses = YES;
    smallCircleAnimation.repeatForever = YES;
    [_smallCircleView pop_addAnimation:smallCircleAnimation forKey:@"scaleAnimation"];

    POPSpringAnimation *bigCircleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    bigCircleAnimation.beginTime = CACurrentMediaTime() + 0.4;
    bigCircleAnimation.toValue = [NSValue valueWithCGPoint:(CGPoint){scale, scale}];
    bigCircleAnimation.springBounciness = bounciness;
    bigCircleAnimation.springSpeed = speed;
    bigCircleAnimation.autoreverses = YES;
    bigCircleAnimation.repeatForever = YES;
    [_bigCircleView pop_addAnimation:bigCircleAnimation forKey:@"scaleAnimation"];
    
    _titleLabel.alpha = 0.0f;
    _titleLabel.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        _titleLabel.alpha = 1.0f;
    }];
}

- (void)stopAnimating
{
    _titleLabel.hidden = YES;
    
    [_avatarImageView pop_removeAllAnimations];
    [_smallCircleView pop_removeAllAnimations];
    [_bigCircleView pop_removeAllAnimations];
    
    _avatarImageView.layer.transform = CATransform3DIdentity;
    _smallCircleView.layer.transform = CATransform3DIdentity;
    _bigCircleView.layer.transform = CATransform3DIdentity;
}

- (IBAction)actionRestartSearch:(UIButton *)sender
{
    self.showRestartScreen = NO;
    
    if (_restartSearchBlock) {
        _restartSearchBlock();
    }
}

@end
