//
//  BTWCardsView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWCardsView.h"

#import "pop.h"

static NSUInteger const kVisibleViews = 3;
static CGFloat const kScaleStep = 0.01;
static CGFloat const kVerticalStep = 5.0f;
static CGFloat const kRotationAngle = M_PI / 12;

@interface BTWCardsView ()
@property (nonatomic, strong) NSMutableArray *cardViews;
@property (nonatomic, assign) BOOL animationInProgress;
@end

@implementation BTWCardsView

#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.declineDirection = BTWCardsViewDirectionRight;
    self.cardViews = [NSMutableArray array];
    
    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panRecognized:)];
    [self addGestureRecognizer:panRecognizer];
    
    UITapGestureRecognizer *tapRecognzier = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognized:)];
    [self addGestureRecognizer:tapRecognzier];
    
}

#pragma mark - Actions
- (UIView<BTWCardsViewItem> *)topCard
{
    return [_cardViews firstObject];
}

- (void)makeActionWithTopCard:(BTWCardsViewAction)action
{
    UIView<BTWCardsViewItem> *topCard = [self topCard];
    if (topCard && !_animationInProgress) {
        
        __weak typeof(self) wself = self;
        [UIView animateWithDuration:0.2 animations:^{
            wself.animationInProgress = YES;
            BTWCardsViewDirection direction = (action == BTWCardsViewActionAccept) ? -_declineDirection : _declineDirection;
            CGFloat windowWidth = [UIApplication sharedApplication].keyWindow.boundsWidth;
            topCard.centerX += direction * windowWidth;
            topCard.transform = CGAffineTransformMakeRotation(direction * kRotationAngle);
            [topCard itemDidMoveToOffset:direction];
        } completion:^(BOOL finished){
            if (action == BTWCardsViewActionDecline) {
                if ([wself.delegate respondsToSelector:@selector(cardsView:didDeclineCard:)]) {
                    [wself.delegate cardsView:wself didDeclineCard:topCard];
                }
            } else if (action == BTWCardsViewActionAccept) {
                if ([wself.delegate respondsToSelector:@selector(cardsView:didAcceptCard:)]) {
                    [wself.delegate cardsView:wself didAcceptCard:topCard];
                }
            }
            [topCard removeFromSuperview];
            [wself.cardViews removeObject:topCard];
            [wself setupCardsAnimated:YES withCompletionHandler:^{
                wself.animationInProgress = NO;
            }];
        }];
    }
}

- (void)acceptCurrentCard
{
    [self makeActionWithTopCard:BTWCardsViewActionAccept];
}

- (void)declineCurrentCard
{
    [self makeActionWithTopCard:BTWCardsViewActionDecline];
}

- (void)hideCurrentCard
{
    [self makeActionWithTopCard:BTWCardsViewActionNone];
}

- (void)reloadData
{
    for (UIView *cardView in _cardViews) {
        [cardView removeFromSuperview];
    }
    [_cardViews removeAllObjects];
    
    NSUInteger numberOfVisibleViews = [_delegate numberOfItemsForCardsView:self]; //MIN(kVisibleViews, [_delegate numberOfItemsForCardsView:self]);
    for (NSInteger i = numberOfVisibleViews - 1; i >= 0; i--) {
        UIView *cardView = [_delegate itemForIndex:i];
        cardView.size = self.size;
        [self addSubview:cardView];
        [_cardViews insertObject:cardView atIndex:0];
    }
    
    [self setupCardsAnimated:NO withCompletionHandler:nil];
}

#pragma mark - Layout
- (CGPoint)centerForCardAtIndex:(NSUInteger)index
{
    CGPoint center;
    center.x = floorf(self.boundsWidth * 0.5);
    center.y = floorf(self.boundsHeight * 0.5 + kVerticalStep * MIN(index, kVisibleViews - 1));
    return center;
}

- (CGFloat)scaleForCardAtIndex:(NSUInteger)index
{
    return (1.0 - kScaleStep * MIN(index, kVisibleViews - 1));
}

- (void)setupCardsAnimated:(BOOL)animated withCompletionHandler:(BTWVoidBlock)completionHandler
{
    if (completionHandler && [_cardViews count] == 0) {
        completionHandler();
    }
    
    for (UIView<BTWCardsViewItem> *cardView in _cardViews) {
        NSUInteger index = [_cardViews indexOfObject:cardView];
        BOOL isLastCard = (index == [_cardViews count] - 1);
        CGPoint center = [self centerForCardAtIndex:index];
        CGFloat scale = 1.0 - kScaleStep * index;
        
        if (animated)
        {
            void (^internalCompletion)(POPAnimation *, BOOL) = nil;
            if (completionHandler && isLastCard) {
                [completionHandler copy];
                __block NSInteger animationsCounter = 0;
                internalCompletion = ^(POPAnimation *animation, BOOL finished){
                    animationsCounter++;
                    if (animationsCounter == 3) {
                        completionHandler();
                    }
                };
            }
            
            POPSpringAnimation *translation = [POPSpringAnimation animation];
            translation.springBounciness = 10.0f;
            translation.property = [POPAnimatableProperty propertyWithName:kPOPViewCenter];
            translation.toValue = [NSValue valueWithCGPoint:center];
            translation.completionBlock = internalCompletion;
            [cardView pop_addAnimation:translation forKey:@"translation"];
            
            POPSpringAnimation *rotation = [POPSpringAnimation animation];
            rotation.springBounciness = 10.0f;
            rotation.property = [POPAnimatableProperty propertyWithName:kPOPLayerRotation];
            rotation.toValue = @(0.0);
            rotation.completionBlock = internalCompletion;
            [cardView.layer pop_addAnimation:rotation forKey:@"rotation"];
            
            POPSpringAnimation *scaleXY = [POPSpringAnimation animation];
            scaleXY.property = [POPAnimatableProperty propertyWithName:kPOPViewScaleXY];
            scaleXY.toValue = [NSValue valueWithCGPoint:(CGPoint){scale, scale}];
            scaleXY.completionBlock = internalCompletion;
            [cardView pop_addAnimation:scaleXY forKey:@"scaleXY"];
        } else {
            cardView.center = center;
            cardView.transform = CGAffineTransformRotate(cardView.transform, 0.0);
            cardView.transform = CGAffineTransformScale(cardView.transform, scale, scale);
            
            if (completionHandler && isLastCard) {
                completionHandler();
            }
        }
    }
}

#pragma mark - Recognizing gestures
- (void)panRecognized:(UIPanGestureRecognizer *)panRecognizer
{
    static CGPoint previousPosition;
    CGPoint currentPosition = [panRecognizer locationInView:panRecognizer.view];
    
    UIView<BTWCardsViewItem> *topCard = [self topCard];
    if (panRecognizer.state == UIGestureRecognizerStateChanged) {
        topCard.centerX += floorf(currentPosition.x - previousPosition.x);
        topCard.centerY += floorf(currentPosition.y - previousPosition.y);

        CGFloat offsetAncor = self.boundsWidth * 0.5;
        CGFloat offset = (topCard.centerX - offsetAncor) / offsetAncor;
        
        // # поворачиваем карту, которую несем
        [topCard itemDidMoveToOffset:offset];
        topCard.transform = CGAffineTransformMakeRotation(kRotationAngle * offset);
        
        // # приподнимаем остальные
        for (NSUInteger i = 1; i < [_cardViews count]; i++) {
            UIView<BTWCardsViewItem> *cardView = _cardViews[i];
            cardView.centerY = [self centerForCardAtIndex:i].y - kVerticalStep * fabs(offset);
        }
    } else if (panRecognizer.state == UIGestureRecognizerStateEnded) {
        if (topCard.centerX < 0 || topCard.centerX > self.boundsWidth) {
            BTWCardsViewDirection direction = topCard.centerX > 0 ? BTWCardsViewDirectionRight : BTWCardsViewDirectionLeft;
            BTWCardsViewAction action = (direction == _declineDirection) ? BTWCardsViewActionDecline : BTWCardsViewActionAccept;
            [self makeActionWithTopCard:action];
        } else {
            [self setupCardsAnimated:YES withCompletionHandler:nil];
            [UIView animateWithDuration:0.15 animations:^{
                [topCard itemDidMoveToOffset:0.0f];
            }];
        }
    }
    
    previousPosition = currentPosition;
}

- (void)tapRecognized:(UITapGestureRecognizer *)tapRecognized
{
    if ([self topCard] && [_delegate respondsToSelector:@selector(cardsView:didTapCard:)]) {
        [_delegate cardsView:self didTapCard:[self topCard]];
    }
}

@end
