//
//  BTWMatchingScanningView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWMatchingScanningView : UIView

@property (nonatomic, assign) BOOL showRestartScreen;
@property (nonatomic, copy) BTWVoidBlock restartSearchBlock;

- (void)startAnimating;
- (void)stopAnimating;

@end
