//
//  BTWMatchingCardView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMatchingCardView.h"
#import "BTWCommonUtils.h"

#import "UIImageView+WebCache.h"

@interface BTWMatchingCardView ()
@property (weak, nonatomic) IBOutlet UILabel *matchingScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *photosCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (weak, nonatomic) IBOutlet UIImageView *yesImageView;
@property (weak, nonatomic) IBOutlet UIImageView *noImageView;
@property (weak, nonatomic, readwrite) IBOutlet UIImageView *photoImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeightConstraint;

@end

@implementation BTWMatchingCardView

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.layer.cornerRadius = 10.0f;
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [BTWStylesheet matchingCardBorderColor].CGColor;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    // Выносим ограничения в аутлеты, чтобы отвязать положение контролов
    // от фрэйма окна и исключить некорректное поведение при афинных преобразованиях
    _contentWidthConstraint.constant = frame.size.width;
    _contentHeightConstraint.constant = frame.size.height;
}

#pragma mark - Setup
- (void)setUser:(MatchingUser *)user
{
    if (![user isEqual:_user]) {
        _user = user;
        _nameLabel.text = [NSString stringWithFormat:@"%@ %@", _user.name, _user.surname];
        _ageLabel.text = [BTWCommonUtils ageStringForAge:_user.age];
        _matchingScoreLabel.text = [NSString stringWithFormat:@"%d%%", [_user.match integerValue]];
        [_photoImageView sd_setImageWithURL:[NSURL URLWithString:_user.photo_url] placeholderImage:[_user bigPlaceholder]];
        
        if ([_user.gallery_count integerValue] > 0) {
            _cameraImageView.hidden = NO;
            _photosCountLabel.hidden = NO;
            _photosCountLabel.text = [_user.gallery_count stringValue];
        } else {
            _cameraImageView.hidden = YES;
            _photosCountLabel.hidden = YES;
        }
    }
}

- (void)itemDidMoveToOffset:(CGFloat)offset
{
    if (offset > 0) {
        _noImageView.alpha = offset;
        _yesImageView.alpha = 0.0f;
    } else {
        _noImageView.alpha = 0.0f;
        _yesImageView.alpha = fabs(offset);
    }
}

@end
