//
//  BTWCardsView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWCardsViewItem.h"

// TODO: 1.Сделать переиспользование карточек

@class BTWCardsView;
@protocol BTWCardsViewDelegate <NSObject>
- (NSUInteger)numberOfItemsForCardsView:(BTWCardsView *)cardsView;
- (UIView<BTWCardsViewItem> *)itemForIndex:(NSUInteger)index;
@optional
- (void)cardsView:(BTWCardsView *)cardsView didAcceptCard:(UIView<BTWCardsViewItem> *)itemView;
- (void)cardsView:(BTWCardsView *)cardsView didDeclineCard:(UIView<BTWCardsViewItem> *)itemView;
- (void)cardsView:(BTWCardsView *)cardsView didTapCard:(UIView<BTWCardsViewItem> *)itemView;
@end

typedef NS_ENUM(NSInteger, BTWCardsViewDirection) {
    BTWCardsViewDirectionLeft = -1,
    BTWCardsViewDirectionRight = 1
};

typedef NS_ENUM(NSUInteger, BTWCardsViewAction) {
    BTWCardsViewActionNone,
    BTWCardsViewActionAccept,
    BTWCardsViewActionDecline
};

@interface BTWCardsView : UIView

@property (nonatomic, weak) IBOutlet id<BTWCardsViewDelegate> delegate;
@property (nonatomic, assign) BTWCardsViewDirection declineDirection;

- (void)reloadData;
- (void)acceptCurrentCard;
- (void)declineCurrentCard;
- (void)hideCurrentCard;

- (UIView<BTWCardsViewItem> *)topCard;

@end
