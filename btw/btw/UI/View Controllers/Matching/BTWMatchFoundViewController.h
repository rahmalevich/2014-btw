//
//  BTWMatchFoundViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWMatchFoundViewController : BTWViewController

- (id)initWithMatch:(User *)match andTopicId:(NSString *)topicId;
- (id)initWithDialog:(Dialog *)dialog;

@end
