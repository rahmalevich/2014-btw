//
//  BTWMatchFoundViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMatchFoundViewController.h"
#import "BTWUserService.h"
#import "ScaleUnwindSegue.h"
#import "BTWTransitionsManager.h"

#import "UIImageView+WebCache.h"

@interface BTWMatchFoundViewController ()
@property (nonatomic, strong) User *match;
@property (nonatomic, copy) NSString *topicId;
@property (nonatomic, strong) Dialog *dialog;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (weak, nonatomic) IBOutlet UIView *matchBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *matchImageView;
@property (weak, nonatomic) IBOutlet UIView *youBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *youImageView;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@end

@implementation BTWMatchFoundViewController

#pragma mark - Initialization
- (id)initWithMatch:(User *)match andTopicId:(NSString *)topicId
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.match = match;
        self.topicId = topicId;
    }
    return self;
}

- (id)initWithDialog:(Dialog *)dialog
{
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.match = dialog.user;
        self.dialog = dialog;
    }
    return self;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _btnChat.layer.cornerRadius = 3.0f;
    _btnChat.layer.borderWidth = 1.0f;
    _btnChat.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _btnShare.layer.cornerRadius = 3.0f;
    _btnShare.layer.borderWidth = 1.0f;
    _btnShare.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _matchBackgroundView.layer.cornerRadius = floorf(_matchBackgroundView.frameWidth/2);
    _matchImageView.layer.cornerRadius = floorf(_matchImageView.frameWidth/2);
    _matchImageView.layer.borderWidth = 2.0f;
    _matchImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    _youBackgroundView.layer.cornerRadius = floorf(_youBackgroundView.frameWidth/2);
    _youImageView.layer.cornerRadius = floorf(_youImageView.frameWidth/2);
    _youImageView.layer.borderWidth = 2.0f;
    _youImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [_matchImageView sd_setImageWithURL:[NSURL URLWithString:_match.photo_mid] placeholderImage:[_match midPlaceholder]];
    [_youImageView sd_setImageWithURL:[NSURL URLWithString:[BTWUserService sharedInstance].authorizedUser.photo_mid] placeholderImage:[[BTWUserService sharedInstance].authorizedUser midPlaceholder]];
    
    // subtitle
    NSDictionary *commonAttributes = @{ NSFontAttributeName : [BTWStylesheet commonFontOfSize:15.0],
                                        NSForegroundColorAttributeName : [BTWStylesheet commonGrayColor] };
    NSDictionary *nameAttributes = @{ NSFontAttributeName : [BTWStylesheet commonFontOfSize:15.0],
                                      NSForegroundColorAttributeName : [BTWStylesheet commonOrangeColor] };

    NSMutableAttributedString *subtitle = [NSMutableAttributedString new];
    [subtitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSLocalizedString(@"you", nil) capitalizedString] attributes:nameAttributes]];
    [subtitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@ ", NSLocalizedString(@"and", nil)] attributes:commonAttributes]];
    [subtitle appendAttributedString:[[NSAttributedString alloc] initWithString:_match.name attributes:nameAttributes]];
    [subtitle appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@", NSLocalizedString(@"like each other", nil)] attributes:commonAttributes]];
    
    _lblSubtitle.attributedText = subtitle;
    
    // chat button
    _btnChat.hidden = ![_topicId isValidString];
}

#pragma mark - Actions
- (IBAction)actionChat:(UIButton *)sender
{
    Dialog *dialogToShow = nil;
    if (!_dialog) {
        Dialog *dialog = [Dialog MR_findFirstByAttribute:@"backend_id" withValue:_topicId];
        if (!dialog) {
            dialog = [Dialog MR_createEntity];
            dialog.backend_id = _topicId;
            dialog.user = _match;
            [dialog.managedObjectContext MR_saveToPersistentStoreAndWait];
        }
        dialogToShow = dialog;
    } else {
        dialogToShow = _dialog;
    }
    [[BTWTransitionsManager sharedInstance] showDialog:dialogToShow];
    
    [self dismiss];
}

- (IBAction)actionShare:(UIButton *)sender
{
    [self dismiss];
}

- (IBAction)actionContinue:(UIButton *)sender
{
    [self dismiss];
}

- (void)dismiss
{
    ScaleUnwindSegue *unwindSegue = [[ScaleUnwindSegue alloc] initWithIdentifier:nil source:self destination:self.parentViewController];
    [unwindSegue perform];
}

@end
