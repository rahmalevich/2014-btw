
//
//  BTWMessagesViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMatchingViewController.h"
#import "BTWMatchingCardView.h"
#import "BTWMatchingDataController.h"
#import "BTWCardsView.h"
#import "BTWTransitionsManager.h"
#import "BTWUserProfileViewController.h"
#import "BTWMatchingScanningView.h"
#import "BTWGalleryButtonsView.h"
#import "BTWMatchFoundViewController.h"
#import "BTWUserService.h"

#import "ScaleSegue.h"

#import "IDMPhotoBrowser.h"
#import "MBProgressHUD.h"

@interface BTWMatchingViewController () <BTWDataControllerDelegate, BTWCardsViewDelegate>
@property (nonatomic, strong) BTWMatchingDataController *matchingDataController;
@property (nonatomic, strong) BTWMatchingScanningView *scanningView;
@property (nonatomic, strong) BTWGalleryButtonsView *buttonsView;
@property (nonatomic, weak) IDMPhotoBrowser *photoBrowser;

@property (weak, nonatomic) IBOutlet BTWCardsView *cardsView;
@end

@implementation BTWMatchingViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.matchingDataController = [[BTWMatchingDataController alloc] initWithDelegate:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    __weak typeof(self) wself = self;
    BTWGalleryButton *likeButton = [BTWGalleryButton buttonWithImage:[UIImage imageNamed:@"icon_gallery_like"] title:NSLocalizedString(@"Yes", nil) action:^{
        [wself.photoBrowser hideBrowserWithCompletionHandler:^{
            [wself actionLike:nil];
        }];
    }];
    BTWGalleryButton *dislikeButton = [BTWGalleryButton buttonWithImage:[UIImage imageNamed:@"icon_gallery_dislike"] title:NSLocalizedString(@"No", nil) action:^{
        [wself.photoBrowser hideBrowserWithCompletionHandler:^{
            [wself actionDislike:nil];
        }];
    }];
    BTWGalleryButton *complainButton = [BTWGalleryButton buttonWithImage:[UIImage imageNamed:@"icon_gallery_complain"] title:NSLocalizedString(@"Complain", nil) action:^{
        NSUInteger photoIndex = wself.photoBrowser.currentPageIndex;
        [wself complainOnImageAtIndex:photoIndex];
    }];
    self.buttonsView = [[BTWGalleryButtonsView alloc] initWithButtons:@[likeButton, dislikeButton, complainButton]];
    [_buttonsView sizeToFit];

    // Добавляем заглушку для поиска
    self.scanningView = [[[NSBundle mainBundle] loadNibNamed:@"BTWMatchingScanningView" owner:self options:nil] firstObject];
    _scanningView.restartSearchBlock = ^{
        [wself.matchingDataController updateProfiles];
    };
    
    [self.view addSubview:_scanningView];
    [_scanningView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_scanningView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(_scanningView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_scanningView]|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:NSDictionaryOfVariableBindings(_scanningView)]];
    
    self.title = NSLocalizedString(@"Matching", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [_cardsView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([_matchingDataController count] == 0) {
        _scanningView.showRestartScreen = NO;
        [_scanningView startAnimating];
        [_matchingDataController updateProfiles];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [_scanningView stopAnimating];
}

#pragma mark - Content setup
- (void)hideCards
{
    if (_scanningView.hidden) {
        _scanningView.alpha = 0.0f;
        _scanningView.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            _scanningView.alpha = 1.0f;
        } completion:^(BOOL finished){
            [_scanningView startAnimating];
        }];
    }
}

- (void)showCards
{
    if (!_scanningView.hidden) {
        [UIView animateWithDuration:0.3 animations:^{
            _scanningView.alpha = 0.0f;
        } completion:^(BOOL finished){
            [_scanningView stopAnimating];
            _scanningView.hidden = YES;
        }];
    }
}

#pragma mark - Actions
- (IBAction)actionLike:(UIButton *)sender
{
    [_cardsView acceptCurrentCard];
}

- (IBAction)actionDislike:(UIButton *)sender
{
    [_cardsView declineCurrentCard];
}

- (void)complainOnImageAtIndex:(NSUInteger)photoIndex
{
    IDMPhoto *photo = [_photoBrowser photoAtIndex:photoIndex];
    if (photo.underlyingImage) {
        MatchingUser *user = ((BTWMatchingCardView *)[_cardsView topCard]).user;
        GalleryImage *currentImage = user.gallery_images.count > photoIndex ? user.gallery_images[photoIndex] : nil;
        if (currentImage) {
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
            [[BTWUserService sharedInstance] complainOnGalleryPhoto:currentImage completionHandler:^(NSError *error)
            {
                UIImage *hudImage = error ? [UIImage imageNamed:@"icon_error"] : [UIImage imageNamed:@"icon_success"];
                UIImageView *hudImageView = [[UIImageView alloc] initWithImage:hudImage];
                hudImageView.size = hudImage.size;
                hud.mode = MBProgressHUDModeCustomView;
                hud.customView = hudImageView;
                hud.labelText = error ? nil : NSLocalizedString(@"Complain sent", nil);
                [hud hide:YES afterDelay:1.0];
                
                if (!error) {
                    // Удаляем фото в просмотрщике
                    [_photoBrowser removePhotoAtIndex:photoIndex];
                    
                    // Удаляем фото из галереи пользователя
                    NSMutableOrderedSet *galleryImages = [NSMutableOrderedSet orderedSetWithOrderedSet:user.gallery_images];
                    [galleryImages removeObject:currentImage];
                    user.gallery_images = [NSOrderedSet orderedSetWithOrderedSet:galleryImages];
                    
                    // Убираем фото с аватара пользователя
                    if ([currentImage.is_avatar boolValue]) {
                        user.photo_url = nil;
                        user.photo_mid = nil;
                        user.photo_small = nil;
                        [_cardsView reloadData];
                    }
                    
                    [user.managedObjectContext MR_saveToPersistentStoreAndWait];
                }
            }];
        }
    }
}

- (IBAction)actionInfo:(UIButton *)sender
{
    BTWMatchingCardView *cardView = (BTWMatchingCardView *)[_cardsView topCard];
    [self openProfileForCard:cardView];
}

- (void)openProfileForCard:(BTWMatchingCardView *)cardView
{
    BTWUserProfileViewController *profileController = (BTWUserProfileViewController *)[[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeUserProfile];
    profileController.user = cardView.user;
    [self.navigationController pushViewController:profileController animated:YES];
}

- (void)openGalleryForCard:(BTWMatchingCardView *)cardView
{
    MatchingUser *user = cardView.user;
    BOOL hasAvatar = [user.photo_url isValidString];
    
    // Проверяем наличие фотографий
    if ([user.gallery_count integerValue] == 0) {
        return;
    }
    
    // Строим модель
    NSMutableArray *photosArray = [NSMutableArray array];
    if ([user.gallery_images count] > 0) {
        for (GalleryImage *galleryImage in user.gallery_images) {
            [photosArray addObject:[IDMPhoto photoWithURL:[NSURL URLWithString:galleryImage.url]]];
        }
    } else {
        NSInteger photosCount = [user.gallery_count integerValue];
        if (hasAvatar) {
            [photosArray addObject:[IDMPhoto photoWithURL:[NSURL URLWithString:user.photo_url]]];
            photosCount--;
        }
        for (NSInteger i = 0; i < photosCount; i++) {
            [photosArray addObject:[IDMPhoto new]];
        }
    }
    
    // Создаем просмотрщик фотографий
    UIView *senderView = hasAvatar ? cardView.photoImageView : nil;
    IDMPhotoBrowser *photoBrowser = [[IDMPhotoBrowser alloc] initWithPhotos:photosArray animatedFromView:senderView];
    photoBrowser.showTopBar = YES;
    
    NSString *titleText = user.fullname;
    if ([user age] > 0) {
        titleText = [titleText stringByAppendingFormat:@", %@", [BTWCommonUtils ageStringForAge:user.age]];
    }
    photoBrowser.titleLabel.font = [BTWStylesheet commonBoldFontOfSize:15.0f];
    photoBrowser.titleLabel.text = titleText;
    photoBrowser.buttonsView.size = _buttonsView.size;
    [photoBrowser.buttonsView addSubview:_buttonsView];
    
    CGFloat doneButtonSize = 40.0f;
    photoBrowser.doneButton.size = CGSizeMake(doneButtonSize, doneButtonSize);
    [photoBrowser.doneButton setImage:[UIImage imageNamed:@"icon_gallery_close"] forState:UIControlStateNormal];
    
    [[BTWTransitionsManager sharedInstance] showModalViewController:photoBrowser];
    
    self.photoBrowser = photoBrowser;
    
    // Инициализируем загрузу галереи
    if ([user.gallery_images count] == 0) {
        [_matchingDataController loadGalleryForCurrentProfileWithCompletionHandler:^(NSError *error){
            [user.managedObjectContext refreshObject:user mergeChanges:YES];
            for (NSInteger i = 0; i < [user.gallery_images count]; i++) {
                GalleryImage *galleryImage = [user.gallery_images objectAtIndex:i];
                IDMPhoto *photo = [photoBrowser photoAtIndex:i];
                photo.photoURL = [NSURL URLWithString:galleryImage.url];
                if (i == photoBrowser.currentPageIndex) {
                    [photo loadUnderlyingImageAndNotify];
                }
            }
        }];
    }
}

#pragma mark - Data Controller delegate
- (void)dataController:(BTWMatchingDataController *)controller didEndLoadingDataWithError:(NSError *)error
{
    if ([controller count] == 0) {
        _scanningView.showRestartScreen = YES;
        [self hideCards];
    } else {
        [_cardsView reloadData];
        [self showCards];
    }
}

#pragma mark - Cards View delegate
- (NSUInteger)numberOfItemsForCardsView:(BTWCardsView *)cardsView
{
    return [_matchingDataController count];
}

- (UIView *)itemForIndex:(NSUInteger)index
{
    BTWMatchingCardView *cardView = [[[NSBundle mainBundle] loadNibNamed:@"BTWMatchingCardView" owner:self options:nil] firstObject];
    cardView.user = [_matchingDataController profileAtIndex:index];
    return cardView;
}

- (void)cardsView:(BTWCardsView *)cardsView didAcceptCard:(BTWMatchingCardView *)itemView
{
    [self makeCardActionAccept:YES];
}

- (void)cardsView:(BTWCardsView *)cardsView didDeclineCard:(BTWMatchingCardView *)itemView
{
    [self makeCardActionAccept:NO];
}

- (void)cardsView:(BTWCardsView *)cardsView didTapCard:(BTWMatchingCardView *)itemView
{
    [self openGalleryForCard:itemView];
}

- (void)makeCardActionAccept:(BOOL)accept
{
    __weak typeof(self) wself = self;
    BTWMatchingBlock completionHandler = ^(NSString *topicId, MatchingUser *user){
        BOOL contactCreated = [topicId isValidString];
        if (contactCreated) {
            [[BTWTransitionsManager sharedInstance] showMatchFoundScreenForUser:user andTopicId:topicId];
        }
        
        if ([wself.matchingDataController count] == 0) {
            [wself hideCards];
            [wself.matchingDataController updateProfiles];
        }
    };
    
    if (accept) {
        [_matchingDataController acceptCurrentProfileWithCompletionHandler:completionHandler];
    } else {
        [_matchingDataController declineCurrentProfileWithCompletionHandler:completionHandler];
    }
}

@end
