//
//  BTWTabBarController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWTabBarController.h"
#import "BTWTransitionsManager.h"
#import "BTWMessagingService.h"
#import "BTWNotificationsService.h"
#import "BTWEventsService.h"

#import "RDVTabBar.h"
#import "RDVTabBarItem.h"
#import "CustomBadge.h"

static CGFloat const kCentralButtonBottomOffset = 3.0f;

@interface BTWTabBarController ()
@property (nonatomic, strong) UIButton *centerButton;
@end

@implementation BTWTabBarController

//#pragma mark - Rotations
//- (BOOL)shouldAutorotate
//{
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}

#pragma mark - Initialization
- (void)dealloc
{
    [[BTWMessagingService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedMessagesCount"];
    [[BTWNotificationsService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedNotificationsCount"];
    [[BTWEventsService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedActiveEventsCount"];
    [[BTWEventsService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedCompletedEventsCount"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[BTWMessagingService sharedInstance] addObserver:self forKeyPath:@"unconfirmedMessagesCount" options:NSKeyValueObservingOptionNew context:nil];
    [[BTWNotificationsService sharedInstance] addObserver:self forKeyPath:@"unconfirmedNotificationsCount" options:NSKeyValueObservingOptionNew context:nil];
    [[BTWEventsService sharedInstance] addObserver:self forKeyPath:@"unconfirmedActiveEventsCount" options:NSKeyValueObservingOptionNew context:nil];
    [[BTWEventsService sharedInstance] addObserver:self forKeyPath:@"unconfirmedCompletedEventsCount" options:NSKeyValueObservingOptionNew context:nil];
    
    [self setupViewControllers];
    [self setupTabBar];
    [self addCenterButton];
    
    [self setNotificationsCount:([BTWMessagingService sharedInstance].unconfirmedMessagesCount + [BTWNotificationsService sharedInstance].unconfirmedNotificationsCount) forIndex:kMailViewControllerIndex];
    [self setNotificationsCount:([BTWEventsService sharedInstance].unconfirmedActiveEventsCount + [BTWEventsService sharedInstance].unconfirmedCompletedEventsCount) forIndex:kActiveRoutesViewControllerIndex];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"unconfirmedMessagesCount"] || [keyPath isEqualToString:@"unconfirmedNotificationsCount"]) {
        NSUInteger count = [BTWMessagingService sharedInstance].unconfirmedMessagesCount + [BTWNotificationsService sharedInstance].unconfirmedNotificationsCount;
        [self setNotificationsCount:count forIndex:kMailViewControllerIndex];
    } else if ([keyPath isEqualToString:@"unconfirmedActiveEventsCount"] || [keyPath isEqualToString:@"unconfirmedCompletedEventsCount"]) {
        NSUInteger count = [BTWEventsService sharedInstance].unconfirmedCompletedEventsCount + [BTWEventsService sharedInstance].unconfirmedActiveEventsCount;
        [self setNotificationsCount:count forIndex:kActiveRoutesViewControllerIndex];
    }
}

- (void)setSelectedIndex:(NSUInteger)newIndex
{
    NSUInteger prevIndex = self.selectedIndex;
    [super setSelectedIndex:newIndex];
    
    _centerButton.selected = (newIndex == kRouteViewControllerIndex);
    
    if (prevIndex != newIndex) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kTabChangedNotification object:nil];
    }
}

- (void)setNotificationsCount:(NSUInteger)count forIndex:(NSUInteger)index
{
    if (self.tabBar.items) {
        RDVTabBarItem *inboxItem = self.tabBar.items[index];
        UIImage *icon = [self iconForIndex:index];
        UIImage *iconImage = [self addBadgeWithCount:count forIcon:icon];
        UIImage *selectedIconImage = [self addBadgeWithCount:count forIcon:[icon imageByColorizingWithColor:[BTWStylesheet commonGreenColor]]];
        [inboxItem setFinishedSelectedImage:selectedIconImage withFinishedUnselectedImage:iconImage];
        
        [self.tabBar setNeedsLayout];
    }
}

#pragma mark - Setup
- (void)setupViewControllers
{
    UIViewController *profileViewController = [[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeMyProfile];
    UIViewController *mailViewController = [[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeMail];
    UIViewController *routeViewController = [[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeRoute];
    UIViewController *activeRoutesViewController = [[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeActiveRoutes];
    UIViewController *settingsViewController = [[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeSettings];
    [self setViewControllers:@[profileViewController, mailViewController, routeViewController, activeRoutesViewController, settingsViewController]];
    
    self.selectedIndex = kRouteViewControllerIndex;
}

- (UIImage *)iconForIndex:(NSInteger)index
{
    NSString *iconName = nil;
    switch (index) {
        case kProfileViewControllerIndex:
            iconName = @"icon_profile";
            break;
        case kMailViewControllerIndex:
            iconName = @"icon_inbox";
            break;
        case kActiveRoutesViewControllerIndex:
            iconName = @"icon_routes";
            break;
        case kSettingsViewControllerIndex:
            iconName = @"icon_settings";
            break;
        default:
            break;
    }
    return [UIImage imageNamed:iconName];
}

- (void)setupTabBar
{
    CGFloat borderButtonsWidth = floorf((self.view.boundsWidth - _centerButton.boundsWidth)/5);
    CGFloat centralButtonsWidth = floorf((self.view.boundsWidth - 2 * borderButtonsWidth)/2);
    CGFloat centralButtonsImageOffset = floorf((centralButtonsWidth - borderButtonsWidth)/2);
    
    for (NSInteger i = 0; i < [self.tabBar.items count]; i++) {
        RDVTabBarItem *item = self.tabBar.items[i];
        item.title = nil;
        
        if (i == kMailViewControllerIndex) {
            item.itemWidth = centralButtonsWidth;
            item.imagePositionAdjustment = UIOffsetMake(-centralButtonsImageOffset, 0.0f);
        } else if (i == kActiveRoutesViewControllerIndex) {
            item.itemWidth = centralButtonsWidth;
            item.imagePositionAdjustment = UIOffsetMake(centralButtonsImageOffset, 0.0f);
        } else if (i == kRouteViewControllerIndex) {
            item.itemWidth = 0.0f;
        } else {
            item.itemWidth = borderButtonsWidth;
        }
        
        CGFloat scale = [UIScreen mainScreen].scale;
        UIImage *normalBackgroundImage = [UIImage imageWithColor:[BTWStylesheet tabBarColor] size:(CGSize){1.0 * scale, kTabBarHeight * scale}];
        UIImage *selectedBackgroundImage = [UIImage imageWithColor:[BTWStylesheet selectedTabColor] size:(CGSize){1.0 * scale, kTabBarHeight * scale}];
        [item setBackgroundSelectedImage:selectedBackgroundImage withUnselectedImage:normalBackgroundImage];
        
        UIImage *iconImage = [self iconForIndex:i];
        if (iconImage) {
            UIImage *selectedIconImage = [iconImage imageByColorizingWithColor:[BTWStylesheet commonGreenColor]];
            if (i == kMailViewControllerIndex || i == kActiveRoutesViewControllerIndex) {
                iconImage = [self addBadgeWithCount:0 forIcon:iconImage];
                selectedIconImage = [self addBadgeWithCount:0 forIcon:selectedIconImage];
            }
            [item setFinishedSelectedImage:selectedIconImage withFinishedUnselectedImage:iconImage];
        }
    }
}

- (void)addCenterButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"icon_search"];
    UIImage *buttonBackground = [UIImage imageNamed:@"btn_search"];
    UIImage *buttonBackgroundSelected = [UIImage imageNamed:@"btn_search_sel"];
    
    CGRect buttonFrame;
    buttonFrame.size = buttonBackground.size;
    buttonFrame.origin.x = floorf((self.view.boundsWidth - buttonBackground.size.width)/2);
    buttonFrame.origin.y = self.view.boundsHeight - buttonBackground.size.height - kCentralButtonBottomOffset;
    
    self.centerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _centerButton.frame = buttonFrame;
    [_centerButton setImage:buttonImage forState:UIControlStateNormal];
    [_centerButton setBackgroundImage:buttonBackground forState:UIControlStateNormal];
    [_centerButton setBackgroundImage:buttonBackgroundSelected forState:UIControlStateSelected];
    [_centerButton addTarget:self action:@selector(centerButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_centerButton];
}

- (UIImage *)addBadgeWithCount:(NSUInteger)count forIcon:(UIImage *)icon
{
    NSAssert(icon, @"ERROR: Icon is nil");
    
    CGFloat defaultBadgeSize = 25.0f;
    CGFloat badgeInset = floorf(0.45 * defaultBadgeSize);
    CGFloat scale = [UIScreen mainScreen].scale;
    
    CGSize size = (CGSize){icon.size.width + 2 * badgeInset, icon.size.height + 2 * badgeInset};
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [icon drawAtPoint:(CGPoint){badgeInset, badgeInset}];

    if (count > 0) {
        BadgeStyle *badgeStyle = [BadgeStyle defaultStyle];
        badgeStyle.badgeInsetColor = [BTWStylesheet badgeColor];
        badgeStyle.badgeFrameColor = [UIColor whiteColor];
        badgeStyle.badgeFrame = YES;
        CustomBadge *badge = [CustomBadge customBadgeWithString:[@(count) stringValue] withStyle:badgeStyle];
        CGContextTranslateCTM(context, size.width - badge.frameWidth, 0.0f);
        [badge.layer renderInContext:context];
    }
    
    UIImage *resultIcon = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultIcon;
}

#pragma mark - Actions
- (void)centerButtonTouched:(id)sender
{
    self.selectedIndex = kRouteViewControllerIndex;
}

@end
