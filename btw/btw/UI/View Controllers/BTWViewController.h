//
//  BTWViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWViewController : UIViewController

- (void)actionBack:(id)sender;

- (void)prepareForParentController;

@end
