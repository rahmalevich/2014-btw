//
//  BTWTabBarController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "RDVTabBarController.h"

static NSString * const kTabChangedNotification = @"kTabChangedNotification";

static NSInteger const kProfileViewControllerIndex = 0;
static NSInteger const kMailViewControllerIndex = 1;
static NSInteger const kRouteViewControllerIndex = 2;
static NSInteger const kActiveRoutesViewControllerIndex = 3;
static NSInteger const kSettingsViewControllerIndex = 4;

@interface BTWTabBarController : RDVTabBarController

@end
