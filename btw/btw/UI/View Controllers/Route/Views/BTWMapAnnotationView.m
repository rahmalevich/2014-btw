//
//  BTWMapAnnotationView.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMapAnnotationView.h"
#import "UIImageView+WebCache.h"

@interface BTWMapAnnotationView ()
@property (weak, nonatomic) IBOutlet UIImageView *pinImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@end

@implementation BTWMapAnnotationView

#pragma mark - Drawing marker image
static BTWMapAnnotationView *_sharedInstance = nil;
+ (UIImage *)markerImageForUser:(RouteUser *)user
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[[NSBundle mainBundle] loadNibNamed:@"BTWMapAnnotationView" owner:self options:nil] firstObject];
    });
    
    [_sharedInstance setupWithUser:user];
    
    UIGraphicsBeginImageContextWithOptions(_sharedInstance.size, NO, 0.0);
    [_sharedInstance.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

#pragma mark - Instance methods
- (void)awakeFromNib
{
    _avatarImageView.layer.cornerRadius = floorf(_avatarImageView.boundsWidth/2);
    _avatarImageView.layer.borderWidth = 1.0f;
    _avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)setupWithUser:(RouteUser *)user
{
    _iconImageView.image = [user blueRoleImage];
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:user.photo_mid] placeholderImage:[user midPlaceholder]];
}

@end
