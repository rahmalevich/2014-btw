//
//  BTWMapAnnotationView.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWMapAnnotationView : UIView

+ (UIImage *)markerImageForUser:(RouteUser *)user;

@end
