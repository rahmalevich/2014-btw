//
//  BTWMapUserInfoViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMapUserInfoViewController.h"
#import "BTWRatingView.h"

#import "UIImageView+WebCache.h"

static CGFloat const kHeight = 145.0f;

@interface BTWMapUserInfoViewController ()
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *carTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *carSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *roleIconImageView;
@property (weak, nonatomic) IBOutlet BTWRatingView *ratingView;
@end

@implementation BTWMapUserInfoViewController

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.autoresizingMask = UIViewAutoresizingNone;
}

- (void)setupLayout
{
    BOOL isDriver = [_user isDriver];
    
    _carTitleLabel.hidden = !isDriver;
    _carSubtitleLabel.hidden = !isDriver;
    _roleIconImageView.image = [_user blueRoleImageBig];
    
    _avatarImageView.layer.cornerRadius = floorf(_avatarImageView.boundsWidth/2);
    
    self.view.frameHeight = kHeight;
    
    self.view.layer.borderColor = [BTWStylesheet commonBlueColor].CGColor;
    self.view.layer.borderWidth = 1.0f;
    self.view.layer.cornerRadius = 5.0f;
    self.view.clipsToBounds = YES;
    
    [self.view layoutIfNeeded];
}

- (UIBezierPath *)backgroundShapePathForSize:(CGSize)size
{
    CGFloat leftCornersRadius = floor(size.height/2);
    CGFloat otherCornersRadius = 5.0f;
    CGFloat pathWidth = size.width;
    CGFloat pathHeight = size.height;
    UIBezierPath *backgroundMaskPath = [UIBezierPath bezierPath];
    [backgroundMaskPath moveToPoint:CGPointMake(0.0f, leftCornersRadius)];
    [backgroundMaskPath addArcWithCenter:CGPointMake(leftCornersRadius, leftCornersRadius) radius:leftCornersRadius startAngle:M_PI endAngle:M_PI * 1.5 clockwise:YES];
    [backgroundMaskPath addLineToPoint:CGPointMake(pathWidth - otherCornersRadius, 0)];
    [backgroundMaskPath addArcWithCenter:CGPointMake(pathWidth - otherCornersRadius, otherCornersRadius) radius:otherCornersRadius startAngle:M_PI * 1.5 endAngle:M_PI * 2 clockwise:YES];
    [backgroundMaskPath addLineToPoint:CGPointMake(pathWidth, pathHeight - otherCornersRadius)];
    [backgroundMaskPath addArcWithCenter:CGPointMake(pathWidth - otherCornersRadius, pathHeight - otherCornersRadius) radius:otherCornersRadius startAngle:0 endAngle:M_PI * 0.5 clockwise:YES];
    [backgroundMaskPath addLineToPoint:CGPointMake(leftCornersRadius, pathHeight)];
    [backgroundMaskPath addArcWithCenter:CGPointMake(leftCornersRadius, pathHeight - leftCornersRadius) radius:leftCornersRadius startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
    [backgroundMaskPath closePath];
    return backgroundMaskPath;
}

- (void)setUser:(RouteUser *)user
{
    if (![_user isEqual:user]) {
        _user = user;
        
        _titleLabel.text = [user fullname];
        _subtitleLabel.text = [NSString stringWithFormat:@"%@, %@", [user roleString], [BTWCommonUtils ageStringForAge:user.age].lowercaseString];
        _ratingView.rating = [user.average_points floatValue];
        
        NSString *carTitleString = nil;
        NSString *carSubtitleString = nil;
        if ([user.car.brand isValidString] && [user.car.model isValidString]) {
            carTitleString = [NSString stringWithFormat:@"%@ %@ %@", user.car.brand, user.car.model, user.car.year ?: @""];
            NSMutableString *mutableSubtitle = [NSMutableString new];
            if ([user.car.number isValidString]) {
                [mutableSubtitle appendFormat:@"%@: %@, ", NSLocalizedString(@"Car number", nil), user.car.number];
            }
            if ([user.car.color isValidString]) {
                [mutableSubtitle appendFormat:@"%@: %@", NSLocalizedString(@"Color", nil), user.car.color];
            }
            carSubtitleString = [NSString stringWithString:mutableSubtitle];
        } else {
            carTitleString = NSLocalizedString(@"Car info not set", nil);
        }
        _carTitleLabel.text = carTitleString;
        _carSubtitleLabel.text = carSubtitleString;
        
        [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:user.photo_mid] placeholderImage:[user midPlaceholder]];
        
        [self setupLayout];
    }
}

#pragma mark - Actions
- (IBAction)actionInfo:(UIButton *)sender
{
    if (_infoBlock) {
        _infoBlock();
    }
}

- (IBAction)actionLike:(UIButton *)sender
{
    if (_likeBlock) {
        _likeBlock();
    }
}

- (IBAction)actionClose:(UIButton *)sender
{
    if (_hideBlock) {
        _hideBlock();
    }
}

@end
