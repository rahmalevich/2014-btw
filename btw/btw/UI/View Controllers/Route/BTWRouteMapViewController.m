//
//  BTWMapViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRouteMapViewController.h"
#import "BTWMapConstants.h"
#import "BTWRouteContainerViewController.h"
#import "BTWMapAnnotationView.h"
#import "BTWMapUserInfoViewController.h"
#import "BTWSliderValueViewController.h"
#import "BTWApiMethod+CommonMethods.h"
#import "BTWNetworkHelper.h"
#import "BTWTabBarController.h"
#import "BTWLocationManager.h"
#import "BTWRouteService.h"
#import "BTWSettingsService.h"
#import "BTWTransitionsManager.h"
#import "BTWUserProfileViewController.h"
#import "BTWUserService.h"
#import "BTWPhoneNumberViewController.h"

#import "PopupSegue.h"
#import "PopupUnwindSegue.h"
#import "UIImage+ImageWithColor.h"
#import "GoogleMaps.h"
#import "MBProgressHUD+CompletionIcon.h"
#import "pop.h"
#import "WYPopoverController.h"

static NSInteger const kTimerSliderStep = 15;
static CGFloat const kSlidersViewVisibleHeight = 210.0f;
static CGFloat const kSlidersViewHiddenHeight = 55.0f;
static CGFloat const kSlidersViewVisibilityOffset = 5.0f;
static int kCurrentUserPinsZIndex = 100;
static int kFoundUserPinsZIndex = 200;

@interface WYPopoverController (Protected)
@property (nonatomic, assign) CGRect rect;
- (void)positionPopover:(BOOL)aAnimated;
@end

@interface BTWRouteMapViewController () <GMSMapViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) BTWMapUserInfoViewController *userInfoVC;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) NSMutableSet *userMarkersSet;
@property (nonatomic, strong) NSSet *routeMarkersSet;
@property (nonatomic, strong) GMSPolyline *routePolyline;
@property (nonatomic, strong) GMSPolyline *userPolyline;
@property (nonatomic, strong) GMSCircle *startRangeCircle;
@property (nonatomic, strong) GMSCircle *finishRangeCircle;
@property (nonatomic, weak) GMSMarker *selectedMarker;
@property (nonatomic, strong) WYPopoverController *sliderValuePopover;

@property (weak, nonatomic) IBOutlet UISegmentedControl *navigationSegmentedControl;
@property (weak, nonatomic) IBOutlet UIView *slidersView;
@property (weak, nonatomic) IBOutlet UIView *swipeIndicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *clockImageView;
@property (weak, nonatomic) IBOutlet UIButton *endSearchButton;
@property (weak, nonatomic) IBOutlet UIButton *updateResultsButton;
@property (weak, nonatomic) IBOutlet UISlider *startRangeSlider;
@property (weak, nonatomic) IBOutlet UISlider *finishRangeSlider;
@property (weak, nonatomic) IBOutlet UISlider *timeRangeSlider;
@property (weak, nonatomic) IBOutlet UILabel *timeRangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *startRangeLowerValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *startRangeUpperValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishRangeLowerValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishRangeUpperValueLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *slidersViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeRangeLeftConstraint;
@end

@implementation BTWRouteMapViewController

#pragma mark - Initialization
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _swipeIndicatorView.layer.cornerRadius = 1.0f;
    
    _navigationSegmentedControl.layer.cornerRadius = 4.0f;
    
    _clockImageView.image = [_clockImageView.image imageWithColor:[BTWStylesheet commonGrayColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeSearchFinished:) name:kRouteSearchFinishedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationManagerDidSetInitialLocation:) name:kLocationManagerDidSetInitialLocationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(distanceUnitsChanged:) name:kDistanceUnitsChangedNotification object:nil];
    
    [self updateAppearence];
    [self updateTimeRangeLabel];
    [self updateSlidersLabels];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Вызов необходим для отработки NSLayoutConstraints и пересчета позиций контролов
    // Почему так - понять не могу
    [self.view setNeedsLayout];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideUserInfoView];
}

- (void)updateAppearence
{
    if ([BTWRouteService sharedInstance].currentRoute) {
        _updateResultsButton.hidden = NO;
        _endSearchButton.hidden = NO;
        _startRangeSlider.value = [[BTWSettingsService settingsValueForKey:kFilterStartRadiusKey] floatValue];
        _finishRangeSlider.value = [[BTWSettingsService settingsValueForKey:kFilterFinishRadiusKey] floatValue];
        _timeRangeSlider.value = [[BTWSettingsService settingsValueForKey:kFilterTimeRangeMinutesKey] floatValue];
        [self setSliderVisible:YES animated:NO];
    } else {
        _updateResultsButton.hidden = YES;
        _endSearchButton.hidden = YES;
        _slidersViewHeight.constant = 0.0f;
        _slidersView.hidden = YES;
    }
}

#pragma mark - Search
- (void)showCurrentRoute
{
    // Маршрут
    _routePolyline.map = nil;
    
    GMSPath *path = [GMSPath pathFromEncodedPath:[BTWRouteService sharedInstance].currentRoute.polyline];
    self.routePolyline = [GMSPolyline polylineWithPath:path];
    _routePolyline.strokeWidth = 4.f;
    _routePolyline.strokeColor = [BTWStylesheet routeColor];
    _routePolyline.map = self.mapView;
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];
    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:kMapBoundsPadding]];
    
    // Маркеры
    for (GMSMarker *marker in _routeMarkersSet) {
        marker.map = nil;
    }
    
    GMSMarker *markerA = [GMSMarker markerWithPosition:[path coordinateAtIndex:0]];
    markerA.icon = [UIImage imageNamed:@"pin_location"];
    markerA.appearAnimation = kGMSMarkerAnimationPop;
    markerA.zIndex = kCurrentUserPinsZIndex;
    markerA.map = self.mapView;
    
    GMSMarker *markerB = [GMSMarker markerWithPosition:[path coordinateAtIndex:[path count] - 1]];
    markerB.groundAnchor = kFinishMarkerGroundAnchor;
    markerB.icon = [UIImage imageNamed:@"pin_finish"];
    markerB.appearAnimation = kGMSMarkerAnimationPop;
    markerB.zIndex = kCurrentUserPinsZIndex;
    markerB.map = self.mapView;
    
    self.routeMarkersSet = [NSSet setWithObjects:markerA, markerB, nil];

    CGFloat startRadiusUnits = [[BTWSettingsService settingsValueForKey:kFilterStartRadiusKey] floatValue];
    _startRangeCircle.map = nil;
    self.startRangeCircle = [GMSCircle circleWithPosition:[path coordinateAtIndex:0] radius:[BTWCommonUtils metersForCurrentDistanceUnits:startRadiusUnits]];
    _startRangeCircle.strokeColor = [UIColor clearColor];
    _startRangeCircle.fillColor = [[BTWStylesheet commonGreenColor] colorWithAlphaComponent:0.5];
    
    CGFloat finishRadiusUnits = [[BTWSettingsService settingsValueForKey:kFilterFinishRadiusKey] floatValue];
    _finishRangeCircle.map = nil;
    self.finishRangeCircle = [GMSCircle circleWithPosition:[path coordinateAtIndex:([path count] - 1)] radius:[BTWCommonUtils metersForCurrentDistanceUnits:finishRadiusUnits]];
    _finishRangeCircle.strokeColor = [UIColor clearColor];
    _finishRangeCircle.fillColor = [[BTWStylesheet finishPinColor] colorWithAlphaComponent:0.5];
    
    [self updateResults];
    [self updateAppearence];
}

- (void)updateResults
{
    [[BTWRouteService sharedInstance] getSearchResultsWithCompletion:^(NSArray *users, NSError *error)
    {
        if (!error) {
            if ([users count] == 0) {
                NSString *title = NSLocalizedString(@"We haven't found anybody", nil);
                NSString *message = [[BTWSettingsService settingsValueForKey:kCommonRoleKey] isEqualToString:kUserRoleDriver] ? NSLocalizedString(@"Route: empty results for driver", nil) : NSLocalizedString(@"Route: empty results for passenger", nil);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alert show];
            }
            
            self.searchResults = users;
            [self updateUserMarkersFindClosestMatch:YES];
        }
    }];
}

- (void)updateUserMarkersFindClosestMatch:(BOOL)findClosestMatch
{
    if (!_userMarkersSet) {
        self.userMarkersSet = [NSMutableSet set];
    }
    
    CLLocationCoordinate2D meStartCoordinate = [_routePolyline.path coordinateAtIndex:0];
    CLLocationCoordinate2D meFinishCoordinate = [_routePolyline.path coordinateAtIndex:([_routePolyline.path count] - 1)];
    CLLocation *meStartLocation = [[CLLocation alloc] initWithLatitude:meStartCoordinate.latitude longitude:meStartCoordinate.longitude];
    CLLocation *meFinishLocation = [[CLLocation alloc] initWithLatitude:meFinishCoordinate.latitude longitude:meFinishCoordinate.longitude];
    CGFloat startRadiusUnits = [[BTWSettingsService settingsValueForKey:kFilterStartRadiusKey] floatValue];
    CGFloat finishRadiusUnits = [[BTWSettingsService settingsValueForKey:kFilterFinishRadiusKey] floatValue];
    
    // Фильтруем найденных пользователей в соответствии со значениями слайдеров
    NSMutableSet *filteredUsersSet = [NSMutableSet set];
    RouteUser *closestMatch = nil;
    for (RouteUser *user in _searchResults) {
        NSTimeInterval timeInterval = [user.departure_time timeIntervalSinceDate:[BTWRouteService sharedInstance].currentRoute.date];

        // Сохраняем ближайшее по времени совпадение
        if (closestMatch) {
            if (fabs(timeInterval) < fabs([closestMatch.departure_time timeIntervalSinceDate:[BTWRouteService sharedInstance].currentRoute.date])) {
                closestMatch = user;
            }
        } else {
            closestMatch = user;
        }
        
        CGFloat filterSeconds = [[BTWSettingsService settingsValueForKey:kFilterTimeRangeMinutesKey] floatValue] * 60;
        if (filterSeconds == 0) {
            if (timeInterval < -kTimerSliderStep * 60 || timeInterval > kTimerSliderStep * 60) {
                continue;
            }
        } else {
            if ((filterSeconds > 0 && (timeInterval < 0 || timeInterval > filterSeconds)) ||
                (filterSeconds < 0 && (timeInterval > 0 || timeInterval < filterSeconds)))
            {
                continue;
            }
        }
        
        GMSPath *userPath = [GMSPath pathFromEncodedPath:user.polyline];
        CLLocationCoordinate2D userStartCoordinate = [userPath coordinateAtIndex:0];
        CLLocation *userStartLocation = [[CLLocation alloc] initWithLatitude:userStartCoordinate.latitude longitude:userStartCoordinate.longitude];
        if ([meStartLocation distanceFromLocation:userStartLocation] > [BTWCommonUtils metersForCurrentDistanceUnits:startRadiusUnits]) {
            continue;
        }
        
        CLLocationCoordinate2D userFinishCoordinate = [userPath coordinateAtIndex:([userPath count] - 1)];
        CLLocation *userFinishLocation = [[CLLocation alloc] initWithLatitude:userFinishCoordinate.latitude longitude:userFinishCoordinate.longitude];
        if ([meFinishLocation distanceFromLocation:userFinishLocation] > [BTWCommonUtils metersForCurrentDistanceUnits:finishRadiusUnits]) {
            continue;
        }
        
        [filteredUsersSet addObject:user];
    }
    
    // показываем ближайшего по времени пользователя
    if (findClosestMatch && [filteredUsersSet count] == 0 && closestMatch) {
        [filteredUsersSet addObject:closestMatch];
        
        NSTimeInterval timeInterval = [closestMatch.departure_time timeIntervalSinceDate:[BTWRouteService sharedInstance].currentRoute.date];
        NSInteger updatedValue = (timeInterval > 0 ? 1 : -1) * ceilf((fabs(timeInterval) / 60) / kTimerSliderStep) * kTimerSliderStep;
        _timeRangeSlider.value = updatedValue;
        [BTWSettingsService setSettingsValue:@(updatedValue) forKey:kFilterTimeRangeMinutesKey];
        [self updateTimeRangeLabel];

        GMSPath *userPath = [GMSPath pathFromEncodedPath:closestMatch.polyline];
        CLLocationCoordinate2D userStartCoordinate = [userPath coordinateAtIndex:0];
        CLLocationCoordinate2D userFinishCoordinate = [userPath coordinateAtIndex:([userPath count] - 1)];
        CLLocation *userStartLocation = [[CLLocation alloc] initWithLatitude:userStartCoordinate.latitude longitude:userStartCoordinate.longitude];
        CLLocation *userFinishLocation = [[CLLocation alloc] initWithLatitude:userFinishCoordinate.latitude longitude:userFinishCoordinate.longitude];
        CGFloat startDistance = [meStartLocation distanceFromLocation:userStartLocation];
        CGFloat finishDistance = [meFinishLocation distanceFromLocation:userFinishLocation];
        if (startDistance > [BTWCommonUtils metersForCurrentDistanceUnits:startRadiusUnits]) {
            CGFloat units = [BTWCommonUtils currentDistanceUnitsForMeters:startDistance];
            _startRangeCircle.radius = startDistance;
            _startRangeSlider.value = units;
            [BTWSettingsService setSettingsValue:@(units) forKey:kFilterStartRadiusKey];
        }
        if (finishDistance > [BTWCommonUtils metersForCurrentDistanceUnits:finishRadiusUnits]) {
            CGFloat units = [BTWCommonUtils currentDistanceUnitsForMeters:finishDistance];
            _finishRangeCircle.radius = finishDistance;
            _finishRangeSlider.value = units;
            [BTWSettingsService setSettingsValue:@(units) forKey:kFilterFinishRadiusKey];
        }
    }
    
    // Скрываем иформацию о пользователе, если его больше нет в выборке
    if (_userInfoVC && ![filteredUsersSet containsObject:_userInfoVC.user]) {
        [self hideUserInfoView];
    }

    // Определяем маркеры для добавления и удаления
    NSSet *mapUsersSet = [_userMarkersSet valueForKey:@"userData"];

    NSMutableSet *annotationsToRemoveSet = [mapUsersSet mutableCopy];
    [annotationsToRemoveSet minusSet:filteredUsersSet];
    NSMutableSet *markersToRemove = [NSMutableSet set];
    for (GMSMarker *marker in _userMarkersSet) {
        if ([annotationsToRemoveSet containsObject:marker.userData]) {
            marker.map = nil;
            [markersToRemove addObject:marker];
        }
    }
    [_userMarkersSet minusSet:markersToRemove];
    
    NSMutableSet *annotationsToAddSet = [filteredUsersSet mutableCopy];
    [annotationsToAddSet minusSet:mapUsersSet];
    
    // Добавляем отфильтрованных пользователей на карту
    NSInteger i = 0;
    for (RouteUser *user in annotationsToAddSet) {
        GMSMarker *marker = [GMSMarker markerWithPosition:user.coordinate];
        marker.userData = user;
        marker.icon = [BTWMapAnnotationView markerImageForUser:user];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.zIndex = kFoundUserPinsZIndex;
        marker.map = self.mapView;
        [_userMarkersSet addObject:marker];
        i++;
    }
}

- (void)processSearchCompletion
{
    // Пользователи
    for (GMSMarker *marker in _userMarkersSet) {
        marker.map = nil;
    }
    self.userMarkersSet = nil;
    
    // Точки маршрута
    for (GMSMarker *marker in _routeMarkersSet) {
        marker.map = nil;
    }
    self.routeMarkersSet = nil;
    
    // Маршрут
    _routePolyline.map = nil;
    self.routePolyline = nil;

    // Маршрут пользователя
    _userPolyline.map = nil;
    self.userPolyline = nil;
    
    // Области поиска
    _startRangeCircle.map = nil;
    _finishRangeCircle.map = nil;
    self.startRangeCircle = nil;
    self.finishRangeCircle = nil;
    
    [self updateAppearence];
}

- (void)routeSearchFinished:(NSNotification *)notification
{
    [self processSearchCompletion];
}

#pragma mark - User Info view
- (void)showInfoForUser:(RouteUser *)user
{
    if (!_userInfoVC) {
        // Попап
        BTWMapUserInfoViewController *mapUserInfoVC = [[BTWMapUserInfoViewController alloc] initWithNibName:nil bundle:nil];
        mapUserInfoVC.view.centerX = self.view.centerX;
        mapUserInfoVC.view.top = 80.0f;
        
        __weak typeof(self) wself = self;
        mapUserInfoVC.infoBlock = ^{
            BTWUserProfileViewController *profileController = (BTWUserProfileViewController *)[[BTWTransitionsManager sharedInstance] viewControllerOfType:BTWViewControllerTypeUserProfile];
            profileController.user = user;
            [wself.containerController.navigationController pushViewController:profileController animated:YES];
        };

        mapUserInfoVC.likeBlock = ^{
            if (![[BTWUserService sharedInstance].authorizedUser.phone_verified boolValue]) {
                // Показываем попап с установкой номера телефона
                [wself hideUserInfoView];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    BTWPhoneNumberViewController *viewController = [[BTWTransitionsManager sharedInstance] showPhoneNumberPopup];
                    viewController.completionBlock = ^{
                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
                        [wself offerRideToUser:user withCompletion:^(NSError *error){
                            [hud handleCompletionForSuccess:(error ? NO : YES)];
                            if (!error) {
                                hud.labelText = NSLocalizedString(@"Route offer sent", nil);
                            }
                        }];
                    };
                });
            } else {
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
                [wself offerRideToUser:user withCompletion:^(NSError *error){
                    [hud handleCompletionForSuccess:(error ? NO : YES)];
                    if (!error) {
                        hud.labelText = NSLocalizedString(@"Route offer sent", nil);
                    }
                }];
            }
        };
        
        mapUserInfoVC.hideBlock = ^{
            [wself hideUserInfoView];
        };

        self.userInfoVC = mapUserInfoVC;
        
        PopupSegue *segue = [[PopupSegue alloc] initWithIdentifier:@"userInfoPopup" source:self destination:mapUserInfoVC];
        [segue perform];
    }
    
    _userInfoVC.user = user;
    
    // Маршрут
    GMSPath *userPath = [GMSPath pathFromEncodedPath:user.polyline];
    
    _userPolyline.map = nil;
    self.userPolyline = [GMSPolyline polylineWithPath:userPath];
    _userPolyline.strokeWidth = 4.f;
    _userPolyline.strokeColor = [BTWStylesheet userRouteColor];
    _userPolyline.map = self.mapView;
}

- (void)offerRideToUser:(RouteUser *)user withCompletion:(BTWErrorBlock)completion
{
    [completion copy];
    
    [[BTWRouteService sharedInstance] offerRideToUser:user withCompletionHandler:^(NSError *error)
    {
        // Удаляем пользователя с карты
        if (!error || ([error.domain isEqualToString:kBTWAPIErrorDomain] && error.code == BTWApiErrorCodeDriveSearchQueryNotFound))
        {
            self.searchResults = [self.searchResults filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (backend_id = %@)", user.backend_id]];
            GMSMarker *userMarker = nil;
            for (GMSMarker *marker in self.userMarkersSet) {
                if ([((User *)marker.userData).backend_id isEqualToString:user.backend_id]) {
                    userMarker = marker;
                    break;
                }
            }
            if (userMarker) {
                [self.userMarkersSet removeObject:userMarker];
                userMarker.map = nil;
            }
        }

        if (completion) {
            completion(error);
        }
    }];
}

- (void)hideUserInfoView
{
    if (_userInfoVC) {
        PopupUnwindSegue *unwindSegue = [[PopupUnwindSegue alloc] initWithIdentifier:@"popupUnwindSegue" source:_userInfoVC  destination:self];
        [unwindSegue perform];
        self.userInfoVC = nil;
    }
    
    _userPolyline.map = nil;
    self.userPolyline = nil;
}

#pragma mark - GMSMapView delegate
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    if ([_userMarkersSet containsObject:marker]) {
        [self showInfoForUser:marker.userData];
        _selectedMarker.zIndex = kFoundUserPinsZIndex;
        self.selectedMarker = marker;
        _selectedMarker.zIndex = kFoundUserPinsZIndex + 1;
    }
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self hideUserInfoView];
}

#pragma mark - Utils
- (void)locationManagerDidSetInitialLocation:(NSNotification *)notificaiton
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLocationManagerDidSetInitialLocationNotification object:nil];
    
    CLLocationCoordinate2D coordinate = [BTWLocationManager sharedInstance].currentLocation.coordinate;
    [self.mapView animateToLocation:coordinate];
}

- (void)distanceUnitsChanged:(NSNotification *)notification
{
    CGFloat startRadiusUnits = [[BTWSettingsService settingsValueForKey:kFilterStartRadiusKey] floatValue];
    CGFloat finishRadiusUnits = [[BTWSettingsService settingsValueForKey:kFilterFinishRadiusKey] floatValue];

    _startRangeCircle.radius = [BTWCommonUtils metersForCurrentDistanceUnits:startRadiusUnits];
    _finishRangeCircle.radius = [BTWCommonUtils metersForCurrentDistanceUnits:finishRadiusUnits];

    [self updateUserMarkersFindClosestMatch:NO];
    [self updateScaleLabel];
    [self updateSlidersLabels];
}

#pragma mark - Actions
- (IBAction)actionUpdate:(UIButton *)sender
{
    [self updateResults];
}

- (IBAction)actionEndSearch:(UIButton *)sender
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = NSLocalizedString(@"Finishing search...", nil);
    
    [[BTWRouteService sharedInstance] endSearchWithCompletion:^(NSError *error){
        [hud hide:YES];
        [self processSearchCompletion];
        [_containerController showRoutePicker];
    }];
}

- (IBAction)actionNavigationSegmentedControl:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0) {
        [_containerController showRoutePicker];
    }
    sender.selectedSegmentIndex = 1;
}

#pragma mark - Sliders
- (IBAction)handleSlidersViewPan:(UIPanGestureRecognizer *)sender
{
    static CGPoint previousLocation;
    static BOOL wasHidden;
    
    CGPoint currentLocation = [sender locationInView:self.view];
    if (sender.state == UIGestureRecognizerStateBegan) {
        [_slidersViewHeight pop_removeAllAnimations];
        wasHidden = (_slidersViewHeight.constant < kSlidersViewVisibleHeight/2);
    } else if (sender.state == UIGestureRecognizerStateChanged) {
        _slidersViewHeight.constant = MAX(kSlidersViewHiddenHeight, _slidersViewHeight.constant + (previousLocation.y - currentLocation.y));
        [_slidersView setNeedsLayout];
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        BOOL showSliders;
        if (wasHidden) {
            showSliders = _slidersViewHeight.constant > kSlidersViewHiddenHeight + kSlidersViewVisibilityOffset;
        } else {
            showSliders = _slidersViewHeight.constant > kSlidersViewVisibleHeight - kSlidersViewVisibilityOffset;
        }
        [self setSliderVisible:showSliders animated:YES];
    }
    previousLocation = currentLocation;
}

- (void)setSliderVisible:(BOOL)showSliders animated:(BOOL)animated
{
    _slidersView.hidden = NO;
    _startRangeCircle.map = showSliders ? self.mapView : nil;
    _finishRangeCircle.map = showSliders ? self.mapView : nil;
    
    CGFloat heightValue = showSliders ? kSlidersViewVisibleHeight + kCentralButtonOffset : kSlidersViewHiddenHeight + kCentralButtonOffset;
    if (animated) {
        POPSpringAnimation *animation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayoutConstraintConstant];
        animation.toValue = @(heightValue);
        animation.springBounciness = 10.0;
        animation.springSpeed = 20.0f;
        [_slidersViewHeight pop_addAnimation:animation forKey:@"animation"];
    } else {
        _slidersViewHeight.constant = heightValue;
        [_slidersView setNeedsLayout];
    }
    
    if (!showSliders) {
        [_sliderValuePopover dismissPopoverAnimated:YES];
        self.sliderValuePopover = nil;
    }
}

- (NSString *)timeRangeStringForMinutes:(CGFloat)minutes
{
    NSString *prefix = minutes == 0 ? @"" : (minutes > 0 ? @"+ " : @"- ");
    NSString *resultString = [NSString stringWithFormat:@"%@%@", prefix, [BTWCommonUtils timeStringForMinutes:minutes]];
    return resultString;
}

- (void)updateTimeRangeLabel
{
    _timeRangeLabel.text = [self timeRangeStringForMinutes:[[BTWSettingsService settingsValueForKey:kFilterTimeRangeMinutesKey] floatValue]];
    CGFloat thumbCenterX = _timeRangeSlider.x + (_timeRangeSlider.value - _timeRangeSlider.minimumValue)/(_timeRangeSlider.maximumValue - _timeRangeSlider.minimumValue) * _timeRangeSlider.frameWidth;
    _timeRangeLeftConstraint.constant = MAX(_timeRangeSlider.x, MIN(self.view.boundsWidth - _timeRangeLabel.boundsWidth, floorf(thumbCenterX - _timeRangeLabel.boundsWidth/2)));
}

- (void)updateSlidersLabels
{
    NSString *lowerValueString, *upperValueString;
    BTWDistanceUnits units = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue];
    if (units == BTWDistanceUnitsKilometers) {
        lowerValueString = [NSString stringWithFormat:@"500 %@", NSLocalizedString(@"m", nil)];
        upperValueString = [NSString stringWithFormat:@"30 %@", NSLocalizedString(@"km", nil)];
    } else {
        lowerValueString = [NSString stringWithFormat:@"0.5 %@", NSLocalizedString(@"ml", nil)];
        upperValueString = [NSString stringWithFormat:@"30 %@", NSLocalizedString(@"ml", nil)];
    }
    
    _startRangeLowerValueLabel.text = lowerValueString;
    _finishRangeLowerValueLabel.text = lowerValueString;
    _startRangeUpperValueLabel.text = upperValueString;
    _finishRangeUpperValueLabel.text = upperValueString;
}

- (NSString *)valueStringForSlider:(UISlider *)sender
{
    NSString *valueString;
    BTWDistanceUnits units = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue];
    if ([sender isEqual:_startRangeSlider]) {
        valueString = [NSString stringWithFormat:@"%.2f %@", sender.value, [BTWCommonUtils shortStringForUnits:units]];
    } else if ([sender isEqual:_finishRangeSlider]) {
        valueString = [NSString stringWithFormat:@"%.2f %@", sender.value, [BTWCommonUtils shortStringForUnits:units]];
    } else if ([sender isEqual:_timeRangeSlider]) {
        valueString = [self timeRangeStringForMinutes:(floorf(sender.value / kTimerSliderStep) * kTimerSliderStep)];
    }
    return valueString;
}

- (CGRect)popupFrameForSlider:(UISlider *)sender
{
    CGFloat thumbWidth = 30.0f;
    CGRect rect;
    rect.origin.x = (sender.x + thumbWidth/2) + (sender.frameWidth - thumbWidth) * (sender.value - sender.minimumValue)/(sender.maximumValue - sender.minimumValue);
    rect.origin.y = [self.view convertPoint:sender.origin fromView:sender.superview].y;
    rect.size = (CGSize){1.0, 1.0};
    return rect;
}

- (IBAction)sliderTouchDown:(UISlider *)sender
{
    BTWSliderValueViewController *contentController = [[BTWSliderValueViewController alloc] initWithNibName:nil bundle:nil];
    [contentController view];
    contentController.titleLabel.text = [self valueStringForSlider:sender];
    WYPopoverController *popover = [[WYPopoverController alloc] initWithContentViewController:contentController];
    popover.popoverContentSize = contentController.view.size;
    
    WYPopoverTheme *theme = [WYPopoverController defaultTheme];
    theme.borderWidth = 0.0f;
    theme.overlayColor = [UIColor clearColor];
    theme.fillTopColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    theme.fillBottomColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    popover.theme = theme;
    
    [popover presentPopoverFromRect:[self popupFrameForSlider:sender] inView:self.view permittedArrowDirections:WYPopoverArrowDirectionDown animated:YES];
    
    self.sliderValuePopover = popover;
}

- (IBAction)sliderTouchUp:(UISlider *)sender
{
    if ([sender isEqual:_timeRangeSlider]) {
        sender.value = [[BTWSettingsService settingsValueForKey:kFilterTimeRangeMinutesKey] floatValue];
    }

    [_sliderValuePopover dismissPopoverAnimated:YES];
    self.sliderValuePopover = nil;
}

- (IBAction)sliderValueChanged:(UISlider *)sender
{
    // Меняем значения в настройках и делаем специфичные изменения в интерфейсе
    id value; NSString *key;
    if ([sender isEqual:_startRangeSlider]) {
        value = @(sender.value);
        key = kFilterStartRadiusKey;
        _startRangeCircle.radius = [BTWCommonUtils metersForCurrentDistanceUnits:sender.value];
    } else if ([sender isEqual:_finishRangeSlider]) {
        value = @(sender.value);
        key = kFilterFinishRadiusKey;
        _finishRangeCircle.radius = [BTWCommonUtils metersForCurrentDistanceUnits:sender.value];
    } else if ([sender isEqual:_timeRangeSlider]) {
        value = @(floorf(sender.value / kTimerSliderStep) * kTimerSliderStep);
        key = kFilterTimeRangeMinutesKey;
        [self updateTimeRangeLabel];
    }
    [BTWSettingsService setSettingsValue:value forKey:key];
    
    // Обновляем круги
    [self updateUserMarkersFindClosestMatch:NO];
    
    // Перемещаем попап
    ((BTWSliderValueViewController *)_sliderValuePopover.contentViewController).titleLabel.text = [self valueStringForSlider:sender];
    _sliderValuePopover.rect = [self popupFrameForSlider:sender];
    [_sliderValuePopover positionPopover:NO];
}

@end