//
//  BTWMapViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMapViewController.h"

@class BTWRouteContainerViewController;
@interface BTWRouteMapViewController : BTWMapViewController

@property (nonatomic, weak) BTWRouteContainerViewController *containerController;

- (void)showCurrentRoute;

@end
