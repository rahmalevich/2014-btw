//
//  BTWMapViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMapViewController.h"
#import "BTWMapConstants.h"
#import "BTWLocationManager.h"

@interface BTWMapViewController () <GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *scaleDistanceView;
@property (weak, nonatomic) IBOutlet UIView *scaleView;
@property (weak, nonatomic) IBOutlet UILabel *scaleLabel;
@property (weak, nonatomic) IBOutlet UIButton *zoomOutButton;
@end

@implementation BTWMapViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CLLocationCoordinate2D coordinate = [BTWLocationManager sharedInstance].currentLocation.coordinate;
    _mapView.camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:kDefaultZoomLevel];
    _mapView.delegate = self;
    _mapView.myLocationEnabled = YES;
    _mapView.settings.rotateGestures = NO;
    _mapView.settings.tiltGestures = NO;
    
    _scaleDistanceView.layer.borderWidth = 1.0f;
    _scaleDistanceView.layer.borderColor = [BTWStylesheet commonGrayColor].CGColor;
    
    [self updateScaleLabel];
}

- (void)updateScaleLabel
{
    CLLocationCoordinate2D middleLeftCoord = [_mapView.projection coordinateForPoint:_scaleLabel.origin];
    CLLocationCoordinate2D middleRightCoord = [_mapView.projection coordinateForPoint:CGPointMake(_scaleLabel.origin.x + _scaleLabel.boundsWidth, _scaleLabel.origin.y)];
    CGFloat meters = GMSGeometryDistance(middleLeftCoord, middleRightCoord);
    
    _scaleLabel.text = [BTWCommonUtils scaleStringForMeters:meters];
}

#pragma mark - GMSMapView delegate
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    [self updateScaleLabel];
}

#pragma mark - Actions
- (IBAction)actionUserLocation:(UIButton *)sender
{
    [_mapView animateToLocation:_mapView.myLocation.coordinate];
}

- (IBAction)actionZoomIn:(UIButton *)sender
{
    [_mapView animateToZoom:(_mapView.camera.zoom + 1)];
}

- (IBAction)actionZoomOut:(UIButton *)sender
{
    [_mapView animateToZoom:(_mapView.camera.zoom - 1)];
}

@end
