//
//  BTWRouteContainerViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRouteContainerViewController.h"
#import "BTWSwipeBetweenViewControllers.h"
#import "BTWSelectRouteViewController.h"
#import "BTWRouteMapViewController.h"
#import "BTWTransitionsManager.h"
#import "BTWRouteService.h"
#import "BTWUserService.h"

@interface BTWRouteContainerViewController ()
@property (nonatomic, strong) BTWSwipeBetweenViewControllers *swipeControllersContainer;
@end

@implementation BTWRouteContainerViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.swipeControllersContainer = [BTWSwipeBetweenViewControllers new];
    
    BTWRouteMapViewController *mapViewController = (BTWRouteMapViewController *)[[BTWTransitionsManager sharedInstance] viewControllerWithIdentifier:@"mapViewController"];
    BTWSelectRouteViewController *selectRouteViewController = (BTWSelectRouteViewController *)[[BTWTransitionsManager sharedInstance] viewControllerWithIdentifier:@"selectRouteViewController"];
    mapViewController.containerController = self;
    selectRouteViewController.containerController = self;
    _swipeControllersContainer.viewControllerArray = [@[selectRouteViewController, mapViewController] mutableCopy];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn:) name:kUserLoggedInNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeSearchFinished:) name:kRouteSearchFinishedNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    _swipeControllersContainer.view.frame = self.view.bounds;
    _swipeControllersContainer.hideHeader = YES;
    _swipeControllersContainer.disableScrolling = YES;
    
    [self.view addSubview:_swipeControllersContainer.view];
    [self addChildViewController:_swipeControllersContainer];
    [_swipeControllersContainer didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)userLoggedIn:(NSNotification *)notification
{
    if ([BTWRouteService sharedInstance].currentRoute) {
        [self showCurrentRouteOnMap];
    }
}

- (void)showRoutePicker
{
    [_swipeControllersContainer setCurrentPageIndex:0];
}

- (void)showMap
{
    [_swipeControllersContainer setCurrentPageIndex:1];
}

- (void)showCurrentRouteOnMap
{
    [_swipeControllersContainer setCurrentPageIndex:1];
    
    BTWRouteMapViewController *mapViewController = (BTWRouteMapViewController *)(_swipeControllersContainer.viewControllerArray[1]);
    [mapViewController view];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [mapViewController showCurrentRoute];
    });
}

- (void)routeSearchFinished:(NSNotification *)notification
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"Companions search ended but you can start a new one", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [alert show];
    
    [_swipeControllersContainer setCurrentPageIndex:0];
}

@end
