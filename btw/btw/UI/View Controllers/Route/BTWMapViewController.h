//
//  BTWMapViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "GoogleMaps.h"

@interface BTWMapViewController : BTWViewController

@property (weak, nonatomic, readonly) GMSMapView *mapView;

- (void)updateScaleLabel;

@end
