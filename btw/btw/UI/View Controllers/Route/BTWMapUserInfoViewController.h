//
//  BTWMapUserInfoViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWMapUserInfoViewController : BTWViewController

@property (nonatomic, strong) RouteUser *user;
@property (nonatomic, copy) BTWVoidBlock infoBlock;
@property (nonatomic, copy) BTWVoidBlock likeBlock;
@property (nonatomic, copy) BTWVoidBlock hideBlock;

@end
