//
//  BTWRouteHistoryTableCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWRouteHistoryTableCell : UITableViewCell

@property (nonatomic, strong) Route *route;

+ (CGFloat)cellHeight;
- (void)setSeparatorHidden:(BOOL)hidden;

@end
