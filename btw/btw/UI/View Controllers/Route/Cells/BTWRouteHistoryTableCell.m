//
//  BTWRouteHistoryTableCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRouteHistoryTableCell.h"

@interface BTWRouteHistoryTableCell ()
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLabel;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@end

@implementation BTWRouteHistoryTableCell

+ (CGFloat)cellHeight
{
    return 85.0f;
}

- (void)awakeFromNib
{
    _separatorImageView.image = [UIImage imageWithColor:[BTWStylesheet separatorColor] size:_separatorImageView.size];
}

- (void)setRoute:(Route *)route
{
    if (![_route isEqual:route]) {
        _route = route;
        _fromLabel.text = route.startPointAddress;
        _toLabel.text = route.endPointAddress;
    }
}

- (void)setSeparatorHidden:(BOOL)hidden
{
    _separatorImageView.hidden = hidden;
}

@end
