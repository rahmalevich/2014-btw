//
//  BTWSelectRouteViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSelectRouteViewController.h"
#import "BTWRouteContainerViewController.h"
#import "BTWRouteHistoryTableCell.h"
#import "BTWFormattingService.h"
#import "BTWLocationManager.h"
#import "BTWRouteService.h"
#import "BTWSwipeBetweenViewControllers.h"
#import "BTWSettingsService.h"
#import "BTWUserService.h"
#import "BTWSettingsAgreementViewController.h"
#import "BTWSystemVersionUtils.h"
#import "BTWQuestionsService.h"
#import "BTWTransitionsManager.h"

#import "BTWRangeSliderWithLabels.h"
#import "BTWRatingView.h"
#import "PopoverButton.h"
#import "PopoverTable.h"
#import "InsetTextField.h"
#import "RoundedBorderButton.h"
#import "KeyboardListener.h"

#import "RMDateSelectionViewController.h"
#import "MBProgressHUD.h"
#import "UIAlertView+Blocks.h"

static NSString * const kRouteHistoryCell = @"routeHistoryCell";
static CGFloat const kStaticHeaderHeight = 449.0f;

static NSInteger const kDriverGenderButtonTag = 100;
static NSInteger const kCarBrandButtonTag = 101;
static NSInteger const kCarModelButtonTag = 102;
static NSInteger const kCarColorButtonTag = 103;
static NSInteger const kCarYearButtonTag = 104;
static NSInteger const kSmokingAllowedButtonTag = 105;

@interface BTWRouteContainerViewController (Protected)
@property (nonatomic, strong) BTWSwipeBetweenViewControllers *swipeControllersContainer;
@end

@interface BTWSelectRouteViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, NSFetchedResultsControllerDelegate, UIGestureRecognizerDelegate,  PopoverButtonDelegate, BTWRatingViewProtocol>

@property (nonatomic, strong) Route *currentRoute;
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, strong) PopoverTable *popoverTable;
@property (nonatomic, strong) NSFetchedResultsController *routesHistoryController;

@property (weak, nonatomic) IBOutlet UISegmentedControl *navigationSegmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *roleDriverButton;
@property (weak, nonatomic) IBOutlet UIButton *rolePassengerButton;
@property (weak, nonatomic) IBOutlet UILabel *driverLabel;
@property (weak, nonatomic) IBOutlet UILabel *passengerLabel;
@property (weak, nonatomic) IBOutlet UILabel *historyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectRoleLabel;
@property (weak, nonatomic) IBOutlet UIButton *timeButton;
@property (weak, nonatomic) IBOutlet UIButton *driverSurveyButton;
@property (weak, nonatomic) IBOutlet UIButton *passengerSurveyButton;
@property (weak, nonatomic) IBOutlet UIButton *findRouteButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIView *findRouteView;
@property (weak, nonatomic) IBOutlet UIView *driverSurveyView;
@property (weak, nonatomic) IBOutlet UIView *passengerSurveyView;
@property (weak, nonatomic) IBOutlet UIView *routeButtonsContainerView;
@property (weak, nonatomic) IBOutlet UITextField *fromTextField;
@property (weak, nonatomic) IBOutlet UITextField *toTextField;
@property (weak, nonatomic) IBOutlet UIImageView *driverDisclosureImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passengerDisclosureImageView;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *surveyFieldsArray;

@property (weak, nonatomic) IBOutlet BTWRangeSliderWithLabels *driverAgeSliderView;
@property (weak, nonatomic) IBOutlet PopoverButton *driverGenderButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carBrandButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carModelButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carColorButton;
@property (weak, nonatomic) IBOutlet PopoverButton *carYearButton;
@property (weak, nonatomic) IBOutlet InsetTextField *carNumberTextField;
@property (strong, nonatomic) IBOutletCollection(PopoverButton) NSArray *smokingAllowedButtonsCollection;

@end

@implementation BTWSelectRouteViewController

#pragma mark - Object lifecycle
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.localContext = [NSManagedObjectContext MR_context];
    self.currentRoute = [Route newRouteInContext:_localContext];
    if ([BTWRouteService sharedInstance].currentRoute) {
        [_currentRoute copyValuesFromRoute:[BTWRouteService sharedInstance].currentRoute];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(routeSearchFinished:) name:kRouteSearchFinishedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(carsUpdated:) name:kQuestionsServiceDidFinishToUpdateCars object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsUpdated:) name:kQuestionsServiceDidUpdateQuestions object:nil];
    
    [[KeyboardListener sharedInstance] addObserver:self forKeyPath:@"isKeyboardVisible" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc
{
    [_localContext rollback];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[KeyboardListener sharedInstance] removeObserver:self forKeyPath:@"isKeyboardVisible"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.routesHistoryController = [Route MR_fetchAllGroupedBy:nil withPredicate:[NSPredicate predicateWithFormat:@"is_current = %@", @(NO)] sortedBy:@"search_date" ascending:NO delegate:self];
    
    for (UIView *aView in _surveyFieldsArray) {
        aView.layer.cornerRadius = 5.0f;
    }
    
    [_tableView registerNib:[UINib nibWithNibName:@"BTWRouteHistoryTableCell" bundle:nil] forCellReuseIdentifier:kRouteHistoryCell];
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.boundsWidth, kCentralButtonOffset)];
    footerView.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = footerView;

    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapRecognized:)];
    tapRecognizer.cancelsTouchesInView = NO;
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
    
    // Кнопки
    _routeButtonsContainerView.layer.cornerRadius = 5.0f;
    _routeButtonsContainerView.layer.borderWidth = 2.0f;
    _routeButtonsContainerView.layer.borderColor = [BTWStylesheet commonGreenColor].CGColor;
    [_findRouteButton setBackgroundImage:[UIImage imageWithColor:[BTWStylesheet commonGreenColor] size:_findRouteButton.size] forState:UIControlStateNormal];
    [_clearButton setBackgroundImage:[UIImage imageWithColor:_clearButton.backgroundColor size:_clearButton.size] forState:UIControlStateNormal];
    
    // Стартовые значения
    _fromTextField.text = _currentRoute.startPointAddress ?: @"";
    _toTextField.text = _currentRoute.endPointAddress ?: @"";
    NSDictionary *attributes = @{NSFontAttributeName: _fromTextField.font, NSForegroundColorAttributeName: _fromTextField.textColor};
    _fromTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Route start", nil) attributes:attributes];
    _toTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Route finish", nil) attributes:attributes];
    
    NSString *dateString = _currentRoute.date ? [[BTWFormattingService shortDateTimeFormatter] stringFromDate:_currentRoute.date] : NSLocalizedString(@"Select time", nil);
    [_timeButton setTitle:dateString forState:UIControlStateNormal];
    
    NSString *role = [BTWSettingsService settingsValueForKey:kCommonRoleKey];
    [self setupRole:role animated:NO];
    
    [self updateAppearence];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (![BTWQuestionsService sharedInstance].carsUpdated) {
        [[BTWQuestionsService sharedInstance] updateCars];
    }
    
    [self setupSurveys];
}

- (void)setupSurveys
{
    // Значения для анкет
    // # возраст водителя
    _driverAgeSliderView.slider.minimumValue = kFilterDefaultLowerAge;
    _driverAgeSliderView.slider.maximumValue = kFilterDefaultUpperAge;
    _driverAgeSliderView.slider.upperValue = [[BTWSettingsService settingsValueForKey:kFilterDriverUpperAgeKey] floatValue] ?: kFilterDefaultLowerAge;
    _driverAgeSliderView.slider.lowerValue = [[BTWSettingsService settingsValueForKey:kFilterDriverLowerAgeKey] floatValue] ?: kFilterDefaultUpperAge;
    _driverAgeSliderView.slider.minimumRange = kFilterMinAgeRange;
    _driverAgeSliderView.sliderValueChangedBlock = ^(NMRangeSlider *slider){
        [BTWSettingsService setSettingsValue:@(floorf(slider.lowerValue)) forKey:kFilterDriverLowerAgeKey];
        [BTWSettingsService setSettingsValue:@(floorf(slider.upperValue)) forKey:kFilterDriverUpperAgeKey];
    };
    
    // # пол водителя
    _driverGenderButton.itemsArray = @[@(BTWFilterGenderTypeAll), @(BTWFilterGenderTypeMale), @(BTWFilterGenderTypeFemale)];
    _driverGenderButton.titleBlock = ^(NSNumber *item){
        NSString *result = nil;
        switch ([item integerValue]) {
            case BTWFilterGenderTypeAll:
                result = NSLocalizedString(@"All", nil);
                break;
            case BTWFilterGenderTypeMale:
                result = NSLocalizedString(@"Men", nil);
                break;
            case BTWFilterGenderTypeFemale:
                result = NSLocalizedString(@"Women", nil);
                break;
        }
        return result;
    };
    NSNumber *selectedItem = [BTWSettingsService settingsValueForKey:kFilterDriverGenderKey];
    _driverGenderButton.selectedItem = selectedItem;
    _driverGenderButton.tag = kDriverGenderButtonTag;
    
    // автомобиль
    Car *usersCar = [BTWUserService sharedInstance].authorizedUser.car;
    
    // # производитель автомобиля
    CarBrand *selectedBrand = [CarBrand MR_findFirstByAttribute:@"name" withValue:usersCar.brand];
    _carBrandButton.itemsArray = [CarBrand MR_findAllSortedBy:@"name" ascending:YES];
    _carBrandButton.titleBlock = ^(CarBrand *carBrand){ return carBrand.name; };
    if (selectedBrand) {
        _carBrandButton.selectedItem = selectedBrand;
    } else if ([usersCar.brand isValidString]) {
        [_carBrandButton setTitle:usersCar.brand forState:UIControlStateNormal];
    }
    _carBrandButton.tag = kCarBrandButtonTag;
    
    // # модель автомобиля
    _carModelButton.itemsArray = selectedBrand.models;
    _carModelButton.selectedItem = usersCar.model;
    _carModelButton.tag = kCarModelButtonTag;
    
    // # цвет автомобиля
    Question *colorQuestion = [Question MR_findFirstByAttribute:@"category" withValue:kColorCategoryKey];
    Answer *selectedAnswer = [[colorQuestion.answers filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"text = %@", usersCar.color]] firstObject];
    NSArray *orderedColors = [colorQuestion.answers.array sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sort_order" ascending:YES]]];
    _carColorButton.itemsArray = orderedColors;
    _carColorButton.titleBlock = ^(Answer *item){ return item.text; };
    if (selectedAnswer) {
        _carColorButton.selectedItem = selectedAnswer;
    } else {
        [_carColorButton setTitle:usersCar.color forState:UIControlStateNormal];
    }
    _carColorButton.tag = kCarColorButtonTag;
    
    // # год автомобиля
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:[NSDate date]];
    NSMutableArray *items = [NSMutableArray array];
    for (NSInteger year = [components year]; year  >= kFilterCarMinYear; year--) {
        [items addObject:[@(year) stringValue]];
    }
    _carYearButton.itemsArray = items;
    _carYearButton.selectedItem = [usersCar.year integerValue] > 0 ? [usersCar.year stringValue] : nil;
    _carYearButton.tag = kCarYearButtonTag;
    
    // # номер автомобиля
    _carNumberTextField.text = usersCar.number;
    _carNumberTextField.tintColor = [BTWStylesheet commonTextColor];
    
    // # курение в автомобиле
    for (PopoverButton *button in _smokingAllowedButtonsCollection) {
        button.itemsArray = @[@NO, @YES];
        button.titleBlock = ^(NSNumber *item){ return [item boolValue] ? NSLocalizedString(@"Yes", nil) : NSLocalizedString(@"No", nil); };
        button.selectedItem = [BTWSettingsService settingsValueForKey:kFilterSmokingAllowedKey];
        button.tag = kSmokingAllowedButtonTag;
    }
}

- (void)updateAppearence
{
    _historyTitleLabel.hidden = ([_routesHistoryController.fetchedObjects count] == 0);
}

#pragma mark - Popover button delegate
- (void)popoverButton:(PopoverButton *)popoverButton didSelectItem:(id)item
{
    if (popoverButton.tag == kDriverGenderButtonTag) {
        [BTWSettingsService setSettingsValue:item forKey:kFilterDriverGenderKey];
    } else if (popoverButton.tag == kSmokingAllowedButtonTag) {
        [BTWSettingsService setSettingsValue:item forKey:kFilterSmokingAllowedKey];
        for (PopoverButton *button in _smokingAllowedButtonsCollection) {
            button.selectedItem = item;
        }
    } else {
        User *authorizedUser = [BTWUserService sharedInstance].authorizedUser;
        if (popoverButton.tag == kCarBrandButtonTag) {
            CarBrand *selectedBrand = (CarBrand *)item;
            if (![selectedBrand.name isEqualToString:authorizedUser.car.brand]) {
                _carModelButton.itemsArray = selectedBrand.models;
                _carModelButton.selectedItem = nil;
                authorizedUser.car.brand = selectedBrand.name;
                authorizedUser.car.model = nil;
            }
        } else if (popoverButton.tag == kCarModelButtonTag) {
            NSString *selectedModel = (NSString *)item;
            authorizedUser.car.model = selectedModel;
        } else if (popoverButton.tag == kCarColorButtonTag) {
            Answer *selectedColor = (Answer *)item;
            authorizedUser.car.color = selectedColor.text;
        } else if (popoverButton.tag == kCarYearButtonTag) {
            NSString *selectedYear = (NSString *)item;
            authorizedUser.car.year = @([selectedYear integerValue]);
        }
        [[BTWUserService sharedInstance] updateCarInfoWithCompletionHandler:nil];
    }
}

#pragma mark - Rating view delegate
- (void)starsSelectionChanged:(BTWRatingView *)ratingView rating:(CGFloat)rating
{
    [BTWSettingsService setSettingsValue:@(rating) forKey:kFilterDriverRatingKey];
}

#pragma mark - Observing keyboard state
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isEqual:[KeyboardListener sharedInstance]] && self.view.window) {
        if ([KeyboardListener sharedInstance].isKeyboardVisible) {
            [self showPopoverTable];
        } else {
            [_popoverTable.popover dismissPopoverAnimated:YES];
            [UIView animateWithDuration:0.3 animations:^{
                _tableView.contentInset = UIEdgeInsetsZero;
            }];
        }
    }
}

#pragma mark - Recognizing gestures
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint location = [touch locationInView:gestureRecognizer.view];
    UIView *subview = [gestureRecognizer.view hitTest:location withEvent:nil];
    BOOL result = ![subview isKindOfClass:[UIControl class]];
    return result;
}

- (void)backgroundTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    [[BTWUIUtils findFirstResponderForView:self.view] resignFirstResponder];
}

#pragma mark - Action
- (IBAction)actionSelectRole:(UIButton *)sender
{
    NSString *role = [sender isEqual:_roleDriverButton] ? kUserRoleDriver : kUserRolePassenger;
    [BTWSettingsService setSettingsValue:role forKey:kCommonRoleKey];
    [self setupRole:role animated:YES];
}

- (IBAction)actionSelectTime:(UIButton *)sender
{
    [self showDatePicker];
}

- (BOOL)validateFields {
    BOOL result = NO;
    
    NSString *role = [BTWSettingsService settingsValueForKey:kCommonRoleKey];
    if ( [role isValidString] &&
         [_currentRoute.startPointAddress isValidString] &&
         [_currentRoute.endPointAddress isValidString] &&
         _currentRoute.date)
    {
        if ([role isEqualToString:kUserRoleDriver]) {
            Car *car = [BTWUserService sharedInstance].authorizedUser.car;
            if ([car carIsValid]) {
                result = YES;
            } else {
                NSString *titleString = NSLocalizedString(@"Error", nil);
                NSString *messageString = NSLocalizedString(@"\nPlease fill car info", nil);
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:titleString message:messageString delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                [alertView show];
            }
        } else {
            result = YES;
        }
    } else {
        NSString *titleString = nil;
        NSString *messageString = nil;
        if (![role isValidString]) {
            titleString = NSLocalizedString(@"Undefined role", nil);
            messageString = NSLocalizedString(@"\nAre you driver or passenger?", nil);
        } else if (!_currentRoute.date) {
            titleString = NSLocalizedString(@"Undefined route", nil);
            messageString = NSLocalizedString(@"\nPlease select date and time", nil);
        } else {
            titleString = ![_currentRoute.startPointAddress isValidString] ? NSLocalizedString(@"Please select starting point", nil) : NSLocalizedString(@"Please select end point", nil);
            messageString = NSLocalizedString(@"\nPlease start to type the address and choose a value from drop-down menu", nil);
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:titleString message:messageString delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
    
    return result;
}

- (IBAction)actionFindRoute:(UIButton *)sender
{
    [_fromTextField resignFirstResponder];
    [_toTextField resignFirstResponder];
    
    if ([self validateFields])
    {
        if ([[BTWSettingsService settingsValueForKey:kLicenseSignedKey] boolValue]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = NSLocalizedString(@"Searching for companions...", nil);
        
            [[BTWRouteService sharedInstance] startSearchWithRoute:_currentRoute completion:^(NSError *error){
                [hud hide:YES];
                if (!error) {
                    [BTWSettingsService setFilterDefaults];
                    [_containerController showCurrentRouteOnMap];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Unable to start search, please try again later", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    [alert show];
                }
            }];
        } else {
            __weak typeof(self) wself = self;
            [[BTWTransitionsManager sharedInstance] showAgreementPopupWithCompletionHandler:^{
                [wself actionFindRoute:nil];
            }];
        }
    }
}

- (IBAction)actionClear:(UIButton *)sender
{
    [_currentRoute clearRoute];
    
    _fromTextField.text = nil;
    _toTextField.text = nil;
    [_timeButton setTitle:NSLocalizedString(@"Select time", nil) forState:UIControlStateNormal];
}

- (IBAction)actionExpand:(UIButton *)sender
{
    [self updateSurveyAppearence:!sender.selected animated:YES];
}

- (IBAction)actionCloseSurvey:(UIButton *)sender
{
    [self updateSurveyAppearence:NO animated:YES];
}

- (IBAction)actionNavigationSegmentedControl:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 1) {
        [_containerController showMap];
    }
    sender.selectedSegmentIndex = 0;
}

#pragma mark - Utils
- (void)showDatePicker
{
    [RMDateSelectionViewController setLocalizedTitleForCancelButton:NSLocalizedString(@"Cancel", nil)];
    [RMDateSelectionViewController setLocalizedTitleForSelectButton:NSLocalizedString(@"Select", nil)];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController dateSelectionController];
    dateSelectionController.disableBouncingWhenShowing = YES;
    dateSelectionController.hideNowButton = YES;
    dateSelectionController.datePicker.date = [BTWCommonUtils nextHourDateForDate:[NSDate date]];
    dateSelectionController.datePicker.minimumDate = [NSDate date];
    
    __weak typeof(self) wself = self;
    [dateSelectionController showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *date){
        [wself.timeButton setTitle:[[BTWFormattingService shortDateTimeFormatter] stringFromDate:date] forState:UIControlStateNormal];
        wself.currentRoute.date = date;
    } andCancelHandler:nil];
}

- (void)setupRole:(NSString *)role animated:(BOOL)animated
{
    if ([role isEqualToString:kUserRolePassenger]) {
        _roleDriverButton.selected = NO;
        _rolePassengerButton.selected = YES;
        _driverLabel.font = [BTWStylesheet commonFontOfSize:_driverLabel.font.pointSize];
        _driverLabel.textColor = [BTWStylesheet commonGrayColor];
        _passengerLabel.font = [BTWStylesheet commonBoldFontOfSize:_driverLabel.font.pointSize];
        _passengerLabel.textColor = [BTWStylesheet commonTextColor];
    } else if ([role isEqualToString:kUserRoleDriver]) {
        _roleDriverButton.selected = YES;
        _rolePassengerButton.selected = NO;
        _driverLabel.font = [BTWStylesheet commonBoldFontOfSize:_driverLabel.font.pointSize];
        _driverLabel.textColor = [BTWStylesheet commonTextColor];
        _passengerLabel.font = [BTWStylesheet commonFontOfSize:_driverLabel.font.pointSize];
        _passengerLabel.textColor = [BTWStylesheet commonGrayColor];
    } else {
        _roleDriverButton.selected = NO;
        _rolePassengerButton.selected = NO;
        _driverLabel.font = [BTWStylesheet commonFontOfSize:_driverLabel.font.pointSize];
        _driverLabel.textColor = [BTWStylesheet commonGrayTextColor];
        _passengerLabel.font = [BTWStylesheet commonFontOfSize:_driverLabel.font.pointSize];
        _passengerLabel.textColor = [BTWStylesheet commonGrayTextColor];
    }
    
    [self updateSurveyAppearence:NO animated:animated];
}

- (void)updateSurveyAppearence:(BOOL)show animated:(BOOL)animated
{
    CGFloat headerHeight;
    NSString *role = [BTWSettingsService settingsValueForKey:kCommonRoleKey];
    
    if ([role isValidString]) {
        BOOL isDriver = [role isEqualToString:kUserRoleDriver];

        UIView *surveyView = isDriver ? _driverSurveyView : _passengerSurveyView;
        UIView *oppositeSurveyView = isDriver ? _passengerSurveyView : _driverSurveyView;
        UIImageView *disclosureImageView = isDriver ? _driverDisclosureImageView : _passengerDisclosureImageView;
        disclosureImageView.image = show ? [UIImage imageNamed:@"icon_up_disclosure_arrow"] : [UIImage imageNamed:@"icon_down_disclosure_arrow"];
        
        UIButton *surveyButton = isDriver ? _driverSurveyButton : _passengerSurveyButton;
        surveyButton.selected = show;
        
        headerHeight = kStaticHeaderHeight + (show ? surveyView.frameHeight : surveyButton.frameHeight);
        
        if (surveyView.hidden) {
            if (animated) {
                surveyView.alpha = 0.0f;
                surveyView.hidden = NO;
                [UIView animateWithDuration:0.3 animations:^{
                    _selectRoleLabel.alpha = 0.0f;
                    oppositeSurveyView.alpha = 0.0f;
                    surveyView.alpha = 1.0f;
                    [self setHeaderHeight:headerHeight animated:NO];
                } completion:^(BOOL finished){
                    _selectRoleLabel.hidden = YES;
                    oppositeSurveyView.hidden = YES;
                }];
            } else {
                _selectRoleLabel.hidden = YES;
                surveyView.hidden = NO;
                oppositeSurveyView.hidden = YES;
                [self setHeaderHeight:headerHeight animated:NO];
            }
        } else {
            [self setHeaderHeight:headerHeight animated:animated];
        }
    } else {
        _selectRoleLabel.hidden = NO;
        _passengerSurveyView.hidden = YES;
        _driverSurveyView.hidden = YES;
        
        headerHeight = kStaticHeaderHeight + _selectRoleLabel.frameHeight;
        [self setHeaderHeight:headerHeight animated:animated];
    }
}

- (void)setHeaderHeight:(CGFloat)height animated:(BOOL)animated
{
    UIView *tableHeaderView = _tableView.tableHeaderView;
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
    }
    
    tableHeaderView.frameHeight = height;
    _tableView.tableHeaderView = tableHeaderView;
    [_tableView.tableHeaderView layoutIfNeeded];
    
    if (animated) {
        [UIView commitAnimations];
    }
}

- (void)showPopoverTable
{
    UIView *textField = [BTWUIUtils findFirstResponderForView:self.view];
    if ([textField isEqual:_fromTextField] || [textField isEqual:_toTextField]) {
        CGFloat padding = 15.0f;
        CGFloat keyboardHeight = [KeyboardListener sharedInstance].keyboardRect.size.height;
        CGPoint contentOffset = CGPointMake(_tableView.contentOffset.x, MIN([_tableView convertPoint:textField.origin fromView:textField.superview].y - padding, _tableView.contentSize.height - _tableView.frameHeight + keyboardHeight));
        CGFloat maxContentOffsetY = _tableView.contentSize.height - _tableView.frameHeight;
        CGFloat contentInset = MAX(contentOffset.y - maxContentOffsetY, 0.0);
        
        [UIView animateWithDuration:0.3 animations:^{
            _tableView.contentOffset = contentOffset;
            _tableView.contentInset = UIEdgeInsetsMake(0, 0, contentInset, 0);
        } completion:^(BOOL finished){
            [_popoverTable presentFromView:textField];
        }];
    }
}

- (void)setupPopoverForTextField:(UITextField *)textField
{
    self.popoverTable = [PopoverTable new];
    _popoverTable.popover.theme.overlayColor = [UIColor clearColor];
    _popoverTable.popover.theme.outerShadowColor = [UIColor lightGrayColor];
    _popoverTable.popover.theme.outerShadowOffset = CGSizeMake(1, 1);
    _popoverTable.popover.theme.outerShadowBlurRadius = 5.0f;
    _popoverTable.cellTitleBlock = ^(BTWPlacemark *item){
        return item.isCurrentLocation ? NSLocalizedString(@"My location", nil) : item.title;
    };
    _popoverTable.cellSubtitleBlock = ^(BTWPlacemark *item){
        return  item.isCurrentLocation ? nil : item.subtitle;
    };
    _popoverTable.shouldHighlightBlock = ^(BTWPlacemark *item){
        return item.isCurrentLocation;
    };
    
    BOOL isFromTextField = [textField isEqual:_fromTextField];
    Route *currentRoute = _currentRoute;
    _popoverTable.selectItemBlock = ^(BTWPlacemark *item){
        textField.text = item.title;
        if (isFromTextField) {
            currentRoute.startPointAddress = item.title;
            currentRoute.startPointReference = item.reference;
            if (CLLocationCoordinate2DIsValid(item.coordinate)) {
                currentRoute.startPointLongitude = @(item.coordinate.longitude);
                currentRoute.startPointLatitude = @(item.coordinate.latitude);
            } else {
                currentRoute.startPointLatitude = currentRoute.startPointLongitude = nil;
            }
        } else {
            currentRoute.endPointAddress = item.title;
            currentRoute.endPointReference = item.reference;
            if (CLLocationCoordinate2DIsValid(item.coordinate)) {
                currentRoute.endPointLongitude = @(item.coordinate.longitude);
                currentRoute.endPointLatitude = @(item.coordinate.latitude);
            } else {
                currentRoute.endPointLatitude = currentRoute.endPointLongitude = nil;
            }
        }
    };
}

- (void)setupPopoverItemsForString:(NSString *)string canShowCurrentLocation:(BOOL)canShowCurrentLocation
{
    __weak typeof(self) wself = self;
    if ([string isValidString]) {
        [[BTWLocationManager sharedInstance] geocodeAddress:string withCompletionHandler:^(NSArray *placemarks, NSError *error)
        {
            wself.popoverTable.itemsArray = placemarks;
        }];
    } else if (canShowCurrentLocation) {
        [[BTWLocationManager sharedInstance] geocodeCurrentLocationWithCompletionHandler:^(BTWPlacemark *placemark, NSError *error)
        {
            if (placemark) {
                wself.popoverTable.itemsArray = @[placemark];
            }
        }];
    } else {
        _popoverTable.itemsArray = @[];
    }
}

#pragma mark - Notificaitons handling
- (void)routeSearchFinished:(NSNotification *)notification
{
    [_currentRoute clearRoute];
    
    _fromTextField.text = @"";
    _toTextField.text = @"";
    [_timeButton setTitle:NSLocalizedString(@"Select time", nil) forState:UIControlStateNormal];
}

- (void)carsUpdated:(NSNotification *)notification
{
    CarBrand *selectedBrand = [CarBrand MR_findFirstByAttribute:@"name" withValue:[BTWUserService sharedInstance].authorizedUser.car.brand];
    _carBrandButton.itemsArray = [CarBrand MR_findAllSortedBy:@"name" ascending:YES];
    _carBrandButton.selectedItem = selectedBrand;
    _carModelButton.itemsArray = selectedBrand.models;
}

- (void)questionsUpdated:(NSNotification *)notification
{
    Question *colorQuestion = [Question MR_findFirstByAttribute:@"category" withValue:kColorCategoryKey];
    Answer *selectedAnswer = [[colorQuestion.answers filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"text = %@", [BTWUserService sharedInstance].authorizedUser.car.color]] firstObject];
    _carColorButton.itemsArray = [NSArray arrayWithArray:colorQuestion.answers.array];
    _carColorButton.selectedItem = selectedAnswer;
}

#pragma mark - UITableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_routesHistoryController.fetchedObjects count];;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BTWRouteHistoryTableCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTWRouteHistoryTableCell *resultCell = [tableView dequeueReusableCellWithIdentifier:kRouteHistoryCell forIndexPath:indexPath];
    [self configureCell:resultCell atIndexPath:indexPath];
    return resultCell;
}

- (void)configureCell:(BTWRouteHistoryTableCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.route = [_routesHistoryController.fetchedObjects objectAtIndex:indexPath.row];
    [cell setSeparatorHidden:indexPath.row == ([_routesHistoryController.fetchedObjects count] - 1)];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView setContentOffset:CGPointZero animated:YES];
    
    NSDate *currentDate = _currentRoute.date;
    Route *route = [_routesHistoryController.fetchedObjects objectAtIndex:indexPath.row];
    [_currentRoute copyValuesFromRoute:route];
    _currentRoute.date = currentDate;
    _fromTextField.text = route.startPointAddress;
    _toTextField.text = route.endPointAddress;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    Route *route = [_routesHistoryController.fetchedObjects objectAtIndex:indexPath.row];

    NSManagedObjectContext *context = route.managedObjectContext;
    [route MR_deleteEntity];
    [context MR_saveToPersistentStoreAndWait];
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [_tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
        {
            [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            if (controller.fetchedObjects.count > 0) {
                NSIndexPath *lastCellIndex = [NSIndexPath indexPathForRow:controller.fetchedObjects.count - 1 inSection:0];
                BTWRouteHistoryTableCell *cell = (BTWRouteHistoryTableCell *)[_tableView cellForRowAtIndexPath:lastCellIndex];
                if (cell) {
                    [self configureCell:cell atIndexPath:lastCellIndex];
                }
            }
            break;
        }
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(BTWRouteHistoryTableCell *)[_tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        case NSFetchedResultsChangeMove:
            [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [_tableView endUpdates];
    [self updateAppearence];
}

#pragma mark - UITextView delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:_fromTextField] || [textField isEqual:_toTextField]) {
        textField.attributedPlaceholder = nil;
        [self setupPopoverForTextField:textField];
        [self setupPopoverItemsForString:textField.text canShowCurrentLocation:[textField isEqual:_fromTextField]];
        [self showPopoverTable];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:_fromTextField] || [textField isEqual:_toTextField]) {
        NSString *placeholder = [textField isEqual:_fromTextField] ? NSLocalizedString(@"Route start", nil) : NSLocalizedString(@"Route finish", nil);
        NSDictionary *attributes = @{NSFontAttributeName:textField.font, NSForegroundColorAttributeName:textField.textColor};
        textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder attributes:attributes];
    } else if ([textField isEqual:_carNumberTextField]) {
        [BTWUserService sharedInstance].authorizedUser.car.number = textField.text;
        [[BTWUserService sharedInstance] updateCarInfoWithCompletionHandler:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([textField isEqual:_fromTextField] || [textField isEqual:_toTextField]) {
        [self setupPopoverItemsForString:newString canShowCurrentLocation:[textField isEqual:_fromTextField]];
    } else if ([textField isEqual:_carNumberTextField]) {
        result = newString.length <= kCarNumberMaxLength;
    }
    return result;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:_fromTextField] || [textField isEqual:_toTextField]) {
        if ([_popoverTable.itemsArray count] > 0) {
            _popoverTable.selectItemBlock(_popoverTable.itemsArray[0]);
            [_popoverTable.popover dismissPopoverAnimated:YES];
        }
    } else if ([textField isEqual:_carNumberTextField]) {
        [BTWUserService sharedInstance].authorizedUser.car.number = textField.text;
        [[BTWUserService sharedInstance] updateCarInfoWithCompletionHandler:nil];
        [textField resignFirstResponder];
    }
    return YES;
}

@end
