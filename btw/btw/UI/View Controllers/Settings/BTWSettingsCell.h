//
//  BTWSettingsCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BTWSettingsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *disclosureImageView;

@end
