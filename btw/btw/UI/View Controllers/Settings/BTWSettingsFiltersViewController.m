//
//  BTWSettingsFiltersViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSettingsFiltersViewController.h"
#import "BTWDropDownCell.h"
#import "BTWRangeSliderCell.h"
#import "BTWSettingsService.h"

static NSString *kDropDownCellID = @"kDropDownCellID";
static NSString *kRangeSliderCellID = @"kRangeSliderCellID";

@interface BTWSettingsFiltersViewController () <UITableViewDelegate, UITableViewDataSource, PopoverButtonDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation BTWSettingsFiltersViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Filters", nil);
    self.navigationItem.rightBarButtonItem = nil;
    
    [_tableView registerNib:[UINib nibWithNibName:@"BTWDropDownCell" bundle:nil] forCellReuseIdentifier:kDropDownCellID];
    [_tableView registerNib:[UINib nibWithNibName:@"BTWRangeSliderCell" bundle:nil] forCellReuseIdentifier:kRangeSliderCellID];
}

#pragma mark - PopoverButton delegate
- (void)popoverButton:(PopoverButton *)popoverButton didSelectItem:(id)item
{
    [BTWSettingsService setSettingsValue:item forKey:kFilterGenderKey];
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 0;
    if (indexPath.row == 0) {
        result = [BTWDropDownCell cellHeight];
    } else {
        result = [BTWRangeSliderCell cellHeight];
    }
    return result;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseID = (indexPath.row == 0) ? kDropDownCellID : kRangeSliderCellID;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseID forIndexPath:indexPath];
    if (indexPath.row == 0) {
        NSNumber *selectedItem = [BTWSettingsService settingsValueForKey:kFilterGenderKey];
        BTWDropDownCell *dropDownCell = (BTWDropDownCell *)cell;
        dropDownCell.backgroundColor = [BTWStylesheet settingsCellColor];
        dropDownCell.separatorView.backgroundColor = [BTWStylesheet commonBackgroundColor];
        dropDownCell.titleLabel.text = NSLocalizedString(@"Show", nil);
        dropDownCell.titleLabel.textColor = [BTWStylesheet settingsTitleColor];
        dropDownCell.popoverButton.itemsArray = @[@(BTWFilterGenderTypeAll), @(BTWFilterGenderTypeMale), @(BTWFilterGenderTypeFemale)];
        dropDownCell.popoverButton.titleBlock = ^(NSNumber *item){
            NSString *result = nil;
            switch ([item integerValue]) {
                case BTWFilterGenderTypeAll:
                    result = NSLocalizedString(@"All", nil);
                    break;
                case BTWFilterGenderTypeMale:
                    result = NSLocalizedString(@"Men", nil);
                    break;
                case BTWFilterGenderTypeFemale:
                    result = NSLocalizedString(@"Women", nil);
                    break;
            }
            return result;
        };
        dropDownCell.popoverButton.selectedItem = selectedItem;
        dropDownCell.popoverButton.shouldHighlightBlock = ^(NSNumber *item){ return [selectedItem isEqual:item]; };
        dropDownCell.popoverButton.delegate = self;
        [dropDownCell.popoverButton setBackgroundColor:[BTWStylesheet commonBackgroundColor]];
    } else {
        BTWRangeSliderCell *sliderCell = (BTWRangeSliderCell *)cell;
        sliderCell.backgroundColor = [BTWStylesheet settingsCellColor];
        sliderCell.separatorView.backgroundColor = [BTWStylesheet commonBackgroundColor];
        sliderCell.titleLabel.textColor = [BTWStylesheet settingsTitleColor];
        sliderCell.lowerValueLabel.textColor = [BTWStylesheet settingsTitleColor];
        sliderCell.upperValueLabel.textColor = [BTWStylesheet settingsTitleColor];
        
        if (indexPath.row == 1) {
            NSString *unitsString = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue] == BTWDistanceUnitsKilometers ? NSLocalizedString(@"km", nil) : NSLocalizedString(@"mi", nil);
            sliderCell.titleLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Search range, %@", nil), unitsString];
            sliderCell.lowerValueLabel.hidden = YES;
            sliderCell.slider.lowerHandleHidden = YES;
            sliderCell.slider.minimumValue = 10.0f;
            sliderCell.slider.maximumValue = 100.0f;
            sliderCell.slider.upperValue = [[BTWSettingsService settingsValueForKey:kFilterRangeKey] floatValue];
            sliderCell.sliderValueChangedBlock = ^(NMRangeSlider *slider){
                [BTWSettingsService setSettingsValue:@(floorf(slider.upperValue)) forKey:kFilterRangeKey];
            };
        } else {
            sliderCell.titleLabel.text = NSLocalizedString(@"Age range", nil);
            sliderCell.lowerValueLabel.hidden = NO;
            sliderCell.slider.lowerHandleHidden = NO;
            sliderCell.slider.minimumValue = 18.0;
            sliderCell.slider.maximumValue = 60.0;
            sliderCell.slider.upperValue = [[BTWSettingsService settingsValueForKey:kFilterUpperAgeKey] floatValue];
            sliderCell.slider.lowerValue = [[BTWSettingsService settingsValueForKey:kFilterLowerAgeKey] floatValue];
            sliderCell.slider.minimumRange = 5.0;
            sliderCell.sliderValueChangedBlock = ^(NMRangeSlider *slider){
                [BTWSettingsService setSettingsValue:@(floorf(slider.lowerValue)) forKey:kFilterLowerAgeKey];
                [BTWSettingsService setSettingsValue:@(floorf(slider.upperValue)) forKey:kFilterUpperAgeKey];
            };
        }
    }
    
    return cell;
}

@end
