//
//  BTWSettingsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSettingsViewController.h"
#import "BTWUIUtils.h"
#import "BTWCommonUtils.h"
#import "BTWUserService.h"
#import "BTWSettingsCell.h"
#import "BTWSettingsFiltersViewController.h"
#import "BTWSettingsAgreementViewController.h"
#import "BTWTransitionsManager.h"
#import "BTWTabBarController.h"

#import "RMDateSelectionViewController.h"
#import "UIImageView+WebCache.h"

@interface BTWSettingsViewController () <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIButton *metricsButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@property (nonatomic, assign) BOOL initialized;
@end

@implementation BTWSettingsViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.title = NSLocalizedString(@"Settings", nil);
    _versionLabel.text = [NSString stringWithFormat:@"v%@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    BTWDistanceUnits units = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue];
    [_metricsButton setTitle:[BTWCommonUtils stringForUnits:units] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    User *user = [BTWUserService sharedInstance].authorizedUser;
    [_photoImageView sd_setImageWithURL:[NSURL URLWithString:user.photo_url] placeholderImage:[user midPlaceholder]];
    _photoImageView.layer.cornerRadius = floorf(_photoImageView.boundsWidth/2);
    _nameLabel.text = [NSString stringWithFormat:@"%@ %@", user.name, user.surname];
}

#pragma mark - Actions
- (IBAction)actionSelectMetrics:(id)sender
{
    [RMDateSelectionViewController setLocalizedTitleForCancelButton:NSLocalizedString(@"Cancel", nil)];
    [RMDateSelectionViewController setLocalizedTitleForSelectButton:NSLocalizedString(@"Select", nil)];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController selectionControllerForObjects:@[@(BTWDistanceUnitsKilometers), @(BTWDistanceUnitsMiles)]];
    dateSelectionController.objectTitleBlock = ^(NSNumber *units){
        return [BTWCommonUtils stringForUnits:[units integerValue]];
    };
    
    __weak typeof(self) wself = self;
    [dateSelectionController showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSNumber *units){
        [BTWSettingsService setSettingsValue:units forKey:kCommonDistanceUnitsKey];
        [wself.metricsButton setTitle:[BTWCommonUtils stringForUnits:[units integerValue]] forState:UIControlStateNormal];
    } andCancelHandler:nil];
}

- (IBAction)actionAgreement:(id)sender
{
    BTWSettingsAgreementViewController *agreementViewController = [[BTWSettingsAgreementViewController alloc] initWithNibName:nil bundle:nil];
    [[BTWTransitionsManager sharedInstance] showModalViewController:agreementViewController];
}

- (IBAction)actionSendFeedback:(id)sender
{
    NSString *subject = [NSString stringWithFormat:@"Togeze (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    NSMutableString *body = [NSMutableString string];
    [body appendString:NSLocalizedString(@"Please describe you question. \nBest regards, Togeze", nil)];
    [body appendString:@"\n"];
    [body appendString:@"\n-----"];
    [body appendString:[NSString stringWithFormat:@"\n- iOS: %@", [UIDevice currentDevice].systemVersion]];
    [body appendString:[NSString stringWithFormat:@"\n- Device: %@", [BTWCommonUtils hardwareDescription]]];
    [body appendString:[NSString stringWithFormat:@"\n- User ID: %@", [BTWUserService sharedInstance].authorizedUser.backend_id]];
    [body appendString:[NSString stringWithFormat:@"\n- Session token: %@", [BTWUserService sharedInstance].sessionKey]];
    NSString *recepient = @"support@togeze.com";

    [[BTWTransitionsManager sharedInstance] showMailComposerWithSubject:subject body:body recepient:recepient];
}

- (IBAction)actionLogout:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Logout", nil) message:NSLocalizedString(@"Are you sure you want to logout?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
    [alertView show];
}

#pragma mark - UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        [[BTWUserService sharedInstance] logout];
    }
}

@end
