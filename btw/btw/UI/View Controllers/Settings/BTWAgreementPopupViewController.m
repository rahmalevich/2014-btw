//
//  BTWAgreementPopupViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.08.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWAgreementPopupViewController.h"
#import "BTWSettingsAgreementViewController.h"
#import "PopupUnwindSegue.h"

@interface BTWAgreementPopupViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentBackgroundView;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@end

@implementation BTWAgreementPopupViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _contentTextView.text = [BTWSettingsAgreementViewController agreementText];
    _contentBackgroundView.layer.cornerRadius = 5.0f;
}

- (void)dismiss
{
    PopupUnwindSegue *unwindSegue = [[PopupUnwindSegue alloc] initWithIdentifier:@"popupUnwindSegue" source:self destination:[UIApplication sharedApplication].keyWindow.rootViewController];
    [unwindSegue perform];
}

- (IBAction)actionCancel:(UIButton *)sender
{
    [self dismiss];
}

- (IBAction)actionAgree:(UIButton *)sender
{
    [BTWSettingsService setSettingsValue:@(YES) forKey:kLicenseSignedKey];
    if (_completionHandler) {
        _completionHandler();
    }
    [self dismiss];
}

@end
