//
//  BTWSettingsCommonViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSettingsCommonViewController.h"
#import "BTWDropDownCell.h"
#import "BTWRangeSliderCell.h"
#import "BTWSettingsService.h"

#import "PopoverButton.h"

static NSInteger kUnitsButtonTag = 100;
static NSInteger kShowButtonTag = 101;

static NSString *kDropDownCellID = @"kDropDownCellID";
static NSString *kRangeSliderCellID = @"kRangeSliderCellID";

@interface BTWSettingsCommonViewController () <UITableViewDelegate, UITableViewDataSource, PopoverButtonDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation BTWSettingsCommonViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Settings", nil);
    self.navigationItem.rightBarButtonItem = nil;
    
    [_tableView registerNib:[UINib nibWithNibName:@"BTWDropDownCell" bundle:nil] forCellReuseIdentifier:kDropDownCellID];
    [_tableView registerNib:[UINib nibWithNibName:@"BTWRangeSliderCell" bundle:nil] forCellReuseIdentifier:kRangeSliderCellID];
}

#pragma mark - PopoverButton delegate
- (void)popoverButton:(PopoverButton *)popoverButton didSelectItem:(id)item
{
    if (popoverButton.tag == kUnitsButtonTag) {
        [BTWSettingsService setSettingsValue:item forKey:kCommonDistanceUnitsKey];
    } else if (popoverButton.tag == kShowButtonTag) {
        [BTWSettingsService setSettingsValue:item forKey:kFilterGenderKey];
    }
}

#pragma mark - UITableView delegate & datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 0;
    if (indexPath.row == 0 || indexPath.row == 1) {
        result = [BTWDropDownCell cellHeight];
    } else {
        result = [BTWRangeSliderCell cellHeight];
    }
    return result;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseId = (indexPath.row == 0 || indexPath.row == 1) ? kDropDownCellID : kRangeSliderCellID;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];

    if (indexPath.row == 0 || indexPath.row == 1) {
        BTWDropDownCell *dropDownCell = (BTWDropDownCell *)cell;
        dropDownCell.popoverButton.delegate = self;
        dropDownCell.backgroundColor = [BTWStylesheet settingsCellColor];
        dropDownCell.separatorView.backgroundColor = [BTWStylesheet commonBackgroundColor];
        dropDownCell.titleLabel.textColor = [BTWStylesheet settingsTitleColor];
        [dropDownCell.popoverButton setBackgroundColor:[BTWStylesheet commonBackgroundColor]];
        if (indexPath.row == 0) {
            dropDownCell.titleLabel.text = NSLocalizedString(@"Units", nil);
            dropDownCell.popoverButton.itemsArray = @[@(BTWDistanceUnitsKilometers), @(BTWDistanceUnitsMiles)];
            dropDownCell.popoverButton.titleBlock = ^(NSNumber *item){
                NSString *result = nil;
                switch ([item integerValue]) {
                    case BTWDistanceUnitsKilometers:
                        result = NSLocalizedString(@"Kilometers", nil);
                        break;
                    case BTWDistanceUnitsMiles:
                        result = NSLocalizedString(@"Miles", nil);
                        break;
                }
                return result;
            };
            NSNumber *selectedItem = [BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey];
            dropDownCell.popoverButton.selectedItem = selectedItem;
            dropDownCell.popoverButton.tag = kUnitsButtonTag;
        } else {
            dropDownCell.titleLabel.text = NSLocalizedString(@"Show", nil);
            dropDownCell.popoverButton.itemsArray = @[@(BTWFilterGenderTypeAll), @(BTWFilterGenderTypeMale), @(BTWFilterGenderTypeFemale)];
            dropDownCell.popoverButton.titleBlock = ^(NSNumber *item){
                NSString *result = nil;
                switch ([item integerValue]) {
                    case BTWFilterGenderTypeAll:
                        result = NSLocalizedString(@"All", nil);
                        break;
                    case BTWFilterGenderTypeMale:
                        result = NSLocalizedString(@"Men", nil);
                        break;
                    case BTWFilterGenderTypeFemale:
                        result = NSLocalizedString(@"Women", nil);
                        break;
                }
                return result;
            };
            NSNumber *selectedItem = [BTWSettingsService settingsValueForKey:kFilterGenderKey];
            dropDownCell.popoverButton.selectedItem = selectedItem;
            dropDownCell.popoverButton.tag = kShowButtonTag;
        }
    } else {
        BTWRangeSliderCell *sliderCell = (BTWRangeSliderCell *)cell;
        sliderCell.backgroundColor = [BTWStylesheet settingsCellColor];
        sliderCell.separatorView.backgroundColor = [BTWStylesheet commonBackgroundColor];
        sliderCell.titleLabel.textColor = [BTWStylesheet settingsTitleColor];
        sliderCell.titleLabel.text = NSLocalizedString(@"Age range", nil);
        sliderCell.sliderView.slider.minimumValue = kFilterDefaultLowerAge;
        sliderCell.sliderView.slider.maximumValue = kFilterDefaultUpperAge;
        sliderCell.sliderView.slider.upperValue = [[BTWSettingsService settingsValueForKey:kFilterUpperAgeKey] floatValue];
        sliderCell.sliderView.slider.lowerValue = [[BTWSettingsService settingsValueForKey:kFilterLowerAgeKey] floatValue];
        sliderCell.sliderView.slider.minimumRange = kFilterMinAgeRange;
        sliderCell.sliderView.sliderValueChangedBlock = ^(NMRangeSlider *slider){
            [BTWSettingsService setSettingsValue:@(floorf(slider.lowerValue)) forKey:kFilterLowerAgeKey];
            [BTWSettingsService setSettingsValue:@(floorf(slider.upperValue)) forKey:kFilterUpperAgeKey];
        };
    }
    
    return cell;
}

@end
