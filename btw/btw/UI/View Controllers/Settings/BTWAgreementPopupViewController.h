//
//  BTWAgreementPopupViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.08.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWAgreementPopupViewController : BTWViewController

@property (nonatomic, copy) BTWVoidBlock completionHandler;

@end
