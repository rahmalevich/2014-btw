//
//  BTWSettingsAgreementViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 09.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWSettingsAgreementViewController : BTWViewController

+ (NSString *)agreementText;

@end
