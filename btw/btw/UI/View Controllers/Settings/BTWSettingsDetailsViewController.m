//
//  BTWSettingsDetailsViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSettingsDetailsViewController.h"

@interface BTWSettingsDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation BTWSettingsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = self.title;
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
