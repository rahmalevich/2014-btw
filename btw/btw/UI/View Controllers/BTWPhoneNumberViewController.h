//
//  BTWPhoneNumberViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 31.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWPhoneNumberViewController : BTWViewController

@property (nonatomic, copy) BTWVoidBlock completionBlock;

@end
