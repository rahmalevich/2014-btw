//
//  BTWActiveRoutesViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWActiveRoutesViewController.h"
#import "BTWActiveRouteCell.h"
#import "BTWScheduledRouteCell.h"
#import "BTWEventsService.h"
#import "BTWLocationManager.h"

#import "MBProgressHUD+CompletionIcon.h"

static NSString * const kActiveRouteCellReuseID = @"kActiveRouteCellReuseID";
static NSString * const kScheduledRouteCellReuseID = @"kScheduledRouteCellReuseID";

@interface BTWActiveRoutesViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, BTWRouteCellDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSFetchedResultsController *activeEventsController;
@end

@implementation BTWActiveRoutesViewController

#pragma mark - Initialization & Memory managment
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    self.activeEventsController = [Event MR_fetchAllSortedBy:@"start_date" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"status IN %@", @[kEventStatusPlanned, kEventStatusInprogress]] groupBy:nil delegate:self];
    
    _tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, kCentralButtonOffset, 0.0f);
    [_tableView registerNib:[UINib nibWithNibName:@"BTWActiveRouteCell" bundle:nil] forCellReuseIdentifier:kActiveRouteCellReuseID];
    [_tableView registerNib:[UINib nibWithNibName:@"BTWScheduledRouteCell" bundle:nil] forCellReuseIdentifier:kScheduledRouteCellReuseID];
    [_tableView reloadData];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAppearence) name:kEventServiceDidUpdateEventsNotfication object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocationUpdate:) name:kLocationManagerDidUpdateLocationNotification object:nil];
    
    [self updateAppearence];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak typeof(self) wself = self;
    [[BTWEventsService sharedInstance] updateEventsWithCompletionHandler:^(NSError *error){
        if (!error) {
            [[BTWEventsService sharedInstance] confirmActiveEventsWithCompletionHandler:nil];
        }
        
        // Делаем задержку для корректной последовательности анимаций
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [wself updateAppearence];
        });
    }];
}

- (void)updateAppearence
{
    if ([_activeEventsController.fetchedObjects count] == 0) {
        if ([BTWEventsService sharedInstance].isLoadingData) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

- (void)handleLocationUpdate:(NSNotification *)notification
{
    for (UITableViewCell *cell in _tableView.visibleCells) {
        if ([cell isKindOfClass:[BTWActiveRouteCell class]]) {
            [(BTWActiveRouteCell *)cell updateProgress];
        }
    }
}

#pragma mark - UITableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_activeEventsController.fetchedObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat result = 0.0f;
    Event *event = _activeEventsController.fetchedObjects[indexPath.row];
    if ([event.status isEqualToString:kEventStatusPlanned]) {
        result = [BTWScheduledRouteCell cellHeight];
    } else if ([event.status isEqualToString:kEventStatusInprogress]) {
        result = [BTWActiveRouteCell cellHeight];
    }
    return result;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTWRouteCell *cell = nil;

    Event *event = _activeEventsController.fetchedObjects[indexPath.row];
    if ([event.status isEqualToString:kEventStatusPlanned]) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:kScheduledRouteCellReuseID forIndexPath:indexPath];
    } else if ([event.status isEqualToString:kEventStatusInprogress]) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:kActiveRouteCellReuseID forIndexPath:indexPath];
    }
    cell.delegate = self;
    cell.event = event;
    
    return cell;
}

- (void)configureCellAtIndexPath:(NSIndexPath *)indexPath
{
    BTWRouteCell *cell = (BTWRouteCell *)[_tableView cellForRowAtIndexPath:indexPath];
    Event *event = _activeEventsController.fetchedObjects[indexPath.row];
    
    if (([cell.event.status isEqualToString:kEventStatusPlanned] && [cell isKindOfClass:[BTWActiveRouteCell class]]) ||
        ([cell.event.status isEqualToString:kEventStatusInprogress] && [cell isKindOfClass:[BTWScheduledRouteCell class]]))
    {
        [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else {
        cell.event = event;
    }
}

#pragma mark - BTWRouteCell delegate
- (void)routeCell:(BTWRouteCell *)cell actionButtonWasTouchedAtIndex:(NSUInteger)index
{
    if ([@[kEventStatusPlanned, kEventStatusInprogress] containsObject:cell.event.status]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if ([cell.event.status isEqualToString:kEventStatusPlanned]) {
            if (index == 1) {
                [[BTWEventsService sharedInstance] finishEvent:cell.event withCompletionHandler:^(NSError *error){
                    [hud handleCompletionForSuccess:(error ? NO : YES)];
                }];
            } else {
                [[BTWEventsService sharedInstance] cancelEvent:cell.event withCompletionHandler:^(NSError *error){
                    [hud handleCompletionForSuccess:(error ? NO : YES)];
                }];
            }
        } else if ([cell.event.status isEqualToString:kEventStatusInprogress]) {
            [[BTWEventsService sharedInstance] finishEvent:cell.event withCompletionHandler:^(NSError *error){
                [hud handleCompletionForSuccess:(error ? NO : YES)];
            }];
        }
    }
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCellAtIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self updateAppearence];
}

@end
