//
//  BTWRoutesHistoryViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWRoutesHistoryViewController.h"
#import "BTWHistoryRouteCell.h"
#import "BTWEventsService.h"

static NSString * const kHistoryRouteCellReuseID = @"kHistoryRouteCellReuseID";

@interface BTWRoutesHistoryViewController () <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet UILabel *placeholderLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSFetchedResultsController *historyEventsController;
@end

@implementation BTWRoutesHistoryViewController

#pragma mark - Initialization & Memory managment
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    self.historyEventsController = [Event MR_fetchAllSortedBy:@"start_date" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"status = %@", kEventStatusCompleted] groupBy:nil delegate:self];
    
    _tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, kCentralButtonOffset, 0.0f);
    [_tableView registerNib:[UINib nibWithNibName:@"BTWHistoryRouteCell" bundle:nil] forCellReuseIdentifier:kHistoryRouteCellReuseID];
    [_tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateAppearence) name:kEventServiceDidUpdateEventsNotfication object:nil];
    
    [self updateAppearence];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    __weak typeof(self) wself = self;
    [[BTWEventsService sharedInstance] updateEventsWithCompletionHandler:^(NSError *error){

        // Делаем задержку для корректной последовательности анимаций
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [wself updateAppearence];
        });
    }];
}

- (void)updateAppearence
{
    if ([_historyEventsController.fetchedObjects count] == 0) {
        if ([BTWEventsService sharedInstance].isLoadingData) {
            [_activityIndicator startAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 0.0f;
            }];
        } else {
            [_activityIndicator stopAnimating];
            [UIView animateWithDuration:0.3 animations:^{
                _placeholderLabel.alpha = 1.0f;
            }];
        }
    } else {
        [_activityIndicator stopAnimating];
        [UIView animateWithDuration:0.3 animations:^{
            _placeholderLabel.alpha = 0.0f;
        }];
    }
}

#pragma mark - UITableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_historyEventsController.fetchedObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BTWHistoryRouteCell cellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BTWHistoryRouteCell *historyRouteCell = (BTWHistoryRouteCell *)[self.tableView dequeueReusableCellWithIdentifier:kHistoryRouteCellReuseID forIndexPath:indexPath];
    historyRouteCell.event = _historyEventsController.fetchedObjects[indexPath.row];
    return historyRouteCell;
}

- (void)configureCell:(BTWRouteCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Event *event = _historyEventsController.fetchedObjects[indexPath.row];
    cell.event = event;
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(BTWRouteCell *)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
    [self updateAppearence];
}

@end
