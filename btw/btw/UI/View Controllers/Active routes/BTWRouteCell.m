//
//  BTWRouteCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 09.06.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWRouteCell.h"
#import "BTWRatingView.h"

#import "UIImageView+WebCache.h"

@interface BTWRouteCell ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIImageView *roleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *startAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishAddressLabel;
@property (weak, nonatomic) IBOutlet BTWRatingView *ratingView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *actionButtonsArray;
@end

@implementation BTWRouteCell

+ (CGFloat)cellHeight
{
    return 0.0f;
}

- (void)awakeFromNib {
    _avatarImageView.layer.cornerRadius = floorf(_avatarImageView.boundsWidth/2);
    [_separatorImageView setImage:[UIImage imageWithColor:[BTWStylesheet separatorColor] size:_separatorImageView.size]];
    
    for (UIButton *button in _actionButtonsArray) {
        [button setBackgroundImage:[UIImage imageWithColor:button.backgroundColor size:button.size] forState:UIControlStateNormal];
        button.layer.cornerRadius = 5.0f;
        button.clipsToBounds = YES;
    }
}

- (void)setEvent:(Event *)event
{
    _event = event;
    
    RouteUser *companion = [event companion];
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:companion.photo_url] placeholderImage:[companion bigPlaceholder]];
    _ratingView.rating = [companion.average_points floatValue];
    _titleLabel.text = [companion fullname];
    _roleImageView.image = [companion roleImage];
    
    NSString *subtitle =  [companion roleString];
    if ([companion isDriver] && ([companion.car.brand isValidString] && [companion.car.model isValidString])) {
        subtitle = [NSString stringWithFormat:@"%@ %@", companion.car.brand, companion.car.model];
        if ([companion.car.number isValidString]) {
            subtitle = [subtitle stringByAppendingFormat:@", %@", companion.car.number];
        }
    }
    _subtitleLabel.text = subtitle;
    
    _startAddressLabel.text = event.start_address;
    _finishAddressLabel.text = event.finish_address;
}

#pragma mark - Actions
- (IBAction)actionButtonTouched:(UIButton *)sender
{
    if ([_delegate respondsToSelector:@selector(routeCell:actionButtonWasTouchedAtIndex:)]) {
        [_delegate routeCell:self actionButtonWasTouchedAtIndex:sender.tag];
    }
}

@end
