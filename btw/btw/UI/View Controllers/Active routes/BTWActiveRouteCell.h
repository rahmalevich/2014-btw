//
//  BTWActiveRouteCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWRouteCell.h"

@interface BTWActiveRouteCell : BTWRouteCell

- (void)updateProgress;

@end
