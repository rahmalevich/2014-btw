//
//  BTWRouteFeedbackViewController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWViewController.h"

@interface BTWRouteStateChangedViewController : BTWViewController

@property (nonatomic, strong) Event *event;

@end
