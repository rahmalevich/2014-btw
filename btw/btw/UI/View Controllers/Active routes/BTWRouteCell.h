//
//  BTWRouteCell.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 09.06.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BTWRouteCell;
@protocol BTWRouteCellDelegate <NSObject>
@optional
- (void)routeCell:(BTWRouteCell *)cell actionButtonWasTouchedAtIndex:(NSUInteger)index;
@end


@interface BTWRouteCell : UITableViewCell

@property (nonatomic, strong) Event *event;
@property (nonatomic, weak) id<BTWRouteCellDelegate> delegate;

+ (CGFloat)cellHeight;

@end
