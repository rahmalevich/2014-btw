//
//  BTWScheduledRouteCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWScheduledRouteCell.h"
#import "BTWFormattingService.h"

@interface BTWScheduledRouteCell ()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation BTWScheduledRouteCell

#pragma mark - Initialization
+ (CGFloat)cellHeight
{
    return 276.0f;
}

#pragma mark - Actions
- (void)setEvent:(Event *)event
{
    [super setEvent:event];
    
    _timeLabel.text = [[BTWFormattingService shortDateTimeFormatter] stringFromDate:event.start_date];
}

@end
