//
//  BTWHistoryRouteCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWHistoryRouteCell.h"
#import "BTWRatingView.h"
#import "BTWFormattingService.h"

@interface BTWRouteCell (Protected)
@property (weak, nonatomic) BTWRatingView *ratingView;
@end

@interface BTWHistoryRouteCell ()
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation BTWHistoryRouteCell

#pragma mark - Initialization
+ (CGFloat)cellHeight
{
    return 165.0f;
}

- (void)setEvent:(Event *)event
{
    [super setEvent:event];
    
    self.ratingView.rating = [event.my_points floatValue];
    _dateLabel.text = [[BTWFormattingService dateFormatterWithFormat:@"dd.MM.yyyy"] stringFromDate:event.start_date];
}

@end
