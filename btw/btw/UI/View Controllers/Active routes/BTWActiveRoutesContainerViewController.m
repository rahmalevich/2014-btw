//
//  BTWActiveRoutesContainerViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWActiveRoutesContainerViewController.h"
#import "BTWSwipeBetweenViewControllers.h"
#import "BTWActiveRoutesViewController.h"
#import "BTWRoutesHistoryViewController.h"
#import "BTWEventsService.h"

@interface BTWActiveRoutesContainerViewController ()
@property (nonatomic, strong) BTWSwipeBetweenViewControllers *swipeControllersContainer;
@end

@implementation BTWActiveRoutesContainerViewController

#pragma mark - Initialization
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.swipeControllersContainer = [BTWSwipeBetweenViewControllers new];
    
    BTWActiveRoutesViewController *activeRoutesController = [[BTWActiveRoutesViewController alloc] initWithNibName:nil bundle:nil];
    BTWRoutesHistoryViewController *routesHistoryController = [[BTWRoutesHistoryViewController alloc] initWithNibName:nil bundle:nil];
    _swipeControllersContainer.viewControllerArray = [@[activeRoutesController, routesHistoryController] mutableCopy];
    
    [[BTWEventsService sharedInstance] addObserver:self forKeyPath:@"unconfirmedActiveEventsCount" options:NSKeyValueObservingOptionNew context:nil];
    [[BTWEventsService sharedInstance] addObserver:self forKeyPath:@"unconfirmedCompletedEventsCount" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)dealloc
{
    [[BTWEventsService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedActiveEventsCount"];
    [[BTWEventsService sharedInstance] removeObserver:self forKeyPath:@"unconfirmedCompletedEventsCount"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Routes", nil);
    self.view.backgroundColor = [BTWStylesheet commonBackgroundColor];
    
    _swipeControllersContainer.sectionTitles = @[NSLocalizedString(@"Active", nil), NSLocalizedString(@"History", nil)];
    
    [self.view addSubview:_swipeControllersContainer.view];
    [self addChildViewController:_swipeControllersContainer];
    [_swipeControllersContainer didMoveToParentViewController:self];
    
    [self updateIndicators];
}

- (void)updateIndicators
{
    [_swipeControllersContainer setLeftIndicatorVisible:([BTWEventsService sharedInstance].unconfirmedActiveEventsCount > 0)];
    [_swipeControllersContainer setRightIndicatorVisible:([BTWEventsService sharedInstance].unconfirmedCompletedEventsCount > 0)];
}

#pragma mark - Observing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"unconfirmedActiveEventsCount"] || [keyPath isEqualToString:@"unconfirmedCompletedEventsCount"]) {
        [self updateIndicators];
    }
}

@end
