//
//  BTWActiveRouteCell.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWActiveRouteCell.h"
#import "BTWUserService.h"
#import "BTWRatingView.h"

@interface BTWRouteCell (Protected)
@property (weak, nonatomic) UIImageView *avatarImageView;
@property (weak, nonatomic) UIImageView *roleImageView;
@property (weak, nonatomic) UIImageView *separatorImageView;
@property (weak, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) UILabel *subtitleLabel;
@property (weak, nonatomic) UILabel *startAddressLabel;
@property (weak, nonatomic) UILabel *finishAddressLabel;
@property (weak, nonatomic) BTWRatingView *ratingView;
@property (weak, nonatomic) UIButton *actionButton;
@end

@interface BTWActiveRouteCell ()
@property (weak, nonatomic) IBOutlet UIView *progressContainerView;
@property (weak, nonatomic) IBOutlet UIView *progressValueView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *progressValueWidthConstraint;
@end

@implementation BTWActiveRouteCell

#pragma mark - Initialization
+ (CGFloat)cellHeight
{
    return 225.0f;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _progressContainerView.layer.cornerRadius = floorf(_progressContainerView.boundsHeight/2);
    _progressValueView.layer.cornerRadius = floorf(_progressValueView.boundsHeight/2);
}

#pragma mark - Setup
- (void)setEvent:(Event *)event
{
    [super setEvent:event];
    
    [self setProgress:[event progress]];
}

- (void)setProgress:(CGFloat)progress
{
    progress = MIN(MAX(0.0f, progress), 1.0f);
    
    _progressValueWidthConstraint.constant = _progressContainerView.frameWidth * progress;
    [self setNeedsLayout];
}

- (void)updateProgress
{
    [self setProgress:[self.event progress]];
}

@end
