//
//  BTWRouteFeedbackViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 28.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWRouteStateChangedViewController.h"
#import "BTWRatingView.h"
#import "BTWFormattingService.h"
#import "BTWFeedbacksService.h"

#import "KeyboardListener.h"
#import "PopupUnwindSegue.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD+CompletionIcon.h"
#import "Appirater.h"

static CGFloat const kBadRatingThresold = 3.0f;
static CGFloat const kCommentTextViewDefaultTop = 10.0f;
static CGFloat const kCommentTextViewIssueTop = 80.0f;

static CGFloat const kDefaultContentHeight = 291.0f;
static CGFloat const kCompletedContentHeight = 358.0f;

@interface BTWRouteStateChangedViewController () <BTWRatingViewProtocol, UITextViewDelegate>
@property (nonatomic, assign) BTWFeedbackReason badFeedbackReason;
@property (nonatomic, assign) BOOL initialized;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *feedbackView;
@property (weak, nonatomic) IBOutlet UIView *dismissBottomView;
@property (weak, nonatomic) IBOutlet UIView *feedbackBottomView;
@property (weak, nonatomic) IBOutlet UIScrollView *containerScrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *marksLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *startLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentPlaceholderLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *roleImageView;
@property (weak, nonatomic) IBOutlet UIButton *carIssueButton;
@property (weak, nonatomic) IBOutlet UIButton *driverIssueButton;
@property (weak, nonatomic) IBOutlet UIButton *otherIssueButton;
@property (weak, nonatomic) IBOutlet UIButton *markButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentTextViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popupHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popupBottomConstraint;
@property (weak, nonatomic) IBOutlet BTWRatingView *averageRatingView;
@property (weak, nonatomic) IBOutlet BTWRatingView *markView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *issueButtonsCollection;
@end

@implementation BTWRouteStateChangedViewController

#pragma mark - Memory managment
- (void)dealloc
{
    [[KeyboardListener sharedInstance] removeObserver:self forKeyPath:@"isKeyboardVisible"];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _contentView.layer.cornerRadius = 10.0f;
    _commentTextView.layer.cornerRadius = 5.0f;
    _photoImageView.layer.cornerRadius = floorf(_photoImageView.boundsWidth/2);
    _markView.minRating = 1.0f;
    _markView.editable = YES;
    
    for (UIButton *button in _buttonsCollection) {
        [button setBackgroundImage:[UIImage imageWithColor:[BTWStylesheet popupColor] size:button.size] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.9 alpha:1.0] size:button.size] forState:UIControlStateHighlighted];
    }
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapRecognized:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    [[KeyboardListener sharedInstance] addObserver:self forKeyPath:@"isKeyboardVisible" options:NSKeyValueObservingOptionNew context:nil];

    [self setupContent];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (!_initialized) {
        _contentHeightConstraint.constant = self.view.frameHeight;
        _popupHeightConstraint.constant = [_event.status isEqualToString:kEventStatusCompleted] ? kCompletedContentHeight : kDefaultContentHeight;
        _popupBottomConstraint .constant = floorf((self.view.frameHeight - _popupHeightConstraint.constant)/2);
        self.initialized = YES;
    }
}

- (void)setEvent:(Event *)event
{
    _event = event;
    [self setupContent];
}

- (void)setupContent
{
    _nameLabel.text = [_event.companion fullname];
    
    NSDate *date = ([_event.status isEqualToString:kEventStatusPlanned] || [_event.status isEqualToString:kEventStatusInprogress]) ? _event.start_date : _event.finish_date;
    _dateLabel.text = [[BTWFormattingService shortDateTimeFormatter] stringFromDate:date];
    _startLabel.text = _event.start_address;
    _finishLabel.text = _event.finish_address;
    [_photoImageView sd_setImageWithURL:[NSURL URLWithString:_event.companion.photo_url] placeholderImage:[_event.companion bigPlaceholder]];
    _roleImageView.image = [_event.companion roleImage];
    _averageRatingView.rating = [_event.companion.average_points floatValue];
    
    NSInteger numberOfFeedbacks = [_event.companion.number_of_feedbacks integerValue];
    if (numberOfFeedbacks > 0) {
        _marksLabel.text = [NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Route: Feedbacks", nil), @(numberOfFeedbacks)];
    } else {
        _marksLabel.hidden = YES;
    }
    
    BOOL showFeedbackControls = [_event.status isEqualToString:kEventStatusCompleted];
    _feedbackBottomView.hidden = !showFeedbackControls;
    _dismissBottomView.hidden = showFeedbackControls;
    
    _titleLabel.text = [self titleForEventState:_event.status];
}

- (NSString *)titleForEventState:(NSString *)eventState
{
    NSString *result = nil;
    if ([eventState isEqualToString:kEventStatusPlanned]) {
        result = NSLocalizedString(@"Route planned", nil);
    } else if ([eventState isEqualToString:kEventStatusInprogress]) {
        result = NSLocalizedString(@"Route started", nil);
    } else if ([eventState isEqualToString:kEventStatusCancelled]) {
        result = NSLocalizedString(@"Route cancelled", nil);
    } else if ([eventState isEqualToString:kEventStatusCompleted]) {
        result = NSLocalizedString(@"Mark route", nil);
    }
    return result;
}

- (void)dismiss
{
    PopupUnwindSegue *unwindSegue = [[PopupUnwindSegue alloc] initWithIdentifier:@"popupUnwindSegue" source:self destination:[UIApplication sharedApplication].keyWindow.rootViewController];
    [unwindSegue perform];
    
    [Appirater userDidSignificantEvent:YES];
}

- (void)selectReason:(BTWFeedbackReason)reason
{
    self.badFeedbackReason = reason;
    for (UIButton *button in _issueButtonsCollection) {
        button.selected = (button.tag == reason);
    }
}

- (void)backgroundTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    [[BTWUIUtils findFirstResponderForView:self.view] resignFirstResponder];
}

#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"isKeyboardVisible"]) {
        CGFloat offset = 5.0f;
        UIEdgeInsets contentInset = UIEdgeInsetsZero;
        CGPoint contentOffset = CGPointZero;
        if ([KeyboardListener sharedInstance].isKeyboardVisible) {
            contentInset.bottom = [KeyboardListener sharedInstance].keyboardRect.size.height + offset;
            contentOffset.y = [KeyboardListener sharedInstance].keyboardRect.size.height - _popupBottomConstraint.constant - _sendButton.boundsHeight;
        }

        [UIView animateWithDuration:0.3 animations:^{
            _containerScrollView.contentInset = contentInset;
            _containerScrollView.contentOffset = contentOffset;
        }];
    }
}

#pragma mark - Rating view delegate
- (void)starsSelectionChanged:(BTWRatingView *)ratingView rating:(CGFloat)rating
{
    _markButton.enabled = YES;
    
    BOOL markIsPositive = ratingView.rating >= kBadRatingThresold;
    BOOL showIssueButtons = NO;
    if (markIsPositive || [[_event companion] isPassenger]) {
        _commentTextViewTopConstraint.constant = kCommentTextViewDefaultTop;
        [self selectReason:BTWFeedbackReasonBlank];
    } else if (self.badFeedbackReason == BTWFeedbackReasonBlank) {
        _commentTextViewTopConstraint.constant = kCommentTextViewIssueTop;
        showIssueButtons = YES;
        [self selectReason:BTWFeedbackReasonOther];
    }
    
    for (UIButton *button in _issueButtonsCollection) {
        button.hidden = showIssueButtons;
    }
    
    [self.view layoutIfNeeded];
}

#pragma mark - UITextView delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (_commentPlaceholderLabel.alpha > 0) {
        [UIView animateWithDuration:0.3 animations:^{
            _commentPlaceholderLabel.alpha = 0.0f;
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0) {
        [UIView animateWithDuration:0.3 animations:^{
            _commentPlaceholderLabel.alpha = 1.0f;
        }];
    }
}

#pragma mark - Actions
- (IBAction)actionClose:(UIButton *)sender
{
    [self dismiss];
}

- (IBAction)actionMark:(UIButton *)sender
{
    if (_markView.rating >= 1.0) {
        _feedbackView.hidden = NO;
        _feedbackView.alpha = 0.0f;
        [UIView animateWithDuration:0.3 animations:^{
            _feedbackView.alpha = 1.0f;
        }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Feedback", nil) message:NSLocalizedString(@"Please select mark for proceed", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)actionSelectIssue:(UIButton *)sender
{
    for (UIButton *button in _issueButtonsCollection) {
        button.selected = NO;
    }
    sender.selected = YES;
    
    self.badFeedbackReason = sender.tag;
}

- (IBAction)actionBack:(UIButton *)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _feedbackView.alpha = 0.0f;
    } completion:^(BOOL finished){
        _feedbackView.hidden = YES;
    }];
}

- (IBAction)actionSendFeedback:(UIButton *)sender
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[BTWFeedbacksService sharedInstance] sendFeedbackForEvent:_event mark:_markView.rating reason:_badFeedbackReason comment:_commentTextView.text completionHandler:^(NSError *error)
    {
        [hud handleCompletionForSuccess:(error ? NO : YES)];
        if (!error) {
            [self dismiss];
        }
    }];
}

@end
