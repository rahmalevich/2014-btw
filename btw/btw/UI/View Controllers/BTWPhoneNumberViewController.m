//
//  BTWPhoneNumberViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 31.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWPhoneNumberViewController.h"
#import "BTWUserService.h"
#import "BTWSettingsService.h"

#import "KeyboardListener.h"
#import "InsetTextField.h"
#import "PopupUnwindSegue.h"
#import "MBProgressHUD+CompletionIcon.h"

static NSInteger const kPhoneNumberLength = 10;
static CGFloat const kKeyboardOffset = 10.0f;

@interface BTWPhoneNumberViewController () <UITextFieldDelegate>
@property (nonatomic, assign) BOOL initialized;
@property (nonatomic, weak) NSTimer *resendButtonTimer;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *phoneNumberContainerView;
@property (weak, nonatomic) IBOutlet UIView *confirmView;
@property (weak, nonatomic) IBOutlet InsetTextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet InsetTextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *resendCodeButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomInsetHeightConstraint;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@end

@implementation BTWPhoneNumberViewController

#pragma mark - View lifecycle
- (void)dealloc
{
    [_resendButtonTimer invalidate];
    [[KeyboardListener sharedInstance] removeObserver:self forKeyPath:@"keyboardRect"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _contentView.layer.cornerRadius = 10.0f;
    _phoneNumberContainerView.layer.cornerRadius = 5.0f;
    _phoneNumberContainerView.layer.borderWidth = 1.0f;
    _phoneNumberContainerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _phoneNumberTextField.contentInset = UIEdgeInsetsMake(0, 10.0, 0, 10.0);
    _codeTextField.layer.cornerRadius = 5.0f;
    _codeTextField.layer.borderWidth = 1.0f;
    _codeTextField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _codeTextField.contentInset = UIEdgeInsetsMake(0, 10.0, 0, 10.0);
    
    for (UIButton *button in _buttonsCollection) {
        [button setBackgroundImage:[UIImage imageWithColor:[BTWStylesheet popupColor] size:button.size] forState:UIControlStateNormal];
        [button setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.9 alpha:1.0] size:button.size] forState:UIControlStateHighlighted];
    }
    
    [[KeyboardListener sharedInstance] addObserver:self forKeyPath:@"keyboardRect" options:NSKeyValueObservingOptionNew context:nil];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapAction:)];
    [self.view addGestureRecognizer:tapRecognizer];
    
    if ([BTWSettingsService delayForNextActivationCodeRequest]) {
        [self setupResendButtonTimer];
    }
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (!_initialized) {
        _bottomInsetHeightConstraint.constant = floorf((self.view.frameHeight - self.contentView.frameHeight)/2);
        self.initialized = YES;
    }
}

- (void)dismiss
{
    PopupUnwindSegue *unwindSegue = [[PopupUnwindSegue alloc] initWithIdentifier:@"popupUnwindSegue" source:self destination:[UIApplication sharedApplication].keyWindow.rootViewController];
    [unwindSegue perform];
}

- (void)requestCode
{
    if ([_phoneNumberTextField.text length] == kPhoneNumberLength) {
        
        // Код страны захардкожен
        NSString *phoneNumber = [@"+7" stringByAppendingString:_phoneNumberTextField.text];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        __weak typeof(self) wself = self;
        [[BTWUserService sharedInstance] requestActivationCodeForPhoneNumber:phoneNumber completionHandler:^(NSError *error)
        {
            [hud handleCompletionForSuccess:(error ? NO : YES)];
            [BTWSettingsService updateActivationCodeRequestsCounter];
            [wself setupResendButtonTimer];
            if (!error) {
                if (_confirmView.hidden) {
                    _confirmView.hidden = NO;
                    _confirmView.alpha = 0.0f;
                    [UIView animateWithDuration:0.3 animations:^{
                        _confirmView.alpha = 1.0f;
                    } completion:^(BOOL finished){
                        [_codeTextField becomeFirstResponder];
                    }];
                }
            }
        }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Phone number is invalid", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}

- (void)setupResendButtonTimer
{
    NSTimeInterval delay = [BTWSettingsService delayForNextActivationCodeRequest];
    if (delay > 0.0f) {
        _resendCodeButton.enabled = NO;
        self.resendButtonTimer = [NSTimer scheduledTimerWithTimeInterval:delay target:self selector:@selector(resendButtonDelayEnded:) userInfo:nil repeats:NO];
    } else {
        _resendCodeButton.enabled = YES;
    }
}

- (void)resendButtonDelayEnded:(NSTimer *)timer
{
    _resendCodeButton.enabled = YES;
}

#pragma mark - Keyboard handling
- (void)backgroundTapAction:(UITapGestureRecognizer *)tapRecognizer
{
    [[BTWUIUtils findFirstResponderForView:self.view] resignFirstResponder];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"keyboardRect"]) {
        CGFloat keyboardHeight = kKeyboardOffset + self.view.frame.size.height - CGRectGetMinY([KeyboardListener sharedInstance].keyboardRect);
        CGFloat defaultPosition = floorf((self.view.frameHeight - self.contentView.frameHeight)/2);
        _bottomInsetHeightConstraint.constant = MAX(keyboardHeight, defaultPosition);
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutSubviews];
        }];
    }
}

#pragma mark - UITextField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return [newText length] <= kPhoneNumberLength;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self requestCode];
    return YES;
}

#pragma mark - Actions
- (IBAction)actionClose:(UIButton *)sender
{
    [self dismiss];
}

- (IBAction)actionRequestCode:(UIButton *)sender
{
    [self requestCode];
}

- (IBAction)actionActivate:(UIButton *)sender
{
    if (_codeTextField.text.length > 0) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow animated:YES];
        [[BTWUserService sharedInstance] confirmPhoneNumberWithCode:_codeTextField.text completionHandler:^(NSError *error){
            hud.labelText = error ? NSLocalizedString(@"Wrong code", nil) : nil;
            if (error) {
                [hud handleCompletionForSuccess:NO];
            } else {
                [hud hide:YES];
                [self dismiss];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    if (_completionBlock) {
                        _completionBlock();
                    }
                });
            } 
        }];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please enter activation code", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)actionBack:(UIButton *)sender
{
    [_phoneNumberTextField becomeFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        _confirmView.alpha = 0.0f;
    } completion:^(BOOL finished){
        _confirmView.hidden = YES;
    }];
}

@end
