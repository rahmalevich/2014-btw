//
//  BTWRegisterViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRegisterViewController.h"
#import "BTWUserService.h"
#import "BTWTransitionsManager.h"
#import "BTWFormattingService.h"

#import "InsetTextField.h"
#import "KeyboardHelper.h"

#import "MBProgressHUD+CompletionIcon.h"
#import "RMDateSelectionViewController.h"

static CGFloat kButtonCornerRadius = 5.0f;

@interface BTWRegisterViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet InsetTextField *nameTextField;
@property (weak, nonatomic) IBOutlet InsetTextField *surnameTextField;
@property (weak, nonatomic) IBOutlet InsetTextField *loginTextField;
@property (weak, nonatomic) IBOutlet InsetTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet InsetTextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *birthdayButton;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic, strong) IBOutletCollection(InsetTextField) NSArray *textFieldsArray;

@property (nonatomic, strong) KeyboardHelper *keyboardHelper;
@property (nonatomic, strong) NSDate *birthdate;
@property (nonatomic, assign) BTWUserGender gender;
@property (nonatomic, assign) BOOL fieldsFlowEnabled;
@property (nonatomic, strong) NSDate *previousValidDate;
@end

@implementation BTWRegisterViewController

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.keyboardHelper = [[KeyboardHelper alloc] initWithTargetView:_contentView scrollView:_scrollView];
    
    _registerButton.layer.cornerRadius = kButtonCornerRadius;
    [_registerButton setBackgroundImage:[UIImage imageWithColor:_registerButton.backgroundColor] forState:UIControlStateNormal];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 20.0f);
    for (InsetTextField *textField in _textFieldsArray) {
        textField.contentInset = inset;
        textField.tintColor = [UIColor whiteColor];
    }
}

- (void)completeRegistration
{
    if ([self validateFields]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Registration", nil)];
        
        [[BTWUserService sharedInstance] registerWithFirstName:_nameTextField.text lastName:_surnameTextField.text birthdate:_birthdate gender:_gender email:_loginTextField.text password:_passwordTextField.text completionHandler:^(NSError *error)
        {
            [hud handleCompletionForSuccess:(error ? NO : YES)];
            if (error) {
                NSString *errorDescription = NSLocalizedString(@"Please try again later", nil);
                if ([error.domain isEqualToString:kBTWAPIErrorDomain]) {
                    if (error.code == BTWApiErrorCodeUserAlreadyRegistered) {
                        errorDescription = NSLocalizedString(@"User with this e-mail address is already registered", nil);
                    }
                }
                hud.labelText = NSLocalizedString(@"Error", nil);
                hud.detailsLabelText = errorDescription;
            } else {
                hud.labelText = NSLocalizedString(@"Registration complete", nil);
            }
            
            if (!error) {
                [[BTWTransitionsManager sharedInstance] processAuthorization];
            }
        }];
    }
}

- (BOOL)validateFields
{
    BOOL result = YES;

    if (![_nameTextField.text isValidString]) {
        result = NO;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please enter your name", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && ![_surnameTextField.text isValidString]) {
        result = NO;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please enter your surname", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && ![BTWValidation validateEmail:_loginTextField.text]) {
        result = NO;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please enter a valid e-mail address", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }

    if (result && ![BTWValidation validatePassword:_passwordTextField.text]) {
        result = NO;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Your password should be at least 8 characters long and contain only letters and numbers", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
    
    if (result && ![_confirmPasswordTextField.text isEqualToString:_passwordTextField.text]) {
        result = NO;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"The passwords do not match", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    }
    
    return result;
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:_nameTextField]) {
        [_surnameTextField becomeFirstResponder];
    } else if ([textField isEqual:_surnameTextField]) {
        [textField resignFirstResponder];
        self.fieldsFlowEnabled = YES;
        [self actionSelectBirthday:nil];
    } else if ([textField isEqual:_loginTextField]) {
        [_passwordTextField becomeFirstResponder];
    } else if ([textField isEqual:_passwordTextField]) {
        [_confirmPasswordTextField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        [self completeRegistration];
    }
    return YES;
}

#pragma mark - Action
- (IBAction)actionRegister:(UIButton *)sender
{
    [self completeRegistration];
}

- (IBAction)actionBack:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionSelectGender:(UIButton *)sender
{
    [RMDateSelectionViewController setLocalizedTitleForCancelButton:NSLocalizedString(@"Cancel", nil)];
    [RMDateSelectionViewController setLocalizedTitleForSelectButton:NSLocalizedString(@"Select", nil)];
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController selectionControllerForObjects:@[@(BTWUserGenderMale), @(BTWUserGenderFemale)]];
    dateSelectionController.objectTitleBlock = ^(id object){
        BTWUserGender gender = [object integerValue];
        return [BTWCommonUtils stringForGender:gender];
    };
    
    __weak typeof(self) wself = self;
    [dateSelectionController showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSNumber *gender){
        wself.gender = [gender integerValue];
        [wself.genderButton setTitle:[BTWCommonUtils stringForGender:wself.gender] forState:UIControlStateNormal];
        if (wself.fieldsFlowEnabled) {
            wself.fieldsFlowEnabled = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [wself.loginTextField becomeFirstResponder];
            });
        }
    } andCancelHandler:^(RMDateSelectionViewController *vc){
        wself.fieldsFlowEnabled = NO;
    }];
}

- (IBAction)actionSelectBirthday:(UIButton *)sender
{
    [RMDateSelectionViewController setLocalizedTitleForCancelButton:NSLocalizedString(@"Cancel", nil)];
    [RMDateSelectionViewController setLocalizedTitleForSelectButton:NSLocalizedString(@"Select", nil)];
    
    
    RMDateSelectionViewController *dateSelectionController = [RMDateSelectionViewController dateSelectionController];
    dateSelectionController.hideNowButton = YES;
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionController.datePicker.minimumDate = [[BTWFormattingService dateFormatterWithFormat:@"MM.dd.yyyy"] dateFromString:@"01.01.1900"];

    [dateSelectionController.datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    if (_birthdate) {
        dateSelectionController.datePicker.date = _birthdate;
    } else {
        NSDateComponents *components = [NSDateComponents new];
        components.year = -19;
        dateSelectionController.datePicker.date = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    }
    self.previousValidDate = dateSelectionController.datePicker.date;
    
    __weak typeof(self) wself = self;
    [dateSelectionController showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *date){
        wself.birthdate = date;
        [wself.birthdayButton setTitle:[[BTWFormattingService shortDateFormatterRelativeFormatting:NO] stringFromDate:date] forState:UIControlStateNormal];
        if (wself.fieldsFlowEnabled) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [wself actionSelectGender:nil];
            });
        }
    } andCancelHandler:^(RMDateSelectionViewController *vc){
        wself.fieldsFlowEnabled = NO;
    }];
}

- (void)dateChanged:(UIDatePicker *)datePicker
{
    NSDateComponents *components = [NSDateComponents new];
    components.year = -18;
    NSDate *maximumDate = [[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate date] options:0];
    if ([maximumDate compare:datePicker.date] == NSOrderedAscending) {
        [datePicker setDate:self.previousValidDate animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You should be 18 years old for using this application", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alertView show];
    } else {
        self.previousValidDate = datePicker.date;
    }
}

@end
