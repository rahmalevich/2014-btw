//
//  BTWAuthViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWAuthViewController.h"
#import "BTWTransitionsManager.h"
#import "BTWUserService.h"
#import "BTWFacebookService.h"
#import "BTWVKService.h"
#import "BTWSettingsService.h"
#import "BTWLoginViewController.h"

#import "MBProgressHUD+CompletionIcon.h"
#import "AFNetworkReachabilityManager.h"

static CGFloat kButtonCornerRadius = 5.0f;

@interface BTWAuthViewController ()
@property (nonatomic, weak) IBOutlet UIView *ageRestrictionBackgroundView;
@property (nonatomic, weak) IBOutlet UIButton *authorizeButton;
@property (nonatomic, weak) IBOutlet UIButton *registerButton;
@property (nonatomic, weak) IBOutlet UIButton *vkButton;
@property (nonatomic, weak) IBOutlet UIButton *fbButton;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *controlsToHideCollection;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *buttonsCollection;
@end

@implementation BTWAuthViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _ageRestrictionBackgroundView.layer.cornerRadius = floorf(_ageRestrictionBackgroundView.boundsWidth/2);
    
    for (UIButton *button in _buttonsCollection) {
        button.clipsToBounds = YES;
        [button setBackgroundImage:[UIImage imageWithColor:button.backgroundColor] forState:UIControlStateNormal];
    }
    
    [_authorizeButton makeRoundedCorners:UIRectCornerBottomLeft | UIRectCornerTopLeft withRadius:kButtonCornerRadius];
    [_registerButton makeRoundedCorners:UIRectCornerBottomRight | UIRectCornerTopRight withRadius:kButtonCornerRadius];
    _vkButton.layer.cornerRadius = kButtonCornerRadius;
    _fbButton.layer.cornerRadius = kButtonCornerRadius;
}

- (void)loginViaFacebook
{
    [self loginViaSocialNetwork:BTWLinkedSocialNetworkFacebook];
}

- (void)loginViaVK
{
    [self loginViaSocialNetwork:BTWLinkedSocialNetworkVk];
}

- (void)loginViaSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork
{
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Auth: reachability error", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        for (UIView *aView in _controlsToHideCollection) {
            aView.alpha = 0.0f;
        }
    } completion:^(BOOL finished){
        for (UIView *aView in _controlsToHideCollection) {
            aView.hidden = YES;
        }
    }];
    
    BTWVoidBlock showControlsBlock = [^{
        for (UIView *aView in _controlsToHideCollection) {
            aView.hidden = NO;
            aView.alpha = 0.0f;
        }
        [UIView animateWithDuration:0.3 animations:^{
            for (UIView *aView in _controlsToHideCollection) {
                aView.alpha = 1.0f;
            }
        }];
    } copy];
    
    NSString *networkName = socialNetwork == BTWLinkedSocialNetworkFacebook ? @"Facebook" : @"VK";
    
    id<BTWSocialNetworkService> socialNetworkService = nil;
    if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
        socialNetworkService = [BTWFacebookService sharedInstance];
    } else if (socialNetwork == BTWLinkedSocialNetworkVk) {
        socialNetworkService = [BTWVKService sharedInstance];
    }
    
    [socialNetworkService setupWithCompletionHandler:^(BOOL opened, NSError *error)
     {
         NSString *userID = socialNetworkService.userID;
         NSString *accessToken = socialNetworkService.accessToken;
         if (error || !([userID isValidString] && [userID isValidString])) {
             showControlsBlock();
             
             NSString *alertMessage = [NSString stringWithFormat:NSLocalizedString(@"Cannot connect to %@", nil), networkName];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:alertMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
             [alert show];
         } else {
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             [[BTWUserService sharedInstance] authorizeWithSocialNetwork:socialNetwork userID:userID accessToken:accessToken completionHandler:^(NSError *error)
              {
                  [hud handleCompletionForSuccess:(error ? NO : YES)];
                  
                  if (!error) {
                      [[BTWTransitionsManager sharedInstance] processAuthorization];
                  } else {
                      [socialNetworkService closeSession];
                      
                      showControlsBlock();
                      
                      NSString *message = nil;
                      if ([error.domain isEqualToString:kBTWAPIErrorDomain] && (error.code == BTWApiErrorCodeAgeRestriction || error.code == BTWApiErrorCodeBirthdayIsMissing)) {
                          message = NSLocalizedString(@"You should be older than 18 years", nil);
                      } else {
                          message = [NSString stringWithFormat:NSLocalizedString(@"Failed to login via %@", nil), networkName];
                      }
                      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                      [alert show];
                  }
              }];
         }
     }];
}

#pragma mark - Actions
- (IBAction)actionAuthorize:(UIButton *)sender
{
    BTWLoginViewController *loginController = [[BTWLoginViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:loginController animated:YES];
}

- (IBAction)actionRegister:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"registerSegue" sender:nil];
}

- (IBAction)actionLoginViaFacebook:(UIButton *)sender
{
    [self loginViaFacebook];
}

- (IBAction)actionLoginViaVK:(UIButton *)sender
{
    [self loginViaVK];
}

@end
