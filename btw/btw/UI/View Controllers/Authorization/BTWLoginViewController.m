//
//  BTWLoginViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.06.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWLoginViewController.h"
#import "BTWUserService.h"
#import "BTWTransitionsManager.h"

#import "InsetTextField.h"
#import "KeyboardHelper.h"
#import "MBProgressHUD+CompletionIcon.h"

static CGFloat kButtonCornerRadius = 5.0f;
@interface BTWLoginViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet InsetTextField *loginTextField;
@property (weak, nonatomic) IBOutlet InsetTextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, strong) KeyboardHelper *keyboardHelper;
@end

@implementation BTWLoginViewController

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.keyboardHelper = [[KeyboardHelper alloc] initWithTargetView:_contentView scrollView:_scrollView];
    
    _loginButton.layer.cornerRadius = kButtonCornerRadius;
    [_loginButton setBackgroundImage:[UIImage imageWithColor:_loginButton.backgroundColor] forState:UIControlStateNormal];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 20.0f);
    _loginTextField.contentInset = inset;
    _loginTextField.tintColor = [UIColor whiteColor];
    _passwordTextField.contentInset = inset;
    _passwordTextField.tintColor = [UIColor whiteColor];
}

#pragma mark - Utils
- (void)completeLogin
{
    if ([_loginTextField.text isValidString] && [_passwordTextField.text isValidString]) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Authorization", nil)];
        [[BTWUserService sharedInstance] authorizeWithEmail:_loginTextField.text password:_passwordTextField.text completionHandler:^(NSError *error)
         {
             [hud handleCompletionForSuccess:(error ? NO : YES)];
             
             if (error) {
                 hud.labelText = NSLocalizedString(@"Error", nil);
                 hud.detailsLabelText = NSLocalizedString(@"Invalid email or password", nil);
             } else {
                 hud.labelText = NSLocalizedString(@"Authorization complete", nil);
             }
             [hud hide:YES afterDelay:1.0];
             
             if (!error) {
                 [[BTWTransitionsManager sharedInstance] processAuthorization];
             }
         }];
    }
}

#pragma mark - UITextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:_loginTextField]) {
        [_passwordTextField becomeFirstResponder];
    } else if ([textField isEqual:_passwordTextField]) {
        [textField resignFirstResponder];
        [self completeLogin];
    }
    
    return YES;
}

#pragma mark - Actions
- (IBAction)actionBack:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionLogin:(UIButton *)sender
{
    [self completeLogin];
}

@end
