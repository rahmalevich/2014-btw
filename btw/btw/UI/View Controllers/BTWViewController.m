//
//  BTWViewController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWViewController.h"
#import "BTWTransitionsManager.h"

@interface BTWViewController ()

@end

@implementation BTWViewController

#pragma mark - Rotating
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
//    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonWithImage:[UIImage imageNamed:@"button_config"] target:[BTWTransitionsManager sharedInstance] action:@selector(showSettings)];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.font = [BTWStylesheet commonFontOfSize:18.0];
    titleLabel.textColor = [BTWStylesheet navigationTitleColor];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    self.navigationItem.titleView = titleLabel;
}

#pragma mark - Actions
- (void)actionBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Abstract

- (void)prepareForParentController {
    // should be overriden
}

@end
