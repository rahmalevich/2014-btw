//
//  MatchingUser.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MatchingUser.h"


@implementation MatchingUser

@dynamic match;
@dynamic latitude;
@dynamic longitude;
@dynamic gallery_count;

@end
