//
//  GalleryImage.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface GalleryImage : NSManagedObject

@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * thumb_url;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSNumber * is_avatar;

@end
