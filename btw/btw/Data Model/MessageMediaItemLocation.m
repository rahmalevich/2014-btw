//
//  MessageMediaItemLocation.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItemLocation.h"


@implementation MessageMediaItemLocation

@dynamic latitude;
@dynamic longitude;

@end
