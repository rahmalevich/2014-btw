//
//  User+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "User+Extensions.h"
#import "BTWFacebookService.h"
#import "BTWVKService.h"

@implementation User (Extensions)

#pragma mark - Helpers
- (NSString *)fullname
{
    return [NSString stringWithFormat:@"%@ %@", self.name, self.surname];
}

- (NSString *)genderString
{
    NSString *resultString = [BTWCommonUtils stringForGender:[self.gender integerValue]];
    return resultString;
}

- (NSInteger)age
{
    NSInteger age = 0;
    if (self.birthday) {
        NSDateComponents *ageComponents = [[NSCalendar currentCalendar] components:NSYearCalendarUnit fromDate:self.birthday toDate:[NSDate date] options:0];
        age = [ageComponents year];
    }
    return age;
}

- (BTWLinkedSocialNetwork)linkedSocialNetwork
{
    BTWLinkedSocialNetwork linkedNetwork = BTWLinkedSocialNetworkBlank;
    if ([self.facebook_id isValidString]) {
        linkedNetwork = BTWLinkedSocialNetworkFacebook;
    } else if ([self.vkontakte_id isValidString]) {
        linkedNetwork = BTWLinkedSocialNetworkVk;
    }
    return linkedNetwork;
}

- (id<BTWSocialNetworkService>)socialNetworkService
{
    id<BTWSocialNetworkService> socialNetworkService = nil;
    if ([self.facebook_id isValidString]) {
        socialNetworkService = [BTWFacebookService sharedInstance];
    } else if ([self.vkontakte_id isValidString]) {
        socialNetworkService = [BTWVKService sharedInstance];
    }
    return socialNetworkService;
}

- (NSString *)socialNetworkID
{
    NSString *networkID = nil;
    if ([self.facebook_id isValidString]) {
        networkID = self.facebook_id;
    } else if ([self.vkontakte_id isValidString]) {
        networkID = self.vkontakte_id;
    }
    return networkID;
}

#pragma mark - Placeholders
- (UIImage *)smallPlaceholder
{
    return [self.gender integerValue] == BTWUserGenderFemale ? [UIImage imageNamed:@"avatar_placeholder_female_small"] : [UIImage imageNamed:@"avatar_placeholder_male_small"];
}

- (UIImage *)midPlaceholder
{
    return [self.gender integerValue] == BTWUserGenderFemale ? [UIImage imageNamed:@"avatar_placeholder_female_mid"] : [UIImage imageNamed:@"avatar_placeholder_male_mid"];
}

- (UIImage *)bigPlaceholder
{
    return [self.gender integerValue] == BTWUserGenderFemale ? [UIImage imageNamed:@"avatar_placeholder_female_big"] : [UIImage imageNamed:@"avatar_placeholder_male_big"];
}

#pragma mark - Import
- (NSArray *)processQuiz:(id)value
{
    NSMutableArray *processedArray = [NSMutableArray array];
    if ([value isKindOfClass:[NSArray class]]) {
        for (id object in (NSArray *)value) {
            NSString *stringObject;
            if ([object isKindOfClass:[NSString class]]) {
                stringObject = (NSString *)object;
            } else {
                stringObject = [object stringValue];
            }
            [processedArray addObject:stringObject];
        }
    }
    return [NSArray arrayWithArray:processedArray];
}

- (void)importQuiz:(id)value
{
    self.quiz = [self processQuiz:value];
}

- (void)importAnswers:(id)answersData
{
    NSArray *quizArray = nil;
    if ([answersData isKindOfClass:[NSDictionary class]]) {
        quizArray = [self processQuiz:answersData[@"quiz"]];
    }
    
    if ([quizArray isValidArray]) {
        NSArray *userAnswers = [Answer MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"backend_id IN %@", quizArray] inContext:self.managedObjectContext];
        self.answers = [NSSet setWithArray:userAnswers];
    }
}

@end
