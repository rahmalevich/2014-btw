//
//  MessageMediaItem+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItem+Extensions.h"
#import "BTWDialogDataController.h"

#import "ObjcAssociatedObjectHelpers.h"
#import "JSQMessagesMediaPlaceholderView.h"
#import "JSQMessagesBubbleImageFactory.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"

@interface MessageMediaItem (Extensions_Private)
@property (strong, nonatomic) UIView *cachedPlaceholderView;
@property (strong, nonatomic) NSNumber *cachedPlaceholderStatus;
@property (strong, nonatomic) UIImage *cachedMediaImage;
@property (strong, nonatomic) BTWMessageMediaView *cachedMediaView;

@property (nonatomic, assign) BOOL isLoadingMedia;

@end

@implementation MessageMediaItem (Extensions)

#pragma mark - Extensions
SYNTHESIZE_ASC_OBJ_ASSIGN(delegate, setDelegate);
SYNTHESIZE_ASC_PRIMITIVE(isLoadingMedia, setIsLoadingMedia, BOOL);

- (NSDictionary *)dictionaryRepresentation
{
    NSAssert(NO, @"Error! required method not implemented in subclass. Need to implement %s", __PRETTY_FUNCTION__);
    return nil;
}

- (void)updateMediaViewWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    NSAssert(NO, @"Error! required method not implemented in subclass. Need to implement %s", __PRETTY_FUNCTION__);
}

- (void)openAttachment
{
    NSAssert(NO, @"Error! required method not implemented in subclass. Need to implement %s", __PRETTY_FUNCTION__);
}

#pragma mark - JSQMessageMediaData protocol
SYNTHESIZE_ASC_OBJ(cachedPlaceholderView, setCachedPlaceholderView);
SYNTHESIZE_ASC_OBJ(cachedPlaceholderStatus, setCachedPlaceholderStatus);
SYNTHESIZE_ASC_OBJ(cachedMediaImage, setCachedMediaImage);
SYNTHESIZE_ASC_OBJ(cachedMediaView, setCachedMediaView);

- (BTWMessageMediaView *)mediaView
{
    if (!self.cachedMediaImage) {
        return nil;
    }
    
    if (self.cachedMediaView == nil) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:self.cachedMediaImage];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.size = kMediaViewDisplaySize;
  
        BTWMessageMediaView *mediaView = [BTWMessageMediaView viewWithContentView:imageView];
        mediaView.clipsToBounds = YES;
        if (self.message.incoming) {
            [[BTWDialogDataController sharedBubbleImageMasker] applyIncomingBubbleImageMaskToMediaView:mediaView];
        } else {
            [[BTWDialogDataController sharedBubbleImageMasker] applyOutgoingBubbleImageMaskToMediaView:mediaView];
        }
        
        self.cachedMediaView = mediaView;
    }
    
    return self.cachedMediaView;
}

- (CGSize)mediaViewDisplaySize
{
    return kMediaViewDisplaySize;
}

- (UIView *)mediaPlaceholderView
{
    BOOL cachedPlaceholderIsUnsent = ([self.cachedPlaceholderStatus integerValue] == BTWDialogMessageStatusUnsent);
    BOOL messageIsUnsent = ([self.message.status integerValue] == BTWDialogMessageStatusUnsent);
    
    if (self.cachedPlaceholderView == nil || cachedPlaceholderIsUnsent != messageIsUnsent)
    {
        UIView *placeholderView = nil;
        CGRect placeholderViewFrame = (CGRect){CGPointZero, [self mediaViewDisplaySize]};
        if ([self.message.status integerValue] == BTWDialogMessageStatusUnsent) {
            UIImageView *errorIconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_chat_error_big"]];
            [errorIconImageView sizeToFit];
            placeholderView = [[JSQMessagesMediaPlaceholderView alloc] initWithFrame:placeholderViewFrame backgroundColor:[BTWStylesheet errorBubbleColor] imageView:errorIconImageView];
        } else {
            placeholderView = [JSQMessagesMediaPlaceholderView viewWithActivityIndicator];
            placeholderView.frame = placeholderViewFrame;
        }

        if (self.message.incoming) {
            [[BTWDialogDataController sharedBubbleImageMasker] applyIncomingBubbleImageMaskToMediaView:placeholderView];
        } else {
            [[BTWDialogDataController sharedBubbleImageMasker] applyOutgoingBubbleImageMaskToMediaView:placeholderView];
        }
        
        self.cachedPlaceholderView = placeholderView;
        self.cachedPlaceholderStatus = self.message.status;
    }
    return self.cachedPlaceholderView;
}

@end
