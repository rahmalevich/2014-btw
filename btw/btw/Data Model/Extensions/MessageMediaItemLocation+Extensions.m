//
//  MessageMediaItemLocation+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItemLocation+Extensions.h"
#import "BTWMapSnapshotter.h"
#import "BTWMapAttachmentViewController.h"
#import "BTWTransitionsManager.h"

@interface MessageMediaItem (Protected)
@property (strong, nonatomic) UIImage *cachedMediaImage;
@property (nonatomic, assign) BOOL isLoadingMedia;
@end

@implementation MessageMediaItemLocation (Extensions)

#pragma mark - Extensions
- (NSDictionary *)dictionaryRepresentation
{
    NSDictionary *itemDictionary = @{ @"type":kBTWMessageMediaItemLocation,
                                      @"latitude":self.latitude,
                                      @"longitude":self.longitude };
    return itemDictionary;
}

- (void)updateMediaViewWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    if (self.isLoadingMedia) {
        return;
    }
    
    if (CLLocationCoordinate2DIsValid(self.coordinate)) {
        self.isLoadingMedia = YES;
        [self createMapViewSnapshotForCoordinate:self.coordinate withCompletionHandler:^(NSError *error){
            self.isLoadingMedia = NO;
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

- (void)openAttachment
{
    BTWMapAttachmentViewController *viewController = [[BTWMapAttachmentViewController alloc] initWithCoordinate:self.coordinate];
    [[BTWTransitionsManager sharedInstance] pushViewController:viewController];
}

- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D coordinate = kCLLocationCoordinate2DInvalid;
    if (self.latitude && self.longitude) {
        coordinate = CLLocationCoordinate2DMake([self.latitude floatValue], [self.longitude floatValue]);
    }
    return coordinate;
}

- (void)setCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.latitude = @(coordinate.latitude);
    self.longitude = @(coordinate.longitude);
}

- (void)createMapViewSnapshotForCoordinate:(CLLocationCoordinate2D)coordinate withCompletionHandler:(BTWErrorBlock)completion
{
    BTWMapSnapshotter *snapshotter = [BTWMapSnapshotter new];
    [snapshotter getSnapshotForLocationCoordinate:self.coordinate withCompletionHandler:^(UIImage *snapshot, NSError *error){
        if (!error) {
            self.cachedMediaImage = snapshot;
        }
        if (completion) {
            completion(error);
        }
    }];
}

@end
