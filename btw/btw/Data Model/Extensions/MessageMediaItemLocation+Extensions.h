//
//  MessageMediaItemLocation+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItemLocation.h"

@interface MessageMediaItemLocation (Extensions)

- (CLLocationCoordinate2D)coordinate;
- (void)setCoordinate:(CLLocationCoordinate2D)coordinate;

@end
