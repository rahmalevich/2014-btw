//
//  Car+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Car.h"

@interface Car (Extensions)

- (BOOL)carIsValid;

@end
