//
//  Event+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Event.h"

extern NSString * const kEventStatusPlanned;
extern NSString * const kEventStatusInprogress;
extern NSString * const kEventStatusCompleted;
extern NSString * const kEventStatusCancelled;

@interface Event (Extensions)

- (RouteUser *)companion;

- (CGFloat)distanceFromFinish;
- (CGFloat)progress;

@end
