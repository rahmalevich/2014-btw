//
//  RouteUser+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "RouteUser.h"

static NSString *kUserRoleDriver = @"driver";
static NSString *kUserRolePassenger = @"passenger";

@interface RouteUser (Extensions) <MKAnnotation>

- (NSString *)roleString;
- (UIImage *)roleImage;
- (UIImage *)blueRoleImage;
- (UIImage *)blueRoleImageBig;
- (BOOL)isDriver;
- (BOOL)isPassenger;

@end
