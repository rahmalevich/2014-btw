//
//  Route+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "Route.h"

@interface Route (Extensions)

+ (Route *)newRoute;
+ (Route *)newRouteInContext:(NSManagedObjectContext *)context;
+ (Route *)routeWithRoute:(Route *)aRoute;
+ (Route *)routeWithRoute:(Route *)aRoute inContext:(NSManagedObjectContext *)context;

- (BOOL)isValidRoute;
- (void)clearRoute;
- (void)copyValuesFromRoute:(Route *)aRoute;
- (void)copyReverseValuesFromRoute:(Route *)aRoute;

@end
