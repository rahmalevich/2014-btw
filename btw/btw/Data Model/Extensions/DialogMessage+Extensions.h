//
//  DialogMessage+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "DialogMessage.h"
#import "JSQMessageData.h"

typedef NS_ENUM(NSUInteger, BTWDialogMessageType) {
    BTWDialogMessageTypeText,
    BTWDialogMessageTypePhoto,
    BTWDialogMessageTypeLocation,
    BTWDialogMessageTypeRoute,
    BTWDialogMessageTypeService
};

typedef NS_ENUM(NSUInteger, BTWDialogMessageStatus) {
    BTWDialogMessageStatusBlank,
    BTWDialogMessageStatusUnsent,
    BTWDialogMessageStatusSent,
    BTWDialogMessageStatusDelivered,
    BTWDialogMessageStatusRead
};

static NSString *kBTWMessageTypeOnline = @"online";
static NSString *kBTWMessageTypeOffline = @"offline";
static NSString *kBTWMessageTypeTyping = @"typing";
static NSString *kBTWMessageTypeSystem = @"system";
static NSString *kBTWMessageTypeUser = @"user";
static NSString *kBTWMessageTypeConfirm = @"confirm";

@interface DialogMessage (Extensions) <JSQMessageData>

- (BOOL)incoming;
- (BOOL)outgoing;
- (BOOL)isSystemMessage;
- (NSDictionary *)dictionaryRepresentation;

@end
