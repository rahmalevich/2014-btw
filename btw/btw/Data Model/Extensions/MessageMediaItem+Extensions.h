//
//  MessageMediaItem+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItem.h"
#import "BTWMessageMediaView.h"

static CGSize kMediaViewDisplaySize = (CGSize){250.0f, 150.0f};

static NSString *kBTWMessageMediaItemLocation = @"location";
static NSString *kBTWMessageMediaItemRoute = @"route";
static NSString *kBTWMessageMediaItemPhoto = @"photo";

@interface MessageMediaItem (Extensions) <JSQMessageMediaData>

@property (nonatomic, assign, readonly) BOOL isLoadingMedia;

- (NSDictionary *)dictionaryRepresentation;
- (void)updateMediaViewWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)openAttachment;

- (BTWMessageMediaView *)mediaView;
- (CGSize)mediaViewDisplaySize;
- (UIView *)mediaPlaceholderView;

@end
