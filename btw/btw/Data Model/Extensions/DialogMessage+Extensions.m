//
//  DialogMessage+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "DialogMessage+Extensions.h"
#import "BTWUserService.h"
#import "BTWFormattingService.h"

@implementation DialogMessage (Extensions)

#pragma mark - Extensions
- (BOOL)incoming
{
    return ![self.sender_id isEqual:[BTWUserService sharedInstance].authorizedUser.backend_id];
}

- (BOOL)outgoing
{
    return ![self incoming];
}

- (NSDictionary *)dictionaryRepresentation
{
    NSDictionary *messageDictionary = @{ @"id": self.backend_id,
                                         @"type": kBTWMessageTypeUser,
                                         @"senderId": self.sender_id,
                                         @"date": [[BTWFormattingService dateTimeFormatterWithTimezone] stringFromDate:self.date],
                                         @"text": self.text ?: [NSNull null],
                                         @"media": self.mediaItem ? [self.mediaItem dictionaryRepresentation] : [NSNull null] };
    return messageDictionary;
}

#pragma mark - JSQMessageData protocol implementation
- (NSString *)senderId
{
    return self.sender_id;
}

- (NSString *)senderDisplayName
{
    NSString *result = nil;
    if ([self.type integerValue] != BTWDialogMessageTypeService) {
        result = self.incoming ? self.dialog.user.fullname: [BTWUserService sharedInstance].authorizedUser.fullname;
    }
    return result;
}

- (BOOL)isMediaMessage
{
    return self.mediaItem ? YES : NO;
}

- (BOOL)isSystemMessage
{
    return [self.type isEqualToString:kBTWMessageTypeSystem];
}

- (id<JSQMessageMediaData>)media
{
    return self.mediaItem;
}

@end
