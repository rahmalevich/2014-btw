//
//  User+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "User.h"
#import "BTWSocialNetworkService.h"

typedef NS_ENUM(NSUInteger, BTWUserGender) {
    BTWUserGenderBlank = 0,
    BTWUserGenderMale = 1,
    BTWUserGenderFemale = 2
};

typedef NS_ENUM(NSUInteger, BTWLinkedSocialNetwork) {
    BTWLinkedSocialNetworkBlank,
    BTWLinkedSocialNetworkFacebook,
    BTWLinkedSocialNetworkVk
};

@interface User (Extensions)

- (NSString *)fullname;
- (NSString *)genderString;
- (NSInteger)age;

- (BTWLinkedSocialNetwork)linkedSocialNetwork;
- (id<BTWSocialNetworkService>)socialNetworkService;
- (NSString *)socialNetworkID;

- (UIImage *)smallPlaceholder;
- (UIImage *)midPlaceholder;
- (UIImage *)bigPlaceholder;

@end