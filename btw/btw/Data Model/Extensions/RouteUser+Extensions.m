//
//  RouteUser+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "RouteUser+Extensions.h"
#import "GoogleMaps.h"

@implementation RouteUser (Extensions)

#pragma mark - Helpers
- (NSString *)roleString
{
    NSString *resultString = nil;
    if ([self isDriver]) {
        resultString = NSLocalizedString(@"driver", nil);
    } else if ([self isPassenger]) {
        resultString = NSLocalizedString(@"passenger", nil);
    }
    return resultString;
}

- (UIImage *)roleImage
{
    UIImage *roleImage = nil;
    if ([self isDriver]) {
        roleImage = [UIImage imageNamed:@"icon_car"];
    } else if ([self isPassenger]) {
        roleImage = [UIImage imageNamed:@"icon_walker"];
    }
    return roleImage;
}

- (UIImage *)blueRoleImage
{
    UIImage *roleImage = nil;
    if ([self isDriver]) {
        roleImage = [UIImage imageNamed:@"icon_car_blue"];
    } else if ([self isPassenger]) {
        roleImage = [UIImage imageNamed:@"icon_walker_blue"];
    }
    return roleImage;
}

- (UIImage *)blueRoleImageBig
{
    UIImage *roleImage = nil;
    if ([self isDriver]) {
        roleImage = [UIImage imageNamed:@"icon_car_blue_big"];
    } else if ([self isPassenger]) {
        roleImage = [UIImage imageNamed:@"icon_walker_blue_big"];
    }
    return roleImage;
}

- (BOOL)isDriver
{
    return [self.role isEqualToString:kUserRoleDriver];
}

- (BOOL)isPassenger
{
    return [self.role isEqualToString:kUserRolePassenger];
}

#pragma mark - MKAnnotation
- (CLLocationCoordinate2D)coordinate
{
    GMSPath *path = [GMSPath pathFromEncodedPath:self.polyline];
    CLLocationCoordinate2D result = [path count] > 0 ? [path coordinateAtIndex:0] : kCLLocationCoordinate2DInvalid;
    return result;
}

@end
