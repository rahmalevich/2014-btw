//
//  Event+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 20.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Event+Extensions.h"
#import "BTWUserService.h"
#import "BTWLocationManager.h"
#import "BTWEventsService.h"

#import "GoogleMaps.h"

NSString * const kEventStatusPlanned = @"planned";
NSString * const kEventStatusInprogress = @"inprogress";
NSString * const kEventStatusCompleted = @"completed";
NSString * const kEventStatusCancelled = @"cancelled";

@implementation Event (Extensions)

- (RouteUser *)companion
{
    RouteUser *companion = [self.passenger.backend_id isEqualToString:[BTWUserService sharedInstance].authorizedUser.backend_id]  ? self.driver : self.passenger;
    return companion;
}

- (CGFloat)distanceFromFinish
{
    GMSPath *path = [GMSPath pathFromEncodedPath:self.polyline];
    CLLocationCoordinate2D finishCoordinate = [path coordinateAtIndex:[path count] - 1];
    CGFloat distanceInMeters = [[BTWLocationManager sharedInstance].currentLocation distanceFromLocation:[[CLLocation alloc] initWithLatitude:finishCoordinate.latitude longitude:finishCoordinate.longitude]];
    return distanceInMeters;
}

- (CGFloat)progress
{
    GMSPath *path = [GMSPath pathFromEncodedPath:self.polyline];
    CLLocationCoordinate2D startCoordinate = [path coordinateAtIndex:0];
    CLLocationCoordinate2D finishCoordinate = [path coordinateAtIndex:[path count] - 1];
    CGFloat totalDistance = [[[CLLocation alloc] initWithLatitude:startCoordinate.latitude longitude:startCoordinate.longitude] distanceFromLocation:[[CLLocation alloc] initWithLatitude:finishCoordinate.latitude longitude:finishCoordinate.longitude]];
    CGFloat currentDistance = [self distanceFromFinish];
    CGFloat progress = 1.0f - currentDistance/(totalDistance - kRouteFinishDistanceInMeters);
    return progress;
}

@end
