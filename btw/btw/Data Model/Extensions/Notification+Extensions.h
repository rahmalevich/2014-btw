//
//  Notification+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Notification.h"

static NSString * const kNotificationTypeLike = @"like";
static NSString * const kNotificationTypeContact = @"contact";
static NSString * const kNotificationTypeRide = @"ride_offer";
static NSString * const kNotificationTypeRideConfirmed = @"ride_confirmed";

@interface Notification (Extensions)

@end
