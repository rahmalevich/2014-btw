//
//  Dialog+Extensions.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 19.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Dialog.h"

@interface Dialog (Extensions)

- (BOOL)isEmpty;

@end
