//
//  Car+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Car+Extensions.h"

@implementation Car (Extensions)

- (BOOL)carIsValid
{
    BOOL result = NO;
    if ([self.brand isValidString] &&
        [self.model isValidString] &&
        [self.color isValidString] &&
        [self.year isValidNumber] &&
        [self.number isValidString])
    {
        result = YES;
    }
    return result;
}

@end
