//
//  Dialog+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 19.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Dialog+Extensions.h"

@implementation Dialog (Extensions)

- (void)setMessages:(NSOrderedSet *)messages
{
    [self willChangeValueForKey:@"messages"];
    [self setPrimitiveValue:messages forKey:@"messages"];
    [self didChangeValueForKey:@"messages"];
    
    DialogMessage *lastMessage = [messages lastObject];
    self.last_message_date = lastMessage.date;
}

- (BOOL)isEmpty
{
    BOOL dialogIsEmpty = ([self.messages filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"type = %@", kBTWMessageTypeUser]] == 0);
    return dialogIsEmpty;
}

@end
