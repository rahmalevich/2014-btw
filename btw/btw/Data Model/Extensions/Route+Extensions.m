//
//  Route+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "Route+Extensions.h"
#import "BTWFormattingService.h"

@implementation Route (Extensions)

+ (Route *)newRoute
{
    return [self newRouteInContext:[NSManagedObjectContext MR_defaultContext]];
}

+ (Route *)newRouteInContext:(NSManagedObjectContext *)context
{
    Route *newRoute = [Route MR_createInContext:context];
    return newRoute;
}

+ (Route *)routeWithRoute:(Route *)aRoute
{
    return [self routeWithRoute:aRoute inContext:[NSManagedObjectContext MR_defaultContext]];
}

+ (Route *)routeWithRoute:(Route *)aRoute inContext:(NSManagedObjectContext *)context
{
    Route *route = [Route newRouteInContext:context];
    [route copyValuesFromRoute:aRoute];
    return route;
}

- (void)copyValuesFromRoute:(Route *)aRoute
{
    self.date = aRoute.date;
    self.polyline = aRoute.polyline;
    self.startPointAddress = aRoute.startPointAddress;
    self.startPointLatitude = aRoute.startPointLatitude;
    self.startPointLongitude = aRoute.startPointLongitude;
    self.startPointReference = aRoute.startPointReference;
    self.endPointAddress = aRoute.endPointAddress;
    self.endPointLatitude = aRoute.endPointLatitude;
    self.endPointLongitude = aRoute.endPointLongitude;
    self.endPointReference = aRoute.endPointReference;
}

- (void)copyReverseValuesFromRoute:(Route *)aRoute
{
    self.date = aRoute.date;
    self.polyline = nil;
    self.startPointAddress = aRoute.endPointAddress;
    self.startPointLatitude = aRoute.endPointLatitude;
    self.startPointLongitude = aRoute.endPointLongitude;
    self.startPointReference = aRoute.endPointReference;
    self.endPointAddress = aRoute.startPointAddress;
    self.endPointLatitude = aRoute.startPointLatitude;
    self.endPointLongitude = aRoute.startPointLongitude;
    self.endPointReference = aRoute.startPointReference;
}

- (BOOL)isValidRoute
{
    BOOL startPointDefined = (self.startPointLatitude && self.startPointLongitude) ||  [self.startPointReference isValidString];
    BOOL endPointDefined = (self.endPointLatitude && self.endPointLongitude) || [self.endPointReference isValidString];

    return startPointDefined && endPointDefined && self.date;
}

- (void)clearRoute
{
    self.date = nil;
    self.startPointAddress = nil;
    self.startPointLatitude = nil;
    self.startPointLongitude = nil;
    self.startPointReference = nil;
    self.endPointAddress = nil;
    self.endPointLatitude = nil;
    self.endPointLongitude = nil;
    self.endPointReference = nil;
}

@end
