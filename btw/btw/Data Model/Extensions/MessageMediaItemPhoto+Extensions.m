//
//  MessageMediaItemPhoto+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItemPhoto+Extensions.h"
#import "BTWTransitionsManager.h"

#import "SDWebImageManager.h"
#import "IDMPhotoBrowser.h"

@interface MessageMediaItem (Protected)
@property (strong, nonatomic) UIImage *cachedMediaImage;
@property (strong, nonatomic) BTWMessageMediaView *cachedMediaView;
@property (nonatomic, assign) BOOL isLoadingMedia;
@end

@implementation MessageMediaItemPhoto (Extensions)

- (NSDictionary *)dictionaryRepresentation
{
    NSDictionary *itemDictionary = @{ @"type":kBTWMessageMediaItemPhoto,
                                      @"full":self.full,
                                      @"thumb":self.thumb,
                                      @"thumb_2":self.thumb_2,
                                      @"thumb_3":self.thumb_3 };
    return itemDictionary;
}

- (NSString *)thumbnail
{
    NSString *thumbnail = nil;
    CGFloat scale = [UIScreen mainScreen].scale;
    if (scale > 1.0f) {
        if (scale > 2.0f) {
            thumbnail = self.thumb_3;
        } else {
            thumbnail = self.thumb_2;
        }
    } else {
        thumbnail = self.thumb;
    }
    return thumbnail;
}

- (void)updateMediaViewWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    if (self.isLoadingMedia) {
        return;
    }
    
    if ([self.thumbnail isValidString]) {
        self.isLoadingMedia = YES;
        [self getThumbnailWithCompletionHandler:^(NSError *error){
            self.isLoadingMedia = NO;
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

- (void)openAttachment
{
    IDMPhotoBrowser *photoBrowser = [[IDMPhotoBrowser alloc] initWithPhotoURLs:@[[NSURL URLWithString:self.full]] animatedFromView:self.cachedMediaView.contentView];
    photoBrowser.scaleImage = self.cachedMediaImage;
    
    CGFloat doneButtonSize = 40.0f;
    photoBrowser.doneButton.size = CGSizeMake(doneButtonSize, doneButtonSize);
    photoBrowser.doneButton.layer.cornerRadius = floorf(doneButtonSize/2);
    photoBrowser.doneButton.clipsToBounds = YES;
    [photoBrowser.doneButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.5] size:CGSizeMake(doneButtonSize, doneButtonSize)] forState:UIControlStateNormal];
    [photoBrowser.doneButton setImage:[UIImage imageNamed:@"icon_gallery_close"] forState:UIControlStateNormal];
    
    [[BTWTransitionsManager sharedInstance] showModalViewController:photoBrowser];
}

- (void)getThumbnailWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];

    SDWebImageManager *imageManager = [SDWebImageManager sharedManager];
    NSURL *thumbnailURL = [NSURL URLWithString:self.thumbnail];
    UIImage *thumbnail = [imageManager.imageCache imageFromDiskCacheForKey:[imageManager cacheKeyForURL:thumbnailURL]];
    
    if (thumbnail) {
        self.cachedMediaImage = thumbnail;
        if (completionHandler) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completionHandler(nil);
            });
        }
    } else {
        [imageManager downloadImageWithURL:thumbnailURL options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
        {
            if (!error) {
                self.cachedMediaImage = image;
            }
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

@end
