//
//  MessageMediaItemRoute+Extensions.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 11.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItemRoute+Extensions.h"
#import "BTWMapSnapshotter.h"
#import "BTWMapAttachmentViewController.h"
#import "BTWTransitionsManager.h"

@interface MessageMediaItem (Protected)
@property (strong, nonatomic) UIImage *cachedMediaImage;
@property (nonatomic, assign) BOOL isLoadingMedia;
@end

@implementation MessageMediaItemRoute (Extensions)

#pragma mark - Extensions
- (NSDictionary *)dictionaryRepresentation
{
    NSDictionary *itemDictionary = @{ @"type":kBTWMessageMediaItemRoute,
                                      @"polyline":self.polyline };
    return itemDictionary;
}

- (void)updateMediaViewWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    if (self.isLoadingMedia) {
        return;
    }
    
    if ([self.polyline isValidString]) {
        self.isLoadingMedia = YES;
        [self createMapViewSnapshotForPolyline:self.polyline withCompletionHandler:^(NSError *error){
            self.isLoadingMedia = NO;
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

- (void)openAttachment
{
    BTWMapAttachmentViewController *viewController = [[BTWMapAttachmentViewController alloc] initWithPolyline:self.polyline andDate:nil];
    [[BTWTransitionsManager sharedInstance] pushViewController:viewController];
}

- (void)createMapViewSnapshotForPolyline:(NSString *)polyline withCompletionHandler:(BTWErrorBlock)completion
{
    BTWMapSnapshotter *snapshotter = [BTWMapSnapshotter new];
    [snapshotter getSnapshotForPolyline:polyline withCompletionHandler:^(UIImage *snapshot, NSError *error){
        if (!error) {
            self.cachedMediaImage = snapshot;
        }
        if (completion) {
            completion(error);
        }
    }];
}

@end
