//
//  Route.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Route.h"


@implementation Route

@dynamic date;
@dynamic search_date;
@dynamic endPointAddress;
@dynamic endPointLatitude;
@dynamic endPointLongitude;
@dynamic endPointReference;
@dynamic polyline;
@dynamic startPointAddress;
@dynamic startPointLatitude;
@dynamic startPointLongitude;
@dynamic startPointReference;
@dynamic is_current;

@end
