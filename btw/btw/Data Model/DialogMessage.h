//
//  DialogMessage.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Dialog, MessageMediaItem;

@interface DialogMessage : NSManagedObject

@property (nonatomic, retain) NSNumber * confirmed;
@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * sender_id;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) Dialog *dialog;
@property (nonatomic, retain) MessageMediaItem *mediaItem;

@end
