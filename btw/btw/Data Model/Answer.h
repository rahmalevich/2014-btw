//
//  Answer.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question, User;

@interface Answer : NSManagedObject

@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) NSString * question_id;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) Question *question;
@property (nonatomic, retain) NSSet *users;
@end

@interface Answer (CoreDataGeneratedAccessors)

- (void)addUsersObject:(User *)value;
- (void)removeUsersObject:(User *)value;
- (void)addUsers:(NSSet *)values;
- (void)removeUsers:(NSSet *)values;

@end
