//
//  Dialog.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 06.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Dialog.h"
#import "DialogMessage.h"
#import "User.h"


@implementation Dialog

@dynamic backend_id;
@dynamic last_message_date;
@dynamic messages;
@dynamic user;
@dynamic unconfirmed_messages_count;

@end
