//
//  Question.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "Question.h"
#import "Answer.h"


@implementation Question

@dynamic backend_id;
@dynamic category;
@dynamic enabled;
@dynamic is_visited;
@dynamic sort_order;
@dynamic text;
@dynamic answers;

@end
