//
//  MessageMediaItemPhoto.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "MessageMediaItemPhoto.h"


@implementation MessageMediaItemPhoto

@dynamic full;
@dynamic thumb;
@dynamic thumb_2;
@dynamic thumb_3;
@dynamic source_path;

@end
