//
//  MessageMediaItem.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DialogMessage;

@interface MessageMediaItem : NSManagedObject

@property (nonatomic, retain) DialogMessage *message;

@end
