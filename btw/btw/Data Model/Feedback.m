//
//  Feedback.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Feedback.h"
#import "User.h"


@implementation Feedback

@dynamic backend_id;
@dynamic date;
@dynamic points;
@dynamic comment;
@dynamic author;

@end
