//
//  RouteUser.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "RouteUser.h"


@implementation RouteUser

@dynamic departure_time;
@dynamic polyline;
@dynamic role;
@dynamic driver_events;
@dynamic passenger_events;

@end
