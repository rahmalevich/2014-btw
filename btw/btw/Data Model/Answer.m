//
//  Answer.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "Answer.h"
#import "Question.h"
#import "User.h"


@implementation Answer

@dynamic backend_id;
@dynamic question_id;
@dynamic sort_order;
@dynamic text;
@dynamic question;
@dynamic users;

@end
