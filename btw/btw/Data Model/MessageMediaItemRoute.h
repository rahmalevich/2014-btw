//
//  MessageMediaItemRoute.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MessageMediaItem.h"


@interface MessageMediaItemRoute : MessageMediaItem

@property (nonatomic, retain) NSString * polyline;

@end
