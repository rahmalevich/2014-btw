//
//  DialogMessage.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 10.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "DialogMessage.h"
#import "Dialog.h"
#import "MessageMediaItem.h"


@implementation DialogMessage

@dynamic backend_id;
@dynamic date;
@dynamic sender_id;
@dynamic text;
@dynamic type;
@dynamic status;
@dynamic dialog;
@dynamic mediaItem;
@dynamic confirmed;

@end
