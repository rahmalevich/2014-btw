//
//  Notification.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Notification.h"
#import "User.h"


@implementation Notification

@dynamic created;
@dynamic confirmed;
@dynamic link;
@dynamic backend_id;
@dynamic travel_polyline;
@dynamic travel_date;
@dynamic type;
@dynamic travel_role;
@dynamic user;

@end
