//
//  Feedback.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Feedback : NSManagedObject

@property (nonatomic, retain) NSNumber * backend_id;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * points;
@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) User *author;

@end
