//
//  CarBrand.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 13.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "CarBrand.h"


@implementation CarBrand

@dynamic name;
@dynamic models;

@end
