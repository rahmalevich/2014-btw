//
//  RouteUser.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "User.h"

@class NSManagedObject;

@interface RouteUser : User

@property (nonatomic, retain) NSDate * departure_time;
@property (nonatomic, retain) NSString * polyline;
@property (nonatomic, retain) NSString * role;
@property (nonatomic, retain) NSSet * driver_events;
@property (nonatomic, retain) NSSet * passenger_events;
@end

@interface RouteUser (CoreDataGeneratedAccessors)

- (void)addDriver_eventsObject:(NSManagedObject *)value;
- (void)removeDriver_eventsObject:(NSManagedObject *)value;
- (void)addDriver_events:(NSSet *)values;
- (void)removeDriver_events:(NSSet *)values;

- (void)addPassenger_eventsObject:(NSManagedObject *)value;
- (void)removePassenger_eventsObject:(NSManagedObject *)value;
- (void)addPassenger_events:(NSSet *)values;
- (void)removePassenger_events:(NSSet *)values;

@end
