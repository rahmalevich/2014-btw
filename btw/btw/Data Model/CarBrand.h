//
//  CarBrand.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 13.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CarBrand : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSArray * models;

@end
