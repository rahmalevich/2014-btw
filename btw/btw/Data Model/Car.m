//
//  Car.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 21.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Car.h"
#import "User.h"


@implementation Car

@dynamic brand;
@dynamic model;
@dynamic color;
@dynamic year;
@dynamic number;
@dynamic user;

@end
