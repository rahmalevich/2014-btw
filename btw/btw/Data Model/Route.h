//
//  Route.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Route : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSDate * search_date;
@property (nonatomic, retain) NSString * endPointAddress;
@property (nonatomic, retain) NSNumber * endPointLatitude;
@property (nonatomic, retain) NSNumber * endPointLongitude;
@property (nonatomic, retain) NSString * endPointReference;
@property (nonatomic, retain) NSString * polyline;
@property (nonatomic, retain) NSString * startPointAddress;
@property (nonatomic, retain) NSNumber * startPointLatitude;
@property (nonatomic, retain) NSNumber * startPointLongitude;
@property (nonatomic, retain) NSString * startPointReference;
@property (nonatomic, retain) NSNumber * is_current;

@end
