//
//  User.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Answer, Car, Dialog, GalleryImage, Notification;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) NSDate * birthday;
@property (nonatomic, retain) NSString * facebook_id;
@property (nonatomic, retain) NSNumber * gender;
@property (nonatomic, retain) NSDate * last_activity;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * online;
@property (nonatomic, retain) NSString * photo_mid;
@property (nonatomic, retain) NSString * photo_small;
@property (nonatomic, retain) NSString * photo_url;
@property (nonatomic, retain) id quiz;
@property (nonatomic, retain) NSNumber * average_points;
@property (nonatomic, retain) NSString * surname;
@property (nonatomic, retain) NSString * vkontakte_id;
@property (nonatomic, retain) NSNumber * number_of_feedbacks;
@property (nonatomic, retain) NSSet *answers;
@property (nonatomic, retain) Car *car;
@property (nonatomic, retain) NSSet *dialogs;
@property (nonatomic, retain) NSOrderedSet *gallery_images;
@property (nonatomic, retain) Notification *notifications;
@property (nonatomic, retain) NSNumber * phone_verified;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addAnswersObject:(Answer *)value;
- (void)removeAnswersObject:(Answer *)value;
- (void)addAnswers:(NSSet *)values;
- (void)removeAnswers:(NSSet *)values;

- (void)addDialogsObject:(Dialog *)value;
- (void)removeDialogsObject:(Dialog *)value;
- (void)addDialogs:(NSSet *)values;
- (void)removeDialogs:(NSSet *)values;

- (void)insertObject:(GalleryImage *)value inGallery_imagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromGallery_imagesAtIndex:(NSUInteger)idx;
- (void)insertGallery_images:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeGallery_imagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInGallery_imagesAtIndex:(NSUInteger)idx withObject:(GalleryImage *)value;
- (void)replaceGallery_imagesAtIndexes:(NSIndexSet *)indexes withGallery_images:(NSArray *)values;
- (void)addGallery_imagesObject:(GalleryImage *)value;
- (void)removeGallery_imagesObject:(GalleryImage *)value;
- (void)addGallery_images:(NSOrderedSet *)values;
- (void)removeGallery_images:(NSOrderedSet *)values;
@end
