//
//  Question.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Answer;

@interface Question : NSManagedObject

@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * enabled;
@property (nonatomic, retain) NSNumber * is_visited;
@property (nonatomic, retain) NSNumber * sort_order;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSOrderedSet *answers;
@end

@interface Question (CoreDataGeneratedAccessors)

- (void)insertObject:(Answer *)value inAnswersAtIndex:(NSUInteger)idx;
- (void)removeObjectFromAnswersAtIndex:(NSUInteger)idx;
- (void)insertAnswers:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeAnswersAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInAnswersAtIndex:(NSUInteger)idx withObject:(Answer *)value;
- (void)replaceAnswersAtIndexes:(NSIndexSet *)indexes withAnswers:(NSArray *)values;
- (void)addAnswersObject:(Answer *)value;
- (void)removeAnswersObject:(Answer *)value;
- (void)addAnswers:(NSOrderedSet *)values;
- (void)removeAnswers:(NSOrderedSet *)values;
@end
