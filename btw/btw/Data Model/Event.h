//
//  Event.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RouteUser;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * backend_id;
@property (nonatomic, retain) NSDate * start_date;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * finish_address;
@property (nonatomic, retain) NSString * polyline;
@property (nonatomic, retain) NSDate * finish_date;
@property (nonatomic, retain) NSString * start_address;
@property (nonatomic, retain) RouteUser *driver;
@property (nonatomic, retain) RouteUser *passenger;
@property (nonatomic, retain) NSNumber * confirmed;
@property (nonatomic, retain) NSNumber * my_points;

@end
