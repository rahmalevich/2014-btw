//
//  Notification.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Notification : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * confirmed;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSNumber * backend_id;
@property (nonatomic, retain) NSString * travel_polyline;
@property (nonatomic, retain) NSDate * travel_date;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * travel_role;
@property (nonatomic, retain) User *user;

@end
