//
//  Car.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 21.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Car : NSManagedObject

@property (nonatomic, retain) NSString * brand;
@property (nonatomic, retain) NSString * model;
@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSString * number;
@property (nonatomic, retain) User *user;

@end
