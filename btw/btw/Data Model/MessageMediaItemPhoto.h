//
//  MessageMediaItemPhoto.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MessageMediaItem.h"


@interface MessageMediaItemPhoto : MessageMediaItem

@property (nonatomic, retain) NSString * full;
@property (nonatomic, retain) NSString * thumb;
@property (nonatomic, retain) NSString * thumb_2;
@property (nonatomic, retain) NSString * thumb_3;
@property (nonatomic, retain) NSString * source_path;

@end
