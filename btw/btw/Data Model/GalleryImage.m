//
//  GalleryImage.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "GalleryImage.h"
#import "User.h"


@implementation GalleryImage

@dynamic backend_id;
@dynamic message;
@dynamic name;
@dynamic status;
@dynamic thumb_url;
@dynamic url;
@dynamic user_id;
@dynamic user;
@dynamic is_avatar;

@end
