//
//  Dialog.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 06.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DialogMessage, User;

@interface Dialog : NSManagedObject

@property (nonatomic, retain) NSString * backend_id;
@property (nonatomic, retain) NSDate * last_message_date;
@property (nonatomic, retain) NSOrderedSet *messages;
@property (nonatomic, retain) User *user;
@property (nonatomic, retain) NSNumber * unconfirmed_messages_count;
@end

@interface Dialog (CoreDataGeneratedAccessors)

- (void)insertObject:(DialogMessage *)value inMessagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromMessagesAtIndex:(NSUInteger)idx;
- (void)insertMessages:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeMessagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInMessagesAtIndex:(NSUInteger)idx withObject:(DialogMessage *)value;
- (void)replaceMessagesAtIndexes:(NSIndexSet *)indexes withMessages:(NSArray *)values;
- (void)addMessagesObject:(DialogMessage *)value;
- (void)removeMessagesObject:(DialogMessage *)value;
- (void)addMessages:(NSOrderedSet *)values;
- (void)removeMessages:(NSOrderedSet *)values;
@end
