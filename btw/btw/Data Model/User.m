//
//  User.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "User.h"
#import "Answer.h"
#import "Car.h"
#import "Dialog.h"
#import "GalleryImage.h"
#import "Notification.h"


@implementation User

@dynamic backend_id;
@dynamic birthday;
@dynamic facebook_id;
@dynamic gender;
@dynamic last_activity;
@dynamic name;
@dynamic online;
@dynamic photo_mid;
@dynamic photo_small;
@dynamic photo_url;
@dynamic quiz;
@dynamic average_points;
@dynamic surname;
@dynamic vkontakte_id;
@dynamic number_of_feedbacks;
@dynamic answers;
@dynamic car;
@dynamic dialogs;
@dynamic gallery_images;
@dynamic notifications;
@dynamic phone_verified;

@end
