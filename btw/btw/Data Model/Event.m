//
//  Event.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "Event.h"
#import "RouteUser.h"


@implementation Event

@dynamic backend_id;
@dynamic start_date;
@dynamic status;
@dynamic type;
@dynamic finish_address;
@dynamic polyline;
@dynamic finish_date;
@dynamic start_address;
@dynamic driver;
@dynamic passenger;
@dynamic confirmed;
@dynamic my_points;

@end
