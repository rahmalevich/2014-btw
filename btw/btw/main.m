//
//  main.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([BTWApplication class]), NSStringFromClass([BTWAppDelegate class]));
    }
}
