//
//  BTWConstants.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString * const kBaseURL = @"http://dev.togeze.com";

static NSString * const kHockeyAppID = @"6dc8606d931964170c7c673a8313221e";
static NSString * const kGoogleMapsAppID = @"AIzaSyDp_yQB94ZtKcr2le2sWc5zRV4nTUpUGYU";
static NSString * const kVKAppID = @"4857786";

static NSInteger const kCarNumberMaxLength = 15;

static NSString * const kAppStoreID = @"979584714";

static NSString * const kAppsFylerDevID = @"Td6ncC2ExkrXMDB2AjH5fa";