//
//  BTWMapConstants.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

static CGPoint const kUserMarkerGroundAnchor = (CGPoint){0.354, 1.0};
static CGPoint const kRouteMarkerGroundAnchor = (CGPoint){0.31, 1.0};
static CGPoint const kFinishMarkerGroundAnchor = (CGPoint){0.33, 1.0};
static CGFloat const kDefaultZoomLevel = 14.0f;
static CGFloat const kMapBoundsPadding = 100.0f;
