//
//  BTWUIUtils.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 25.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWUIUtils : NSObject

+ (BOOL)isIpad;
+ (BOOL)isSmallScreenDevice;
+ (UIView *)findFirstResponderForView:(UIView *)aView;
+ (BOOL)viewControllerIsVisible:(UIViewController *)viewController;
+ (BOOL)isRussianLocale;

@end

@interface UIView (Frames)

@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat left;
@property (nonatomic, assign) CGFloat top;
@property (nonatomic, assign) CGFloat bottom;
@property (nonatomic, assign) CGFloat right;
@property (nonatomic, assign) CGFloat frameHeight;
@property (nonatomic, assign) CGFloat frameWidth;
@property (nonatomic, assign) CGFloat centerX;
@property (nonatomic, assign) CGFloat centerY;
@property (nonatomic, assign) CGFloat boundsHeight;
@property (nonatomic, assign) CGFloat boundsWidth;
@property (nonatomic, assign) CGFloat boundsX;
@property (nonatomic, assign) CGFloat boundsY;

//Apply ceilf function to view position coordinates.
- (void)ceilPos;

// get rect for an image within some photo view (useful for vertical images to show their top)
+ (CGRect)frameForImage:(UIImage *)image inContainerView:(UIView *)containerView;

//Shows borders for view with specified color.
//Used for debug layout.
- (void)showBordersWithColor:(UIColor*)color;

+ (UIView *)viewWithFrame:(CGRect)frame;

@end

@interface UIView (RoundedCorners)
- (void)makeRoundedCorners:(UIRectCorner)corners withRadius:(CGFloat)radius;
@end

@interface UIColor (Conveniens)
+ (UIColor *)colorWithHex:(int)hexValue;
+ (UIColor *)colorWithHex:(int)hexValue alpha:(CGFloat)alpha;
+ (NSString *)hexFromColor:(UIColor *)color;
@end

@interface UIBarButtonItem (Generation)
+ (UIBarButtonItem *)barButtonWithImage:(UIImage *)image target:(id)target action:(SEL)action;
+ (UIBarButtonItem *)barButtonWithTitle:(NSString *)title target:(id)target action:(SEL)action;
@end

@interface UIImage (Conveniens)
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color;
- (UIImage *)imageByColorizingWithColor:(UIColor *)color;
@end