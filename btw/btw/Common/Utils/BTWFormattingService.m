//
//  BTWDateService.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 21.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWFormattingService.h"

@interface BTWFormattingService ()
+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format;
@end

@implementation BTWFormattingService

#pragma mark - Caching
NSMutableDictionary *_sharedDictionary = nil;
+ (NSMutableDictionary *)sharedFormattersDicitionary
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedDictionary = [NSMutableDictionary dictionary];
    });
    return _sharedDictionary;
}

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format
{
    NSDateFormatter *resultFormatter = [self sharedFormattersDicitionary][format];
    if (!resultFormatter) {
        resultFormatter = [[NSDateFormatter alloc] init];
        resultFormatter.dateFormat = format;
        [self sharedFormattersDicitionary][format] = resultFormatter;
    }
    return resultFormatter;
}

#pragma mark - Public
+ (NSDateFormatter *)dateTimeFormatterWithTimezone
{
    NSDateFormatter *formatter = [self dateFormatterWithFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    return formatter;
}

+ (NSDateFormatter *)shortDateTimeFormatter
{
    static NSString *kShortDateTimeFormatterKey = @"kShortDateTimeFormatterKey";
    NSDateFormatter *formatter = [self sharedFormattersDicitionary][kShortDateTimeFormatterKey];
    if (!formatter) {
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterMediumStyle;
        formatter.timeStyle = NSDateFormatterShortStyle;
        formatter.doesRelativeDateFormatting = YES;
        [self sharedFormattersDicitionary][kShortDateTimeFormatterKey] = formatter;
    }
    return formatter;
}

+ (NSDateFormatter *)shortDateFormatter
{
    return [self shortDateFormatterRelativeFormatting:YES];
}

+ (NSDateFormatter *)shortDateFormatterRelativeFormatting:(BOOL)relativeFormatting
{
    static NSString *kShortDateFormatterKey = @"kShortDateFormatterKey";
    NSDateFormatter *formatter = [self sharedFormattersDicitionary][kShortDateFormatterKey];
    if (!formatter) {
        formatter = [NSDateFormatter new];
        formatter.dateStyle = NSDateFormatterMediumStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
        [self sharedFormattersDicitionary][kShortDateFormatterKey] = formatter;
    }
    formatter.doesRelativeDateFormatting = relativeFormatting;
    return formatter;
}

+ (NSDateFormatter *)timeFormatter
{
    NSDateFormatter *formatter = [self dateFormatterWithFormat:@"HH:mm:ss"];
    return formatter;
}

+ (NSDateFormatter *)shortTimeFormatter
{
    NSDateFormatter *formatter = [self dateFormatterWithFormat:@"HH:mm"];
    return formatter;
}

@end
