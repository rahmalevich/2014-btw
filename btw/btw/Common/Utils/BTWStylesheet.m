//
//  BTWStylesheet.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 01.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWStylesheet.h"

@implementation BTWStylesheet

#pragma mark - Initialization
+ (void)setupCommonStyle
{
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    [[UINavigationBar appearance] setBarTintColor:[BTWStylesheet navigationBarColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    NSDictionary *attributes = @{ NSForegroundColorAttributeName : [UIColor whiteColor] };
    [[UIBarButtonItem appearance] setTitleTextAttributes:attributes forState:UIControlStateNormal];
}

#pragma mark - Common Fonts
+ (UIFont *)commonLightFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize];
}

+ (UIFont *)commonFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
}

+ (UIFont *)commonBoldFontOfSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
}

#pragma mark - Common Colors
+ (UIColor *)commonGrayColor
{
    return [UIColor colorWithHex:0x6b747b];
}

+ (UIColor *)commonOrangeColor
{
    return [UIColor colorWithHex:0xE29626];
}

+ (UIColor *)commonRedColor
{
    return [UIColor colorWithHex:0xc0392b];
}

+ (UIColor *)commonBackgroundColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)navigationTitleColor
{
    return [UIColor colorWithHex:0xffffff];
}

+ (UIColor *)navigationBarColor
{
    return [UIColor colorWithHex:0x455b83];
}

+ (UIColor *)tabBarColor
{
    return [self navigationBarColor];
}

+ (UIColor *)selectedTabColor
{
    return [UIColor colorWithHex:0x3f5170];
}

+ (UIColor *)segmentedControlColor
{
    return [UIColor colorWithHex:0xf8f8f8];
}

+ (UIColor *)facebookBlueColor
{
    return [UIColor colorWithHex:0x5768b1];
}

+ (UIColor *)vkBlueColor
{
    return [UIColor colorWithHex:0x41668c];
}

+ (UIColor *)placeholderGrayColor
{
    return [UIColor colorWithHex:0xb4b4b4];
}

+ (UIColor *)badgeColor
{
    return [UIColor redColor];
}

+ (UIColor *)lightDisclosureColor
{
    return [UIColor colorWithHex:0xaab1be];
}

+ (UIColor *)popupColor
{
    return [UIColor colorWithHex:0xFAF9F9];
}

+ (UIColor *)separatorColor
{
    return [UIColor colorWithWhite:0.9f alpha:1.0f];
}

+ (UIColor *)commonGreenColor
{
    return [UIColor colorWithHex:0x98bb2f];
}

+ (UIColor *)commonTextColor
{
    return [UIColor blackColor];
}

+ (UIColor *)commonGrayTextColor
{
    return [UIColor colorWithHex:0x8b8b8a];
}

+ (UIColor *)commonBlueColor
{
    return [UIColor colorWithHex:0x455b83];
}

+ (UIColor *)commonHeaderColor
{
    return [UIColor colorWithHex:0xf4f4f4];
}

#pragma mark - Popover
+ (UIFont *)popoverTableCellTitleFont
{
    return [self commonFontOfSize:13.0];
}

+ (UIFont *)popoverTableCellHighlightedTitleFont
{
    return [self commonBoldFontOfSize:13.0];
}

+ (UIFont *)popoverTableCellSubtitleFont
{
    return [self commonFontOfSize:13.0];
}

+ (UIColor *)popoverTableCellTitleColor
{
    return [UIColor colorWithWhite:0.4 alpha:1.0];
}

+ (UIColor *)popoverTableCellHighlightedTitleColor
{
    return [UIColor blackColor];
}

+ (UIColor *)popoverTableCellSubtitleColor
{
    return [UIColor colorWithWhite:0.4 alpha:1.0];
}

#pragma mark - Profile
+ (UIColor *)profileFieldsTextColor
{
    return [UIColor colorWithHex:0x374650];
}

+ (UIColor *)profileFieldsBorderColor
{
    return [UIColor colorWithHex:0xe2e4e7];
}

+ (UIColor *)profileNavigationBackgroundColor
{
    return [self commonHeaderColor];
}

#pragma mark - Route
+ (UIColor *)routeBorderColor
{
    return [UIColor colorWithHex:0xd6d6db];
}

+ (UIColor *)routeHistoryBorderColor
{
    return [UIColor colorWithHex:0x374650];
}

+ (UIColor *)routeColor
{
    return [UIColor colorWithHex:0x455b83];
}

+ (UIColor *)startPinColor
{
    return [UIColor colorWithHex:0x8d3647];
}

+ (UIColor *)finishPinColor
{
    return [UIColor colorWithHex:0x356f97];
}

+ (UIColor *)driverColor
{
    return [UIColor colorWithHex:0xe79823];
}

+ (UIColor *)passengerColor
{
    return [UIColor colorWithHex:0x41647c];
}

+ (UIColor *)userRouteColor
{
    return [UIColor colorWithHex:0x27916F];
}

+ (UIColor *)roleSurveyBackgroundColor
{
    return [UIColor colorWithHex:0xe7e7e7];
}

#pragma mark - Matching
+ (UIColor *)matchingCardBorderColor
{
    return [UIColor colorWithHex:0xd4d6d6];
}

#pragma mark - Settings
+ (UIColor *)settingsTitleColor
{
    return [UIColor colorWithHex:0x374650];
}

+ (UIColor *)settingsCellColor
{
    return [UIColor colorWithHex:0xe3e8ed];
}

#pragma mark - Dialogs
+ (UIColor *)onlineColor
{
    return [UIColor colorWithHex:0x95c888];
}

+ (UIColor *)offlineColor
{
    return [UIColor colorWithHex:0x8d3647];
}

+ (UIColor *)errorBubbleColor
{
    return [UIColor colorWithHex:0xfff1f4];
}

+ (UIColor *)dialogBackgroundColor
{
    return [UIColor colorWithHex:0xf3f6fa];
}

+ (UIColor *)incomingMessageBubbleColor
{
    return [UIColor colorWithHex:0xe6e9ee];
}

+ (UIColor *)incomingTextColor
{
    return [self commonTextColor];
}

+ (UIColor *)outgoingMessageBubbleColor
{
    return [UIColor colorWithHex:0x455b83];
}

+ (UIColor *)outgoingTextColor
{
    return [UIColor whiteColor];
}

+ (UIColor *)composerBackgroundColor
{
    return [UIColor colorWithHex:0xf8f8f8];
}

+ (UIColor *)composerControlsColor
{
    return [self commonBlueColor];
}

#pragma mark - Notifications
+ (UIColor *)notificationsTextColor
{
    return [UIColor colorWithHex:0x587186];
}

@end
