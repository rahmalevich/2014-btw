//
//  BTWCommonUtils.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWCommonUtils.h"
#import "BTWSettingsService.h"
#import "BTWFormattingService.h"

#include <sys/types.h>
#include <sys/sysctl.h>

static CGFloat kImageMaxSideSize = 1000.0f;

@implementation BTWCommonUtils

#pragma mark - Images
+ (NSString *)savePhoto:(UIImage *)photo
{
    NSString *dateString = [[BTWFormattingService dateFormatterWithFormat:@"hhMMyyyy_HHmmss"] stringFromDate:[NSDate date]];
    return [self savePhoto:photo withName:dateString];
}

+ (NSString *)savePhoto:(UIImage *)photo withName:(NSString *)photoName
{
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *photoPath = [documentsPath stringByAppendingFormat:@"/%@", photoName];
    
    NSString *folderPath = [photoPath stringByDeletingLastPathComponent];
    if ([folderPath isValidString]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    UIImage *fixedSizeImage = [self fixSize:photo];
    UIImage *fixedOrientationPhoto = [self fixOrientation:fixedSizeImage];
    BOOL photoCreated = [[NSFileManager defaultManager] createFileAtPath:photoPath contents:UIImageJPEGRepresentation(fixedOrientationPhoto, 0.5) attributes:nil];
    return photoCreated ? photoPath : nil;
}

+ (UIImage *)fixSize:(UIImage *)aImage
{
    CGFloat aspectRatio = aImage.size.width / aImage.size.height;
    
    CGSize newSize;
    if (aImage.size.width > aImage.size.height) {
        newSize.width = MIN(kImageMaxSideSize, aImage.size.width);
        newSize.height = newSize.width / aspectRatio;
    } else {
        newSize.height = MIN(kImageMaxSideSize, aImage.size.height);
        newSize.width = newSize.height * aspectRatio;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [aImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)fixOrientation:(UIImage *)aImage
{
    // No-op if the orientation is already correct
    if (aImage.imageOrientation == UIImageOrientationUp)
        return aImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, aImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, aImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (aImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, aImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, aImage.size.width, aImage.size.height,
                                             CGImageGetBitsPerComponent(aImage.CGImage), 0,
                                             CGImageGetColorSpace(aImage.CGImage),
                                             CGImageGetBitmapInfo(aImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (aImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.height,aImage.size.width), aImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,aImage.size.width,aImage.size.height), aImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

#pragma mark - Strings
+ (NSString *)ageStringForAge:(NSInteger)age
{
    NSString *resultString = nil;
    if (age > 0) {
        NSString *yearsString = nil;
        NSInteger lastDigit = age % 10;
        switch (lastDigit) {
            case 1:
                yearsString = NSLocalizedString(@"year", nil);
                break;
            case 2:
            case 3:
            case 4:
                if (age != 12 && age != 13 && age != 14) {
                    yearsString = NSLocalizedString(@"years_1", nil);
                    break;
                }
            default:
                yearsString = NSLocalizedString(@"years_2", nil);
                break;
        }
        resultString = [NSString stringWithFormat:@"%ld %@", (long)age, yearsString];
    } else {
        resultString = NSLocalizedString(@"Age not specified", nil);
    }
    return resultString;
}

+ (NSString *)scaleStringForMeters:(CGFloat)meters
{
    NSString *resultString = nil;
    BTWDistanceUnits units = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue];
    if (units == BTWDistanceUnitsKilometers) {
        CGFloat kilometers = meters / 1000;
        if (kilometers > 1) {
            resultString = [NSString stringWithFormat:@"%.0f %@", floorf(kilometers), NSLocalizedString(@"km", nil)];
        } else {
            CGFloat roundedMeters = meters > 100 ? floorf(meters/100) * 100 : meters;
            resultString = [NSString stringWithFormat:@"%.0f %@", roundedMeters, NSLocalizedString(@"m", nil)];
        }
    } else {
        CGFloat miles = meters / 1609;
        if (miles > 1) {
            resultString = [NSString stringWithFormat:@"%.0f %@", floorf(miles), NSLocalizedString(@"ml", nil)];
        } else {
            CGFloat feets = meters / 0.3048;
            CGFloat roundedFeets;
            if (feets > 1000) {
                roundedFeets = floorf(feets/500) * 500;
            } else if (feets > 100) {
                roundedFeets = floorf(feets/100) * 100;
            } else {
                roundedFeets = feets;
            }
            resultString = [NSString stringWithFormat:@"%.0f %@", roundedFeets, NSLocalizedString(@"ft", nil)];
        }
    }
    
    return resultString;
}

+ (CGFloat)metersForCurrentDistanceUnits:(CGFloat)distanceUnits
{
    CGFloat result = 0;
    BTWDistanceUnits units = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue];
    if (units == BTWDistanceUnitsKilometers) {
        result = distanceUnits * 1000;
    } else {
        result = distanceUnits * 1609;
    }
    return result;
}

+ (CGFloat)currentDistanceUnitsForMeters:(CGFloat)meters
{
    CGFloat result = 0;
    BTWDistanceUnits units = [[BTWSettingsService settingsValueForKey:kCommonDistanceUnitsKey] integerValue];
    if (units == BTWDistanceUnitsKilometers) {
        result = meters / 1000;
    } else {
        result = meters / 1609;
    }
    return result;
}

+ (NSString *)timeStringForMinutes:(CGFloat)minutes
{
    NSInteger absMinutes = (NSInteger)fabsf(minutes);
    NSInteger hours = absMinutes / 60;
    NSInteger restMinutes = absMinutes % 60;
    NSMutableString *resultString = [NSMutableString string];
    if (hours > 24) {
        [resultString appendString:@"> 24 ч"];
    } else {
        if (hours > 0) {
            [resultString appendFormat:@"%02ld ч ", (long)hours];
        }
        [resultString appendFormat:(hours > 0 ? @"%02ld мин" : @"%ld мин"), (long)restMinutes];
    }
    return [NSString stringWithString:resultString];
}

+ (NSString *)stringValueForObject:(id)object
{
    NSString *resultString = nil;
    if ([object isKindOfClass:[NSString class]]) {
        resultString = (NSString *)object;
    } else if ([object isKindOfClass:[NSNumber class]]) {
        NSNumber *numberValue = (NSNumber *)object;
        resultString = [numberValue stringValue];
    }
    return resultString;
}

+ (NSString *)hardwareDescription
{
    NSString *hardware = [self hardwareString];
    if ([hardware isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    if ([hardware isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    if ([hardware isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    if ([hardware isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    if ([hardware isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    if ([hardware isEqualToString:@"iPhone3,3"]) return @"iPhone 4 CDMA";
    if ([hardware isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    if ([hardware isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    if ([hardware isEqualToString:@"iPhone5,2"]) return @"iPhone 5 CDMA GSM";
    if ([hardware isEqualToString:@"iPhone5,3"]) return @"iPhone 5C";
    if ([hardware isEqualToString:@"iPhone5,4"]) return @"iPhone 5C CDMA GSM";
    if ([hardware isEqualToString:@"iPhone6,1"]) return @"iPhone 5S";
    if ([hardware isEqualToString:@"iPhone6,2"]) return @"iPhone 5S CDMA GSM";
    if ([hardware isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    if ([hardware isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    
    if ([hardware isEqualToString:@"iPod1,1"]) return @"iPodTouch 1G";
    if ([hardware isEqualToString:@"iPod2,1"]) return @"iPodTouch 2G";
    if ([hardware isEqualToString:@"iPod3,1"]) return @"iPodTouch 3G";
    if ([hardware isEqualToString:@"iPod4,1"]) return @"iPodTouch 4G";
    if ([hardware isEqualToString:@"iPod5,1"]) return @"iPodTouch 5G";
    
    if ([hardware isEqualToString:@"iPad1,1"]) return @"iPad";
    if ([hardware isEqualToString:@"iPad1,2"]) return @"iPad 3G";
    if ([hardware isEqualToString:@"iPad2,1"]) return @"iPad 2 WIFI";
    if ([hardware isEqualToString:@"iPad2,2"]) return @"iPad 2";
    if ([hardware isEqualToString:@"iPad2,3"]) return @"iPad 2 CDMA";
    if ([hardware isEqualToString:@"iPad2,4"]) return @"iPad 2";
    if ([hardware isEqualToString:@"iPad2,5"]) return @"iPad Mini WIFI";
    if ([hardware isEqualToString:@"iPad2,6"]) return @"iPad Mini";
    if ([hardware isEqualToString:@"iPad2,7"]) return @"iPad Mini WIFI CDMA";
    if ([hardware isEqualToString:@"iPad3,1"]) return @"iPad 3 WIFI";
    if ([hardware isEqualToString:@"iPad3,2"]) return @"iPad 3 WIFI CDMA";
    if ([hardware isEqualToString:@"iPad3,3"]) return @"iPad 3";
    if ([hardware isEqualToString:@"iPad3,4"]) return @"iPad 4 WIFI";
    if ([hardware isEqualToString:@"iPad3,5"]) return @"iPad 4";
    if ([hardware isEqualToString:@"iPad3,6"]) return @"iPad 4 GSM CDMA";
    if ([hardware isEqualToString:@"iPad4,1"]) return @"iPad Air WIFI";
    if ([hardware isEqualToString:@"iPad4,2"]) return @"iPad Air CELLULAR";
    if ([hardware isEqualToString:@"iPad4,4"]) return @"iPad Mini 2G WIFI";
    if ([hardware isEqualToString:@"iPad4,5"]) return @"iPad Mini 2G CELLULAR";
    
    if ([hardware isEqualToString:@"i386"]) return @"Simulator";
    if ([hardware isEqualToString:@"x86_64"]) return @"Simulator";
    
    return nil;
}

+ (NSString *)hardwareString
{
    size_t size = 100;
    char *hw_machine = malloc(size);
    int name[] = {CTL_HW,HW_MACHINE};
    sysctl(name, 2, hw_machine, &size, NULL, 0);
    NSString *hardware = [NSString stringWithUTF8String:hw_machine];
    free(hw_machine);
    return hardware;
}

+ (NSString *)stringForGender:(BTWUserGender)gender
{
    NSString *resultString = nil;
    switch (gender) {
        case BTWUserGenderMale:
            resultString = NSLocalizedString(@"male", nil);
            break;
        case BTWUserGenderFemale:
            resultString = NSLocalizedString(@"female", nil);
            break;
        default:
            resultString = NSLocalizedString(@"not selected", nil);
            break;
    }
    return resultString;
}

+ (NSString *)stringForUnits:(BTWDistanceUnits)units
{
    NSString *result = nil;
    switch (units) {
        case BTWDistanceUnitsKilometers:
            result = NSLocalizedString(@"Kilometers", nil);
            break;
        case BTWDistanceUnitsMiles:
            result = NSLocalizedString(@"Miles", nil);
            break;
    }
    return result;
}

+ (NSString *)shortStringForUnits:(BTWDistanceUnits)units
{
    NSString *result = nil;
    switch (units) {
        case BTWDistanceUnitsKilometers:
            result = NSLocalizedString(@"km", nil);
            break;
        case BTWDistanceUnitsMiles:
            result = NSLocalizedString(@"ml", nil);
            break;
    }
    return result;
}

#pragma mark - Dates
+ (BOOL)dateIsToday:(NSDate *)date
{
    return [self date:date isSameToDate:[NSDate date]];
}

+ (BOOL)date:(NSDate *)firstDate isSameToDate:(NSDate *)secondDate;
{
    NSUInteger componentFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components1 = [calendar components:componentFlags fromDate:firstDate];
    NSDateComponents *components2 = [calendar components:componentFlags fromDate:secondDate];
    return ((components1.year == components2.year) &&
            (components1.month == components2.month) &&
            (components1.day == components2.day));
}

+ (NSDate *)nextHourDateForDate:(NSDate *)inDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components: NSEraCalendarUnit|NSYearCalendarUnit| NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit fromDate:inDate];
    [comps setHour:[comps hour] + 1];
    return [calendar dateFromComponents:comps];
}

@end
