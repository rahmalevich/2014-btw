//
//  BTWUIUtils.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 25.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWUIUtils.h"

@implementation BTWUIUtils

+ (BOOL)isIpad
{
    static BOOL isIpad = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isIpad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    });
    return isIpad;
}

+ (BOOL)isSmallScreenDevice
{
    return [[UIScreen mainScreen] bounds].size.height < 568;
}

+ (UIView *)findFirstResponderForView:(UIView *)aView
{
    UIView *resultView = nil;
    if ([aView isFirstResponder]) {
        resultView = aView;
    } else {
        for (UIView *subview in aView.subviews) {
            resultView = [self findFirstResponderForView:subview];
            if (resultView) {
                break;
            }
        }
    }
    return resultView;
}

+ (BOOL)viewControllerIsVisible:(UIViewController *)viewController
{
    BOOL result = NO;
    if (viewController.isViewLoaded && viewController.view.window) {
        result = YES;
    }
    return result;
}

+ (BOOL)isRussianLocale
{
    return [[NSLocale currentLocale].localeIdentifier isEqualToString:@"ru_RU"];
}

@end

@implementation UIView (Frames)

- (void)setOrigin:(CGPoint)loc {
    CGRect rc = self.frame;
    rc.origin = loc;
    self.frame = rc;
}

-(void)setSize:(CGSize)sz {
    CGRect rc = self.frame;
    rc.size = sz;
    self.frame = rc;
}

- (void)setX:(CGFloat)x {
    CGRect rc = self.frame;
    rc.origin.x = x;
    self.frame = rc;
}

- (void)setY:(CGFloat)y {
    CGRect rc = self.frame;
    rc.origin.y = y;
    self.frame = rc;
}

- (void)setLeft:(CGFloat)x { self.x = x; }
- (void)setTop:(CGFloat)y { self.y = y; }

- (void)setRight:(CGFloat)r {
    CGRect rc = self.frame;
    rc.origin.x = r-rc.size.width;
    self.frame = rc;
}

-(void) setBottom:(CGFloat)b
{
    CGRect rc = self.frame;
    rc.origin.y = b-rc.size.height;
    self.frame = rc;
}

-(void) setFrameWidth:(CGFloat)w
{
    CGRect rc = self.frame;
    rc.size.width = w;
    self.frame = rc;
}

-(void) setFrameHeight:(CGFloat)h
{
    CGRect rc = self.frame;
    rc.size.height = h;
    self.frame = rc;
}

-(void) setCenterX:(CGFloat) x
{
    CGPoint pt = self.center;
    pt.x = x;
    self.center = pt;
}

-(void) setCenterY:(CGFloat) y
{
    CGPoint pt = self.center;
    pt.y = y;
    self.center = pt;
}

-(void) setBoundsX:(CGFloat) x
{
    CGRect rc = self.bounds;
    rc.origin.x = x;
    self.bounds = rc;
}

-(void) setBoundsY:(CGFloat) y
{
    CGRect rc = self.bounds;
    rc.origin.y = y;
    self.bounds = rc;
}

-(void) setBoundsWidth:(CGFloat)w
{
    CGRect rc = self.bounds;
    rc.size.width = w;
    self.bounds = rc;
}

-(void) setBoundsHeight:(CGFloat)h
{
    CGRect rc = self.bounds;
    rc.size.height = h;
    self.bounds = rc;
}

-(CGPoint)origin
{
    return self.frame.origin;
}

-(CGSize) size
{
    return self.frame.size;
}

-(CGFloat) x {
    return self.frame.origin.x;
}

-(CGFloat) y
{
    return self.frame.origin.y;
}

-(CGFloat) top
{
    return self.frame.origin.y;
}

-(CGFloat) left {
    return self.frame.origin.x;
}

-(CGFloat) right {
    return self.frame.origin.x + self.frame.size.width;
}

-(CGFloat) bottom
{
    return self.frame.origin.y + self.frame.size.height;
}

-(CGFloat) frameWidth
{
    return self.frame.size.width;
}

-(CGFloat) frameHeight
{
    return self.frame.size.height;
}

-(CGFloat) centerX {
    return self.center.x;
}

-(CGFloat) centerY {
    return self.center.y;
}

-(CGFloat) boundsX
{
    return self.bounds.origin.x;
}

-(CGFloat) boundsY {
    return self.bounds.origin.y;
}

-(CGFloat) boundsWidth {
    return self.bounds.size.width;
}

-(CGFloat) boundsHeight {
    return self.bounds.size.height;
}

- (void)ceilPos {
    CGRect currentFrame = self.frame;
    self.frame = CGRectMake(ceilf(currentFrame.origin.x), ceilf(currentFrame.origin.y),currentFrame.size.width, currentFrame.size.height);
}

+ (CGRect)frameForImage:(UIImage *)image inContainerView:(UIView *)containerView {
    CGRect frame = CGRectZero;
    CGFloat ratio = image.size.height / image.size.width;
    if (ratio <= 1.0) {
        frame = containerView.bounds;
    } else {
        frame = CGRectMake(0, 0, containerView.boundsWidth, floorf(containerView.boundsWidth * ratio));
    }
    
    return frame;
}

- (void)showBordersWithColor:(UIColor*)color
{
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = [color CGColor];
}

+ (UIView *)viewWithFrame:(CGRect)frame
{
    return [[UIView alloc] initWithFrame:frame];
}

@end

@implementation UIView (RoundedCorners)

- (void)makeRoundedCorners:(UIRectCorner)corners withRadius:(CGFloat)radius
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:(CGSize){radius, radius}];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path = [path CGPath];
    maskLayer.fillColor = [UIColor blackColor].CGColor;
    self.layer.mask = maskLayer;
    
    [self setNeedsDisplay];
}

@end

@implementation UIColor (Conveniens)

+ (UIColor *)colorWithHex:(int)hexValue
{
    return [self colorWithHex:hexValue alpha:1.];
}

+ (UIColor *)colorWithHex:(int)hexValue alpha:(CGFloat)alpha
{
    return [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0
                           green:((float)((hexValue & 0xFF00) >> 8))/255.0
                            blue:((float)(hexValue & 0xFF))/255.0
                           alpha:alpha];
}

+ (NSString *)hexFromColor:(UIColor *)color
{
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"0x%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}

@end

@implementation UIBarButtonItem (Generation)

+ (UIBarButtonItem *)barButtonWithImage:(UIImage *)image target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = (CGRect){CGPointZero, image.size};
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *)barButtonWithTitle:(NSString *)title target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[BTWStylesheet commonBoldFontOfSize:15.0]];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end

@implementation UIImage (Conveniens)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, (CGRect){.size = size});
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    return [UIImage imageWithColor:color size:CGSizeMake(1, 1)];
}

- (UIImage *)imageByColorizingWithColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextSaveGState(ctx);
    CGContextDrawImage(ctx, area, self.CGImage);
    CGContextRestoreGState(ctx);
    [color set];
    CGContextSetBlendMode(ctx, kCGBlendModeSourceAtop);
    CGContextFillRect(ctx, area);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end