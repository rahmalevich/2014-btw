//
//  KeyboardHelper.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface KeyboardHelper : NSObject

@property (nonatomic, strong, readonly) UIView *targetView;
@property (nonatomic, strong, readonly) UIScrollView *scrollView;

- (instancetype)initWithTargetView:(UIView *)targetView scrollView:(UIScrollView *)scrollView;

@end
