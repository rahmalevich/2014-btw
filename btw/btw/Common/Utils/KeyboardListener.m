//
//  KeyboardListener.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 21.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "KeyboardListener.h"

@interface KeyboardListener ()
@property (nonatomic, assign) BOOL isKeyboardVisible;
@property (nonatomic, assign) CGRect keyboardRect;
@end

@implementation KeyboardListener

#pragma mark - Initialization
+ (void)initialize
{
    [self sharedInstance];
}

static KeyboardListener *_sharedInstance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [KeyboardListener new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

#pragma mark - Notifications
- (void)keyboardWillShow:(NSNotification *)notification
{
    self.isKeyboardVisible = YES;
    
    NSDictionary *info = [notification userInfo];
    self.keyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    self.keyboardRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    self.isKeyboardVisible = NO;
}

@end
