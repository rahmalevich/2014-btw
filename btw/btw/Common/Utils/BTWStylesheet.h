//
//  BTWStylesheet.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 01.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static CGFloat const kCentralButtonOffset = 15.0f;

@interface BTWStylesheet : NSObject

+ (void)setupCommonStyle;

// Common Fonts
+ (UIFont *)commonLightFontOfSize:(CGFloat)fontSize;
+ (UIFont *)commonFontOfSize:(CGFloat)fontSize;
+ (UIFont *)commonBoldFontOfSize:(CGFloat)fontSize;

// Common Colors
+ (UIColor *)commonGrayColor;
+ (UIColor *)commonOrangeColor;
+ (UIColor *)commonRedColor;
+ (UIColor *)commonBackgroundColor;
+ (UIColor *)tabBarColor;
+ (UIColor *)selectedTabColor;
+ (UIColor *)navigationBarColor;
+ (UIColor *)navigationTitleColor;
+ (UIColor *)segmentedControlColor;
+ (UIColor *)facebookBlueColor;
+ (UIColor *)placeholderGrayColor;
+ (UIColor *)badgeColor;
+ (UIColor *)lightDisclosureColor;
+ (UIColor *)popupColor;
+ (UIColor *)separatorColor;
+ (UIColor *)commonGreenColor;
+ (UIColor *)commonTextColor;
+ (UIColor *)commonGrayTextColor;
+ (UIColor *)commonBlueColor;
+ (UIColor *)commonHeaderColor;

// Popover
+ (UIFont *)popoverTableCellTitleFont;
+ (UIFont *)popoverTableCellHighlightedTitleFont;
+ (UIFont *)popoverTableCellSubtitleFont;
+ (UIColor *)popoverTableCellTitleColor;
+ (UIColor *)popoverTableCellHighlightedTitleColor;
+ (UIColor *)popoverTableCellSubtitleColor;

// Profile
+ (UIColor *)profileFieldsTextColor;
+ (UIColor *)profileFieldsBorderColor;
+ (UIColor *)profileNavigationBackgroundColor;

// Route
+ (UIColor *)routeBorderColor;
+ (UIColor *)routeHistoryBorderColor;
+ (UIColor *)routeColor;
+ (UIColor *)startPinColor;
+ (UIColor *)finishPinColor;
+ (UIColor *)driverColor;
+ (UIColor *)passengerColor;
+ (UIColor *)userRouteColor;
+ (UIColor *)roleSurveyBackgroundColor;

// Matching
+ (UIColor *)matchingCardBorderColor;

// Settings
+ (UIColor *)settingsTitleColor;
+ (UIColor *)settingsCellColor;

// Dialogs
+ (UIColor *)onlineColor;
+ (UIColor *)offlineColor;
+ (UIColor *)errorBubbleColor;
+ (UIColor *)dialogBackgroundColor;
+ (UIColor *)incomingMessageBubbleColor;
+ (UIColor *)incomingTextColor;
+ (UIColor *)outgoingMessageBubbleColor;
+ (UIColor *)outgoingTextColor;
+ (UIColor *)composerBackgroundColor;
+ (UIColor *)composerControlsColor;

// Notifications
+ (UIColor *)notificationsTextColor;

@end
