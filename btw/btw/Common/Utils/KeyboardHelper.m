//
//  KeyboardHelper.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 14.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "KeyboardHelper.h"

@interface KeyboardHelper () <UIGestureRecognizerDelegate>
@property (nonatomic, strong, readwrite) UIView *targetView;
@property (nonatomic, strong, readwrite) UIScrollView *scrollView;
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, assign) CGFloat keyboardHeight;
@end

@implementation KeyboardHelper

#pragma mark - Initialization
- (instancetype)initWithTargetView:(UIView *)targetView scrollView:(UIScrollView *)scrollView
{
    if (self = [super init]) {
        self.targetView = targetView;
        self.scrollView = scrollView;
        _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;

        self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapRecognized:)];
        _tapRecognizer.delegate = self;
        [_targetView addGestureRecognizer:_tapRecognizer];

        // cancelsTouchesInView = NO, чтобы кроме обработки жеста, нажимались кнопки
        // в некоторых случаях кнопки не получают событие touchUpInside, поэтому приходится
        // обрабатывать нажатие по touchDown
        _tapRecognizer.cancelsTouchesInView = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [_targetView removeGestureRecognizer:_tapRecognizer];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Tap recognizer
- (void)backgroundTapRecognized:(UITapGestureRecognizer *)tapRecognizer
{
    [[BTWUIUtils findFirstResponderForView:_targetView] resignFirstResponder];
}

#pragma mark - Keyboard notifications
- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    if (_targetView.window) {
        NSDictionary *info = [notification userInfo];
        CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGFloat newKeyboardHeight = _targetView.window.size.height - kbRect.origin.y;
        
        UIEdgeInsets contentInsets = _scrollView.contentInset;
        contentInsets.bottom += newKeyboardHeight - _keyboardHeight;
        _scrollView.contentInset = contentInsets;
        _scrollView.scrollIndicatorInsets = contentInsets;
        
        self.keyboardHeight = newKeyboardHeight;
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (_targetView.window) {
        NSDictionary *info = [notification userInfo];
        CGRect kbRect = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        CGRect aRect = _targetView.frame;
        aRect.origin.y = _scrollView.contentOffset.y;
        aRect.size.height -= kbRect.size.height;
        
        UIView *firstResponder = [BTWUIUtils findFirstResponderForView:_targetView];
        CGPoint responderBottom = CGPointMake(firstResponder.x, firstResponder.y + firstResponder.frameHeight);
        CGPoint convertedBottom = [_scrollView convertPoint:responderBottom fromView:firstResponder.superview];
        if (!CGRectContainsPoint(aRect, convertedBottom))
        {
            [_scrollView scrollRectToVisible:[_targetView convertRect:(CGRect){responderBottom, (CGSize){1, 1}} fromView:firstResponder.superview] animated:YES];
        }
    }
}

@end
