//
//  BTWCommonUtils.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSettingsService.h"

@interface BTWCommonUtils : NSObject

// Images
+ (UIImage *)fixOrientation:(UIImage *)aImage;
+ (NSString *)savePhoto:(UIImage *)photo;
+ (NSString *)savePhoto:(UIImage *)photo withName:(NSString *)photoName;

// Strings
+ (NSString *)stringValueForObject:(id)object;
+ (NSString *)ageStringForAge:(NSInteger)age;
+ (NSString *)scaleStringForMeters:(CGFloat)meters;
+ (CGFloat)metersForCurrentDistanceUnits:(CGFloat)distanceUnits;
+ (CGFloat)currentDistanceUnitsForMeters:(CGFloat)meters;
+ (NSString *)timeStringForMinutes:(CGFloat)minutes;
+ (NSString *)hardwareDescription;
+ (NSString *)stringForGender:(BTWUserGender)gender;
+ (NSString *)stringForUnits:(BTWDistanceUnits)units;
+ (NSString *)shortStringForUnits:(BTWDistanceUnits)units;

// Dates
+ (BOOL)dateIsToday:(NSDate *)date;
+ (BOOL)date:(NSDate *)firstDate isSameToDate:(NSDate *)secondDate;
+ (NSDate *)nextHourDateForDate:(NSDate *)inDate;

@end
