//
//  BTWDateService.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 21.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWFormattingService : NSObject

+ (NSDateFormatter *)dateFormatterWithFormat:(NSString *)format;

+ (NSDateFormatter *)dateTimeFormatterWithTimezone;
+ (NSDateFormatter *)shortDateTimeFormatter;
+ (NSDateFormatter *)shortDateFormatter;
+ (NSDateFormatter *)shortDateFormatterRelativeFormatting:(BOOL)relativeFormatting;
+ (NSDateFormatter *)timeFormatter;
+ (NSDateFormatter *)shortTimeFormatter;

@end
