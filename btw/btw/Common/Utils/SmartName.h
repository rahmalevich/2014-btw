//
//  SmartName.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

typedef NS_ENUM(NSUInteger, SmartPadezh) {
    SmartPadezhImenitelnij = 0,
    SmartPadezhRoditelnij = 1,
    SmartPadezhDatelnij = 2,
    SmartPadezhVinitelnij = 3,
    SmartPadezhTvoritelnij = 4,
    SmartPadezhPredlojnij = 5
};

typedef NS_ENUM(NSUInteger, SmartPol) {
    SmartPolMujskoj = 0,
    SmartPolJenskij = 1
};

@interface SmartName : NSObject

+ (SmartName *)sharedInstance;
- (NSString *)processName:(NSString *)name pol:(SmartPol)pol padezh:(SmartPadezh)padezh;

@end
