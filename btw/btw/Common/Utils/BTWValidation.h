//
//  BTWValidation.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 21.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface NSObject(Validation)
- (BOOL)isValid;
- (BOOL)isValidString;
- (BOOL)isValidDictionary;
- (BOOL)isValidArray;
- (BOOL)isValidNumber;
- (id)validValueForKey:(id)key;
@end

@interface NSNull(Validation)
@end

@interface NSString(Validation)
@end

@interface NSNumber(Validation)
@end

@interface NSArray(Validation)
@end

@interface NSDictionary(ValidationAndParsing)
- (NSString *)stringIdWithKey:(NSString *)key;
- (NSString *)stringWithKey:(NSString *)key;

- (NSNumber *)numberWithKey:(NSString *)key;
- (BOOL)boolWithKey:(NSString *)key;
- (NSInteger)intWithKey:(NSString *)key;

- (long long)longLongWithKey:(NSString *)key;

- (NSURL *)urlWithKey:(NSString *)key;
- (NSDictionary *)dictionaryWithKey:(NSString *)key;
- (NSArray *)arrayWithKey:(NSString *)key;
@end

@interface BTWValidation : NSObject
+ (BOOL)validateEmail:(NSString *)email;
+ (BOOL)validatePassword:(NSString *)password;
@end
