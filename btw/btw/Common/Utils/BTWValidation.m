//
//  BTWValidation.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 21.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@implementation NSObject(Validation)

- (BOOL)isValid
{
    return YES;
}

- (BOOL)isValidString
{
    return NO;
}

- (BOOL)isValidDictionary
{
    return NO;
}

- (BOOL)isValidArray
{
    return NO;
}

- (BOOL)isValidNumber
{
    return NO;
}

- (id) validValueForKey:(id)key
{
    return nil;
}

@end

@implementation NSNull(Validation)

- (BOOL)isValid
{
    return NO;
}

@end

@implementation NSString(Validation)

- (BOOL)isValid
{
    return self.length > 0;
}

- (BOOL)isValidString
{
    return self.isValid;
}

@end

@implementation NSArray(Validation)

- (BOOL)isValid
{
    return self.count > 0;
}

- (BOOL)isValidArray
{
    return self.isValid;
}

@end

@implementation NSNumber(Validation)

- (BOOL)isValidNumber
{
    return YES;
}

@end

@implementation NSDictionary(ValidationAndParsing)

- (BOOL)isValid
{
    return self.count > 0;
}

- (BOOL)isValidDictionary
{
    return self.isValid;
}

- (NSString *)stringIdWithKey:(NSString *)key
{
    id value = self[key];
    if ([value isValidString]) {
        return value;
    }
    if ([value isValidNumber]) {
        return [value stringValue];
    }
    return nil;
}

- (NSString *)stringWithKey:(NSString *)key
{
    NSString *value = self[key];
    return value.isValidString ? value : nil;
}

- (NSNumber *)numberWithKey:(NSString *)key
{
    NSNumber *value = self[key];
    return value.isValidNumber ? value : nil;
}

- (BOOL)boolWithKey:(NSString *)key
{
    return [[self numberWithKey:key] boolValue];
}

- (NSInteger)intWithKey:(NSString *)key
{
    return [[self numberWithKey:key] intValue];
}

- (long long)longLongWithKey:(NSString *)key
{
    return [[self numberWithKey:key] longLongValue];
}

- (NSURL *)urlWithKey:(NSString *)key
{
    NSString *value = [self stringWithKey:key];
    if (value) {
        return [NSURL URLWithString:value];
    }
    return nil;
}

- (id) validValueForKey:(id)key
{
    NSObject *value = self[key];
    return value.isValid ? value : nil;
}

- (NSDictionary *)dictionaryWithKey:(NSString *)key
{
    NSDictionary *value = self[key];
    return value.isValidDictionary ? value : nil;
}

- (NSArray *)arrayWithKey:(NSString *)key
{
    NSArray *value = self[key];
    return value.isValidArray ? value : nil;
}

@end

@implementation BTWValidation

+ (BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validatePassword:(NSString *)candidate
{
    NSString *passwordRegex = @"[A-Z0-9a-z]{8,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordTest evaluateWithObject:candidate];
}

@end
