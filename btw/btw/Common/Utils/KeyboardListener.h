//
//  KeyboardListener.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 21.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface KeyboardListener : NSObject

+ (instancetype)sharedInstance;

- (BOOL)isKeyboardVisible;
- (CGRect)keyboardRect;

@end
