//
//  UIImage+ImageWithColor.h
//  WordClock
//
//  Created by James Rutherford on 2012-12-03.
//  Copyright (c) 2012 Braxio Interactive. All rights reserved.
//



@interface UIImage (ImageWithColor)

- (UIImage *)imageWithColor:(UIColor *)color;

@end