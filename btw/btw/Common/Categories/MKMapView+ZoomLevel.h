//
//  MKMapView+ZoomLevel.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

@property (nonatomic, assign) CGFloat zoomLevel;

- (void)setZoomLevel:(CGFloat)zoomLevel animated:(BOOL)animated;
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate zoomLevel:(CGFloat)zoomLevel animated:(BOOL)animated;
- (CGFloat)metersInPixels:(CGFloat)pixels;

@end
