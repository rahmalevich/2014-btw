//
//  MKMapView+ZoomLevel.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "MKMapView+ZoomLevel.h"
#import <objc/runtime.h>

#define MERCATOR_OFFSET 268435456
#define MERCATOR_RADIUS 85445659.44705395
#define KILOMETERS_IN_DEGREE 111
#define METERS_TO_DEGRREES(x) (x * 1.0f/(KILOMETERS_IN_DEGREE * 1000.0f))
#define DEGREES_TO_METERS(x) (x * KILOMETERS_IN_DEGREE * 1000.0f)

static CGFloat const kMinZoomLevel = 1.0f;
static CGFloat const kMaxZoomLevel = 17.0f;

@implementation MKMapView (ZoomLevel)

#pragma mark - Zoom level
- (CGFloat)zoomLevel
{
    CGFloat zoomLevel = log2((double)(self.bounds.size.width / self.visibleMapRect.size.width)) + 19.0f;
    return zoomLevel;
}

- (void)setZoomLevel:(CGFloat)zoomLevel
{
    [self setZoomLevel:zoomLevel animated:NO];
}

- (void)setZoomLevel:(CGFloat)zoomLevel animated:(BOOL)animated
{
    [self willChangeValueForKey:@"zoomLevel"];
    zoomLevel = MIN(MAX(kMinZoomLevel, zoomLevel), kMaxZoomLevel);
    [self setCenterCoordinate:self.centerCoordinate zoomLevel:zoomLevel animated:animated];
    [self didChangeValueForKey:@"zoomLevel"];
}

#pragma mark -
#pragma mark Map conversion methods

- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude
{
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX
{
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY
{
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
}

#pragma mark -
#pragma mark Helper methods

- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)mapView centerCoordinate:(CLLocationCoordinate2D)centerCoordinate andZoomLevel:(CGFloat)zoomLevel
{
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    // determine the scale value from the zoom level
    CGFloat zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = mapView.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}

#pragma mark -
#pragma mark Public methods

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate zoomLevel:(CGFloat)zoomLevel animated:(BOOL)animated
{
    // use the zoom level to compute the region
    MKCoordinateSpan span = [self coordinateSpanWithMapView:self centerCoordinate:centerCoordinate andZoomLevel:zoomLevel];
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    // set the region like normal
    [self setRegion:region animated:animated];
}

- (CGFloat)metersInPixels:(CGFloat)pixels
{
    MKZoomScale currentZoomScale = (CGFloat)(self.bounds.size.width / self.visibleMapRect.size.width);
    CLLocationDegrees degrees = [self pixelSpaceXToLongitude:pixels / currentZoomScale] - [self pixelSpaceXToLongitude:0];
    CGFloat meters = DEGREES_TO_METERS(degrees);
    return meters;
}

@end
