//
//  BTWBlocks.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 30.07.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

typedef NS_ENUM(NSUInteger, BTWNetworkProgressType) {
    BTWNetworkProgressTypeUpload,
    BTWNetworkProgressTypeDownload
};

typedef void(^BTWNetworkResponseBlock)(id response, NSError *error);
typedef id(^BTWResponseParsingBlock)(id response, NSError **error);
typedef void(^BTWNetworkProgressBlock)(BTWNetworkProgressType type, CGFloat progress);

typedef void(^BTWVoidBlock)(void);
typedef void(^BTWDataBlock)(id data);
typedef void(^BTWErrorBlock)(NSError *error);
