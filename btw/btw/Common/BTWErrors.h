//
//  BTWErrors.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 27.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString *const kBTWAPIErrorDomain = @"kBTWAPIErrorDomain";
typedef NS_ENUM(NSUInteger, BTWApiErrorCode) {
    BTWApiErrorCodeUnknown = 0,
    BTWApiErrorCodeServerLocked = 1,
    BTWApiErrorCodeInvalidRequest = 2,
    BTWApiErrorCodeSessionNotFound = 3,
    BTWApiErrorCodeLoginFailed = 5,
    BTWApiErrorCodeProfileNotFound = 7,
    BTWApiErrorCodeAuthentificationFailed = 8,
    BTWApiErrorCodeUnsupportedPlatform = 10,
    BTWApiErrorCodeDriveSearchQueryNotFound = 11,
    BTWApiErrorCodeResourceNotFound = 15,
    BTWApiErrorCodeUserNotFound = 16,
    BTWApiErrorCodeStorageRestriction = 23,
    BTWApiErrorCodeUserAlreadyRegistered = 34,
    BTWApiErrorCodeBirthdayIsMissing = 51,
    BTWApiErrorCodeAgeRestriction = 52,
    BTWApiErrorCodeServerError = 500
};

static NSString *const kBTWInternalErrorDomain = @"kBTWInternalErrorDomain";
typedef NS_ENUM(NSUInteger, BTWInternalErrorCode) {
    BTWInternalErrorCodeUnknown = 0,
    BTWInternalErrorCodeWrongFacebookID = 1,
    BTWInternalErrorCodeEmptyFacebookSession = 2,
    BTWInternalErrorCodeWrongVkID = 3,
    BTWInternalErrorCodeEmptyVkSession = 4,
    BTWInternalErrorNetworkConnectionLost = 10
};
