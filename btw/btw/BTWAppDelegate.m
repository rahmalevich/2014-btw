//
//  BTWAppDelegate.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWAppDelegate.h"
#import "BTWUserService.h"
#import "BTWTransitionsManager.h"
#import "BTWSettingsService.h"
#import "BTWPushNotificationsService.h"
#import "BTWSystemVersionUtils.h"
#import "BTWEventsService.h"

#import <HockeySDK/HockeySDK.h>
#import <FacebookSDK/FacebookSDK.h>
#import "VKSdk.h"
#import "MBProgressHUD.h"
#import "AFNetworkReachabilityManager.h"
#import "GoogleMaps.h"
#import "SVGeocoder.h"
#import "Appirater.h"

#ifdef DEBUG
#import <FLEX/FLEXManager.h>
#endif

@interface BTWAppDelegate ()
@end

@implementation BTWAppDelegate

#pragma mark - Application lifecycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:kHockeyAppID];
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator authenticateInstallation];
    [BITHockeyManager sharedHockeyManager].crashManager.crashManagerStatus = BITCrashManagerStatusAutoSend;
    
    [GMSServices provideAPIKey:kGoogleMapsAppID];
    [SVGeocoder provideAPIKey:kGoogleMapsAppID];
    
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    [Appirater setAppId:kAppStoreID];
    [Appirater setDaysUntilPrompt:0];
    [Appirater setUsesUntilPrompt:0];
    [Appirater setSignificantEventsUntilPrompt:1];
    [Appirater setTimeBeforeReminding:2];
    [Appirater setCustomAlertMessage:NSLocalizedString(@"Enjoyed the ride with Togeze? Rate app in AppStore!", nil)];
    [Appirater setDebug:NO];
    
    [AppsFlyerTracker sharedTracker].appleAppID = kAppStoreID;
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = kAppsFylerDevID;
    
    [BTWStylesheet setupCommonStyle];
    
    [BTWSettingsService setDefaultsIfNeeded];
    if ([BTWSettingsService isFirstLaunch]) {
        [[AppsFlyerTracker sharedTracker] trackEvent:@"install" withValue:nil];
    }
    
    [[BTWPushNotificationsService sharedInstance] setup];
    
    [BTWEventsService sharedInstance];
    
    BOOL authorized = ([BTWUserService sharedInstance].authorizedUser != nil);
    UIViewController *initialViewController = nil;
    if (authorized) {
        initialViewController = [[BTWTransitionsManager sharedInstance] appEntryPointVC];
    } else {
        BOOL greetingShowed = [[BTWSettingsService settingsValueForKey:kGreetingsShowedKey] boolValue];
        if (greetingShowed) {
            initialViewController = [[BTWTransitionsManager sharedInstance] authorizationEntryPointVC];
        } else {
            initialViewController = [[BTWTransitionsManager sharedInstance] greetingVC];
            [BTWSettingsService setSettingsValue:@(YES) forKey:kGreetingsShowedKey];
        }
    }

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = initialViewController;
    [self.window makeKeyAndVisible];
    
    BOOL launchedFromPushNotification = NO;
    if (launchOptions != nil) {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
            launchedFromPushNotification = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [[BTWPushNotificationsService sharedInstance] handleNotification:dictionary fromBackground:YES];
            });
        }
    }
    
    if (authorized) {
        if (!launchedFromPushNotification) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
            hud.labelText = [NSString stringWithFormat:@"%@...", NSLocalizedString(@"Authorization", nil)];
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:AFNetworkingReachabilityDidChangeNotification object:nil];
    }
    
    return YES;
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AFNetworkingReachabilityDidChangeNotification object:nil];
    if (![AFNetworkReachabilityManager sharedManager].reachable) {
        [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Auth: reachability error", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    } else {
        [[BTWUserService sharedInstance] reloginWithCompletionHandler:^(NSError *error){
            [MBProgressHUD hideAllHUDsForView:self.window animated:YES];
        }];
    }
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL handled = NO;
    handled = [FBSession.activeSession handleOpenURL:url];
    if (!handled) {
        handled = [VKSdk processOpenURL:url fromApplication:sourceApplication];
    }
    return handled;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBAppCall handleDidBecomeActive];
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
    [application setApplicationIconBadgeNumber:0];
}

#pragma mark - APN
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[BTWPushNotificationsService sharedInstance] setTokenWithData:deviceToken];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    BOOL receivedInBackground = (application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground);
    [[BTWPushNotificationsService sharedInstance] handleNotification:userInfo fromBackground:receivedInBackground];
}

@end


@implementation BTWApplication

- (void)sendEvent:(UIEvent *)event {
#ifdef DEBUG
    if (self.applicationState == UIApplicationStateActive && event.type == UIEventTypeMotion && event.subtype == UIEventSubtypeMotionShake) {
        [[FLEXManager sharedManager] showExplorer];
    }
#endif
    [super sendEvent:event];
}

@end
