//
//  TRDataController.h
//  Trumpet
//
//  Created by Mikhail Rakhmalevich on 31.07.14.
//  Copyright (c) 2014 Trumpet. All rights reserved.
//

@class BTWDataController;
@protocol BTWDataControllerDelegate <NSObject>
- (void)dataController:(BTWDataController *)controller didEndLoadingDataWithError:(NSError *)error;
@end

@interface BTWDataController : NSObject

@property (nonatomic, weak) id<BTWDataControllerDelegate> delegate;

- (id)initWithDelegate:(id<BTWDataControllerDelegate>)delegate;

@end
