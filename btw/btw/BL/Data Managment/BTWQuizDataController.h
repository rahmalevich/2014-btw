//
//  BTWProfileQuestionsDataController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDataController.h"

@interface BTWQuizDataController : BTWDataController

@property (nonatomic, strong, readonly) Question *currentQuestion;

- (NSInteger)numberOfQuestions;
- (NSInteger)currentQuestionIndex;
- (void)nextQuestion;
- (void)previousQuestion;
- (void)restartQuestionnaire;
- (void)chooseAnswer:(Answer *)answer;

@end
