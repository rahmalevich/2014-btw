//
//  BTWProfileFeedbacksDataController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWProfileFeedbacksDataController.h"
#import "BTWFeedbacksService.h"

@interface BTWProfileFeedbacksDataController ()
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, strong, readwrite) NSArray *feedbacksArray;
@property (nonatomic, assign, readwrite) BOOL isLoadingData;
@end

@implementation BTWProfileFeedbacksDataController

#pragma mark - Initialization & Memory managment
- (id)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithDelegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.localContext = [NSManagedObjectContext MR_context];
}

- (void)dealloc
{
    [_localContext rollback];
}

#pragma mark - Data methods
- (void)setUser:(User *)user
{
    _user = user;
    [self reloadData];
}

- (void)reloadData
{
    if (!_user || _isLoadingData) {
        return;
    }
    self.isLoadingData = YES;
    
    __weak typeof(self) wself = self;
    NSMutableArray *feedbacksArray = [NSMutableArray array];
    [[BTWFeedbacksService sharedInstance] getFeedbacksForUser:_user processingBlock:^(NSDictionary *response)
    {
        [wself.localContext rollback];
        NSArray *feedbacksDataArray = response[@"payload"];
        for (NSDictionary *feedbackDict in feedbacksDataArray) {
            Feedback *feedback = [Feedback MR_importFromObject:feedbackDict inContext:wself.localContext];
            [feedbacksArray addObject:feedback];
        }
    } completionHandler:^(NSError *error){
        wself.isLoadingData = NO;
        if (!error) {
            wself.feedbacksArray = [NSArray arrayWithArray:feedbacksArray];
        }
        [wself.delegate dataController:wself didEndLoadingDataWithError:error];
    }];
}

@end
