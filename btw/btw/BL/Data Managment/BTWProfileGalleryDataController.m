//
//  BTWProfileGalleryDataController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWProfileGalleryDataController.h"
#import "BTWUserService.h"

// TODO: Получать количество фотографий с сервера
static NSInteger const kMaxPhotosCount = 5;

@interface BTWProfileGalleryDataController ()
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, strong) NSArray *photosArray;
@end

@implementation BTWProfileGalleryDataController

#pragma mark - Initialization & Memory managment
- (id)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithDelegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.localContext = [NSManagedObjectContext MR_context];
}

- (void)dealloc
{
    [_localContext rollback];
}

- (void)updatePhotos
{
    __weak typeof(self) wself = self;
    NSMutableArray *photosArray = [NSMutableArray array];
    [[BTWUserService sharedInstance] getGalleryListForUser:_user processingBlock:^(NSDictionary *response)
    {
        [wself.localContext rollback];
        NSArray *photosDataArray = response[@"payload"];
        for (NSDictionary *photoDict in photosDataArray) {
            GalleryImage *galleryImage = [GalleryImage MR_importFromObject:photoDict inContext:wself.localContext];
            [photosArray addObject:galleryImage];
        }
    } completionHandler:^(NSError *error) {
        wself.photosArray = [NSArray arrayWithArray:photosArray];
        [wself.delegate dataController:wself didEndLoadingDataWithError:error];
    }];
}

- (BOOL)canUploadMorePhotos
{
    return [_photosArray count] < kMaxPhotosCount;
}

- (void)complainOnPhoto:(GalleryImage *)photo completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    [[BTWUserService sharedInstance] complainOnGalleryPhoto:photo completionHandler:^(NSError *error){
        if (!error) {
            NSMutableArray *mutablePhotos = [NSMutableArray arrayWithArray:wself.photosArray];
            [mutablePhotos removeObject:photo];
            wself.photosArray = [NSArray arrayWithArray:mutablePhotos];
            
            [wself.delegate dataController:wself didEndLoadingDataWithError:error];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

@end
