//
//  BTWProfileFeedbacksDataController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWDataController.h"

@interface BTWProfileFeedbacksDataController : BTWDataController

@property (nonatomic, strong) User *user;
@property (nonatomic, strong, readonly) NSArray *feedbacksArray;
@property (nonatomic, assign, readonly) BOOL isLoadingData;

- (void)reloadData;

@end
