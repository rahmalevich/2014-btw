//
//  BTWDialogsDataController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDialogDataController.h"
#import "BTWMessagingService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+Contacts.h"
#import "BTWApiMethod+CommonMethods.h"

#import "JSQMessagesBubbleImageFactory.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"

#import "AFNetworkReachabilityManager.h"

static NSInteger kDefaultHistoryBatchCount = 20;
static CGFloat kTypingTimerPeriod = 5.0f;
static NSTimeInterval kStayAliveInterval = 120.0f;

@interface BTWDialogDataController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, assign) BOOL isLoadingData;
@property (nonatomic, assign) BOOL hasEarlierMessages;
@property (nonatomic, weak) id<BTWDialogDataControllerDelegate> delegate;

@property (nonatomic, strong) Dialog *dialog;
@property (nonatomic, strong) DialogMessage *startMessage;
@property (nonatomic, strong) NSArray *dataModel;
@property (nonatomic, strong) NSFetchedResultsController *dialogDataController;

@property (nonatomic, weak) NSTimer *stayAliveTimer;
@property (nonatomic, weak) NSTimer *typingTimer;
@end

@implementation BTWDialogDataController
@dynamic delegate;

#pragma mark - Shared objects
UIEdgeInsets bubbleImageEdgeInsets = (UIEdgeInsets){30.0f, 10.0f, 10.0f, 30.0f};

static JSQMessagesBubbleImageFactory *_sharedFactory = nil;
+ (JSQMessagesBubbleImageFactory *)sharedBubbleImageFactory
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedFactory = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage imageNamed:@"bubble_min"] capInsets:bubbleImageEdgeInsets];
    });
    return _sharedFactory;
}

static JSQMessagesMediaViewBubbleImageMasker *_sharedMasker = nil;
+ (JSQMessagesMediaViewBubbleImageMasker *)sharedBubbleImageMasker
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedMasker = [[JSQMessagesMediaViewBubbleImageMasker alloc] initWithBubbleImageFactory:[self sharedBubbleImageFactory]];
    });
    return _sharedMasker;
}

#pragma mark - Initiailization
- (id)initWithDialog:(Dialog *)dialog delegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        self.dialog = dialog;
        self.startMessage = dialog.messages.count > kDefaultHistoryBatchCount ? dialog.messages.reversedOrderedSet[kDefaultHistoryBatchCount - 1] : [dialog.messages firstObject];
        self.dialogDataController = [DialogMessage MR_fetchAllGroupedBy:nil withPredicate:[NSPredicate predicateWithFormat:@"dialog = %@", dialog] sortedBy:nil ascending:YES delegate:self];
        
        [[BTWMessagingService sharedInstance].activeDialogsIDs addObject:dialog.backend_id];
        
        [[AFNetworkReachabilityManager sharedManager] addObserver:self forKeyPath:@"reachable" options:NSKeyValueObservingOptionNew context:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(typingMessageNotificationReceived:) name:kTypingMessageReceivedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onlineNotificationReceived:) name:kOnlineMessageReceivedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offlineNotificationReceived:) name:kOfflineMessageReceivedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleApplicationBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        self.stayAliveTimer = [NSTimer scheduledTimerWithTimeInterval:kStayAliveInterval target:self selector:@selector(sendStayAliveRequest:) userInfo:nil repeats:YES];
        
        [self getHistory];
    }
    return self;
}

- (void)dealloc
{
    if ([_dialog.backend_id isValidString]) {
        [[BTWMessagingService sharedInstance].activeDialogsIDs removeObject:_dialog.backend_id];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[AFNetworkReachabilityManager sharedManager] removeObserver:self forKeyPath:@"reachable"];
}

// Оставновка таймеров необходима для снятия ссылок на объект
- (void)stopTimers
{
    [_stayAliveTimer invalidate];
    [_typingTimer invalidate];
}

- (void)setStartMessage:(DialogMessage *)startMessage
{
    if (![_startMessage isEqual:startMessage]) {
        _startMessage = startMessage;
        [self updateModel];
    }
    
    NSUInteger startMessageIndex = [_dialog.messages indexOfObject:startMessage];
    self.hasEarlierMessages = startMessageIndex != NSNotFound && startMessageIndex > 0;
}

- (void)updateModel
{
    NSUInteger messageIndex = [_dialog.messages indexOfObject:_startMessage];
    if (messageIndex == NSNotFound) {
        self.dataModel = _dialog.messages.array;
    } else {
        self.dataModel = [_dialog.messages.array subarrayWithRange:NSMakeRange(messageIndex, _dialog.messages.count - messageIndex)];
    }
}

#pragma mark - Staying alive
- (void)sendStayAliveRequest:(NSTimer *)timer
{
    BTWApiMethod *pingRequest = [BTWApiMethod pingMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:pingRequest callback:nil];
}

#pragma mark - Observers
- (void)handleApplicationBecomeActive:(NSNotification *)notification
{
    [self getHistory];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"reachable"]) {
        if ([AFNetworkReachabilityManager sharedManager].reachable) {
            [self getHistory];
        }
    }
}

- (void)typingMessageNotificationReceived:(NSNotification *)notification
{
    NSDictionary *messageDict = [notification userInfo];
    if ([messageDict[@"topic"] isEqualToString:_dialog.backend_id]) {
        [_typingTimer invalidate];
        self.typingTimer = [NSTimer scheduledTimerWithTimeInterval:kTypingTimerPeriod target:self selector:@selector(typingTimerFinished:) userInfo:nil repeats:NO];
        [self.delegate userDidStartTypingMessageForDataController:self];
    }
}

- (void)typingTimerFinished:(NSTimer *)timer
{
    [self.delegate userDidFinishTypingMessageForDataController:self];
}

- (void)onlineNotificationReceived:(NSNotification *)notification
{
    NSDictionary *messageDict = [notification userInfo];
    if ([messageDict[@"topic"] isEqualToString:_dialog.backend_id]) {
        [_typingTimer invalidate];
        _dialog.user.online = @(YES);
        [_dialog.user.managedObjectContext MR_saveToPersistentStoreAndWait];
        [self.delegate dataControllerDidReceiveOnlineMessage:self];
    }
}

- (void)offlineNotificationReceived:(NSNotification *)notification
{
    NSDictionary *messageDict = [notification userInfo];
    if ([messageDict[@"topic"] isEqualToString:_dialog.backend_id]) {
        [_typingTimer invalidate];
        _dialog.user.online = @(NO);
        _dialog.user.last_activity = [NSDate date];
        [_dialog.user.managedObjectContext MR_saveToPersistentStoreAndWait];
        [self.delegate dataControllerDidReceiveOfflineMessage:self];
    }
}

#pragma mark - Public Methods
- (void)getHistory
{
    self.isLoadingData = YES;
    
    __weak typeof(self) wself = self;
    [[BTWMessagingService sharedInstance] getHistoryForDialog:_dialog withCompletionHandler:^(NSError *error){
        if (!error) {
            [[BTWMessagingService sharedInstance] confirmHistoryForDialog:wself.dialog];
        }
        wself.isLoadingData = NO;
    }];
}

- (void)showEarlierMessages
{
    NSUInteger startMessageIndex = [_dialog.messages indexOfObject:_startMessage];
    NSUInteger newStartMessageIndex = startMessageIndex != NSNotFound && startMessageIndex > kDefaultHistoryBatchCount ? startMessageIndex - kDefaultHistoryBatchCount : 0;
    self.startMessage = [_dialog.messages objectAtIndex:newStartMessageIndex];
}

- (NSUInteger)messagesCount
{
    return [_dataModel count];
}

- (DialogMessage *)messageAtIndex:(NSUInteger)index
{
    return _dataModel[index];
}

- (NSUInteger)indexForMessage:(DialogMessage *)message
{
    return [_dataModel indexOfObject:message];
}

#pragma mark - NSFetchedResultsController delegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (_typingTimer) {
        [_typingTimer invalidate];
        [self.delegate userDidFinishTypingMessageForDataController:self];
    }

    // Смещаем стартовое сообщение, если сообщений стало больше
    if ([_dialog.messages count] > 0) {
        NSUInteger startMessageIndex = [_dialog.messages indexOfObject:_startMessage];
        NSUInteger minStartMessageIndex = _dialog.messages.count > kDefaultHistoryBatchCount ? _dialog.messages.count - kDefaultHistoryBatchCount : 0;
        if (startMessageIndex > minStartMessageIndex) {
            self.startMessage = _dialog.messages[minStartMessageIndex];
        }
    }
    
    [self updateModel];
    
    [self.delegate dataController:self didEndLoadingDataWithError:nil];
}

@end
