//
//  BTWMatchingDataController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMatchingDataController.h"
#import "BTWNotificationsService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+Matching.h"
#import "BTWApiMethod+GalleryMethods.h"

#import "SDWebImagePrefetcher.h"

static CGFloat kEmptySearchResultsDelay = 7.0f;

@interface BTWMatchingDataController ()
@property (nonatomic, strong) NSMutableArray *usersArray;
@property (nonatomic, assign, readwrite) BOOL isLoadingData;
@end

@implementation BTWMatchingDataController

#pragma mark - Initialization & Memory managment
- (id)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithDelegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    // В процессе работы приложения мы не удаляем всех найденных пользователей,
    // чтобы иметь возомжность работать с объектами в других контроллерах.
    // Поэтому очищаем пользователей при старте, когда данные гарантированно
    // нигде не используются
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
    [MatchingUser MR_truncateAllInContext:context];
    [context MR_saveToPersistentStoreAndWait];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didMakeActionWithLike:) name:kDidMakeActionWithLikeNotification object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Model
- (void)updateProfiles
{
    if (_isLoadingData) {
        return;
    }
    self.isLoadingData = YES;
    
    self.usersArray = nil;
    
    __weak typeof(self) wself = self;
    NSMutableArray *importedUserIds = [NSMutableArray array];
    BTWApiMethod *searchMethod = [BTWApiMethod searchMatchesMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:searchMethod backgroundProcessing:^(NSDictionary *response)
    {
        NSArray *rawUsersArray = response[@"payload"];
        NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
        for (NSDictionary *rawUser in rawUsersArray){
            if ([rawUser isValidDictionary]) {
                MatchingUser *user = [MatchingUser MR_importFromObject:rawUser inContext:context];
                [importedUserIds addObject:user.backend_id];
            }
        }
        [context MR_saveToPersistentStoreAndWait];
        
    } callback:^(id response, NSError *error) {
        
        NSArray *usersArray = [MatchingUser MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"backend_id IN %@", importedUserIds]];
        BTWVoidBlock completionBlock = [^{
            wself.isLoadingData = NO;
            wself.usersArray = [NSMutableArray arrayWithArray:usersArray];
            [wself.delegate dataController:wself didEndLoadingDataWithError:error];
        } copy];
        
        if ([usersArray count]) {
            NSMutableArray *urlsArray = [NSMutableArray array];
            for (MatchingUser *user in usersArray) {
                if ([user.photo_url isValidString]) {
                    [urlsArray addObject:[NSURL URLWithString:user.photo_url]];
                }
            }
            if ([urlsArray count]) {
                [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:urlsArray progress:nil completed:^(NSUInteger noOfFinishedUrls, NSUInteger noOfSkippedUrls)
                 {
                     completionBlock();
                 }];
            } else {
                completionBlock();
            }
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kEmptySearchResultsDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                completionBlock();
            });
        }
    }];
}

- (NSUInteger)count
{
    return [_usersArray count];
}

- (MatchingUser *)currentProfile
{
    return [_usersArray firstObject];
}

- (MatchingUser *)nextProfile
{
    return [_usersArray count] > 1 ? _usersArray[1] : nil;
}

- (MatchingUser *)profileAtIndex:(NSUInteger)index
{
    return [_usersArray count] > index ? _usersArray[index] : nil;
}

#pragma mark - Handling actions with notifications
- (void)didMakeActionWithLike:(NSNotification *)notification
{
    NSString *userId = [notification userInfo][kNotificationUserIdKey];
    if ([userId isValidString]) {
        NSArray *userMatch = [_usersArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"backend_id = %@", userId]];
        if ([userMatch isValidArray]) {
            [_usersArray removeObjectsInArray:userMatch];
            [self.delegate dataController:self didEndLoadingDataWithError:nil];
        }
    }
}

#pragma mark - Actions
- (void)acceptCurrentProfileWithCompletionHandler:(BTWMatchingBlock)completionHandler
{
    [self markCurrentProfileAsAccepted:YES withCompletionHandler:completionHandler];
}

- (void)declineCurrentProfileWithCompletionHandler:(BTWMatchingBlock)completionHandler
{
    [self markCurrentProfileAsAccepted:NO withCompletionHandler:completionHandler];
}

- (void)markCurrentProfileAsAccepted:(BOOL)accept withCompletionHandler:(BTWMatchingBlock)completionHandler
{
    MatchingUser *user = [self currentProfile];

    BTWApiMethod *actionMethod = [BTWApiMethod markMatchMethodForUser:user asKeeped:accept];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:actionMethod callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            if (!error) {
                NSString *topicId = [response valueForKeyPath:@"payload.topic"];
                completionHandler(topicId, user);
            }
        }
    }];
    
    [_usersArray removeObject:user];
}

#pragma mark - Gallery
- (void)loadGalleryForCurrentProfileWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    MatchingUser *user = [self currentProfile];
    BTWApiMethod *listMethod = [BTWApiMethod listMethodForUser:user];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:listMethod backgroundProcessing:^(NSDictionary *response)
    {
        // Парсим фотографии и связываем их с пользователем
        NSMutableOrderedSet *photosSet = [NSMutableOrderedSet orderedSet];
        NSArray *photosDataArray = response[@"payload"];
        for (NSDictionary *photoDict in photosDataArray) {
            if ([photoDict isValidDictionary]) {
                GalleryImage *galleryImage = [GalleryImage MR_importFromObject:photoDict inContext:user.managedObjectContext];
                [photosSet addObject:galleryImage];
            }
        }
        user.gallery_images = photosSet;
        [user.managedObjectContext MR_saveToPersistentStoreAndWait];
        
    } callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

@end
