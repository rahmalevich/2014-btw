//
//  BTWProfileQuestionsDataController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWQuizDataController.h"
#import "BTWQuestionsService.h"
#import "BTWUserService.h"

#pragma mark - BTWQuizDataController
@interface BTWQuizDataController ()
@property (nonatomic, strong) NSArray *questionsArray;
@property (nonatomic, strong, readwrite) Question *currentQuestion;
@end

@implementation BTWQuizDataController

#pragma mark - Initialization
- (instancetype)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (instancetype)initWithDelegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.questionsArray = nil;
    self.currentQuestion = nil;
    
    [self setupContent];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsServiceDidUpdateQuestions:) name:kQuestionsServiceDidUpdateQuestions object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionsServiceDidFailToUpdateQuestions:) name:kQuestionsServiceDidFailToUpdateQuestions object:nil];
}

- (void)setupContent
{
    self.questionsArray = [Question MR_findAllSortedBy:@"sort_order" ascending:YES withPredicate:[NSPredicate predicateWithFormat:@"category = %@", kQuestionsCategoryKey]];
    self.currentQuestion = [[_questionsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"is_visited != YES"]] firstObject];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Update
- (void)questionsServiceDidUpdateQuestions:(NSNotification *)notification
{
    [self setupContent];
    [self.delegate dataController:self didEndLoadingDataWithError:nil];
}

- (void)questionsServiceDidFailToUpdateQuestions:(NSNotification *)notification
{
    [self.delegate dataController:self didEndLoadingDataWithError:notification.userInfo[kQuestionsServiceNotificationErrorKey]];
}

- (void)chooseAnswer:(Answer *)answer
{
    answer.question.is_visited = @(YES);
    
    User *authorizedUser = [BTWUserService sharedInstance].authorizedUser;
    NSMutableSet *oldAnswers = [authorizedUser.answers mutableCopy];
    [oldAnswers intersectSet:[answer.question.answers set]];
    [authorizedUser removeAnswers:oldAnswers];
    [authorizedUser addAnswersObject:answer];
    [self saveChanges];
    
    [[BTWQuestionsService sharedInstance] submitAnswers];
}

#pragma mark - Utils
- (void)saveChanges
{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (NSInteger)numberOfQuestions
{
    return [_questionsArray count];
}

- (NSInteger)currentQuestionIndex
{
    return _currentQuestion ? [_questionsArray indexOfObject:_currentQuestion] : NSNotFound;
}

- (void)nextQuestion
{
    BOOL hasAnswer = [[BTWUserService sharedInstance].authorizedUser.answers intersectsSet:[_currentQuestion.answers set]];
    if (hasAnswer) {
        _currentQuestion.is_visited = @(YES);
        [self saveChanges];
    }
    
    NSInteger currentQuestionIndex = [self currentQuestionIndex];
    if (currentQuestionIndex < [_questionsArray count] - 1) {
        self.currentQuestion = _questionsArray[currentQuestionIndex + 1];
    } else {
        self.currentQuestion = nil;
    }
}

- (void)previousQuestion
{
    NSInteger currentQuestionIndex = [self currentQuestionIndex];
    if (currentQuestionIndex > 0) {
        self.currentQuestion = _questionsArray[currentQuestionIndex - 1];
    }
}

- (void)restartQuestionnaire
{
    for (Question *question in _questionsArray) {
        question.is_visited = @(NO);
    }
    self.currentQuestion = [_questionsArray firstObject];
    [self saveChanges];
}

@end
