//
//  BTWMapDataController.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWMapDataController.h"
#import "BTWLocationManager.h"

#import <stdlib.h>

@interface BTWMapDataController ()
@property (nonatomic, strong) NSManagedObjectContext *localContext;
@property (nonatomic, strong) NSArray *usersArray;
@end

@implementation BTWMapDataController

#pragma mark - Initialization & Memory managment
- (id)init
{
    if (self = [super init]) {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithDelegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super initWithDelegate:delegate]) {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    self.localContext = [NSManagedObjectContext MR_context];
}

- (void)dealloc
{
    [_localContext rollback];
}

#pragma mark - Utils
- (void)updateUsers
{
    NSArray *data = @[
                      @{
                          @"id" : @"1",
                          @"name" : @"Елена",
                          @"surname" : @"Иванова",
                          @"age" : @(25),
                          @"match" : @(57),
                          @"photo": @{
                                  @"avatarBig_420x345" : @"2_mid.png",
                                  @"avatarMid_160x160" : @"2_mid.png",
                                  @"avatarSmall_50x50" : @"2_small.png"
                                  }
                          },
                      @{
                          @"id" : @"2",
                          @"name" : @"Анна",
                          @"surname" : @"Петрова",
                          @"age" : @(25),
                          @"match" : @(84),
                          @"photo": @{
                                  @"avatarBig_420x345" : @"3_mid.png",
                                  @"avatarMid_160x160" : @"3_mid.png",
                                  @"avatarSmall_50x50" : @"3_small.png"
                                  }
                          },
                      @{
                          @"id" : @"3",
                          @"name" : @"Ольга",
                          @"surname" : @"Сидорова",
                          @"age" : @(23),
                          @"match" : @(95),
                          @"photo": @{
                                  @"avatarBig_420x345" : @"4_mid.png",
                                  @"avatarMid_160x160" : @"4_mid.png",
                                  @"avatarSmall_50x50" : @"4_small.png"
                                  }
                          },
                      @{
                          @"id" : @"4",
                          @"name" : @"Татьяна",
                          @"surname" : @"Васильева",
                          @"age" : @(26),
                          @"match" : @(83),
                          @"photo": @{
                                  @"avatarBig_420x345" : @"5_mid.png",
                                  @"avatarMid_160x160" : @"5_mid.png",
                                  @"avatarSmall_50x50" : @"5_small.png"
                                  }
                          },
                      @{
                          @"id" : @"5",
                          @"name" : @"Юлия",
                          @"surname" : @"Антонова",
                          @"age" : @(25),
                          @"match" : @(34),
                          @"photo": @{
                                  @"avatarBig_420x345" : @"6_mid.png",
                                  @"avatarMid_160x160" : @"6_mid.png",
                                  @"avatarSmall_50x50" : @"6_small.png"
                                  }
                          }
                      ];
    
    NSMutableArray *usersArray = [NSMutableArray array];
    [_localContext rollback];
    
    NSInteger numberOfUsers = (NSInteger)arc4random_uniform(4) + 1;
    for (NSInteger i = 1; i <= numberOfUsers; i++) {
        
        NSDictionary *newUserDict = data[i-1];
        User *newUser = [User MR_importFromObject:newUserDict inContext:_localContext];
        
        CGFloat x = arc4random_uniform(7);
        CGFloat y = arc4random_uniform(7);
        
        [[BTWLocationManager sharedInstance] updateLocation];
        CLLocationCoordinate2D coordinate = [BTWLocationManager sharedInstance].currentLocation.coordinate;
        newUser.latitude = @(coordinate.latitude + 0.001 * (x - 5));
        newUser.longitude = @(coordinate.longitude + 0.001 * (y - 5));
        newUser.role = @(i % 2);
        
        [usersArray addObject:newUser];
    }
    self.usersArray = [NSArray arrayWithArray:usersArray];
    
    [self.delegate dataController:self didEndLoadingDataWithError:nil];
}

@end