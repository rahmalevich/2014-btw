//
//  BTWProfileGalleryDataController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 04.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDataController.h"

@interface BTWProfileGalleryDataController : BTWDataController

@property (nonatomic, strong) User *user;
@property (nonatomic, readonly) NSArray *photosArray;

- (void)updatePhotos;
- (BOOL)canUploadMorePhotos;
- (void)complainOnPhoto:(GalleryImage *)photo completionHandler:(BTWErrorBlock)completionHandler;

@end
