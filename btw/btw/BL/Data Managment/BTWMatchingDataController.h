//
//  BTWMatchingDataController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 07.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDataController.h"

typedef void(^BTWMatchingBlock)(NSString *topicId, MatchingUser *user);

@interface BTWMatchingDataController : BTWDataController

@property (nonatomic, assign, readonly) BOOL isLoadingData;

- (void)updateProfiles;

- (NSUInteger)count;
- (MatchingUser *)currentProfile;
- (MatchingUser *)nextProfile;
- (MatchingUser *)profileAtIndex:(NSUInteger)index;

- (void)acceptCurrentProfileWithCompletionHandler:(BTWMatchingBlock)completionHandler;
- (void)declineCurrentProfileWithCompletionHandler:(BTWMatchingBlock)completionHandler;

- (void)loadGalleryForCurrentProfileWithCompletionHandler:(BTWErrorBlock)completionHandler;

@end
