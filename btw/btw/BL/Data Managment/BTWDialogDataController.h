//
//  BTWDialogsDataController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 24.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDataController.h"

/* TODO: Сделать реальную постраничную загрузку сообщений
 * Обработка кейсов:
 * - догрузка неизвестного количество сообщений в конец (при переписке с другого устройства)
 */

@class BTWDialogDataController, JSQMessagesAvatarImage, JSQMessagesBubbleImageFactory, JSQMessagesMediaViewBubbleImageMasker;
@protocol BTWDialogDataControllerDelegate <BTWDataControllerDelegate>
- (void)userDidStartTypingMessageForDataController:(BTWDialogDataController *)controller;
- (void)userDidFinishTypingMessageForDataController:(BTWDialogDataController *)controller;
- (void)dataControllerDidReceiveOnlineMessage:(BTWDialogDataController *)controller;
- (void)dataControllerDidReceiveOfflineMessage:(BTWDialogDataController *)controller;
@end

@interface BTWDialogDataController : BTWDataController

@property (nonatomic, strong, readonly) Dialog *dialog;
@property (nonatomic, assign, readonly) BOOL isLoadingData;
@property (nonatomic, assign, readonly) BOOL hasEarlierMessages;

+ (JSQMessagesBubbleImageFactory *)sharedBubbleImageFactory;
+ (JSQMessagesMediaViewBubbleImageMasker *)sharedBubbleImageMasker;

- (id)initWithDialog:(Dialog *)dialog delegate:(id<BTWDialogDataControllerDelegate>)delegate;
- (void)stopTimers;

- (void)getHistory;
- (void)showEarlierMessages;

- (NSUInteger)messagesCount;
- (DialogMessage *)messageAtIndex:(NSUInteger)index;
- (NSUInteger)indexForMessage:(DialogMessage *)message;

@end
