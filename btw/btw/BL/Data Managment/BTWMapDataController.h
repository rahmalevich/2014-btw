//
//  BTWMapDataController.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 25.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWDataController.h"

@interface BTWMapDataController : BTWDataController

@property (nonatomic, weak) id<BTWDataControllerDelegate> delegate;
@property (nonatomic, strong) Route *route;

- (NSArray *)usersArray;
- (void)updateUsers;

@end
