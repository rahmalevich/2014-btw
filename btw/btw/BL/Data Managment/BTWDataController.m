//
//  TRDataController.m
//  Trumpet
//
//  Created by Mikhail Rakhmalevich on 31.07.14.
//  Copyright (c) 2014 Trumpet. All rights reserved.
//

#import "BTWDataController.h"

@implementation BTWDataController

- (id)initWithDelegate:(id<BTWDataControllerDelegate>)delegate
{
    if (self = [super init]) {
        self.delegate = delegate;
    }
    return self;
}

@end
