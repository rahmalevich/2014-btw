//
//  BTWQuestionsService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString * const kQuestionsServiceDidUpdateQuestions = @"kQuestionsServiceDidUpdateQuestions";
static NSString * const kQuestionsServiceDidFailToUpdateQuestions = @"kQuestionsServiceDidFailToUpdateQuestions";
static NSString * const kQuestionsServiceNotificationErrorKey = @"kQuestionsServiceNotificationErrorKey";
static NSString * const kQuestionsServiceDidFinishToUpdateCars = @"kQuestionsServiceDidFinishToUpdateCars";

NSString *kQuestionsCategoryKey;
NSString *kDetailsCategoryKey;
NSString *kColorCategoryKey;

@interface BTWQuestionsService : NSObject

@property (nonatomic, assign, readonly) BOOL questionsUpdated;
@property (nonatomic, assign, readonly) BOOL questionsAreLoading;

@property (nonatomic, assign, readonly) BOOL carsUpdated;
@property (nonatomic, assign, readonly) BOOL carsAreLoading;

+ (instancetype)sharedInstance;
- (void)updateQuestions;
- (void)submitAnswers;
- (void)cleanQuestions;
- (void)updateCars;

@end
