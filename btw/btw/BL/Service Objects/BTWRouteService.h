//
//  BTWRouteService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString * const kRouteSearchFinishedNotification = @"kRouteSearchFinishedNotificaiton";

@interface BTWRouteService : NSObject

@property (nonatomic, strong, readonly) Route *currentRoute;

+ (BTWRouteService *)sharedInstance;

- (void)startSearchWithRoute:(Route *)route completion:(BTWErrorBlock)completionHandler;
- (void)getSearchResultsWithCompletion:(void(^)(NSArray *, NSError*))completionHandler;
- (void)endSearchWithCompletion:(BTWErrorBlock)completionHandler;
- (void)offerRideToUser:(RouteUser *)user withCompletionHandler:(BTWErrorBlock)completionHandler;

@end
