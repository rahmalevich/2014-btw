//
//  BTWPushNotificationsService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

static NSString * const kPushNotificationTypeLike = @"like";
static NSString * const kPushNotificationTypeContact = @"contact";
static NSString * const kPushNotificaitonTypeMessage = @"message";
static NSString * const kPushNotificationTypeTravel = @"travel";
static NSString * const kPushNotificationTypeEvent = @"event";

@interface BTWPushNotificationsService : NSObject

@property (nonatomic, copy, readonly) NSString *deviceToken;

+ (BTWPushNotificationsService *)sharedInstance;
- (void)setup;

- (void)setTokenWithData:(NSData *)tokenData;
- (void)handleNotification:(NSDictionary *)userInfo fromBackground:(BOOL)background;
- (void)subscribe;
- (void)unsubscribe;

@end
