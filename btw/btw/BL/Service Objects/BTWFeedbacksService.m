//
//  BTWFeedbacksService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWFeedbacksService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+Feedbacks.h"

@implementation BTWFeedbacksService

#pragma mark - Initialization
static BTWFeedbacksService *_sharedInstance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWFeedbacksService new];
    });
    return _sharedInstance;
}

#pragma mark - Public methods
- (void)getFeedbacksForUser:(User *)user processingBlock:(BTWDataBlock)processingBlock completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    [processingBlock copy];
    
    BTWApiMethod *getFeedbacksMethod = [BTWApiMethod getFeedbacksMethodForUser:user.backend_id];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getFeedbacksMethod backgroundProcessing:processingBlock callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)sendFeedbackForEvent:(Event *)event mark:(CGFloat)mark reason:(BTWFeedbackReason)reason comment:(NSString *)comment completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *sendFeedbackMethod = [BTWApiMethod addFeedbackMethodForEvent:event.backend_id mark:mark reason:reason comment:comment];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:sendFeedbackMethod callback:^(NSDictionary *response, NSError *error)
    {
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

@end
