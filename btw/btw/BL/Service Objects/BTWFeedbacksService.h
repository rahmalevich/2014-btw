//
//  BTWFeedbacksService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, BTWFeedbackReason) {
    BTWFeedbackReasonBlank,
    BTWFeedbackReasonWrongDriver,
    BTWFeedbackReasonWrongCar,
    BTWFeedbackReasonOther
};

@interface BTWFeedbacksService : NSObject

+ (instancetype)sharedInstance;
- (void)getFeedbacksForUser:(User *)user processingBlock:(BTWDataBlock)processingBlock completionHandler:(BTWErrorBlock)completionHandler;
- (void)sendFeedbackForEvent:(Event *)event mark:(CGFloat)mark reason:(BTWFeedbackReason)reason comment:(NSString *)comment completionHandler:(BTWErrorBlock)completionHandler;

@end
