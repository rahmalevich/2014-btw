//
//  BTWUserService.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 26.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWUserService.h"
#import "BTWApiMethod+ProfileMethods.h"
#import "BTWApiMethod+GalleryMethods.h"
#import "BTWNetworkHelper.h"
#import "BTWTransitionsManager.h"
#import "BTWCommonUtils.h"
#import "BTWFacebookService.h"
#import "BTWVKService.h"
#import "BTWMessagingService.h"
#import "BTWPushNotificationsService.h"
#import "BTWQuestionsService.h"
#import "BTWSettingsService.h"

static NSString * const kAuthorizedUserId = @"BTWAuthorizedUserId";
static NSString * const kAuthorizedUserSessionKey = @"BTWAuthorizedUserSessionKey";

@interface BTWUserService ()
@property (nonatomic, strong, readwrite) User *authorizedUser;
@property (nonatomic, copy, readwrite) NSString *sessionKey;
@property (nonatomic, assign, readwrite) BOOL isLoggedIn;
@property (nonatomic, assign) BOOL isSendingLoginRequest;
@end

@implementation BTWUserService

#pragma mark - Initialization
static BTWUserService *_sharedInstance = nil;
+ (BTWUserService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWUserService new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:kAuthorizedUserId];
        if ([userId isValidString]) {
            User *authorizedUser = [User MR_findFirstByAttribute:@"backend_id" withValue:userId];
            NSString *sessionKey = [[NSUserDefaults standardUserDefaults] objectForKey:kAuthorizedUserSessionKey];
            if (authorizedUser) {
                self.authorizedUser = authorizedUser;
                self.sessionKey = sessionKey;
            }
        }
    }
    return self;
}

#pragma mark - Service methods
- (BOOL)processAuthorizationResponse:(NSDictionary *)responseDict error:(NSError **)error
{
    NSDictionary *profileDict = [responseDict valueForKeyPath:@"payload.profile"];
    NSString *userId = profileDict[@"id"];
    NSString *key = [responseDict valueForKeyPath:@"payload.token"];

    if ([userId isValidString] && [key isValidString])
    {
        self.sessionKey = key;
        self.authorizedUser = [User MR_importFromObject:profileDict];
        if (!_authorizedUser.car) {
            _authorizedUser.car = [Car MR_createInContext:_authorizedUser.managedObjectContext];
        }
        
        [_authorizedUser.managedObjectContext MR_saveToPersistentStoreAndWait];
        
        [[NSUserDefaults standardUserDefaults] setObject:_authorizedUser.backend_id forKey:kAuthorizedUserId];
        [[NSUserDefaults standardUserDefaults] setObject:_sessionKey forKey:kAuthorizedUserSessionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        BOOL wasLoggedIn = _isLoggedIn;
        self.isLoggedIn = YES;
        
        if (!wasLoggedIn) {
            [BTWSettingsService setUserSpecificDefaults];
            
            id<BTWSocialNetworkService> socialNetworkService = [_authorizedUser socialNetworkService];
            [socialNetworkService setupWithCompletionHandler:^(BOOL result, NSError *error){
                if (!error) {
                    [socialNetworkService updateFriendsAndInterests];
                }
            } silent:YES];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoggedInNotification object:nil];
            
            // Делаем запросы по таймеру, чтобы дать серверу возможность
            // обработать авторизацию
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[BTWMessagingService sharedInstance] startMessaging];
                [[BTWQuestionsService sharedInstance] updateQuestions];
                [[BTWQuestionsService sharedInstance] updateCars];
            });
        }
    } else {
        if (error != NULL) {
            *error = [NSError errorWithDomain:kBTWAPIErrorDomain code:BTWApiErrorCodeUnknown userInfo:nil];
        }
    }
    
    BOOL result = error ? YES : NO;
    return result;
}

- (void)updateAvatarWithData:(NSDictionary *)responseDict
{
    _authorizedUser.photo_url = [responseDict valueForKey:@"avatarBig_420x420"];
    _authorizedUser.photo_mid = [responseDict valueForKey:@"avatarMid_160x160"];
    _authorizedUser.photo_small = [responseDict valueForKey:@"avatarSmall_50x50"];
    [_authorizedUser.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserAvatarChangedNotification object:nil];
}

#pragma mark - Authorization & Registration
- (void)registerWithFirstName:(NSString *)firstName lastName:(NSString *)lastName birthdate:(NSDate *)date gender:(BTWUserGender)gender email:(NSString *)email password:(NSString *)password completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *registerMethod = [BTWApiMethod registerMethodWithFirstName:firstName lastName:lastName birthdate:date gender:gender email:email password:password];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:registerMethod callback:^(NSDictionary *response, NSError *error){
        if (!error) {
            [[AppsFlyerTracker sharedTracker] trackEvent:@"reg" withValue:nil];
            [self authorizeWithEmail:email password:password completionHandler:completionHandler];
        } else if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)registerWithSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork userID:(NSString *)userID sessionKey:(NSString *)sessionKey completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *registerMethod = nil;
    if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
        registerMethod = [BTWApiMethod registerMethodWithFacebookID:userID sessionKey:sessionKey];
    } else if (socialNetwork == BTWLinkedSocialNetworkVk) {
        registerMethod = [BTWApiMethod registerMethodWithVkID:userID sessionKey:sessionKey];
    }
    
    if (registerMethod) {
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:registerMethod callback:^(NSDictionary *response, NSError *error){
            if (!error) {
                [[AppsFlyerTracker sharedTracker] trackEvent:@"reg" withValue:nil];
                [self authorizeWithSocialNetwork:socialNetwork userID:userID accessToken:sessionKey completionHandler:completionHandler];
            } else if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

- (void)authorizeWithEmail:(NSString *)email password:(NSString *)password completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];

    BTWApiMethod *loginMethod = [BTWApiMethod loginMethodWithEmail:email password:password];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:loginMethod callback:^(NSDictionary *response, NSError *error){
        if (!error) {
            [self processAuthorizationResponse:response error:&error];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)authorizeWithSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork userID:(NSString *)userID accessToken:(NSString *)accessToken completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    BTWApiMethod *loginMethod = nil;
    
    if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
        loginMethod = [BTWApiMethod loginMethodWithFacebookID:userID sessionKey:accessToken];
    } else if (socialNetwork == BTWLinkedSocialNetworkVk) {
        loginMethod = [BTWApiMethod loginMethodWithVkID:userID sessionKey:accessToken];
    }
    
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:loginMethod callback:^(NSDictionary *response, NSError *error)
     {
         if (error && [error.domain isEqualToString:kBTWAPIErrorDomain] && (error.code == BTWApiErrorCodeProfileNotFound || error.code == BTWApiErrorCodeUserNotFound))
         {
             [self registerWithSocialNetwork:socialNetwork userID:userID sessionKey:accessToken completionHandler:completionHandler];
         } else {
             if (!error) {
                 [self processAuthorizationResponse:response error:&error];
             }
             
             if (completionHandler) {
                 completionHandler(error);
             }
         }
     }];
}

- (void)reloginWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    if (self.isSendingLoginRequest) {
        return;
    }
    self.isSendingLoginRequest = YES;
    
    if (_authorizedUser) {
        BTWApiMethod *loginMethod = [BTWApiMethod loginMethodWithUserID:_authorizedUser.backend_id token:_sessionKey];
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:loginMethod callback:^(NSDictionary *response, NSError *error)
        {
            self.isSendingLoginRequest = NO;
            
            if (!error) {
                [self processAuthorizationResponse:response error:&error];
            }
            
            if (error && [error.domain isEqualToString:kBTWAPIErrorDomain] &&
                ( error.code == BTWApiErrorCodeLoginFailed ||
                  error.code == BTWApiErrorCodeProfileNotFound ||
                  error.code == BTWApiErrorCodeAuthentificationFailed ||
                  error.code == BTWApiErrorCodeUserNotFound ) )
            {
                [self logout];
            }
            
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
}

- (void)logout
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserWillLogoutNotification object:nil];
    
    [[BTWQuestionsService sharedInstance] cleanQuestions];
    
    [[BTWMessagingService sharedInstance] stopMessaging];
    [[BTWMessagingService sharedInstance] cleanDialogs];
    
    [[BTWFacebookService sharedInstance] closeSession];
    [[BTWVKService sharedInstance] closeSession];
    
    [[BTWTransitionsManager sharedInstance] showAuthorization];
    
    self.authorizedUser = nil;
    self.sessionKey = nil;
    self.isLoggedIn = NO;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kAuthorizedUserId];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kAuthorizedUserSessionKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserLoggedOutNotification object:nil];
}

#pragma mark - Profile
- (void)saveProfileWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *saveProfileMethod = [BTWApiMethod saveProfileMethodWithProfile:_authorizedUser];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:saveProfileMethod callback:^(NSDictionary *response, NSError *error)
    {
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)updateCarInfoWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [_authorizedUser.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    [completionHandler copy];
    
    BTWApiMethod *updateCarMethod = [BTWApiMethod updateCarMethod:_authorizedUser.car];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:updateCarMethod callback:^(NSDictionary *response, NSError *error)
    {
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)getFullProfileForUser:(User *)user withCompletionHander:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *getProfileMethod = [BTWApiMethod getFullProfileMethodForUser:user];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getProfileMethod callback:^(NSDictionary *response, NSError *error){
        if (!error) {
            NSDictionary *userDict = response[@"payload"];
            [user MR_importValuesForKeysWithObject:userDict];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)uploadAvatar:(UIImage *)avatar progressHandler:(BTWNetworkProgressBlock)progressBlock withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [progressBlock copy];
    [completionHandler copy];

    __weak typeof(self) wself = self;
    [self reloginWithCompletionHandler:^(NSError *error){
        if (!error) {
            [[AppsFlyerTracker sharedTracker] trackEvent:@"photo" withValue:nil];
            
            UIImage *fixedOrientationImage = [BTWCommonUtils fixOrientation:avatar];
            BTWApiMethod *uploadAvatarMethod = [BTWApiMethod addMethodForImage:fixedOrientationImage asAvatar:YES];
            [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:uploadAvatarMethod progressHandler:progressBlock backgroundProcessing:nil callback:^(NSDictionary *response, NSError *error)
            {
                if (!error) {
                    [wself updateAvatarWithData:response[@"payload"]];
                }
                if (completionHandler) {
                    completionHandler(error);
                }
            }];
        } else {
            completionHandler(error);
        }
    }];
}

- (void)linkSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork userID:(NSString *)userID sessionKey:(NSString *)sessionKey completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *linkMethod = nil;
    if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
        linkMethod = [BTWApiMethod linkMethodWithFacebookID:userID sessionKey:sessionKey];
    } else if (socialNetwork == BTWLinkedSocialNetworkVk) {
        linkMethod = [BTWApiMethod linkMethodWithVkID:userID sessionKey:sessionKey];
    }
    
    if (linkMethod) {
        __weak typeof(self) wself = self;
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:linkMethod callback:^(NSDictionary *response, NSError *error)
         {
             if (!error) {
                 if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
                     wself.authorizedUser.facebook_id = userID;
                 } else if (socialNetwork == BTWLinkedSocialNetworkVk) {
                     wself.authorizedUser.vkontakte_id = userID;
                 }
                 [wself.authorizedUser.managedObjectContext MR_saveToPersistentStoreAndWait];
             }
         
             if (completionHandler) {
                 completionHandler(error);
             }
         }];
    }
}

- (void)getFriendsFromSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork forUserID:(NSString *)userID completionHandler:(BTWNetworkResponseBlock)completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        BTWApiMethod *method = nil;
        if (socialNetwork == BTWLinkedSocialNetworkVk) {
            method = [BTWApiMethod getVKFriendsForUserID:userID];
        } else if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
            method = [BTWApiMethod getFacebookFreindsForUserID:userID];
        }
        
        if (method) {
            [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:method callback:^(id response, NSError *error){
                if (!error) {
                    completionHandler(response[@"payload"], nil);
                } else {
                    completionHandler(nil, error);
                }
            }];
        } else {
            completionHandler(nil, [NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeUnknown userInfo:nil]);
        }
    }
}

- (void)getUserFromSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork byUserID:(NSString *)userID completionHandler:(BTWNetworkResponseBlock)completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        BTWApiMethod *method = nil;
        if (socialNetwork == BTWLinkedSocialNetworkVk) {
            method = [BTWApiMethod getUserFromVKMethodForID:userID];
        } else if (socialNetwork == BTWLinkedSocialNetworkFacebook) {
            method = [BTWApiMethod getUserFromFacebookForID:userID];
        }
        
        if (method) {
            [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:method callback:completionHandler];
        }
    }
}

#pragma mark - Gallery
- (void)uploadGalleryPhoto:(UIImage *)photo name:(NSString *)name message:(NSString *)message progressHandler:(BTWNetworkProgressBlock)progressBlock withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [progressBlock copy];
    [completionHandler copy];
    
    [self reloginWithCompletionHandler:^(NSError *error){
        if (!error) {
            [[AppsFlyerTracker sharedTracker] trackEvent:@"photo" withValue:nil];
            
            UIImage *fixedOrientationImage = [BTWCommonUtils fixOrientation:photo];
            BTWApiMethod *addPhotoMethod = [BTWApiMethod addMethodForImage:fixedOrientationImage];
            [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:addPhotoMethod progressHandler:progressBlock backgroundProcessing:nil callback:^(NSDictionary *response, NSError *error)
             {
                 if (completionHandler) {
                     completionHandler(error);
                 }
             }];
        } else {
            completionHandler(error);
        }
    }];
}

- (void)removeGalleryPhoto:(GalleryImage *)galleryPhoto completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    BOOL isAvatar = [galleryPhoto.is_avatar boolValue];
    BTWApiMethod *removeMethod = [BTWApiMethod removeMethodForGalleryImage:galleryPhoto];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:removeMethod callback:^(NSDictionary *response, NSError *error)
     {
         if (!error) {
             if (isAvatar) {
                 [wself updateAvatarWithData:nil];
             }
         }
         
         if (completionHandler) {
             completionHandler(error);
         }
     }];
}

- (void)complainOnGalleryPhoto:(GalleryImage *)galleryPhoto completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *complainMethod = [BTWApiMethod comlaintMethodForImage:galleryPhoto];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:complainMethod callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)setAvatarWithGalleryPhoto:(GalleryImage *)gallerPhoto completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    BTWApiMethod *setAvatarMethod = [BTWApiMethod setAvatarMethodForImage:gallerPhoto];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:setAvatarMethod callback:^(NSDictionary *response, NSError *error)
     {
         if (!error) {
             [wself updateAvatarWithData:response[@"payload"]];
         }
         if (completionHandler) {
             completionHandler(error);
         }
     }];
}

- (void)getGalleryListWithProcessingBlock:(BTWDataBlock)processingBlock completionHandler:(BTWErrorBlock)completionHandler
{
    [self getGalleryListForUser:_authorizedUser processingBlock:processingBlock completionHandler:completionHandler];
}

- (void)getGalleryListForUser:(User *)user processingBlock:(BTWDataBlock)processingBlock completionHandler:(BTWErrorBlock)completionHandler
{
    [processingBlock copy];
    [completionHandler copy];
    
    BTWApiMethod *listMethod = [BTWApiMethod listMethodForUser:user];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:listMethod backgroundProcessing:processingBlock callback:^(NSDictionary *response, NSError *error)
    {
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

#pragma mark - Phone number
- (void)requestActivationCodeForPhoneNumber:(NSString *)phoneNumber completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *setPhoneMethod = [BTWApiMethod setPhoneMethodForNumber:phoneNumber];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:setPhoneMethod callback:^(NSDictionary *response, NSError *error) {
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)confirmPhoneNumberWithCode:(NSString *)activationCode completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    BTWApiMethod *confirmPhoneMethod = [BTWApiMethod confirmPhoneMethodForCode:activationCode];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:confirmPhoneMethod callback:^(NSDictionary *response, NSError *error) {
        if (!error) {
            [[AppsFlyerTracker sharedTracker] trackEvent:@"phoneappr" withValue:nil];
            wself.authorizedUser.phone_verified = @(YES);
            [wself.authorizedUser.managedObjectContext MR_saveToPersistentStoreAndWait];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

@end
