//
//  BTWEventsService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWEventsService.h"
#import "BTWUserService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+Events.h"
#import "BTWLocationManager.h"
#import "BTWFormattingService.h"
#import "BTWNotificationsService.h"
#import "BTWTransitionsManager.h"

#pragma mark - BTWEventsService
@interface BTWEventsService ()
@property (nonatomic, assign) BOOL isLoadingData;
@property (nonatomic, assign, readwrite) NSUInteger unconfirmedActiveEventsCount;
@property (nonatomic, assign, readwrite) NSUInteger unconfirmedCompletedEventsCount;
@property (nonatomic, strong) NSMutableDictionary *timersDict;
@end

@implementation BTWEventsService

#pragma mark - Initialization & Lifecycle
static BTWEventsService *_sharedInstance = nil;
+ (BTWEventsService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWEventsService new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.timersDict = [NSMutableDictionary dictionary];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn:) name:kUserLoggedInNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOut:) name:kUserLoggedOutNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)userLoggedIn:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocationChanged:) name:kLocationManagerDidUpdateLocationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleApplicationBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAcceptedOffer:) name:kDidAcceptRideOfferNotification object:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setupTimers];
        [self updateEventsWithCompletionHandler:nil];
    });
}

- (void)userLoggedOut:(NSNotification *)notification
{
    [self cleanEvents];
    [self stopAllTimers];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLocationManagerDidUpdateLocationNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDidAcceptRideOfferNotification object:nil];
}

- (void)handleApplicationBecomeActive:(NSNotification *)notification
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self updateEventsWithCompletionHandler:nil];
    });
}

- (void)handleLocationChanged:(NSNotification *)notification
{
    [self checkFinishedRoutes];
}

- (void)handleAcceptedOffer:(NSNotification *)notification
{
//    [self updateEventsWithCompletionHandler:nil];
}

- (void)cleanEvents
{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
    [Event MR_truncateAllInContext:context];
    [context MR_saveToPersistentStoreAndWait];
    
    self.unconfirmedActiveEventsCount = 0;
    self.unconfirmedCompletedEventsCount = 0;
}

#pragma mark - Events confirmation
- (void)confirmActiveEventsWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    self.unconfirmedActiveEventsCount = 0;
    [self confirmEventsWithStatuses:@[kEventStatusPlanned, kEventStatusInprogress] completionHandler:completionHandler];
}

- (void)confirmCompletedEventsWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    self.unconfirmedCompletedEventsCount = 0;
    [self confirmEventsWithStatuses:@[kEventStateCompleted, kEventStatusCancelled] completionHandler:completionHandler];
}

- (void)confirmEventsWithStatuses:(NSArray *)statuses completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    for (Event *event in [Event MR_findAll]) {
        if ([statuses containsObject:event.status]) {
            event.confirmed = @(YES);
        }
    }
    
    BTWApiMethod *confirmMethod = [BTWApiMethod confirmEventsMethodForStates:statuses];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:confirmMethod callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

#pragma mark - Loading data
- (void)updateEventsWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    if (_isLoadingData) {
        return;
    }
    
    [completionHandler copy];
    
    self.isLoadingData = YES;
    
    __weak typeof(self) wself = self;
    __block NSUInteger unconfirmedActiveCount = 0;
    __block NSUInteger unconfirmedCompletedCount = 0;
    BTWApiMethod *eventsListMethod = [BTWApiMethod eventsListMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:eventsListMethod backgroundProcessing:^(NSDictionary *response)
     {
         NSManagedObjectContext *localContext = [NSManagedObjectContext MR_context];
         
         NSMutableSet *importedEventsIDs = [NSMutableSet new];
         NSArray *eventsArray = response[@"payload"];
         
         for (NSDictionary *eventDict in eventsArray) {
             NSString *status = eventDict[@"status"];
             Event *event = [Event MR_importFromObject:eventDict inContext:localContext];
             if (![event.driver.backend_id isEqualToString:[BTWUserService sharedInstance].authorizedUser.backend_id]) {
                 event.start_address = eventDict[@"passengerStartAddress"];
                 event.finish_address = eventDict[@"passengerFinishAddress"];
                 event.start_date = [[BTWFormattingService dateTimeFormatterWithTimezone] dateFromString:eventDict[@"passengerStart"]];
                 event.finish_date = [[BTWFormattingService dateTimeFormatterWithTimezone] dateFromString:eventDict[@"passengerFinish"]];
                 event.polyline = eventDict[@"passengerPolyline"];
             } else {
                 event.start_address = eventDict[@"driverStartAddress"];
                 event.finish_address = eventDict[@"driverFinishAddress"];
                 event.start_date = [[BTWFormattingService dateTimeFormatterWithTimezone] dateFromString:eventDict[@"driverStart"]];
                 event.finish_date = [[BTWFormattingService dateTimeFormatterWithTimezone] dateFromString:eventDict[@"driverFinish"]];
                 event.polyline = eventDict[@"driverPolyline"];
             }
             event.driver.role = kUserRoleDriver;
             event.passenger.role = kUserRolePassenger;
             [importedEventsIDs addObject:event.backend_id];
             
             if (![event.confirmed boolValue]) {
                 if ([@[kEventStateCompleted, kEventStatusCancelled] containsObject:status]) {
//                     unconfirmedCompletedCount++;
                 } else if ([@[kEventStatePlanned, kEventStatusInprogress] containsObject:status]) {
                     unconfirmedActiveCount++;
                 }
             }
         }
     
         [Event MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (backend_id IN %@)", importedEventsIDs] inContext:localContext];
         
         [localContext MR_saveToPersistentStoreAndWait];
     } callback:^(NSDictionary *response, NSError *error) {
         if (!error) {
             [wself setupTimers];
             wself.unconfirmedActiveEventsCount = unconfirmedActiveCount;
             wself.unconfirmedCompletedEventsCount = unconfirmedCompletedCount;
         }

         wself.isLoadingData = NO;
         [[NSNotificationCenter defaultCenter] postNotificationName:kEventServiceDidUpdateEventsNotfication object:nil];
         if (completionHandler) {
             completionHandler(error);
         }
     }];
}

- (void)cancelEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [self updateState:kEventStateCancelled forEvent:event withCompletionHandler:^(NSError *error){
        if (!error) {
            [self invalidateTimerForEventWithID:event.backend_id];
            NSManagedObjectContext *context = event.managedObjectContext;
            [event MR_deleteEntity];
            [context MR_saveToPersistentStoreAndWait];
        }

        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)startEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [self updateState:kEventStateInprogress forEvent:event withCompletionHandler:^(NSError *error){
        if (!error) {
            [self setStatus:kEventStatusInprogress forEventWithID:event.backend_id];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)finishEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [self updateState:kEventStateCompleted forEvent:event withCompletionHandler:^(NSError *error){
        if (!error) {
            [self setStatus:kEventStateCompleted forEventWithID:event.backend_id];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)updateState:(NSString *)state forEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *cancelMethod = [BTWApiMethod updateMethodForEvent:event.backend_id toState:state];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:cancelMethod callback:^(id response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

#pragma mark - Event timers
- (void)setupTimers
{
    [self stopAllTimers];
    for (Event *event in [Event MR_findAll]) {
        // Заводим таймер старта или стартуем запланированные поездки
        if ([event.status isEqualToString:kEventStatePlanned]) {
            if ([event.start_date compare:[NSDate date]] == NSOrderedDescending) {
                [self setupTimerForEvent:event];
            } else {
                event.status = kEventStatusInprogress;
            }
        }
        
        // Заводим таймер окончания или завершаем текущие поездки
        if ([event.status isEqualToString:kEventStatusInprogress]) {
            if ([event.finish_date compare:[NSDate date]] == NSOrderedDescending) {
                [self setupTimerForEvent:event];
            } else {
                event.status = kEventStatusCompleted;
            }
        }
    }
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

- (void)checkFinishedRoutes
{
    for (Event *event in [Event MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"status = %@", kEventStateInprogress]])
    {
        CGFloat distanceInMeters = [event distanceFromFinish];
        if (distanceInMeters <= kRouteFinishDistanceInMeters) {
            [self finishEvent:event withCompletionHandler:nil];
        }
    }
}

- (void)setupTimerForEvent:(Event *)event
{
    [self invalidateTimerForEventWithID:event.backend_id];
 
    NSNumber *eventID = event.backend_id;
    NSString *newStatus = nil;
    NSDate *date = nil;
    if ([event.status isEqualToString:kEventStatusPlanned]) {
        newStatus = kEventStatusInprogress;
        date = event.start_date;
    } else if ([event.status isEqualToString:kEventStatusInprogress]) {
        newStatus = kEventStatusCompleted;
        date = event.finish_date;
    }
    
    if (newStatus) {
        NSMethodSignature *methodSignature = [self methodSignatureForSelector:@selector(setStatus:forEventWithID:)];
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSignature];
        [invocation setTarget:self];
        [invocation setSelector:@selector(setStatus:forEventWithID:)];
        [invocation setArgument:&newStatus atIndex:2];
        [invocation setArgument:&eventID atIndex:3];
        [invocation retainArguments];
        NSTimer *eventTimer = [NSTimer scheduledTimerWithTimeInterval:[date timeIntervalSinceDate:[NSDate date]] invocation:invocation repeats:NO];
        _timersDict[eventID] = eventTimer;
    }
}

- (void)setStatus:(NSString *)status forEventWithID:(NSNumber *)eventID
{
    [self invalidateTimerForEventWithID:eventID];

    Event *event = [Event MR_findFirstByAttribute:@"backend_id" withValue:eventID];
    if (event) {
        event.status = status;
        [event.managedObjectContext MR_saveToPersistentStoreAndWait];
        
        if ([status isEqualToString:kEventStatusInprogress]) {
            [self setupTimerForEvent:event];
        }
        
        [[BTWTransitionsManager sharedInstance] showPopupForEvent:event];
    } else {
        BTWErrorBlock completionHandler = [^(NSError *error){
            if (!error) {
                Event *fetchedEvent = [Event MR_findFirstByAttribute:@"backend_id" withValue:eventID];
                if (fetchedEvent) {
                    [[BTWTransitionsManager sharedInstance] showPopupForEvent:fetchedEvent];
                }
            }
        } copy];
        [self updateEventsWithCompletionHandler:completionHandler];
    }
    
    if ([status isEqualToString:kEventStatusPlanned]) {
        [[AppsFlyerTracker sharedTracker] trackEvent:@"realride" withValue:nil];
    } else if ([status isEqualToString:kEventStatusCompleted]) {
        [[AppsFlyerTracker sharedTracker] trackEvent:@"rideend" withValue:nil];
    }
}

- (void)invalidateTimerForEventWithID:(NSNumber *)eventID
{
    NSTimer *timer = _timersDict[eventID];
    [timer invalidate];
    [_timersDict removeObjectForKey:eventID];
}

- (void)stopAllTimers
{
    for (NSTimer *timer in [_timersDict allValues]) {
        [timer invalidate];
    }
    [_timersDict removeAllObjects];
}

#pragma mark - Processing push notifications
- (void)handleChangedStatus:(NSString *)status forEventWithID:(NSNumber *)eventID
{
    [self setStatus:status forEventWithID:eventID];
}

@end
