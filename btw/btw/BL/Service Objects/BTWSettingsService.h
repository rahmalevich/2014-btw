//
//  BTWSettingsService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString * const kDistanceUnitsChangedNotification = @"kDistanceUnitsChangedNotification";

static NSString * const kFirstLaunchKey = @"kFirstLaunchKey";
static NSString * const kFirstAuthorizationsDictKey = @"kFirstAuthorizationsDictKey";
static NSString * const kGreetingsShowedKey = @"kGreetingsShowedKey";
static NSString * const kLicenseSignedKey = @"kLicenseSignedKey";
static NSString * const kFilterGenderKey = @"kFilterGenderKey";
static NSString * const kFilterStartRadiusKey = @"kFilterStartRadiusKey";
static NSString * const kFilterFinishRadiusKey = @"kFilterFinishRadiusKey";
static NSString * const kFilterTimeRangeMinutesKey = @"kFilterTimeRangeMinutesKey";
static NSString * const kFilterLowerAgeKey = @"kFilterLowerAgeKey";
static NSString * const kFilterUpperAgeKey = @"kFilterUpperAgeKey";
static NSString * const kFilterDriverLowerAgeKey = @"kFilterDriverLowerAgeKey";
static NSString * const kFilterDriverUpperAgeKey = @"kFilterDriverUpperAgeKey";
static NSString * const kFilterDriverGenderKey = @"kFilterDriverGenderKey";
static NSString * const kFilterDriverRatingKey = @"kFilterDriverRatingKey";
static NSString * const kFilterCarYearKey = @"kFilterCarYearKey";
static NSString * const kFilterSmokingAllowedKey = @"kFilterSmokingAllowedKey";
static NSString * const kCommonDistanceUnitsKey = @"kCommonDistanceUnitsKey";
static NSString * const kCommonRoleKey = @"kCommonRoleKey";
static NSString * const kActivationCodeRequestsDictionary = @"kActivationCodeRequestsDictionary";
static NSString * const kActivationCodeLastRequestKey = @"kActivationCodeLastRequestKey";
static NSString * const kActivationCodeRequestsCountKey = @"kActivationCodeRequestsCountKey";

static NSInteger const kFilterDefaultLowerAge = 18;
static NSInteger const kFilterDefaultUpperAge = 60;
static NSInteger const kFilterMinAgeRange = 5;
static NSInteger const kFilterCarMinYear = 1980;
static CGFloat const kFilterRatingDefaultValue = 2.5;

typedef NS_ENUM(NSUInteger, BTWFilterGenderType) {
    BTWFilterGenderTypeAll,
    BTWFilterGenderTypeMale,
    BTWFilterGenderTypeFemale
};

typedef NS_ENUM(NSUInteger, BTWDistanceUnits) {
    BTWDistanceUnitsKilometers,
    BTWDistanceUnitsMiles
};

@interface BTWSettingsService : NSObject

+ (BOOL)isFirstLaunch;
+ (BOOL)isFirstAuthorizationForUser:(User *)user;

+ (void)updateActivationCodeRequestsCounter;
+ (NSTimeInterval)delayForNextActivationCodeRequest;

+ (id)settingsValueForKey:(NSString *)key;
+ (void)setSettingsValue:(id)value forKey:(NSString *)key;
+ (void)clearValueForKey:(NSString *)key;

+ (void)setDefaultsIfNeeded;
+ (void)setFilterDefaults;
+ (void)setUserSpecificDefaults;

@end
