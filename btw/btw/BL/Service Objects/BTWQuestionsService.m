//
//  BTWQuestionsService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWQuestionsService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+ProfileMethods.h"
#import "BTWUserService.h"

NSString *kQuestionsCategoryKey = @"questions";
NSString *kDetailsCategoryKey = @"details";
NSString *kColorCategoryKey = @"car_color";

#pragma mark - Import categories
@interface Question (Import)
- (void)importBackend_id:(id)value;
- (void)importAnswers:(id)value;
@end

@implementation Question (Import)

- (void)importBackend_id:(id)value
{
    self.backend_id = [BTWCommonUtils stringValueForObject:value];
}

- (void)importAnswers:(id)value
{
    NSArray *rawAnswersArray = [value valueForKey:@"answers"];
    if ([rawAnswersArray isValidArray]) {
        NSMutableArray *answersArray = [NSMutableArray array];
        for (NSDictionary *answerData in rawAnswersArray) {
            Answer *newAnswer = [Answer MR_importFromObject:answerData inContext:self.managedObjectContext];
            [answersArray addObject:newAnswer];
        }
        [answersArray sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"sort_order" ascending:YES]]];
        self.answers = [NSOrderedSet orderedSetWithArray:answersArray];
    }
}

@end

@interface Answer (Import)
- (void)importBackend_id:(id)value;
@end

@implementation Answer (Import)

- (void)importBackend_id:(id)value
{
    self.backend_id = [BTWCommonUtils stringValueForObject:value];
}

@end

#pragma mark - BTWQuestionsService
@interface BTWQuestionsService ()
@property (nonatomic, assign, readwrite) BOOL questionsUpdated;
@property (nonatomic, assign, readwrite) BOOL questionsAreLoading;
@property (nonatomic, assign, readwrite) BOOL cardUpdated;
@property (nonatomic, assign, readwrite) BOOL carsAreLoading;
@end

@implementation BTWQuestionsService

static BTWQuestionsService *_sharedInstance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWQuestionsService new];
    });
    return _sharedInstance;
}

- (void)updateQuestions
{
    if (_questionsAreLoading) {
        return;
    }
    self.questionsAreLoading = YES;
    
    BTWApiMethod *getQuizMethod = [BTWApiMethod getQuizMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getQuizMethod backgroundProcessing:^(NSDictionary *response)
     {
         NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
         
         // 1. Маппим вопросы и ответы
         NSMutableSet *importedQuestions = [NSMutableSet set];
         NSArray *questionsArray = response[@"payload"];
         for (NSDictionary *questionDict in questionsArray) {
             Question *newQuestion = [Question MR_importFromObject:questionDict inContext:context];
             [importedQuestions addObject:newQuestion];
         }
         
         // 2. Удаляем из базы отсутсвующие вопросы
         [Question MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF IN %@)", importedQuestions] inContext:context];
         
         // 3. Маппим ответы из профиля
         User *authorizedUser = [User MR_findFirstByAttribute:@"backend_id" withValue:[BTWUserService sharedInstance].authorizedUser.backend_id inContext:context];
         if ([authorizedUser.quiz isValidArray]) {
             NSArray *userAnswers = [Answer MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"backend_id IN %@", authorizedUser.quiz] inContext:context];
             authorizedUser.answers = [NSSet setWithArray:userAnswers];
         }
         
         [context MR_saveOnlySelfAndWait];
     } callback:^(NSDictionary *response, NSError *error){
         self.questionsAreLoading = NO;
         if (error) {
             [[NSNotificationCenter defaultCenter] postNotificationName:kQuestionsServiceDidFailToUpdateQuestions object:nil userInfo:@{kQuestionsServiceNotificationErrorKey : error}];
         } else {
             self.questionsUpdated = YES;
             [[NSNotificationCenter defaultCenter] postNotificationName:kQuestionsServiceDidUpdateQuestions object:nil];
         }
     }];
}

- (void)submitAnswers
{
    NSArray *answers = [[BTWUserService sharedInstance].authorizedUser.answers allObjects];
    BTWApiMethod *submitMethod = [BTWApiMethod submitMethodForAnswers:answers];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:submitMethod callback:^(id response, NSError *error){
        if (!error) {
            [[AppsFlyerTracker sharedTracker] trackEvent:@"interests" withValue:nil];
        }
    }];
}

- (void)cleanQuestions
{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
    [Question MR_truncateAllInContext:context];
    [context MR_saveToPersistentStoreAndWait];
}

- (void)updateCars
{
    if (_carsAreLoading) {
        return;
    }
    self.carsAreLoading = YES;
    
    BTWApiMethod *updateMethod = [BTWApiMethod getCarsMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:updateMethod backgroundProcessing:^(NSDictionary *response){
        // сохраняем машины в базу
        NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
        
        NSMutableSet *importedBrands = [NSMutableSet set];
        NSDictionary *carBrandsDict = response[@"payload"];
        for (NSString *key in carBrandsDict) {
            NSArray *modelsArray = carBrandsDict[key];
            CarBrand *brandToImport = [CarBrand MR_findFirstByAttribute:@"name" withValue:key inContext:context];
            if (!brandToImport) {
                brandToImport = [CarBrand MR_createInContext:context];
            }
            brandToImport.name = key;
            brandToImport.models = modelsArray;
            [importedBrands addObject:brandToImport];
        }
        [CarBrand MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF IN %@)", importedBrands] inContext:context];
        
        [context MR_saveOnlySelfAndWait];
    } callback:^(NSDictionary *response, NSError *error){
        self.carsAreLoading = NO;
        if (!error) {
            self.cardUpdated = YES;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kQuestionsServiceDidFinishToUpdateCars object:nil];
    }];
}

@end
