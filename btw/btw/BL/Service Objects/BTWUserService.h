//
//  BTWUserService.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 26.08.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

static NSString * const kUserLoggedInNotification = @"kUserLoggedInNotification";
static NSString * const kUserWillLogoutNotification = @"kUserWillLogoutNotification";
static NSString * const kUserLoggedOutNotification = @"kUserLoggedOutNotification";
static NSString * const kUserAvatarChangedNotification = @"kUserAvatarChangedNotification";

@interface BTWUserService : NSObject

@property (nonatomic, strong, readonly) User *authorizedUser;
@property (nonatomic, copy, readonly) NSString *sessionKey;
@property (nonatomic, assign, readonly) BOOL isLoggedIn;

+ (BTWUserService *)sharedInstance;

- (void)registerWithFirstName:(NSString *)firstName lastName:(NSString *)lastName birthdate:(NSDate *)date gender:(BTWUserGender)gender email:(NSString *)email password:(NSString *)password completionHandler:(BTWErrorBlock)completionHandler;
- (void)authorizeWithEmail:(NSString *)email password:(NSString *)password completionHandler:(BTWErrorBlock)completionHandler;
- (void)authorizeWithSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork userID:(NSString *)userId accessToken:(NSString *)accessToken completionHandler:(BTWErrorBlock)completionHandler;
- (void)reloginWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)logout;

- (void)saveProfileWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)updateCarInfoWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)getFullProfileForUser:(User *)user withCompletionHander:(BTWErrorBlock)completionHandler;
- (void)uploadAvatar:(UIImage *)avatar progressHandler:(BTWNetworkProgressBlock)progressBlock withCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)linkSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork userID:(NSString *)userID sessionKey:(NSString *)sessionKey completionHandler:(BTWErrorBlock)completionHandler;
- (void)getFriendsFromSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork forUserID:(NSString *)userID completionHandler:(BTWNetworkResponseBlock)completionHandler;
- (void)getUserFromSocialNetwork:(BTWLinkedSocialNetwork)socialNetwork byUserID:(NSString *)userID completionHandler:(BTWNetworkResponseBlock)completionHandler;

- (void)uploadGalleryPhoto:(UIImage *)photo name:(NSString *)name message:(NSString *)message progressHandler:(BTWNetworkProgressBlock)progressBlock withCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)removeGalleryPhoto:(GalleryImage *)galleryPhoto completionHandler:(BTWErrorBlock)completionHandler;
- (void)complainOnGalleryPhoto:(GalleryImage *)galleryPhoto completionHandler:(BTWErrorBlock)completionHandler;
- (void)setAvatarWithGalleryPhoto:(GalleryImage *)gallerPhoto completionHandler:(BTWErrorBlock)completionHandler;
- (void)getGalleryListWithProcessingBlock:(BTWDataBlock)processingBlock completionHandler:(BTWErrorBlock)completionHandler;
- (void)getGalleryListForUser:(User *)user processingBlock:(BTWDataBlock)processingBlock completionHandler:(BTWErrorBlock)completionHandler;

- (void)requestActivationCodeForPhoneNumber:(NSString *)phoneNumber completionHandler:(BTWErrorBlock)completionHandler;
- (void)confirmPhoneNumberWithCode:(NSString *)activationCode completionHandler:(BTWErrorBlock)completionHandler;

@end
