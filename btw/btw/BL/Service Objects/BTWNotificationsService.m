//
//  BTWNotificationsService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWNotificationsService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+Notifications.h"
#import "BTWApiMethod+RouteMethods.h"
#import "BTWApiMethod+Matching.h"
#import "BTWUserService.h"

@interface BTWNotificationsService () <NSFetchedResultsControllerDelegate>
@property (nonatomic, assign, readwrite) BOOL isLoadingData;
@property (nonatomic, assign, readwrite) NSUInteger unconfirmedNotificationsCount;
@end

@implementation BTWNotificationsService

#pragma mark - Initialization
static BTWNotificationsService *_sharedInstance = nil;
+ (BTWNotificationsService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWNotificationsService new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        [self cleanNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn:) name:kUserLoggedInNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOut:) name:kUserLoggedOutNotification object:nil];
    }
    return self;
}

- (void)userLoggedIn:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleApplicationBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self updateNotificationsWithCompletionHandler:nil];
    });
}

- (void)userLoggedOut:(NSNotification *)notification
{
    [self cleanNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)handleApplicationBecomeActive:(NSNotification *)notification
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self updateNotificationsWithCompletionHandler:nil];
    });
}

- (void)cleanNotifications
{
    self.unconfirmedNotificationsCount = 0;
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
    [Notification MR_truncateAllInContext:context];
    [context MR_saveToPersistentStoreAndWait];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public methods
- (void)updateNotificationsWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    if (_isLoadingData) {
        return;
    }
    
    [completionHandler copy];
    
    self.isLoadingData = YES;
    
    __weak typeof(self) wself = self;
    __block NSUInteger unconfirmedCount = 0;
    BTWApiMethod *likedMeMethod = [BTWApiMethod notificationsListMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:likedMeMethod backgroundProcessing:^(NSDictionary *response)
    {
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_context];

        NSMutableSet *importedNotificationsIDs = [NSMutableSet new];
        NSArray *notificationsArray = response[@"payload"];
        for (NSDictionary *notificationDict in notificationsArray) {
            NSString *type = notificationDict[@"type"];
            if ([type isEqualToString:kNotificationTypeRide] || [type isEqualToString:kNotificationTypeLike]) {
                Notification *notification = [Notification MR_importFromObject:notificationDict inContext:localContext];
                if (![notification.confirmed boolValue]) {
                    unconfirmedCount++;
                }
                [importedNotificationsIDs addObject:notification.backend_id];
            }
        }
        [Notification MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (backend_id IN %@)", importedNotificationsIDs] inContext:localContext];
        
        [localContext MR_saveToPersistentStoreAndWait];
        
    } callback:^(NSDictionary *response, NSError *error) {
        wself.isLoadingData = NO;
        wself.unconfirmedNotificationsCount = unconfirmedCount;
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)confirmNotificationsWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    for (Notification *notification in [Notification MR_findAll]) {
        notification.confirmed = @(YES);
    }
    self.unconfirmedNotificationsCount = 0;
    
    BTWApiMethod *confirmMethod = [BTWApiMethod notificationsConfirmMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:confirmMethod callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)acceptNotification:(Notification *)notification withCompletionHadnler:(BTWNetworkResponseBlock)completionHandler
{
    [self makeAction:YES withNotification:notification completionHandler:completionHandler];
}

- (void)declineNotification:(Notification *)notification withCompletionHadnler:(BTWNetworkResponseBlock)completionHandler
{
    [self makeAction:NO withNotification:notification completionHandler:completionHandler];
}

- (void)makeAction:(BOOL)accept withNotification:(Notification *)notification completionHandler:(BTWNetworkResponseBlock)completionHandler
{
    [completionHandler copy];
    
    NSString *notificationName = nil;
    BTWApiMethod *method = nil;
    if ([notification.type isEqualToString:kNotificationTypeLike]) {
        notificationName = kDidMakeActionWithLikeNotification;
        method = [BTWApiMethod markMatchMethodForUser:notification.user asKeeped:accept];
    } else if ([notification.type isEqualToString:kNotificationTypeRide]) {
        notificationName = accept ? kDidAcceptRideOfferNotification : kDidRejectRideOfferNotification;
        method = [BTWApiMethod markOfferMethodAsAccepted:accept forId:notification.link];
    }
    
    if (method) {
        __weak typeof(self) wself = self;
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:method callback:^(NSDictionary *response, NSError *error)
        {
            if (!error) {
                // Уведомляем другие объекты о выполненном действии
                if ([notificationName isValidString]) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:@{kNotificationUserIdKey : notification.user.backend_id}];
                }
            
                // Изменяем счетчик
                if (![notification.confirmed boolValue]) {
                    wself.unconfirmedNotificationsCount = MAX(0, wself.unconfirmedNotificationsCount - 1);
                }
            
                // Удаляем нотификацию из базы
                NSManagedObjectContext *context = notification.managedObjectContext;
                [notification MR_deleteEntity];
                [context MR_saveToPersistentStoreAndWait];
            }
            
            if (completionHandler) {
                completionHandler(response, error);
            }
        }];
    }
}

@end
