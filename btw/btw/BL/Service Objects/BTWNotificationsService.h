//
//  BTWNotificationsService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

static NSString * const kNotificationUserIdKey = @"kNotificationUserIdKey";
static NSString * const kDidMakeActionWithLikeNotification = @"kDidMakeActionWithLikeNotification";
static NSString * const kDidAcceptRideOfferNotification = @"kDidAcceptRideOfferNotification";
static NSString * const kDidRejectRideOfferNotification = @"kDidRejectRideOfferNotification";

@interface BTWNotificationsService : NSObject

@property (nonatomic, assign, readonly) BOOL isLoadingData;
@property (nonatomic, assign, readonly) NSUInteger unconfirmedNotificationsCount;

+ (BTWNotificationsService *)sharedInstance;
- (void)updateNotificationsWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)confirmNotificationsWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)acceptNotification:(Notification *)notification withCompletionHadnler:(BTWNetworkResponseBlock)completionHandler;
- (void)declineNotification:(Notification *)notification withCompletionHadnler:(BTWNetworkResponseBlock)completionHandler;

@end
