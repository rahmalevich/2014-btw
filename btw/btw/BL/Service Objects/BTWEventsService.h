//
//  BTWEventsService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

static NSString * const kEventServiceDidUpdateEventsNotfication = @"kEventServiceDidUpdateEventsNotification";
static CGFloat const kRouteFinishDistanceInMeters = 1000.0f;

@interface BTWEventsService : NSObject

@property (nonatomic, assign, readonly) BOOL isLoadingData;
@property (nonatomic, assign, readonly) NSUInteger unconfirmedActiveEventsCount;
@property (nonatomic, assign, readonly) NSUInteger unconfirmedCompletedEventsCount;

+ (BTWEventsService *)sharedInstance;
- (void)updateEventsWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)cancelEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)startEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)finishEvent:(Event *)event withCompletionHandler:(BTWErrorBlock)completionHandler;

- (void)handleChangedStatus:(NSString *)status forEventWithID:(NSString *)eventID;

- (void)confirmActiveEventsWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)confirmCompletedEventsWithCompletionHandler:(BTWErrorBlock)completionHandler;

@end
