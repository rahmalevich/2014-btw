//
//  BTWMessagingService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

// Объект занимается
// - кэшированием контактов
// - подпиской на соответствующие MQTT-топики
// - кэшированием сообщений
// - менеджментом отправки/получения сообщений через MQTT-брокер
// - уведомлении других объектов о получении сообщения через нотификации

static NSString * const kTypingMessageReceivedNotification = @"kTypingMessageReceivedNotification";
static NSString * const kOnlineMessageReceivedNotification = @"kOnlineMessageReceivedNotification";
static NSString * const kOfflineMessageReceivedNotification = @"kOfflineMessageReceivedNotification";

@interface BTWMessagingService : NSObject

@property (nonatomic, assign, readonly) BOOL isLoadingDialogs;
@property (nonatomic, assign, readonly) NSUInteger unconfirmedMessagesCount;
@property (nonatomic, strong, readonly) NSMutableSet *activeDialogsIDs;

+ (BTWMessagingService *)sharedInstance;

- (void)startMessaging;
- (void)stopMessaging;

- (void)updateDialogsWithCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)getDialogWithTopicId:(NSString *)topicId completionHandler:(BTWErrorBlock)completionHandler;
- (void)getHistoryForDialog:(Dialog *)dialog withCompletionHandler:(BTWErrorBlock)completionHandler;
- (void)confirmHistoryForDialog:(Dialog *)dialog;

- (void)sendTypingMessageToDialog:(Dialog *)dialog;
- (void)sendMessageWithText:(NSString *)text toDialog:(Dialog *)dialog;
- (void)sendCurrentLocationToDialog:(Dialog *)dialog;
- (void)sendCurrentRouteToDialog:(Dialog *)dialog;
- (void)sendPhoto:(UIImage *)image toDialog:(Dialog *)dialog withProgressHandler:(BTWNetworkProgressBlock)progressBlock completionHandler:(BTWErrorBlock)completionHandler;
- (void)resendMessage:(DialogMessage *)message;

- (void)removeDialog:(Dialog *)dialog withCompletionHandler:(BTWErrorBlock)completionHandler;

- (void)cleanDialogs;

@end
