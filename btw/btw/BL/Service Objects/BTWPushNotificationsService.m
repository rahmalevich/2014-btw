//
//  BTWPushNotificationsService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 17.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWPushNotificationsService.h"
#import "BTWUserService.h"
#import "BTWMessagingService.h"
#import "BTWNotificationsService.h"
#import "BTWTransitionsManager.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+PushNotifications.h"
#import "BTWMailContainerViewController.h"
#import "BTWSystemVersionUtils.h"
#import "BTWEventsService.h"

#import <AudioToolbox/AudioToolbox.h>

static NSString *kDeviceTokenKey = @"kDeviceTokenKey";

@interface BTWPushNotificationsService ()
@property (nonatomic, copy, readwrite) NSString *deviceToken;
@end

@implementation BTWPushNotificationsService

#pragma mark - Initialization
static BTWPushNotificationsService *_sharedInstance = nil;
+ (BTWPushNotificationsService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWPushNotificationsService new];
    });
    return _sharedInstance;
}

- (void)setup
{
    self.deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceTokenKey];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedIn:) name:kUserLoggedInNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userWillLogout:) name:kUserWillLogoutNotification object:nil];
}

- (void)userLoggedIn:(NSNotification *)notification
{
    [self subscribe];
}

- (void)userWillLogout:(NSNotification *)notification
{
    [self unsubscribe];
}

#pragma mark - Lifecycle
- (void)setTokenWithData:(NSData *)tokenData
{
    NSString* newToken = [tokenData description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    self.deviceToken = newToken;
    
    if ([BTWUserService sharedInstance].isLoggedIn) {
        [self subscribe];
    }
}

- (void)handleNotification:(NSDictionary *)userInfo fromBackground:(BOOL)background
{
    if (!background) {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    }
    
    NSString *type = userInfo[@"type"];
    if ([type isEqualToString:kPushNotificationTypeContact]) {
        NSString *topicId = userInfo[@"topic"];
        if (background) {
            // переходим в список контактов и обновляем
            BTWTabBarController *tabBarController = [BTWTransitionsManager sharedInstance].tabBarController;
            tabBarController.selectedIndex = kMailViewControllerIndex;
            UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
            BTWMailContainerViewController *mailController = (BTWMailContainerViewController *)navigationController.topViewController;
            [mailController showContacts];
            [[BTWMessagingService sharedInstance] getDialogWithTopicId:topicId completionHandler:nil];
        } else {
            [[BTWMessagingService sharedInstance] updateDialogsWithCompletionHandler:nil];
        }
    } else if ([type isEqualToString:kPushNotificaitonTypeMessage]) {
        if (background) {
            // Показываем диалог
            NSString *topicId = userInfo[@"topic"];
            Dialog *dialog = [Dialog MR_findFirstByAttribute:@"backend_id" withValue:topicId];
            if (dialog) {
                [[BTWTransitionsManager sharedInstance] showDialog:dialog];
            } else {
                [[BTWMessagingService sharedInstance] getDialogWithTopicId:topicId completionHandler:^(NSError *error){
                    if (!error) {
                        Dialog *newDialog = [Dialog MR_findFirstByAttribute:@"backend_id" withValue:topicId];
                        [[BTWTransitionsManager sharedInstance] showDialog:newDialog];
                    }
                }];
            }
        }
    } else if ([type isEqualToString:kPushNotificationTypeLike] || [type isEqualToString:kPushNotificationTypeTravel]) {
        if (background) {
            // Показываем список нотификаций и обновляем
            BTWTabBarController *tabBarController = [BTWTransitionsManager sharedInstance].tabBarController;
            tabBarController.selectedIndex = kMailViewControllerIndex;
            UINavigationController *navigationController = (UINavigationController *)tabBarController.selectedViewController;
            BTWMailContainerViewController *mailController = (BTWMailContainerViewController *)navigationController.topViewController;
            [mailController showNotifications];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Route", nil) message:NSLocalizedString(@"You received new route offer", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
            [alert show];
            [[BTWNotificationsService sharedInstance] updateNotificationsWithCompletionHandler:nil];
        }
    } else if ([type isEqualToString:kPushNotificationTypeEvent]) {
        NSString *eventID = userInfo[@"id"];
        NSString *status = userInfo[@"state"];
        [[BTWEventsService sharedInstance] handleChangedStatus:status forEventWithID:eventID];
    }
}

- (void)subscribe
{
    if ([_deviceToken isValidString]) {
        NSString *currentToken = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceTokenKey];
        if (![currentToken isEqualToString:_deviceToken]) {
            
            // Отписываемся для текущего токена
            if ([currentToken isValidString]) {
                BTWApiMethod *unsubsribeMethod = [BTWApiMethod unsubscribeMethodForToken:currentToken];
                [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:unsubsribeMethod callback:^(id response, NSError *error){
                    NSLog(@"%@", response);
                }];
            }
            
            // Подписываемся для нового
            if ([_deviceToken isValidString]) {
                BTWApiMethod *subscribeMethod = [BTWApiMethod subscribeMethodForToken:_deviceToken];
                [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:subscribeMethod callback:^(id response, NSError *error){
                    if (!error) {
                        [[NSUserDefaults standardUserDefaults] setValue:_deviceToken forKey:kDeviceTokenKey];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                }];
            }
        }
    } else {
        NSUInteger notificationType = (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert);
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationType categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationType];
        }
    }
}

- (void)unsubscribe
{
    NSString *currentToken = [[NSUserDefaults standardUserDefaults] valueForKey:kDeviceTokenKey];
    if ([currentToken isValidString]) {
        BTWApiMethod *unsubsribeMethod = [BTWApiMethod unsubscribeMethodForToken:currentToken];
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:unsubsribeMethod callback:^(id response, NSError *error){
            if (!error) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:kDeviceTokenKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }];
    }
}

@end
