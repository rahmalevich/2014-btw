//
//  BTWRouteService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWRouteService.h"
#import "BTWUserService.h"
#import "BTWLocationManager.h"
#import "BTWNetworkHelper.h"
#import "BTWFormattingService.h"

#import "BTWApiMethod+GoogleServices.h"
#import "BTWApiMethod+RouteMethods.h"

#import "SPGooglePlacesPlaceDetailQuery.h"
#import "SDWebImagePrefetcher.h"

static NSTimeInterval const kDefaultSearchTimeInterval = 20 * 60.0;
static NSInteger const kHistoryItemsToStore = 10;

@interface BTWRouteService ()
@property (nonatomic, strong) NSManagedObjectContext *usersContext;
@property (nonatomic, strong, readwrite) Route *currentRoute;
@property (nonatomic, weak) NSTimer *searchTimer;
@end

@implementation BTWRouteService

#pragma mark - Initialization
static BTWRouteService *_sharedInstance = nil;
+ (BTWRouteService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWRouteService new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        
        // Восстанавливаем данные о поиске
        Route *currentRoute = [Route MR_findFirstByAttribute:@"is_current" withValue:@(YES)];
        if (currentRoute) {
            NSTimeInterval searchRestTime = kDefaultSearchTimeInterval - fabs([currentRoute.search_date timeIntervalSinceNow]);
            if (searchRestTime <= 0) {
                currentRoute.is_current = @(NO);
                [currentRoute.managedObjectContext MR_saveToPersistentStoreAndWait];
            } else {
                self.currentRoute = currentRoute;
                self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:searchRestTime target:self selector:@selector(searchFinished:) userInfo:nil repeats:NO];
            }
        }
        
        // Создаем контекст для пользователей
        self.usersContext = [NSManagedObjectContext MR_context];
        
        // Подписываемся на нотификацию о логауте
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedOutNotification:) name:kUserLoggedOutNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_usersContext rollback];
}

#pragma mark - Data methods
- (void)setupCurrentRouteWithRoute:(Route *)route
{
    // Обрабатываем оставновку текущего поиска
    [self processSearchCompletion];
    
    // Сохраняем текущий маршрут и удаляем просроченные
    self.currentRoute = [Route routeWithRoute:route];
    _currentRoute.is_current = @(YES);
    _currentRoute.search_date = [NSDate date];
    
    // Сохраняем только последние 10 маршрутов
    NSArray *routesToStore = [Route MR_findAllSortedBy:@"search_date" ascending:NO withPredicate:nil];
    if ([routesToStore count] > kHistoryItemsToStore) {
        routesToStore = [routesToStore subarrayWithRange:NSMakeRange(0, kHistoryItemsToStore)];
    }
    [Route MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF IN %@)", routesToStore]];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    
    // Заводим таймер
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:kDefaultSearchTimeInterval target:self selector:@selector(searchFinished:) userInfo:nil repeats:NO];
}

- (void)resolveLocationsForRoute:(Route *)route withCompletion:(BTWErrorBlock)completionHandler
{
    if (route.startPointLatitude && route.startPointLongitude && route.endPointLatitude && route.endPointLongitude)
    {
        completionHandler(nil);
    } else {
        __weak typeof(self) wself = self;
        
        [completionHandler copy];
        
        // Сбрасываем маршрут, потому что одна из точек изменилась
        route.polyline = nil;
        
        BTWVoidBlock checkEndPointBlock = [^{
            if (route.endPointLatitude && route.endPointLongitude) {
                completionHandler(nil);
            } else {
                [wself getCoordintateForReference:route.endPointReference withCompletionHandler:^(CGFloat latitude, CGFloat longitude, NSError *error)
                 {
                     if (!error) {
                         route.endPointLatitude = @(latitude);
                         route.endPointLongitude = @(longitude);
                         completionHandler(nil);
                     } else {
                         completionHandler(error);
                     }
                 }];
            }
        } copy];
        
        BTWVoidBlock checkStartPointBlock = [^{
            if (route.startPointLatitude && route.startPointLongitude) {
                checkEndPointBlock();
            } else {
                [wself getCoordintateForReference:route.startPointReference withCompletionHandler:^(CGFloat latitude, CGFloat longitude, NSError *error)
                 {
                     if (!error) {
                         route.startPointLatitude = @(latitude);
                         route.startPointLongitude = @(longitude);
                         checkEndPointBlock();
                     } else {
                         completionHandler(error);
                     }
                 }];
            }
        } copy];
        
        checkStartPointBlock();
    }
}

- (void)getCoordintateForReference:(NSString *)reference withCompletionHandler:(void (^)(CGFloat latitude, CGFloat longitude, NSError *error))completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        SPGooglePlacesPlaceDetailQuery *query = [[SPGooglePlacesPlaceDetailQuery alloc] initWithApiKey:kGoogleMapsAppID];
        query.reference = reference;
        [query fetchPlaceDetail:^(NSDictionary *placeDictionary, NSError *error) {
            if (error) {
                completionHandler(0, 0, error);
            } else {
                CGFloat latitude = [[placeDictionary valueForKeyPath:@"geometry.location.lat"] floatValue];
                CGFloat longitude = [[placeDictionary valueForKeyPath:@"geometry.location.lng"] floatValue];
                completionHandler(latitude, longitude, nil);
            }
        }];
    }
}

- (void)requestPathForRoute:(Route *)route withCompletionHandler:(BTWErrorBlock)completionHandler
{
    if (completionHandler) {
        if ([route.polyline isValidString]) {
            completionHandler(nil);
        } else {
            [completionHandler copy];
            
            BTWApiMethod *directionsMethod = [BTWApiMethod directionMethodForRoute:route];
            [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:directionsMethod callback:^(NSDictionary *response, NSError *error)
             {
                 if (!error) {
                     if ([response[@"status"] isEqualToString:@"OK"]) {
                         NSDictionary *routes = [[response objectForKey:@"routes"] firstObject];
                         NSDictionary *routeDict = [routes objectForKey:@"overview_polyline"];
                         NSString *overview_route = [routeDict objectForKey:@"points"];
                         route.polyline = overview_route;
                         
                         completionHandler(nil);
                     } else {
                         completionHandler([NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeUnknown userInfo:nil]);
                     }
                 } else {
                     completionHandler(error);
                 }
             }];
        }
    }
}

- (void)cleanRouteHistory
{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
    [Route MR_truncateAllInContext:context];
    [context MR_saveToPersistentStoreAndWait];
}

#pragma mark - Search
- (void)startSearchWithRoute:(Route *)route completion:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    [self resolveLocationsForRoute:route withCompletion:^(NSError *error){
        if (!error) {
            [wself requestPathForRoute:route withCompletionHandler:^(NSError *error){
                if (!error) {
                    BTWApiMethod *searchMethod = [BTWApiMethod startMethodForRoute:route];
                    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:searchMethod callback:^(id response, NSError *error){
                        if (!error) {
                            [[AppsFlyerTracker sharedTracker] trackEvent:@"ride" withValue:nil];
                            [wself setupCurrentRouteWithRoute:route];
                        }
                        if (completionHandler) {
                            completionHandler(error);
                        }
                    }];
                } else {
                    if (completionHandler) {
                        completionHandler(error);
                    }
                }
            }];
        } else {
            if (completionHandler) {
                completionHandler(error);
            }
        }
    }];

}

- (void)getSearchResultsWithCompletion:(void (^)(NSArray *, NSError *))completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        NSManagedObjectContext *usersContext = _usersContext;
        
        BTWApiMethod *getResultsMethod = [BTWApiMethod searchResultsMethod];
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getResultsMethod callback:^(id response, NSError *error)
        {
            if (!error) {
                [usersContext rollback];
                
                NSArray *rawUsers = response[@"payload"];
                NSMutableArray *mutableUsers = [NSMutableArray array];
                NSMutableArray *urlsArray = [NSMutableArray array];
                for (NSDictionary *rawUserDict in rawUsers) {
                    RouteUser *user = [RouteUser MR_importFromObject:rawUserDict inContext:usersContext];
                    [mutableUsers addObject:user];
                    if ([user.photo_mid isValidString]) {
                        [urlsArray addObject:[NSURL URLWithString:user.photo_mid]];
                    }
                }
                NSArray *users = [NSArray arrayWithArray:mutableUsers];
                
                if ([urlsArray count]) {
                    [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:urlsArray progress:nil completed:^(NSUInteger noOfFinishedUrls, NSUInteger noOfSkippedUrls)
                     {
                         completionHandler(users, nil);
                     }];
                } else {
                    completionHandler(users, nil);
                }
            } else {
                completionHandler(nil, error);
            }
        }];
    }
}

- (void)endSearchWithCompletion:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    
    BTWApiMethod *endSearchMethod = [BTWApiMethod endMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:endSearchMethod callback:^(id response, NSError *error){
        if (!error) {
            [wself processSearchCompletion];
        }
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)processSearchCompletion
{
    // Переносим маршрут в историтю
    _currentRoute.is_current = @(NO);
    
    // Удаляем такой же, если есть
    [Route MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"SELF != %@ AND (startPointAddress = %@ AND endPointAddress = %@)", _currentRoute, _currentRoute.startPointAddress, _currentRoute.endPointAddress]];
    
    [_currentRoute.managedObjectContext MR_saveToPersistentStoreAndWait];
    self.currentRoute = nil;
    
    [_searchTimer invalidate];
}

#pragma mark - Ride interaction
- (void)offerRideToUser:(RouteUser *)user withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *offerMethod = [BTWApiMethod makeOfferMethodForUser:user];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:offerMethod callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

#pragma mark - Timer handling
- (void)searchFinished:(NSTimer *)timer
{
    [self processSearchCompletion];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kRouteSearchFinishedNotification object:nil];
}

#pragma mark - Logout processing
- (void)userLoggedOutNotification:(NSNotification *)notification
{
    [self processSearchCompletion];
    [self cleanRouteHistory];
}

@end
