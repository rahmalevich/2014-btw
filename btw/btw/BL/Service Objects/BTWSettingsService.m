//
//  BTWSettingsService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSettingsService.h"
#import "BTWUserService.h"

static NSString * const kPreviousUserKey = @"kPreviousUserKey";

static CGFloat const kFilterDefaultStartRadius = 5.0;
static CGFloat const kFilterDefaultFinishRadius = 5.0;
static NSInteger const kFilterDefaultTimeRange = 0;
static NSInteger const kFilterAgeOffset = 10;

static NSTimeInterval const kActivationCodeSmallDelay = 10.0f;
static NSTimeInterval const kActivationCodeLargeDelay = 60.0f * 60.0f * 12;
static NSInteger const kActivationCodeBoundingCount = 5;

@implementation BTWSettingsService

static BOOL _isFirstLaunch;
+ (void)initialize
{
    _isFirstLaunch = [[self settingsValueForKey:kFirstLaunchKey] boolValue];
}

+ (BOOL)isFirstLaunch
{
    return _isFirstLaunch;
}

+ (BOOL)isFirstAuthorizationForUser:(User *)user
{
    BOOL isFirstLaunch = NO;
    if ([user.backend_id isValidString]) {
        NSMutableDictionary *firstAuthsDict = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kFirstAuthorizationsDictKey]];
        BOOL wasAuthorized = [[firstAuthsDict objectForKey:user.backend_id] boolValue];
        isFirstLaunch = !wasAuthorized;
        if (isFirstLaunch) {
            firstAuthsDict[user.backend_id] = @(YES);
            [[NSUserDefaults standardUserDefaults] setObject:firstAuthsDict forKey:kFirstAuthorizationsDictKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    return isFirstLaunch;
}

+ (id)settingsValueForKey:(NSString *)key
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}

+ (void)setSettingsValue:(id)value forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([key isEqualToString:kCommonDistanceUnitsKey]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kDistanceUnitsChangedNotification object:nil];
    }
}

+ (void)clearValueForKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setDefaultsIfNeeded
{
    if (![[self settingsValueForKey:kFirstLaunchKey] boolValue]) {
        [self setSettingsValue:@(YES) forKey:kFirstLaunchKey];
        [self setSettingsValue:@(BTWDistanceUnitsKilometers) forKey:kCommonDistanceUnitsKey];
        [self setSettingsValue:@(kFilterDefaultLowerAge) forKey:kFilterLowerAgeKey];
        [self setSettingsValue:@(kFilterDefaultUpperAge) forKey:kFilterUpperAgeKey];
        [self setSettingsValue:@(kFilterDefaultLowerAge) forKey:kFilterDriverLowerAgeKey];
        [self setSettingsValue:@(kFilterDefaultUpperAge) forKey:kFilterDriverUpperAgeKey];
        [self setSettingsValue:@(BTWFilterGenderTypeAll) forKey:kFilterGenderKey];
        [self setSettingsValue:@(kFilterDefaultStartRadius) forKey:kFilterStartRadiusKey];
        [self setSettingsValue:@(kFilterDefaultFinishRadius) forKey:kFilterFinishRadiusKey];
        [self setSettingsValue:@(kFilterDefaultTimeRange) forKey:kFilterTimeRangeMinutesKey];
    }
}

+ (void)setUserSpecificDefaults
{
    User *user = [BTWUserService sharedInstance].authorizedUser;
    NSString *previousUserId = [self settingsValueForKey:kPreviousUserKey];

    if (![previousUserId isEqualToString:user.backend_id]) {
        [self setSettingsValue:user.backend_id forKey:kPreviousUserKey];
        
        BTWFilterGenderType genderType;
        switch ([user.gender integerValue]) {
            case BTWUserGenderMale:
                genderType = BTWFilterGenderTypeFemale;
                break;
            case BTWUserGenderFemale:
                genderType = BTWFilterGenderTypeMale;
                break;
            default:
                genderType = BTWFilterGenderTypeAll;
                break;
        }
        [self clearValueForKey:kCommonRoleKey];
        [self setSettingsValue:@(genderType) forKey:kFilterGenderKey];
        [self setSettingsValue:@(MAX(kFilterDefaultLowerAge, user.age - kFilterAgeOffset)) forKey:kFilterLowerAgeKey];
        [self setSettingsValue:@(MIN(kFilterDefaultUpperAge, user.age + kFilterAgeOffset)) forKey:kFilterUpperAgeKey];
        [self setSettingsValue:@(kFilterDefaultLowerAge) forKey:kFilterDriverLowerAgeKey];
        [self setSettingsValue:@(kFilterDefaultUpperAge) forKey:kFilterDriverUpperAgeKey];
        [self setSettingsValue:@(BTWUserGenderBlank) forKey:kFilterDriverGenderKey];
        [self setSettingsValue:@(kFilterRatingDefaultValue) forKey:kFilterDriverRatingKey];
        [self setSettingsValue:@(NO) forKey:kFilterSmokingAllowedKey];
    }
}

+ (void)setFilterDefaults
{
    [self setSettingsValue:@(kFilterDefaultStartRadius) forKey:kFilterStartRadiusKey];
    [self setSettingsValue:@(kFilterDefaultFinishRadius) forKey:kFilterFinishRadiusKey];
    [self setSettingsValue:@(kFilterDefaultTimeRange) forKey:kFilterTimeRangeMinutesKey];
}

+ (void)updateActivationCodeRequestsCounter
{
    NSMutableDictionary *activationsDictionary = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:kActivationCodeRequestsDictionary]];
    NSMutableDictionary *userDictionary = [NSMutableDictionary dictionaryWithDictionary:activationsDictionary[[BTWUserService sharedInstance].authorizedUser.backend_id]];
    userDictionary[kActivationCodeRequestsCountKey] = @([userDictionary[kActivationCodeRequestsCountKey] integerValue] + 1);
    userDictionary[kActivationCodeLastRequestKey] = [NSDate date];
    activationsDictionary[[BTWUserService sharedInstance].authorizedUser.backend_id] = userDictionary;
    [[NSUserDefaults standardUserDefaults] setObject:activationsDictionary forKey:kActivationCodeRequestsDictionary];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSTimeInterval)delayForNextActivationCodeRequest
{
    NSDictionary *activationsDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:kActivationCodeRequestsDictionary];
    NSDictionary *userDictionary = activationsDictionary[[BTWUserService sharedInstance].authorizedUser.backend_id];
    NSInteger counter = [userDictionary[kActivationCodeRequestsCountKey] integerValue];
    NSDate *lastRequestDate = userDictionary[kActivationCodeLastRequestKey];
    
    NSTimeInterval delayForCounter = 0.0f;
    if (counter > 0) {
        delayForCounter = (counter % kActivationCodeBoundingCount == 0) ? kActivationCodeLargeDelay : kActivationCodeSmallDelay;
    }
    NSTimeInterval timeIntervalSinceLastRequest = [[NSDate date] timeIntervalSinceDate:lastRequestDate];
    NSTimeInterval resultTimeInterval = MAX(delayForCounter - timeIntervalSinceLastRequest, 0.0f);

    return resultTimeInterval;
}

@end
