//
//  BTWMessagingService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMessagingService.h"
#import "BTWUserService.h"
#import "BTWLocationManager.h"
#import "BTWRouteService.h"
#import "BTWNetworkHelper.h"
#import "BTWApiMethod+Contacts.h"
#import "BTWCommonUtils.h"
#import "BTWTransitionsManager.h"

#import "MQTTKit.h"
#import "AFNetworkReachabilityManager.h"

static NSString *kMQTTHost = @"togeze.com";
static CGFloat kMessageSendingFailTimer = 10.0f;

@interface MQTTClient (Protected)
@property (nonatomic, assign) BOOL connected;
@end

@interface BTWMessagingService () <NSFetchedResultsControllerDelegate>
@property (nonatomic, assign, readwrite) BOOL isLoadingDialogs;
@property (nonatomic, assign, readwrite) NSUInteger unconfirmedMessagesCount;
@property (nonatomic, strong, readwrite) NSMutableSet *activeDialogsIDs;
@property (nonatomic, strong) MQTTClient *mqttClient;
@property (nonatomic, strong) NSMutableSet *subscribedTopicsIds;
@property (nonatomic, strong) NSFetchedResultsController *dialogsController;
@end

@implementation BTWMessagingService

#pragma mark - Initialization
static BTWMessagingService *_sharedInstance = nil;
+ (BTWMessagingService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWMessagingService new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.activeDialogsIDs = [NSMutableSet set];
        [[AFNetworkReachabilityManager sharedManager] addObserver:self forKeyPath:@"reachable" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[AFNetworkReachabilityManager sharedManager] removeObserver:self forKeyPath:@"reachable"];
}

#pragma mark - Messaging setup
- (void)startMessaging
{
    // Проверяем наличие авторизации
    if (![BTWUserService sharedInstance].authorizedUser) {
        return;
    }
    
    // Проверяем наличие текущего подключения
    if (_mqttClient.connected) {
        return;
    }
 
    // Инициализируем значения
    self.subscribedTopicsIds = [NSMutableSet set];
    [self updateUnconfirmedMessagesCount];
    
    // Создаем MQTT-клиент
    __weak typeof(self) wself = self;
    self.mqttClient = [[MQTTClient alloc] initWithClientId:[BTWUserService sharedInstance].authorizedUser.backend_id];
    _mqttClient.messageHandler = ^(MQTTMessage *message){
        [wself handleMessage:message];
    };
    [_mqttClient connectToHost:kMQTTHost completionHandler:^(MQTTConnectionReturnCode code){
        if (code == ConnectionAccepted) {
            
            // Подписываемся на закэшированные диалоги
            for (Dialog *dialog in [Dialog MR_findAll]) {
                [self subscribeToTopicWithID:dialog.backend_id];
            }
            
            // Обновляем список диалогов
            [wself updateDialogsWithCompletionHandler:^(NSError *error){
                if (!error) {
                    [wself processUnsentMessages];
                }
            }];
        }
    }];
    
    // Подписываемся на обновления диалогов
    self.dialogsController = [Dialog MR_fetchAllGroupedBy:nil withPredicate:nil sortedBy:nil ascending:YES delegate:self];
    
    // Отслеживаем жизненный цикл приложения
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleApplicationBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)stopMessaging
{
    // Отписывемся от всех топиков
    NSSet *subscribedTopicCopy = [_subscribedTopicsIds copy];
    for (NSString *topicId in subscribedTopicCopy) {
        [self unsubscribeFromTopicWithID:topicId];
    }
    self.subscribedTopicsIds = nil;
    
    // Закрываем сессию
    [_mqttClient cleanSession];
    self.mqttClient = nil;
    
    // Перестаем следить за обновлением диалогов
    self.dialogsController = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)handleApplicationBecomeActive:(NSNotification *)notification
{
    [self updateDialogsWithCompletionHandler:nil];
}

- (void)subscribeToTopicWithID:(NSString *)topicId
{
    [_mqttClient subscribe:topicId withCompletionHandler:^(NSArray *grangedQos){
        // TODO: обработка grantedQos?
        dispatch_async(dispatch_get_main_queue(), ^{
            [self sendOnlineMessageToDialogWithId:topicId];
            [_subscribedTopicsIds addObject:topicId];
        });
    }];
}

- (void)unsubscribeFromTopicWithID:(NSString *)topicId
{
    [self sendOfflineMessageToDialogWithId:topicId withCompletionHandler:^(NSError *error){
        dispatch_async(dispatch_get_main_queue(), ^{
            [_mqttClient unsubscribe:topicId withCompletionHandler:nil];
            [_subscribedTopicsIds removeObject:topicId];
        });
    }];
}

- (void)cleanDialogs
{
    self.unconfirmedMessagesCount = 0;
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
    [Dialog MR_truncateAllInContext:context];
    [context MR_saveToPersistentStoreAndWait];
}

- (void)updateUnconfirmedMessagesCount
{
    NSUInteger unconfirmedCount = [[Dialog MR_aggregateOperation:@"sum:" onAttribute:@"unconfirmed_messages_count" withPredicate:nil] integerValue];
    self.unconfirmedMessagesCount = unconfirmedCount;
}

#pragma mark - Handling network status
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"reachable"]) {
        if ([AFNetworkReachabilityManager sharedManager].reachable) {
            if (_mqttClient.connected) {
                NSLog(@"*** LOGOUT? REACHABLE");
                [self updateDialogsWithCompletionHandler:nil];
            } else {
                [self startMessaging];
            }
        }
    }
}

#pragma mark - Handling dialogs update
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    NSString *topicId = [(Dialog *)anObject backend_id];
    if ([topicId isValidString]) {
        switch(type) {
            case NSFetchedResultsChangeInsert:
                [self subscribeToTopicWithID:topicId];
                break;
            case NSFetchedResultsChangeDelete:
                [self unsubscribeFromTopicWithID:topicId];
                break;
            default:
                break;
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self updateUnconfirmedMessagesCount];
}

#pragma mark - Internal logic
- (void)handleMessage:(MQTTMessage *)message
{
    NSDictionary *messageDict = [NSJSONSerialization JSONObjectWithData:message.payload options:0 error:nil];
    
    NSString *type = messageDict[@"type"];
    if ([type isEqualToString:kBTWMessageTypeConfirm]) {
        NSString *messageId = [messageDict valueForKeyPath:@"params.msgid"];
        if ([messageId isValidString]) {
            NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
            DialogMessage *message = [DialogMessage MR_findFirstByAttribute:@"backend_id" withValue:messageId inContext:context];
            if (message.outgoing) {
                message.confirmed = @(YES);
                message.status = @(BTWDialogMessageStatusDelivered);
                [context MR_saveToPersistentStoreAndWait];
            }
        }
    } else if ([type isEqualToString:kBTWMessageTypeSystem] ||
               [type isEqualToString:kBTWMessageTypeUser])
    {
        NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
        DialogMessage *newMessage = [self importMessageFromDict:messageDict inContext:context];
        
        Dialog *dialog = [Dialog MR_findFirstByAttribute:@"backend_id" withValue:message.topic inContext:context];
        NSMutableOrderedSet *dialogMessages = [dialog.messages mutableCopy];
        [dialogMessages addObject:newMessage];
        dialog.messages = [dialogMessages copy];
        
        if ([type isEqualToString:kBTWMessageTypeUser]) {
            if (newMessage.incoming && [newMessage.confirmed boolValue] == NO) {
                if (![_activeDialogsIDs containsObject:newMessage.dialog.backend_id]) {
                    // Ставим единичнку для пустого диалога, т.к. unconfirmerMessages у него и так возвращается 1
                    dialog.unconfirmed_messages_count = @(dialog.isEmpty ? 1 : [dialog.unconfirmed_messages_count integerValue] + 1);
                } else {
                    [self sendConfirmationForMessage:newMessage];
                }
            }
        }
        
        [context MR_saveToPersistentStoreAndWait];
    } else if ([type isEqualToString:kBTWMessageTypeTyping] ||
               [type isEqualToString:kBTWMessageTypeOnline] ||
               [type isEqualToString:kBTWMessageTypeOffline])
    {
        if (![messageDict[@"senderId"] isEqualToString:[BTWUserService sharedInstance].authorizedUser.backend_id]) {
            NSString *notificationName = nil;
            if ([type isEqualToString:kBTWMessageTypeTyping]) {
                notificationName = kTypingMessageReceivedNotification;
            } else if ([type isEqualToString:kBTWMessageTypeOnline]) {
                notificationName = kOnlineMessageReceivedNotification;
            } else if ([type isEqualToString:kBTWMessageTypeOffline]) {
                notificationName = kOfflineMessageReceivedNotification;
            }
            
            if (notificationName) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil userInfo:@{@"topic":message.topic, @"payload":messageDict}];
                });
            }
        }
    }
}

- (DialogMessage *)importMessageFromDict:(NSDictionary *)messageDict inContext:(NSManagedObjectContext *)context
{
    DialogMessage *newMessage = [DialogMessage MR_importFromObject:messageDict inContext:context];
    if (newMessage.outgoing) {
        newMessage.status = @(BTWDialogMessageStatusDelivered);
    }
    NSDictionary *mediaDict = messageDict[@"media"];
    if ([mediaDict isValidDictionary] && !newMessage.mediaItem) {
        NSString *type = mediaDict[@"type"];
        if ([type isEqualToString:kBTWMessageMediaItemLocation]) {
            MessageMediaItemLocation *locationItem = [MessageMediaItemLocation MR_createInContext:context];
            locationItem.latitude = mediaDict[@"latitude"];
            locationItem.longitude = mediaDict[@"longitude"];
            newMessage.mediaItem = locationItem;
        } else if ([type isEqualToString:kBTWMessageMediaItemRoute]) {
            MessageMediaItemRoute *routeItem = [MessageMediaItemRoute MR_createInContext:context];
            routeItem.polyline = mediaDict[@"polyline"];
            newMessage.mediaItem = routeItem;
        } else if ([type isEqualToString:kBTWMessageMediaItemPhoto]) {
            MessageMediaItemPhoto *photoItem = [MessageMediaItemPhoto MR_createInContext:context];
            photoItem.full = mediaDict[@"full"];
            photoItem.thumb = mediaDict[@"thumb"];
            photoItem.thumb_2 = mediaDict[@"thumb_2"];
            photoItem.thumb_3 = mediaDict[@"thumb_3"];
            newMessage.mediaItem = photoItem;
        }
    }

    return newMessage;
}

#pragma mark - Data methods

/*
 * Загружаем список диалогов, кэшируем его, подписываемся на новые, отписываемся от удаленных
 */
- (void)updateDialogsWithCompletionHandler:(BTWErrorBlock)completionHandler
{
    if ([BTWUserService sharedInstance].authorizedUser == nil || _isLoadingDialogs) {
        return;
    }
    
    [completionHandler copy];
    self.isLoadingDialogs = YES;
    
    NSMutableSet *importedDialogsIds = [NSMutableSet set], *removedDialogsIds = [NSMutableSet set];
    BTWApiMethod *getContactsMethod = [BTWApiMethod getContactsMethod];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getContactsMethod backgroundProcessing:^(NSDictionary *response)
    {
        // Импортируем новые диалоги
        NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
        NSArray *contactsArray = response[@"payload"];
        for (NSDictionary *contactDict in contactsArray) {
            Dialog *newDialog = [Dialog MR_importFromObject:contactDict inContext:context];
            [importedDialogsIds addObject:newDialog.backend_id];
            
            // Обрабатываем последнее сообщение диалога
            NSDictionary *lastMessageDict = contactDict[@"lastMessage"];
            if ([lastMessageDict isValidDictionary]) {
                DialogMessage *lastMessage = [self importMessageFromDict:lastMessageDict inContext:context];
                NSMutableOrderedSet *mutableMessages = [NSMutableOrderedSet orderedSetWithOrderedSet:newDialog.messages];
                [mutableMessages addObject:lastMessage];
                newDialog.messages = [NSOrderedSet orderedSetWithOrderedSet:mutableMessages];
            }
        }

        // Удаляем остутствующие
        NSArray *removedDialogs = [Dialog MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"NOT (backend_id IN %@)", importedDialogsIds] inContext:context];
        for (Dialog *dialog in removedDialogs) {
            [removedDialogsIds addObject:dialog.backend_id];
            [dialog MR_deleteInContext:context];
        }
        [context MR_saveToPersistentStoreAndWait];
    }
    callback:^(NSDictionary *response, NSError *error)
    {
        self.isLoadingDialogs = NO;
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

/*
 * Загрузка отдельного диалога
 */
- (void)getDialogWithTopicId:(NSString *)topicId completionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *getDialogMethod = [BTWApiMethod getContactMethodForTopicId:topicId];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getDialogMethod backgroundProcessing:^(NSDictionary *response){
        // Импортируем новый диалог
        NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
        NSDictionary *contactDict = response[@"payload"];
        if ([contactDict isValidDictionary]) {
            Dialog *newDialog = [Dialog MR_importFromObject:contactDict inContext:context];
            
            // Обрабатываем последнее сообщение диалога
            NSDictionary *lastMessageDict = contactDict[@"lastMessage"];
            if ([lastMessageDict isValidDictionary]) {
                DialogMessage *lastMessage = [self importMessageFromDict:lastMessageDict inContext:context];
                NSMutableOrderedSet *mutableMessages = [NSMutableOrderedSet orderedSetWithOrderedSet:newDialog.messages];
                [mutableMessages addObject:lastMessage];
                newDialog.messages = [NSOrderedSet orderedSetWithOrderedSet:mutableMessages];
            }
        }
        [context MR_saveToPersistentStoreAndWait];
    } callback:^(NSDictionary *response, NSError *error){
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

/*
 * Загрузка истории сообщений целиком
 */
- (void)getHistoryForDialog:(Dialog *)dialog withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    BTWApiMethod *getHistoryMethod = [BTWApiMethod getHistoryMethodForDialog:dialog];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:getHistoryMethod backgroundProcessing:^(NSDictionary *response)
     {
         NSManagedObjectContext *context = [NSManagedObjectContext MR_context];
         
         // Парсим полученные сообщения
         NSArray *rawMessages = [response valueForKeyPath:@"payload.messages"];
         Dialog *localDialog = [Dialog MR_findFirstByAttribute:@"backend_id" withValue:dialog.backend_id inContext:context];
         NSMutableOrderedSet *newMessagesSet = [NSMutableOrderedSet new];
         for (NSDictionary *messageDict in rawMessages) {
             DialogMessage *newMessage = [self importMessageFromDict:messageDict inContext:context];
             [newMessagesSet addObject:newMessage];
         }
         
         // Мерджим с закэшированными ранее
//         NSMutableOrderedSet *allMessages = [NSMutableOrderedSet orderedSetWithOrderedSet:localDialog.messages];
//         for (DialogMessage *newMessage in [newMessagesSet reversedOrderedSet]) {
//             if ([allMessages containsObject:newMessage]) {
//                 [allMessages moveObjectsAtIndexes:[NSIndexSet indexSetWithIndex:[allMessages indexOfObject:newMessage]] toIndex:0];
//             } else {
//                 [allMessages insertObject:newMessage atIndex:0];
//             }
//         }
//         
//         localDialog.messages = [NSOrderedSet orderedSetWithOrderedSet:allMessages];
         
         localDialog.messages = [NSOrderedSet orderedSetWithOrderedSet:newMessagesSet];
         [context MR_saveToPersistentStoreAndWait];
     } callback:^(NSDictionary *response, NSError *error){
         if (completionHandler) {
             completionHandler(error);
         }
     }];
}

/*
 * Выставляем отметку о прочтении для всех входящих сообщений диалога
 */
- (void)confirmHistoryForDialog:(Dialog *)dialog
{
    if ([dialog.backend_id isValidString]) {
        // Отправляем подтверждение через MQTT
        for (DialogMessage *message in dialog.messages) {
            if (message.incoming && [message.confirmed boolValue] == NO) {
                message.confirmed = @(YES);
                [self sendConfirmationForMessage:message];
            }
        }
        
        // Подтверждаем всю историю, чтобы сбросить флаг для новых контактов
        BTWApiMethod *confirmMethod = [BTWApiMethod confirmHistoryMethodForDialog:dialog];
        [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:confirmMethod callback:nil];
        
        dialog.unconfirmed_messages_count = @(0);
        [dialog.managedObjectContext MR_saveToPersistentStoreAndWait];
    }
}

#pragma mark - Public messaging methods
- (void)sendTypingMessageToDialog:(Dialog *)dialog
{
    NSDictionary *printingDictionary = @{ @"type" : kBTWMessageTypeTyping,
                                          @"senderId" : [BTWUserService sharedInstance].authorizedUser.backend_id };
    NSData *data = [NSJSONSerialization dataWithJSONObject:printingDictionary options:0 error:nil];
    [_mqttClient publishData:data toTopic:dialog.backend_id withQos:AtMostOnce retain:NO completionHandler:nil];
}

- (void)sendMessageWithText:(NSString *)text toDialog:(Dialog *)dialog
{
    DialogMessage *newMessage = [self createNewMessageForDialog:dialog];
    newMessage.text = text;
    [newMessage.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    [self sendMessage:newMessage];
}

- (void)sendCurrentLocationToDialog:(Dialog *)dialog
{
    DialogMessage *newMessage = [self createNewMessageForDialog:dialog];
    
    CLLocationCoordinate2D coordinate = [[BTWLocationManager sharedInstance] currentLocation].coordinate;
    MessageMediaItemLocation *locationItem = [MessageMediaItemLocation MR_createInContext:newMessage.managedObjectContext];
    [locationItem setCoordinate:coordinate];
    newMessage.mediaItem = locationItem;
    [newMessage.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    [self sendMessage:newMessage];
}

- (void)sendCurrentRouteToDialog:(Dialog *)dialog
{
    DialogMessage *newMessage = [self createNewMessageForDialog:dialog];
    
    MessageMediaItemRoute *routeItem = [MessageMediaItemRoute MR_createInContext:newMessage.managedObjectContext];
    routeItem.polyline = [BTWRouteService sharedInstance].currentRoute.polyline;
    newMessage.mediaItem = routeItem;
    [newMessage.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    [self sendMessage:newMessage];
}

- (void)sendPhoto:(UIImage *)image toDialog:(Dialog *)dialog withProgressHandler:(BTWNetworkProgressBlock)progressBlock completionHandler:(BTWErrorBlock)completionHandler
{
    NSAssert(image, @"ERROR: Image is nil");
    
    [completionHandler copy];
    
    DialogMessage *newMessage = [self createNewMessageForDialog:dialog];
    
    MessageMediaItemPhoto *photoItem = [MessageMediaItemPhoto MR_createInContext:newMessage.managedObjectContext];
    photoItem.source_path = [BTWCommonUtils savePhoto:image];
    newMessage.mediaItem = photoItem;
    [newMessage.managedObjectContext MR_saveToPersistentStoreAndWait];

    __weak typeof(self) wself = self;
    [self uploadImage:image forMessage:newMessage withProgressBlock:progressBlock completionHandler:^(NSError *error){
        if (!error) {
            [wself sendMessage:newMessage];
        } else {
            newMessage.status = @(BTWDialogMessageStatusUnsent);
            [newMessage.managedObjectContext MR_saveToPersistentStoreAndWait];
        }
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

- (void)uploadImage:(UIImage *)image forMessage:(DialogMessage *)message withProgressBlock:(BTWNetworkProgressBlock)progressBlock completionHandler:(BTWErrorBlock)completionHandler
{
    NSAssert([message.mediaItem isKindOfClass:[MessageMediaItemPhoto class]], @"ERROR: Wrong media item class");
    NSAssert(image, @"ERROR: Image is nil");

    [completionHandler copy];
    [progressBlock copy];
    
    [[BTWUserService sharedInstance] reloginWithCompletionHandler:^(NSError *error){
        if (!error) {
            UIImage *fixedOrientationImage = [BTWCommonUtils fixOrientation:image];
            BTWApiMethod *uploadAttachmentMethod = [BTWApiMethod uploadPhotoAttachmentMethod:fixedOrientationImage];
            [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:uploadAttachmentMethod progressHandler:progressBlock backgroundProcessing:nil callback:^(NSDictionary *response, NSError *error)
             {
                 NSDictionary *photoDict = response[@"payload"];
                 if (!error) {
                     if ([photoDict isValidDictionary]) {
                         MessageMediaItemPhoto *photoItem = (MessageMediaItemPhoto *)message.mediaItem;
                         [[NSFileManager defaultManager] removeItemAtPath:photoItem.source_path error:nil];
                         photoItem.source_path = nil;
                         photoItem.full = photoDict[@"full"];
                         photoItem.thumb = photoDict[@"thumbSmall_250x150"];
                         photoItem.thumb_2 = photoDict[@"thumbMid_500x300"];
                         photoItem.thumb_3 = photoDict[@"thumbBig_750x450"];
                         [photoItem.managedObjectContext MR_saveToPersistentStoreAndWait];
                     } else {
                         error = [NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeUnknown userInfo:nil];
                     }
                 }
                 
                 if (completionHandler) {
                     completionHandler(error);
                 }
             }];
        } else {
            if (completionHandler) {
                completionHandler(error);
            }
        }
    }];
}

- (void)resendMessage:(DialogMessage *)message
{
    if ([message.mediaItem isKindOfClass:[MessageMediaItemPhoto class]]) {
        NSString *path = ((MessageMediaItemPhoto *)message.mediaItem).source_path;
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        if (image) {
            __weak typeof(self) wself = self;
            [self uploadImage:image forMessage:message withProgressBlock:nil completionHandler:^(NSError *error){
                [wself sendMessage:message];
            }];
        } else {
            NSManagedObjectContext *context = message.managedObjectContext;
            [message MR_deleteEntity];
            [context MR_saveToPersistentStoreAndWait];
        }
    } else {
        [self sendMessage:message];
    }
}

#pragma mark - Private messaging methods
- (DialogMessage *)createNewMessageForDialog:(Dialog *)dialog
{
    // Создаем сообщение в базе и обновляем закэшированные данные
    DialogMessage *newMessage = [DialogMessage MR_createEntity];
    newMessage.backend_id = [NSUUID UUID].UUIDString;
    newMessage.date = [NSDate date];
    newMessage.type = BTWDialogMessageTypeText;
    newMessage.sender_id = [BTWUserService sharedInstance].authorizedUser.backend_id;
    newMessage.status = @(BTWDialogMessageStatusBlank);
    
    NSMutableOrderedSet *dialogMessages = [dialog.messages mutableCopy];
    [dialogMessages addObject:newMessage];
    dialog.messages = [dialogMessages copy];
    
    return newMessage;
}

- (void)sendMessage:(DialogMessage *)message
{
    NSLog(@"*** SENDING MESSAGE: %@ %@", message, message.text);
    
    // Отправляем сообщение через MQTT-брокер
    // Задержка необходима для отражения процесса отправки в интерфейсе
    NSData *data = [NSJSONSerialization dataWithJSONObject:[message dictionaryRepresentation] options:0 error:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSTimer *failTimer = [NSTimer scheduledTimerWithTimeInterval:kMessageSendingFailTimer target:self selector:@selector(processSendingFail:) userInfo:message repeats:NO];
        [_mqttClient publishData:data toTopic:message.dialog.backend_id withQos:AtLeastOnce retain:NO completionHandler:^(int mid)
         {
             [failTimer invalidate];
             message.status = @(BTWDialogMessageStatusSent);
             [message.managedObjectContext MR_saveToPersistentStoreAndWait];
         }];
    });
}

- (void)sendOnlineMessageToDialogWithId:(NSString *)topicId
{
    NSDictionary *printingDictionary = @{ @"type" : kBTWMessageTypeOnline,
                                          @"senderId" : [BTWUserService sharedInstance].authorizedUser.backend_id };
    NSData *data = [NSJSONSerialization dataWithJSONObject:printingDictionary options:0 error:nil];
    [_mqttClient publishData:data toTopic:topicId withQos:AtMostOnce retain:NO completionHandler:nil];
}

- (void)sendOfflineMessageToDialogWithId:(NSString *)topicId withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
 
    NSDictionary *printingDictionary = @{ @"type" : kBTWMessageTypeOffline,
                                          @"senderId" : [BTWUserService sharedInstance].authorizedUser.backend_id };
    NSData *data = [NSJSONSerialization dataWithJSONObject:printingDictionary options:0 error:nil];
    [_mqttClient publishData:data toTopic:topicId withQos:AtMostOnce retain:NO completionHandler:^(int mid){
        if (completionHandler) {
            completionHandler(nil);
        }
    }];
}

- (void)sendConfirmationForMessage:(DialogMessage *)message
{
    NSDictionary *printingDictionary = @{ @"type" : kBTWMessageTypeConfirm,
                                          @"senderId" : [BTWUserService sharedInstance].authorizedUser.backend_id,
                                          @"params" : @{@"msgid" : message.backend_id} };
    NSData *data = [NSJSONSerialization dataWithJSONObject:printingDictionary options:0 error:nil];
    [_mqttClient publishData:data toTopic:message.dialog.backend_id withQos:AtMostOnce retain:NO completionHandler:nil];
}

- (void)processSendingFail:(NSTimer *)timer
{
    DialogMessage *message = [timer userInfo];
    message.status = @(BTWDialogMessageStatusUnsent);
    [message.managedObjectContext MR_saveToPersistentStoreAndWait];
}

- (void)processUnsentMessages
{
    NSArray *sendingMessages = [DialogMessage MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"sender_id = %@ AND (status = %@ OR status = NULL)", [BTWUserService sharedInstance].authorizedUser.backend_id, @(BTWDialogMessageStatusBlank)]];
    if ([sendingMessages count] > 0) {
        for (DialogMessage *message in sendingMessages) {
            message.status = @(BTWDialogMessageStatusUnsent);
        }
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    }
    
    NSMutableArray *messagesToResend = [NSMutableArray array];
    for (Dialog *dialog in [Dialog MR_findAll]) {
        NSOrderedSet *unsentMessages = [dialog.messages filteredOrderedSetUsingPredicate:[NSPredicate predicateWithFormat:@"status = %@", @(BTWDialogMessageStatusUnsent)]];
        [messagesToResend addObjectsFromArray:unsentMessages.array];
    }
    
    for (int i = 0; i < [messagesToResend count]; i++) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(i * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self resendMessage:messagesToResend[i]];
        });
    }
}

#pragma mark - Contact actions
- (void)removeDialog:(Dialog *)dialog withCompletionHandler:(BTWErrorBlock)completionHandler
{
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    BTWApiMethod *removeMethod = [BTWApiMethod removeMethodForDialog:dialog];
    [[BTWNetworkHelper sharedInstance] sendRequestWithMethod:removeMethod callback:^(id response, NSError *error){
        if (!error) {
            [wself.activeDialogsIDs removeObject:dialog.backend_id];
            [dialog MR_deleteEntity];
            [dialog.managedObjectContext MR_saveToPersistentStoreAndWait];
        }
        
        if (completionHandler) {
            completionHandler(error);
        }
    }];
}

@end
