//
//  BTWVKService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWSocialNetworkService.h"
#import "VKSdk.h"

static NSString * const kVkSessionOpenedNotification = @"VkSessionOpenedNotification";
static NSString * const kVkSessionFailedToOpenNotification = @"VkSessionFailedToOpenNotification";
static NSString * const kVkSessionClosedNotification = @"VkSessionClosedNotification";
static NSString * const kVkFriendsLoadedNotification = @"VkFriendsLoadedNotification";
static NSString * const kVkInterestsLoadedNotification = @"VkInterestsLoadedNotification";

@interface BTWVKService : NSObject <BTWSocialNetworkService>

@property (nonatomic, copy, readonly) NSString *userID;
@property (nonatomic, copy, readonly) NSString *accessToken;
@property (nonatomic, assign, readonly) BOOL isOpeningSession;
@property (nonatomic, assign, readonly) BOOL isLoadingFriends;
@property (nonatomic, assign, readonly) BOOL isLoadingInterests;
@property (nonatomic, strong, readonly) NSArray *friends;
@property (nonatomic, strong, readonly) NSArray *interests;

+ (instancetype)sharedInstance;

- (BOOL)isReady;
- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler;
- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler silent:(BOOL)silent;
- (void)closeSession;

- (void)updateFriendsAndInterests;

@end
