//
//  BTWFacebookService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWSocialNetworkService.h"
#import <FacebookSDK/FacebookSDK.h>

static NSString * const kFacebookSessionOpenedNotification = @"kFacebookSessionOpenedNotification";
static NSString * const kFacebookSessionFailedToOpenNotification = @"kFacebookSessionFailedToOpenNotification";
static NSString * const kFacebookSessionClosedNotification = @"kFacebookSessionClosedNotification";
static NSString * const kFacebookFriendsLoadedNotification = @"kFacebookFriendsLoadedNotification";
static NSString * const kFacebookInterestsLoadedNotification = @"kFacebookInterestsLoadedNotification";

@interface BTWFacebookService : NSObject <BTWSocialNetworkService>

@property (nonatomic, strong, readonly) NSArray *friends;
@property (nonatomic, strong, readonly) NSArray *interests;
@property (nonatomic, assign, readonly) BOOL isOpeningSession;
@property (nonatomic, assign, readonly) BOOL isLoadingFriends;
@property (nonatomic, assign, readonly) BOOL isLoadingInterests;

+ (BTWFacebookService *)sharedInstance;

- (BOOL)isReady;
- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler;
- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler silent:(BOOL)silent;
- (void)closeSession;

- (NSString *)userID;
- (NSString *)accessToken;

- (void)updateFriendsAndInterests;
- (NSURL *)imageURLForGraphObject:(FBGraphObject *)object;
- (NSString *)titleForGraphObject:(FBGraphObject *)object;

- (void)getMutualFriendsForUser:(NSString *)userID withCompletionHandler:(BTWNetworkResponseBlock)completionHandler;
- (void)getMutualInterestsForUser:(NSString *)userId withCompletionHandler:(BTWNetworkResponseBlock)completionHandler;

@end
