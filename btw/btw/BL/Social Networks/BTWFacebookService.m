//
//  BTWFacebookService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 12.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWFacebookService.h"
#import "BTWUserService.h"

@interface BTWFacebookService ()
@property (nonatomic, strong) NSDictionary <FBGraphUser> *me;
@property (nonatomic, assign, readwrite) BOOL isOpeningSession;
@property (nonatomic, assign, readwrite) BOOL isLoadingFriends;
@property (nonatomic, assign, readwrite) BOOL isLoadingInterests;
@property (nonatomic, strong, readwrite) NSArray *friends;
@property (nonatomic, strong, readwrite) NSArray *interests;
@end

@implementation BTWFacebookService

#pragma mark - Initialization
static BTWFacebookService *_sharedInstance = nil;
+ (BTWFacebookService *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWFacebookService new];
    });
    return _sharedInstance;
}

#pragma mark - Getters
- (NSString *)userID
{
    NSString *result = nil;
    if ([self isReady]) {
        result = _me.objectID;
    }
    return result;
}

- (NSString *)accessToken
{
    NSString *result = nil;
    if ([self isReady]) {
        result = FBSession.activeSession.accessTokenData.accessToken;
    }
    return result;
}

#pragma mark - Setup
- (BOOL)isReady
{
    BOOL result = self.me && (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended);
    return result;
}

- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler
{
    [self setupWithCompletionHandler:completionHandler silent:NO];
}

- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler silent:(BOOL)silent
{
    self.isOpeningSession = YES;
    [completionHandler copy];
    
    __weak typeof(self) wself = self;
    BTWDataBlock internalCompletionBlock = [^(NSError *error){
        wself.isOpeningSession = NO;
        if (error) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookSessionFailedToOpenNotification object:nil];
        }
        if (completionHandler) {
            completionHandler(error ? NO : YES, error);
        }
    } copy];
    
    BTWVoidBlock requestMeBlock = ^{
        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error){
            if (!error) {
                NSString *userID = user.objectID;
                NSString *currentFacebookID = [BTWUserService sharedInstance].authorizedUser.facebook_id;
                if ([currentFacebookID isValidString] && ![currentFacebookID isEqualToString:userID])
                {
                    [FBSession.activeSession closeAndClearTokenInformation];
                    error = [NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeWrongFacebookID userInfo:nil];
                } else {
                    wself.me = user;
                    [wself sessionStateChanged:FBSession.activeSession state:FBSession.activeSession.state error:error];
                }
            }
            internalCompletionBlock(error);
        }];
    };
    
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        if (self.me) {
            internalCompletionBlock(nil);
        } else {
            requestMeBlock();
        }
        return;
    }
    
    if (!silent || FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"user_friends", @"user_likes", @"user_birthday"] allowLoginUI:!silent completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error)
        {
            if (state != FBSessionStateClosed) {
                BOOL opened = (!error && state == FBSessionStateOpen);
                if (opened) {
                    requestMeBlock();
                } else {
                    internalCompletionBlock(nil);
                }
            }
        }];
    } else {
        internalCompletionBlock([NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeEmptyFacebookSession userInfo:nil]);
    }
}

- (void)closeSession
{
    if ([self isReady]) {
        self.me = nil;
        self.friends = nil;
        self.interests = nil;
        [FBSession.activeSession closeAndClearTokenInformation];
        [self sessionStateChanged:FBSession.activeSession state:FBSessionStateClosed error:nil];
    }
}

- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    if (!error && state == FBSessionStateOpen){
        [self userLoggedIn];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        [self userLoggedOut];
    }
    
    if (error){
        [FBSession.activeSession closeAndClearTokenInformation];
        [self userLoggedOut];
    }
}

- (void)userLoggedIn
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookSessionOpenedNotification object:nil];
}

- (void)userLoggedOut
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookSessionClosedNotification object:nil];
}

#pragma mark - Friends
- (void)updateFriendsAndInterests
{
    [self updateFriends];
    [self updateInterests];
}

- (void)updateInterests
{
    self.isLoadingInterests = YES;
    [FBRequestConnection startWithGraphPath:@"/me/likes" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        self.isLoadingInterests = NO;
        if(self.isReady && !error) {
            self.interests = [result objectForKey:@"data"];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookInterestsLoadedNotification object:nil];
    }];
}

- (void)updateFriends
{
    self.isLoadingFriends = YES;
    
    BTWDataBlock successBlock = [^(NSArray *friends){
        self.isLoadingFriends = NO;
        if ([self isReady]) {
            self.friends = friends;
            [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookFriendsLoadedNotification object:nil];
        }
    } copy];
    
    BTWErrorBlock errorBlock = [^(NSError *error){
        self.isLoadingFriends = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kFacebookFriendsLoadedNotification object:nil];
    } copy];
    
    [[BTWUserService sharedInstance] getFriendsFromSocialNetwork:BTWLinkedSocialNetworkFacebook forUserID:[BTWUserService sharedInstance].authorizedUser.backend_id completionHandler:^(NSArray *friends, NSError *error){
        if (!error) {
            if ([friends count] > 0) {
                successBlock(friends);
            } else {
                successBlock(nil);
            }
        } else {
            errorBlock(error);
        }
    }];
}

- (void)getMutualFriendsForUser:(NSString *)userID withCompletionHandler:(BTWNetworkResponseBlock)completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        [[BTWUserService sharedInstance] getFriendsFromSocialNetwork:BTWLinkedSocialNetworkFacebook forUserID:userID completionHandler:^(NSArray *userFriends, NSError *error){
            if (!error) {
                if ([userFriends count] > 0) {
                    NSMutableArray *mutualFriends = [NSMutableArray array];
                    for (NSDictionary *friend in _friends) {
                        if ([userFriends containsObject:friend[@"id"]]) {
                            [mutualFriends addObject:friend];
                        }
                    }
                    completionHandler([NSArray arrayWithArray:mutualFriends], nil);
                } else {
                    completionHandler(nil, nil);
                }
            } else {
                completionHandler(nil, error);
            }
        }];
    }
}

- (void)getMutualInterestsForUser:(NSString *)userId withCompletionHandler:(BTWNetworkResponseBlock)completionHandler
{
    [completionHandler copy];
    
    FBRequest *interestsRequest = [FBRequest requestForGraphPath:[NSString stringWithFormat:@"%@/likes", userId]];
    [interestsRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         NSArray *mutualInterests = nil;
         if (!error) {
             mutualInterests = result[@"data"];
             NSArray *myInterestsIDs = [_interests valueForKey:@"id"];
             mutualInterests = [mutualInterests filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id IN %@", myInterestsIDs]];
         }
         
         if (completionHandler) {
             completionHandler(mutualInterests, error);
         }
     }];
}

- (NSURL *)imageURLForGraphObject:(FBGraphObject *)object
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=100&height=100", [object valueForKey:@"id"]]];
}

- (NSString *)titleForGraphObject:(FBGraphObject *)object
{
    return [object valueForKey:@"name"];
}

@end
