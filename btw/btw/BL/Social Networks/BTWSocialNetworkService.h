//
//  BTWSocialNetworkService.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

typedef void(^BTWSocialNetworkCompletionBlock)(BOOL result, NSError *error);

@protocol BTWSocialNetworkService <NSObject>

- (BOOL)isOpeningSession;
- (BOOL)isReady;
- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler;
- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler silent:(BOOL)silent;
- (void)closeSession;

- (NSString *)userID;
- (NSString *)accessToken;

- (BOOL)isLoadingFriends;
- (BOOL)isLoadingInterests;
- (void)updateFriendsAndInterests;
- (NSArray *)friends;
- (NSArray *)interests;

- (void)getMutualFriendsForUser:(NSString *)userID withCompletionHandler:(BTWNetworkResponseBlock)completionHandler;
- (void)getMutualInterestsForUser:(NSString *)userId withCompletionHandler:(BTWNetworkResponseBlock)completionHandler;

@end