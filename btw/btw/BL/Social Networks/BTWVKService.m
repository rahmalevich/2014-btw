//
//  BTWVKService.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.04.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWVKService.h"
#import "BTWUserService.h"
#import "BTWTransitionsManager.h"

@interface BTWVKService () <VKSdkDelegate>
@property (nonatomic, copy) BTWSocialNetworkCompletionBlock completionHandler;
@property (nonatomic, copy, readwrite) NSString *userID;
@property (nonatomic, copy, readwrite) NSString *accessToken;
@property (nonatomic, assign, readwrite) BOOL isOpeningSession;
@property (nonatomic, assign, readwrite) BOOL isLoadingFriends;
@property (nonatomic, assign, readwrite) BOOL isLoadingInterests;
@property (nonatomic, strong, readwrite) NSArray *friends;
@property (nonatomic, strong, readwrite) NSArray *interests;
@end

@implementation BTWVKService

#pragma mark - Initialization
static BTWVKService *_sharedInstance = nil;
+ (instancetype)sharedInstance;
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWVKService new];
    });
    return _sharedInstance;
}

- (BOOL)isReady
{
    return [VKSdk isLoggedIn];
}

- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler
{
    [self setupWithCompletionHandler:completionHandler silent:NO];
}

- (void)setupWithCompletionHandler:(BTWSocialNetworkCompletionBlock)completionHandler silent:(BOOL)silent
{
    if (_isOpeningSession) {
        return;
    }
    
    self.completionHandler = completionHandler;
    [VKSdk initializeWithDelegate:self andAppId:kVKAppID];
    if ([VKSdk wakeUpSession]) {
        [self handleOpenedSessionWithToken:[VKSdk getAccessToken]];
    } else if (!silent) {
        self.isOpeningSession = YES;
        [VKSdk authorize:@[@"friends", @"groups"]];
    } else {
        [self handleSessionOpeningError:[NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeEmptyVkSession userInfo:nil]];
    }
}

- (void)handleOpenedSessionWithToken:(VKAccessToken *)token
{
    self.isOpeningSession = NO;
    
    NSError *error = nil;
    NSString *currentVkID = [BTWUserService sharedInstance].authorizedUser.vkontakte_id;
    if ([currentVkID isValidString] && ![currentVkID isEqualToString:token.userId]) {
        [VKSdk forceLogout];
        error = [NSError errorWithDomain:kBTWAPIErrorDomain code:BTWInternalErrorCodeWrongVkID userInfo:nil];
    } else {
        self.userID = token.userId;
        self.accessToken = token.accessToken;
    }
    
    if (!error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kVkSessionOpenedNotification object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kVkSessionFailedToOpenNotification object:nil];
    }
    if (_completionHandler) {
        _completionHandler((error ? NO : YES), error);
        self.completionHandler = nil;
    }
}

- (void)handleSessionOpeningError:(NSError *)error
{
    self.isOpeningSession = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:kVkSessionFailedToOpenNotification object:nil];
    if (_completionHandler) {
        _completionHandler(NO, error);
    }
}

- (void)closeSession
{
    [VKSdk forceLogout];
    [[NSNotificationCenter defaultCenter] postNotificationName:kVkSessionClosedNotification object:nil];
}

#pragma mark - VKSdk delegate
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    VKCaptchaViewController *captchaController = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [[BTWTransitionsManager sharedInstance] showModalViewController:captchaController];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    // TODO: handle?
}

- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    [self handleSessionOpeningError:authorizationError.httpError];
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [[BTWTransitionsManager sharedInstance] showModalViewController:controller];
}

- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    [self handleOpenedSessionWithToken:newToken];
}

#pragma mark - Public methods
- (void)updateFriendsAndInterests
{
    [self updateFriends];
    [self updateInterests];
}

- (void)updateInterests
{
    self.isLoadingInterests = YES;
    NSDictionary *parameters = @{ @"extended" : @1,
                                  @"fields" : @"photo_100",
                                  @"count" : @100 };
    VKRequest *request = [VKRequest requestWithMethod:@"groups.get" andParameters:parameters andHttpMethod:@"GET" classOfModel:[VKGroup class]];
    [request executeWithResultBlock:^(VKResponse *response){
        self.isLoadingInterests = NO;
        if ([self isReady]) {
            self.interests = response.json[@"items"];
            [[NSNotificationCenter defaultCenter] postNotificationName:kVkInterestsLoadedNotification object:nil];
        }
    } errorBlock:^(NSError *error){
        self.isLoadingInterests = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kVkInterestsLoadedNotification object:nil];
    }];
}

- (void)updateFriends
{
    self.isLoadingFriends = YES;
    
    BTWDataBlock successBlock = [^(NSArray *friends){
        self.isLoadingFriends = NO;
        if ([self isReady]) {
            self.friends = friends;
            [[NSNotificationCenter defaultCenter] postNotificationName:kVkFriendsLoadedNotification object:nil];
        }
    } copy];
    
    BTWErrorBlock errorBlock = [^(NSError *error){
        self.isLoadingFriends = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kVkFriendsLoadedNotification object:nil];
    } copy];
    
    [[BTWUserService sharedInstance] getFriendsFromSocialNetwork:BTWLinkedSocialNetworkVk forUserID:[BTWUserService sharedInstance].authorizedUser.backend_id completionHandler:^(NSArray *friends, NSError *error){
        if (!error) {
            if ([friends count] > 0) {
                successBlock(friends);
            } else {
                successBlock(nil);
            }
        } else {
            errorBlock(error);
        }
    }];
}

- (void)getMutualFriendsForUser:(NSString *)userID withCompletionHandler:(BTWNetworkResponseBlock)completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
    
        [[BTWUserService sharedInstance] getFriendsFromSocialNetwork:BTWLinkedSocialNetworkVk forUserID:userID completionHandler:^(NSArray *userFriends, NSError *error){
            if (!error) {
                if ([userFriends count] > 0) {
                    NSMutableArray *mutualFriends = [NSMutableArray array];
                    for (NSDictionary *friend in _friends) {
                        if ([userFriends containsObject:friend[@"id"]]) {
                            [mutualFriends addObject:friend];
                        }
                    }
                    completionHandler([NSArray arrayWithArray:mutualFriends], nil);
                } else {
                    completionHandler(nil, nil);
                }
            } else {
                completionHandler(nil, error);
            }
        }];
    }
}

- (void)getMutualInterestsForUser:(NSString *)userId withCompletionHandler:(BTWNetworkResponseBlock)completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        NSDictionary *parameters = @{ @"user_id" : userId };
        VKRequest *request = [VKRequest requestWithMethod:@"groups.get" andParameters:parameters andHttpMethod:@"GET" classOfModel:[VKGroup class]];
        [request executeWithResultBlock:^(VKResponse *response){
            NSArray *mutualInterestsIDs = response.json[@"items"];
            NSArray *mutualInterests = [_interests filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id IN %@", mutualInterestsIDs]];
            completionHandler(mutualInterests, nil);
        } errorBlock:^(NSError *error){
            completionHandler(nil, error);
        }];
    }
}

@end
