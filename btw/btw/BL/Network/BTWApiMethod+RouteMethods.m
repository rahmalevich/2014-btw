//
//  BTWApiMethod+RouteMethods.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod+RouteMethods.h"

#import "BTWFormattingService.h"
#import "BTWSettingsService.h"

static CGFloat kMaxDistance = 30;

@implementation BTWApiMethod (RouteMethods)

+ (BTWApiMethod *)startMethodForRoute:(Route *)route;
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    
    NSString *role = [BTWSettingsService settingsValueForKey:kCommonRoleKey];
    NSNumber *gender, *minAge, *maxAge, *rating;
    if ([role isEqualToString:kUserRolePassenger]) {
        gender = [BTWSettingsService settingsValueForKey:kFilterDriverGenderKey];
        minAge = [BTWSettingsService settingsValueForKey:kFilterDriverLowerAgeKey];
        maxAge = [BTWSettingsService settingsValueForKey:kFilterDriverUpperAgeKey];
        rating = [BTWSettingsService settingsValueForKey:kFilterDriverRatingKey];
    } else {
        gender = @(BTWUserGenderBlank);
        minAge = @(0);
        maxAge = @(100);
        rating = @(0);
    }
    
    payload[@"role"] = [BTWSettingsService settingsValueForKey:kCommonRoleKey];
    payload[@"departureTime"] = [[BTWFormattingService dateTimeFormatterWithTimezone] stringFromDate:route.date];
    payload[@"polyline"] = route.polyline;
    payload[@"gender"] = gender;
    payload[@"minAge"] = minAge;
    payload[@"maxAge"] = maxAge;
    payload[@"distance"] = @(kMaxDistance);
    payload[@"smokingAllowed"] = @([[BTWSettingsService settingsValueForKey:kFilterSmokingAllowedKey] integerValue]);
    payload[@"startAddress"] = route.startPointAddress;
    payload[@"finishAddress"] = route.endPointAddress;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"drive-search/start" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)searchResultsMethod
{
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"drive-search/result" params:nil];
    return resultMethod;
}

+ (BTWApiMethod *)endMethod
{
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"drive-search/end" params:nil];
    return resultMethod;
}

+ (BTWApiMethod *)makeOfferMethodForUser:(RouteUser *)routeUser
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"recepient"] = routeUser.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"drive-search/make-offer" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getOfferMethodForId:(NSString *)offerId
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = offerId;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"drive-search/get-offer" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)markOfferMethodAsAccepted:(BOOL)accepted forId:(NSString *)offerId
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"offerId"] = offerId;
    payload[@"accepted"] = @(accepted);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"drive-search/mark-offer" params:parameters];
    return resultMethod;
}

@end
