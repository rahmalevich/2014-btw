//
//  BTWApiMethod+CommonMethods.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod+CommonMethods.h"

@implementation BTWApiMethod (CommonMethods)

+ (BTWApiMethod *)pingMethod
{
    BTWApiMethod *pingMethod = [self methodWithHTTPMethod:@"POST" URLString:@"ping" params:nil];
    return pingMethod;
}

@end
