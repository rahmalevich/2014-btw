//
//  BTWApiMethod+Feedbacks.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod.h"
#import "BTWFeedbacksService.h"

@interface BTWApiMethod (Feedbacks)

+ (BTWApiMethod *)getFeedbacksMethodForUser:(NSString *)userID;
+ (BTWApiMethod *)addFeedbackMethodForEvent:(NSNumber *)eventID mark:(CGFloat)mark reason:(BTWFeedbackReason)reason comment:(NSString *)comment;

@end
