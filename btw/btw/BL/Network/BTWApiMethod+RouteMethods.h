//
//  BTWApiMethod+RouteMethods.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 26.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (RouteMethods)

+ (BTWApiMethod *)startMethodForRoute:(Route *)route;
+ (BTWApiMethod *)searchResultsMethod;
+ (BTWApiMethod *)endMethod;
+ (BTWApiMethod *)makeOfferMethodForUser:(RouteUser *)routeUser;
+ (BTWApiMethod *)getOfferMethodForId:(NSString *)offerId;
+ (BTWApiMethod *)markOfferMethodAsAccepted:(BOOL)accepted forId:(NSString *)offerId;

@end
