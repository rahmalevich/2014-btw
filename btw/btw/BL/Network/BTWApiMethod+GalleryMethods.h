//
//  BTWApiMethod+GalleryMethods.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (GalleryMethods)

+ (BTWApiMethod *)addMethodForImage:(UIImage *)image;
+ (BTWApiMethod *)addMethodForImage:(UIImage *)image asAvatar:(BOOL)asAvatar;
+ (BTWApiMethod *)removeMethodForGalleryImage:(GalleryImage *)galleryImage;
+ (BTWApiMethod *)listMethodForUser:(User *)user;
+ (BTWApiMethod *)setAvatarMethodForImage:(GalleryImage *)galleryImage;
+ (BTWApiMethod *)comlaintMethodForImage:(GalleryImage *)galleryImage;

@end
