//
//  BTWApiMethod.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod.h"
#import "BTWUserService.h"
#import "BTWLocationManager.h"

#import "NSString+MD5.h"

@interface BTWApiMethod ()
@property (nonatomic, strong, readwrite) NSString *httpMethod;
@property (nonatomic, strong, readwrite) NSString *urlString;
@property (nonatomic, strong, readwrite) NSDictionary *params;
@end

@implementation BTWApiMethod

#pragma mark - Initialization
+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params
{
    return [self methodWithHTTPMethod:method URLString:urlString params:params addBaseParams:YES];
}

+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params addBaseParams:(BOOL)addBaseParams
{
    BTWApiMethod *newMethod = [BTWApiMethod new];
    newMethod.httpMethod = method;
    newMethod.urlString = addBaseParams ? [NSString stringWithFormat:@"%@/%@", kBaseURL, urlString] : urlString;
    
    if (addBaseParams) {
        NSMutableDictionary *mutableParameters = [NSMutableDictionary dictionaryWithDictionary:params];
        mutableParameters[@"appVersion"] = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
        
        if (!mutableParameters[@"payload"]) {
            mutableParameters[@"payload"] = [NSNull null];
        }
        
        [newMethod addSignatureToParamsDict:mutableParameters];
        newMethod.params = [NSDictionary dictionaryWithDictionary:mutableParameters];
    } else {
        newMethod.params = params;
    }

    return newMethod;
}

#pragma mark - Signature
- (void)addSignatureToParamsDict:(NSMutableDictionary *)params
{
    BTWUserService *userService = [BTWUserService sharedInstance];
    if (userService.isLoggedIn) {
        NSInteger time = [[NSDate date] timeIntervalSince1970];
        params[@"userId"] = userService.authorizedUser.backend_id;
        params[@"time"] = @(time);
        params[@"signature"] = [[NSString stringWithFormat:@"%@%ld%@", userService.authorizedUser.backend_id, (long)time, userService.sessionKey] MD5Digest];
        
        CLLocationCoordinate2D coordinate = [BTWLocationManager sharedInstance].currentLocation.coordinate;
        params[@"lat"] = @(coordinate.latitude);
        params[@"lon"] = @(coordinate.longitude);
    }
}

- (void)updateSignature
{
    NSMutableDictionary *mutableParams = [_params mutableCopy];
    [self addSignatureToParamsDict:mutableParams];
    self.params = [NSDictionary dictionaryWithDictionary:mutableParams];
}

@end
