//
//  BTWApiMethod+Contacts.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (Contacts)

+ (BTWApiMethod *)getContactsMethod;
+ (BTWApiMethod *)getContactMethodForTopicId:(NSString *)topicId;
+ (BTWApiMethod *)getHistoryMethodForDialog:(Dialog *)dialog;
+ (BTWApiMethod *)getHistoryMethodForDialog:(Dialog *)dialog offset:(NSUInteger)offset count:(NSUInteger)count;
+ (BTWApiMethod *)uploadPhotoAttachmentMethod:(UIImage *)image;
+ (BTWApiMethod *)removeMethodForDialog:(Dialog *)dialog;
+ (BTWApiMethod *)confirmHistoryMethodForDialog:(Dialog *)dialog;

@end
