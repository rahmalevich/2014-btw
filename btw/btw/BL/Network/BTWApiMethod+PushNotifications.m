//
//  BTWApiMethod+Notifications.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod+PushNotifications.h"

@implementation BTWApiMethod (PushNotifications)

+ (BTWApiMethod *)subscribeMethodForToken:(NSString *)token
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"deviceToken"] = token;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"ios-notifications/subscribe" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)unsubscribeMethodForToken:(NSString *)token
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"deviceToken"] = token;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"ios-notifications/unsubscribe" params:parameters];
    return resultMethod;
}

@end
