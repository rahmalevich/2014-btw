//
//  BTWApiMethod+Events.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod+Events.h"

@implementation BTWApiMethod (Events)

+ (BTWApiMethod *)eventsListMethod
{
    return [self eventsListMethodWithOffset:-1 count:0 state:nil];
}

+ (BTWApiMethod *)eventsListMethodWithOffset:(NSInteger)offset count:(NSInteger)count state:(NSString *)state
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    if (offset >= 0) {
        payload[@"offset"] = @(offset);
        payload[@"count"] = @(count);
    }
    if ([state isValidString]) {
        payload[@"state"] = state;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"events/list" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)updateMethodForEvent:(NSNumber *)eventID toState:(NSString *)state
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = eventID;
    payload[@"status"] = state;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"events/update" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)confirmEventsMethodForStates:(NSArray *)statesArray
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = statesArray;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"events/confirm" params:parameters];
    return resultMethod;
}

@end
