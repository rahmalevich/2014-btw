//
//  BTWApiMethod+GoogleServices.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod+GoogleServices.h"
#import "GoogleMaps.h"

static NSString * const kGoogleDirectionsURL = @"https://maps.googleapis.com/maps/api/directions/json";
static NSString * const kGoogleStaticMapsURL = @"http://maps.googleapis.com/maps/api/staticmap";

static NSString * const kFinishPinURL_2 = @"http://54.165.207.34/images/pins/pin_finish_g.png";
static NSString * const kLocationPinURL_2 = @"http://54.165.207.34/images/pins/pin_location_g.png";

@implementation BTWApiMethod (GoogleServices)

+ (BTWApiMethod *)directionMethodForRoute:(Route *)route
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"origin"] = [NSString stringWithFormat:@"%@,%@", route.startPointLatitude, route.startPointLongitude];
    parameters[@"destination"] = [NSString stringWithFormat:@"%@,%@", route.endPointLatitude, route.endPointLongitude];
    parameters[@"key"] = kGoogleMapsAppID;
    parameters[@"sensor"] = @"false";
    
    return [self methodWithHTTPMethod:@"GET" URLString:kGoogleDirectionsURL params:parameters addBaseParams:NO];
}

+ (BTWApiMethod *)staticMapMethodForCoordinate:(CLLocationCoordinate2D)coordinate
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    CGFloat scale = [UIScreen mainScreen].scale;
    parameters[@"size"] = [NSString stringWithFormat:@"%dx%d", (int)(kMediaViewDisplaySize.width * scale), (int)(kMediaViewDisplaySize.height * scale)];
    
    NSString *locationPinURL = kLocationPinURL_2;
    parameters[@"markers"] = [NSString stringWithFormat:@"icon:%@|shadow:true|%f,%f", locationPinURL, coordinate.latitude, coordinate.longitude];
    
    return [self methodWithHTTPMethod:@"GET" URLString:kGoogleStaticMapsURL params:parameters addBaseParams:NO];
}

+ (BTWApiMethod *)staticMapMethodForPolyline:(NSString *)polyline
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    CGFloat scale = [UIScreen mainScreen].scale;
    parameters[@"size"] = [NSString stringWithFormat:@"%dx%d", (int)(kMediaViewDisplaySize.width * scale), (int)(kMediaViewDisplaySize.height * scale)];
    
    NSString *startPinURL, *finishPinURL;
    startPinURL = kLocationPinURL_2;
    finishPinURL = kFinishPinURL_2;

    GMSPath *path = [GMSPath pathFromEncodedPath:polyline];
    CLLocationCoordinate2D startCoordinate = [path coordinateAtIndex:0];
    CLLocationCoordinate2D finishCoordinate = [path coordinateAtIndex:[path count] - 1];
    NSString *startMarkerString = [NSString stringWithFormat:@"icon:%@|%f,%f", startPinURL, startCoordinate.latitude, startCoordinate.longitude];
    NSString *finishMarkerString = [NSString stringWithFormat:@"icon:%@|%f,%f", finishPinURL, finishCoordinate.latitude, finishCoordinate.longitude];
    parameters[@"markers"] = [NSSet setWithObjects:startMarkerString, finishMarkerString, nil];
    parameters[@"path"] = [NSString stringWithFormat:@"weight:4|color:%@ff|enc:%@", [UIColor hexFromColor:[BTWStylesheet routeColor]], polyline];
    
    return [self methodWithHTTPMethod:@"GET" URLString:kGoogleStaticMapsURL params:parameters addBaseParams:NO];
}

@end
