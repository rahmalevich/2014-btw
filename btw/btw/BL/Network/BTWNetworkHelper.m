//
//  BTWNetworkHelper.m
//  BTW
//
//  Created by Mikhail Rakhmalevich on 30.07.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWNetworkHelper.h"
#import "BTWApiMethod.h"
#import "BTWFormattingService.h"
#import "BTWUserService.h"

#import "AFNetworking.h"
#import "NSString+MD5.h"

@interface BTWNetworkHelper ()
@property (nonatomic, strong) AFHTTPRequestOperationManager *manager;
@end

@implementation BTWNetworkHelper

#pragma mark - Initialization
static BTWNetworkHelper *_sharedInstance = nil;
+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWNetworkHelper new];
        _sharedInstance.manager = [[AFHTTPRequestOperationManager alloc] init];
        _sharedInstance.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        ((AFJSONResponseSerializer *)_sharedInstance.manager.responseSerializer).readingOptions = NSJSONReadingAllowFragments;
    });
    return _sharedInstance;
}

#pragma mark - Sending requests
- (NSError *)APIErrorFromResponse:(NSDictionary *)response
{
    NSError *resultError = nil;    
    NSInteger responseCode = [response[@"code"] integerValue];
    if (![response isValidDictionary] || responseCode > 0) {
        BTWApiErrorCode errorCode = responseCode > 0 ? responseCode : BTWApiErrorCodeUnknown;
        resultError = [NSError errorWithDomain:kBTWAPIErrorDomain code:errorCode userInfo:nil];
    }
    return resultError;
}

- (NSURLRequest *)requestWithMethod:(BTWApiMethod *)method
{
    NSURLRequest *request = [_manager.requestSerializer requestWithMethod:method.httpMethod URLString:method.urlString parameters:method.params error:nil];
    return request;
}

- (NSOperation *)operationWithMethod:(BTWApiMethod *)method callback:(BTWNetworkResponseBlock)callback
{
    return [self operationWithMethod:method progressHandler:nil backgroundProcessing:nil callback:callback];
}

- (NSOperation *)operationWithMethod:(BTWApiMethod *)method backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback
{
    return [self operationWithMethod:method progressHandler:nil backgroundProcessing:backgroundProcessingBlock callback:callback];
}

- (NSOperation *)operationWithMethod:(BTWApiMethod *)method progressHandler:(BTWNetworkProgressBlock)progressBlock backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback
{
    [progressBlock copy];
    [backgroundProcessingBlock copy];
    [callback copy];
    
    typeof(self) __weak wself = self;
    BTWNetworkResponseBlock internalCallback = ^(id response, NSError *error){
        if (!error) {
            NSError *apiError = [response isKindOfClass:[NSDictionary class]] ? [wself APIErrorFromResponse:response] : nil;
            if (apiError) { // проверяем наличие ошибки API в ответе
                NSLog(@"*** REQUEST %@ FAILED WITH API ERROR \n%@ %@", method.urlString, error, response);
                if (apiError.code == BTWApiErrorCodeSessionNotFound || apiError.code == BTWApiErrorCodeAuthentificationFailed) { // пытаемся сделать релогин
                    [[BTWUserService sharedInstance] reloginWithCompletionHandler:^(NSError *error){
                        if (!error) {
                            [method updateSignature];
                            [wself sendRequestWithMethod:method progressHandler:progressBlock backgroundProcessing:backgroundProcessingBlock callback:callback];
                        } else if (callback) {
                            callback(response, error);
                        }
                    }];
                } else if (callback) {
                    callback(response, apiError);
                }
            } else if (backgroundProcessingBlock) { // если ошибок нет, обрабатываем ответ в бэкграунде
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    backgroundProcessingBlock(response);
                    if (callback) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            callback(response, error);
                        });
                    }
                });
            } else { // ... или вызваем коллбэк, если обработка не нужна
                if (callback) {
                    callback(response, error);
                }
            }
        } else {
            if (callback) {
                callback(response, error);
            }
        }
    };
    
    NSURLRequest *request = [self requestWithMethod:method];
    AFHTTPRequestOperation *operation = [_manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        internalCallback(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"*** REQUEST %@ FAILED WITH SERVER ERROR \n%@", [request.URL absoluteString], error);
        internalCallback(nil, error);
    }];
    
    if (progressBlock) {
        [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite){
            progressBlock(BTWNetworkProgressTypeUpload, (CGFloat)totalBytesWritten/totalBytesExpectedToWrite);
        }];
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead){
            progressBlock(BTWNetworkProgressTypeDownload, (CGFloat)bytesRead/totalBytesRead);
        }];
    }
    
    return operation;
}

- (void)enqueueOperation:(NSOperation *)operation
{
    AFHTTPRequestOperation *httpOperation = (AFHTTPRequestOperation *)operation;
    NSURLRequest *request = httpOperation.request;
    NSString *bodyString = request.HTTPBody ? [NSJSONSerialization JSONObjectWithData:request.HTTPBody options:NSJSONReadingAllowFragments error:nil] : @"";
    NSLog(@"*** SENDING REQUEST %@\n%@", [request.URL absoluteString], bodyString);
    
    [_manager.operationQueue addOperation:operation];
}

- (void)sendRequestWithMethod:(BTWApiMethod *)method callback:(BTWNetworkResponseBlock)callback
{
    [self sendRequestWithMethod:method progressHandler:nil backgroundProcessing:nil callback:callback];
}

- (void)sendRequestWithMethod:(BTWApiMethod *)method backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback
{
    [self sendRequestWithMethod:method progressHandler:nil backgroundProcessing:backgroundProcessingBlock callback:callback];
}

- (void)sendRequestWithMethod:(BTWApiMethod *)method progressHandler:(BTWNetworkProgressBlock)progressBlock backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback
{
    [self sendRequestWithMethod:method responseType:BTWNetworkResponseTypeBlank progressHandler:progressBlock backgroundProcessing:backgroundProcessingBlock callback:callback];
}

- (void)sendRequestWithMethod:(BTWApiMethod *)method responseType:(BTWNetworkResponseType)responseType progressHandler:(BTWNetworkProgressBlock)progressBlock backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback
{
    AFHTTPRequestOperation *operation = (AFHTTPRequestOperation *)[self operationWithMethod:method progressHandler:progressBlock backgroundProcessing:backgroundProcessingBlock callback:callback];
    
    if (responseType != BTWNetworkResponseTypeBlank) {
        AFHTTPResponseSerializer *serializer = nil;
        if (responseType == BTWNetworkResponseTypeText) {
            serializer = [AFHTTPResponseSerializer new];
            serializer.acceptableContentTypes = [NSSet setWithObject:@"text/plain"];
        } else if (responseType == BTWNetworkResponseTypeJSON) {
            serializer = [AFJSONResponseSerializer new];
        } else if (responseType == BTWNetworkResponseTypeImage) {
            serializer = [AFImageResponseSerializer new];
            ((AFImageResponseSerializer *)serializer).imageScale = [UIScreen mainScreen].scale;
        }
        operation.responseSerializer = serializer;
    }
    
    [self enqueueOperation:operation];
}

#pragma mark - Image downloading
- (void)getImageWithMethod:(BTWApiMethod *)method callback:(BTWNetworkResponseBlock)callback
{
    AFHTTPRequestOperation *operation = (AFHTTPRequestOperation *)[self operationWithMethod:method progressHandler:nil backgroundProcessing:nil callback:callback];
    
    AFImageResponseSerializer *responseSerializer = [AFImageResponseSerializer new];
    responseSerializer.imageScale = [UIScreen mainScreen].scale;
    operation.responseSerializer = responseSerializer;
    
    [self enqueueOperation:operation];
}

@end
