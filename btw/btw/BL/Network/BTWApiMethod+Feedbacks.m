//
//  BTWApiMethod+Feedbacks.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 29.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod+Feedbacks.h"

@implementation BTWApiMethod (Feedbacks)

+ (BTWApiMethod *)getFeedbacksMethodForUser:(NSString *)userID
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = userID;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"responses/list" params:parameters];
    return resultMethod;
    
}

+ (BTWApiMethod *)addFeedbackMethodForEvent:(NSNumber *)eventID mark:(CGFloat)mark reason:(BTWFeedbackReason)reason comment:(NSString *)comment
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"eventId"] = eventID;
    payload[@"points"] = @((int)mark);
    payload[@"reason"] = @(reason);
    payload[@"comment"] = [comment isValidString] ? comment : @"";
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"responses/add" params:parameters];
    return resultMethod;
}

@end
