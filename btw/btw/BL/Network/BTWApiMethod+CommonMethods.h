//
//  BTWApiMethod+CommonMethods.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (CommonMethods)

+ (BTWApiMethod *)pingMethod;

@end
