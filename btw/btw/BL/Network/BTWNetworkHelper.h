//
//  BTWNetworkHelper.h
//  BTW
//
//  Created by Mikhail Rakhmalevich on 30.07.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

typedef NS_ENUM(NSUInteger, BTWNetworkResponseType) {
    BTWNetworkResponseTypeBlank = 0,
    BTWNetworkResponseTypeText,
    BTWNetworkResponseTypeJSON,
    BTWNetworkResponseTypeImage
};

@class BTWApiMethod;
@interface BTWNetworkHelper : NSObject

+ (instancetype)sharedInstance;

- (NSOperation *)operationWithMethod:(BTWApiMethod *)method callback:(BTWNetworkResponseBlock)callback;
- (NSOperation *)operationWithMethod:(BTWApiMethod *)method backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback;
- (NSOperation *)operationWithMethod:(BTWApiMethod *)method progressHandler:(BTWNetworkProgressBlock)progressBlock backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback;
- (void)enqueueOperation:(NSOperation *)operation;

- (void)sendRequestWithMethod:(BTWApiMethod *)method callback:(BTWNetworkResponseBlock)callback;
- (void)sendRequestWithMethod:(BTWApiMethod *)method backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback;
- (void)sendRequestWithMethod:(BTWApiMethod *)method progressHandler:(BTWNetworkProgressBlock)progressBlock backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback;
- (void)sendRequestWithMethod:(BTWApiMethod *)method responseType:(BTWNetworkResponseType)responseType progressHandler:(BTWNetworkProgressBlock)progressBlock backgroundProcessing:(BTWDataBlock)backgroundProcessingBlock callback:(BTWNetworkResponseBlock)callback;

- (void)getImageWithMethod:(BTWApiMethod *)method callback:(BTWNetworkResponseBlock)callback;

@end
