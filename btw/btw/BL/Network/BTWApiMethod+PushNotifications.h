//
//  BTWApiMethod+Notifications.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (PushNotifications)

+ (BTWApiMethod *)subscribeMethodForToken:(NSString *)token;
+ (BTWApiMethod *)unsubscribeMethodForToken:(NSString *)token;

@end
