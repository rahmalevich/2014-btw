//
//  BTWApiMethod+Notifications.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod+Notifications.h"

@implementation BTWApiMethod (Notifications)

+ (BTWApiMethod *)notificationsListMethod
{
    return [self notificationsListMethodWithOffset:-1 count:0];
}

+ (BTWApiMethod *)notificationsListMethodWithOffset:(NSInteger)offset count:(NSUInteger)count
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    if (offset >= 0) {
        payload[@"offset"] = @(offset);
        payload[@"count"] = @(count);
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"notifications/list" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)notificationsConfirmMethod
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = @[];
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"notifications/confirm" params:parameters];
    return resultMethod;
}

@end
