//
//  BTWApiMethod+ProfileMethods.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (ProfileMethods)

+ (BTWApiMethod *)createUUIDMethod;
+ (BTWApiMethod *)registerMethodWithFirstName:(NSString *)firstName lastName:(NSString *)lastName birthdate:(NSDate *)birthdate gender:(BTWUserGender)gender email:(NSString *)email password:(NSString *)password;
+ (BTWApiMethod *)registerMethodWithFacebookID:(NSString *)facebookID sessionKey:(NSString *)sessionKey;
+ (BTWApiMethod *)registerMethodWithVkID:(NSString *)vkID sessionKey:(NSString *)sessionKey;
+ (BTWApiMethod *)loginMethodWithEmail:(NSString *)email password:(NSString *)password;
+ (BTWApiMethod *)loginMethodWithFacebookID:(NSString *)facebookID sessionKey:(NSString *)sessionKey;
+ (BTWApiMethod *)loginMethodWithVkID:(NSString *)vkID sessionKey:(NSString *)sessionKey;
+ (BTWApiMethod *)loginMethodWithUserID:(NSString *)userID token:(NSString *)token;
+ (BTWApiMethod *)saveProfileMethodWithProfile:(User *)profile;
+ (BTWApiMethod *)getFullProfileMethodForUser:(User *)user;
+ (BTWApiMethod *)linkMethodWithFacebookID:(NSString *)facebookID sessionKey:(NSString *)sessionKey;
+ (BTWApiMethod *)linkMethodWithVkID:(NSString *)vkID sessionKey:(NSString *)sessionKey;
+ (BTWApiMethod *)getUserFromFacebookForID:(NSString *)userID;
+ (BTWApiMethod *)getUserFromVKMethodForID:(NSString *)userID;

+ (BTWApiMethod *)getQuizMethod;
+ (BTWApiMethod *)submitMethodForAnswers:(NSArray *)answers;

+ (BTWApiMethod *)getCarsMethod;
+ (BTWApiMethod *)updateCarMethod:(Car *)car;

+ (BTWApiMethod *)getVKFriendsForUserID:(NSString *)userID;
+ (BTWApiMethod *)getFacebookFreindsForUserID:(NSString *)userID;

+ (BTWApiMethod *)setPhoneMethodForNumber:(NSString *)number;
+ (BTWApiMethod *)confirmPhoneMethodForCode:(NSString *)code;

@end
