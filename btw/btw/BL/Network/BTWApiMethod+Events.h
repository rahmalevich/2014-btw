//
//  BTWApiMethod+Events.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 18.05.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

static NSString * const kEventStatePlanned = @"planned";
static NSString * const kEventStateInprogress = @"inprogress";
static NSString * const kEventStateCompleted = @"completed";
static NSString * const kEventStateCancelled = @"cancelled";

@interface BTWApiMethod (Events)

+ (BTWApiMethod *)eventsListMethod;
+ (BTWApiMethod *)eventsListMethodWithOffset:(NSInteger)offset count:(NSInteger)count state:(NSString *)state;
+ (BTWApiMethod *)updateMethodForEvent:(NSNumber *)eventID toState:(NSString *)state;
+ (BTWApiMethod *)confirmEventsMethodForStates:(NSArray *)statesArray;

@end
