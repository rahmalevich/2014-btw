//
//  BTWApiMethod+ProfileMethods.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod+ProfileMethods.h"
#import "BTWFormattingService.h"

@implementation BTWApiMethod (ProfileMethods)

+ (BTWApiMethod *)createUUIDMethod
{
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"uuid/create" params:nil];
    return resultMethod;
}

+ (BTWApiMethod *)registerMethodWithFirstName:(NSString *)firstName lastName:(NSString *)lastName birthdate:(NSDate *)birthdate gender:(BTWUserGender)gender email:(NSString *)email password:(NSString *)password
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"pass"] = password;
    payload[@"type"] = @"password";
    payload[@"name"] = firstName;
    payload[@"surname"] = lastName;
    payload[@"gender"] = @(gender);
    if (birthdate) {
        payload[@"birthday"] = [[BTWFormattingService dateFormatterWithFormat:@"yyyy.MM.dd"] stringFromDate:birthdate];
    }
    
    payload[@"platform"] = @"ios";
    payload[@"deviceInfo"] = [UIDevice currentDevice].model;

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"userId"] = email;
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"register" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)registerMethodWithFacebookID:(NSString *)facebookID sessionKey:(NSString *)sessionKey
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"facebookId"] = facebookID;
    payload[@"pass"] = sessionKey;
    payload[@"type"] = @"facebook";
    payload[@"platform"] = @"ios";
    payload[@"deviceInfo"] = [UIDevice currentDevice].model;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"userId"] = facebookID;
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"register" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)registerMethodWithVkID:(NSString *)vkID sessionKey:(NSString *)sessionKey
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"vkontakteId"] = vkID;
    payload[@"pass"] = sessionKey;
    payload[@"type"] = @"vkontakte";
    payload[@"platform"] = @"ios";
    payload[@"deviceInfo"] = [UIDevice currentDevice].model;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"userId"] = vkID;
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"register" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)loginMethodWithEmail:(NSString *)email password:(NSString *)password
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"pass"] = password;
    payload[@"type"] = @"password";
    payload[@"locale"] = [NSLocale autoupdatingCurrentLocale].localeIdentifier;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"userId"] = email;
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"login" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)loginMethodWithFacebookID:(NSString *)facebookID sessionKey:(NSString *)sessionKey
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"facebookId"] = facebookID;
    payload[@"pass"] = sessionKey;
    payload[@"type"] = @"facebook";
    payload[@"locale"] = [NSLocale autoupdatingCurrentLocale].localeIdentifier;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"login" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)loginMethodWithVkID:(NSString *)vkID sessionKey:(NSString *)sessionKey
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"vkontakteId"] = vkID;
    payload[@"pass"] = sessionKey;
    payload[@"type"] = @"vkontakte";
    payload[@"locale"] = [NSLocale autoupdatingCurrentLocale].localeIdentifier;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"login" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)loginMethodWithUserID:(NSString *)userID token:(NSString *)token
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"pass"] = token;
    payload[@"type"] = @"token";
    payload[@"locale"] = [NSLocale autoupdatingCurrentLocale].localeIdentifier;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"userId"] = userID;
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"login" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)saveProfileMethodWithProfile:(User *)profile
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = profile.backend_id;
    payload[@"name"] = profile.name;
    payload[@"surname"] = profile.surname;
    payload[@"gender"] = profile.gender;
    if (profile.birthday) {
        payload[@"birthday"] = [[BTWFormattingService dateFormatterWithFormat:@"yyyy.MM.dd"] stringFromDate:profile.birthday];
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"profile/put" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getFullProfileMethodForUser:(User *)user
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = user.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"profile/get" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)linkMethodWithFacebookID:(NSString *)facebookID sessionKey:(NSString *)sessionKey
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"facebookId"] = facebookID;
    payload[@"accessToken"] = sessionKey;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"facebook/link" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)linkMethodWithVkID:(NSString *)vkID sessionKey:(NSString *)sessionKey
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"vkontakteId"] = vkID;
    payload[@"accessToken"] = sessionKey;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"vkontakte/link" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getQuizMethod
{
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"quiz/list" params:nil];
    return resultMethod;
}

+ (BTWApiMethod *)submitMethodForAnswers:(NSArray *)answers
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = [answers valueForKey:@"backend_id"];
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"quiz/submit" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getCarsMethod
{
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"directory/cars" params:nil];
    return resultMethod;
}

+ (BTWApiMethod *)getVKFriendsForUserID:(NSString *)userID
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = userID;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"vkontakte/friends" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getFacebookFreindsForUserID:(NSString *)userID
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = userID;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"facebook/friends" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)updateCarMethod:(Car *)car
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    if ([car.brand isValidString]) {
        payload[@"brand"] = car.brand;
    }
    if ([car.model isValidString]) {
        payload[@"model"] = car.model;
    }
    if ([car.color isValidString]) {
        payload[@"color"] = car.color;
    }
    if ([car.year integerValue] > 0) {
        payload[@"year"] = car.year;
    }
    if ([car.number isValidString]) {
        payload[@"number"] = car.number;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"profile/update-car" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)setPhoneMethodForNumber:(NSString *)number
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = number;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"profile/set-phone" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)confirmPhoneMethodForCode:(NSString *)code
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = code;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"profile/confirm-phone" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getUserFromFacebookForID:(NSString *)userID
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = userID;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"facebook/get" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getUserFromVKMethodForID:(NSString *)userID
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = userID;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"vkontakte/get" params:parameters];
    return resultMethod;
}

@end
