//
//  BTWApiMethod+GalleryMethods.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 05.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod+GalleryMethods.h"

@implementation BTWApiMethod (GalleryMethods)

+ (BTWApiMethod *)addMethodForImage:(UIImage *)image
{
    return [self addMethodForImage:image asAvatar:NO];
}

+ (BTWApiMethod *)addMethodForImage:(UIImage *)image asAvatar:(BOOL)asAvatar
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"dataBase64"] = [UIImageJPEGRepresentation(image, 0.5) base64EncodedStringWithOptions:0];
    payload[@"cropX"] = @(0);
    payload[@"cropY"] = @(0);
    payload[@"cropWidth"] = @(image.size.width);
    payload[@"cropHeight"] = @(image.size.height);
    payload[@"asAvatar"] = @(asAvatar);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"gallery/add" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)removeMethodForGalleryImage:(GalleryImage *)galleryImage
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = galleryImage.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"gallery/remove" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)listMethodForUser:(User *)user
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"userId"] = user.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"gallery/list" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)setAvatarMethodForImage:(GalleryImage *)galleryImage
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = galleryImage.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"gallery/to-avatar" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)comlaintMethodForImage:(GalleryImage *)galleryImage
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = galleryImage.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"gallery/complaint" params:parameters];
    return resultMethod;
}

@end
