//
//  BTWApiMethod+GoogleServices.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (GoogleServices)

+ (BTWApiMethod *)directionMethodForRoute:(Route *)route;
+ (BTWApiMethod *)staticMapMethodForCoordinate:(CLLocationCoordinate2D)coordinate;
+ (BTWApiMethod *)staticMapMethodForPolyline:(NSString *)polyline;

@end
