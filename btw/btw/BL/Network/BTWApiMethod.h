//
//  BTWApiMethod.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 01.11.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTWApiMethod : NSObject

@property (nonatomic, strong, readonly) NSString *httpMethod;
@property (nonatomic, strong, readonly) NSString *urlString;
@property (nonatomic, strong, readonly) NSDictionary *params;

+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params;
+ (id)methodWithHTTPMethod:(NSString *)method URLString:(NSString *)urlString params:(NSDictionary *)params addBaseParams:(BOOL)addSpecificParams;

- (void)updateSignature;

@end
