//
//  BTWApiMethod+Notifications.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 02.03.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (Notifications)

+ (BTWApiMethod *)notificationsListMethod;
+ (BTWApiMethod *)notificationsListMethodWithOffset:(NSInteger)offset count:(NSUInteger)count;
+ (BTWApiMethod *)notificationsConfirmMethod;

@end
