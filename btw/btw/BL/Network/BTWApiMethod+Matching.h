//
//  BTWApiMethod+Matching.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod.h"

@interface BTWApiMethod (Matching)

+ (BTWApiMethod *)searchMatchesMethod;
+ (BTWApiMethod *)markMatchMethodForUser:(User *)user asKeeped:(BOOL)keep;
+ (BTWApiMethod *)likedMeMethod;

@end
