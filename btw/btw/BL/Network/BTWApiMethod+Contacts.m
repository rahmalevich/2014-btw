//
//  BTWApiMethod+Contacts.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 03.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod+Contacts.h"

@implementation BTWApiMethod (Contacts)

+ (BTWApiMethod *)getContactsMethod
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = @[];
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"contacts/list" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getContactMethodForTopicId:(NSString *)topicId
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"id"] = topicId;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"contacts/get" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getHistoryMethodForDialog:(Dialog *)dialog
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"topic"] = dialog.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"contacts/history" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)getHistoryMethodForDialog:(Dialog *)dialog offset:(NSUInteger)offset count:(NSUInteger)count
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"topic"] = dialog.backend_id;
    payload[@"offset"] = @(offset);
    payload[@"count"] = @(count);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"contacts/history" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)uploadPhotoAttachmentMethod:(UIImage *)image
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"dataBase64"] = [UIImageJPEGRepresentation(image, 0.5) base64EncodedStringWithOptions:0];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"chat-media/image" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)removeMethodForDialog:(Dialog *)dialog
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"profileId"] = dialog.user.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"contacts/remove" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)confirmHistoryMethodForDialog:(Dialog *)dialog
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"topic"] = dialog.backend_id;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"contacts/confirm-history" params:parameters];
    return resultMethod;
}

@end
