//
//  BTWApiMethod+Matching.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 23.01.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWApiMethod+Matching.h"
#import "BTWSettingsService.h"
#import "BTWLocationManager.h"

static CGFloat kCount = 10;
static CGFloat kMaxDistance = 10000;

@implementation BTWApiMethod (Matching)

+ (BTWApiMethod *)searchMatchesMethod
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"count"] = @(kCount);
    payload[@"gender"] = [BTWSettingsService settingsValueForKey:kFilterGenderKey];
    payload[@"minAge"] = [BTWSettingsService settingsValueForKey:kFilterLowerAgeKey];
    payload[@"maxAge"] = [BTWSettingsService settingsValueForKey:kFilterUpperAgeKey];
    payload[@"maxDistance"] = @(kMaxDistance);
    
    CLLocationCoordinate2D currentLocation = [BTWLocationManager sharedInstance].currentLocation.coordinate;
    payload[@"location"] = @{@"lon":@(currentLocation.longitude), @"lat":@(currentLocation.latitude)};
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"person/search" params:parameters];
    return resultMethod;

}

+ (BTWApiMethod *)markMatchMethodForUser:(User *)user asKeeped:(BOOL)keep
{
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    payload[@"profileId"] = user.backend_id;
    payload[@"keep"] = @(keep);

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"payload"] = payload;
    
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"person/mark" params:parameters];
    return resultMethod;
}

+ (BTWApiMethod *)likedMeMethod
{
    BTWApiMethod *resultMethod = [BTWApiMethod methodWithHTTPMethod:@"POST" URLString:@"person/liked-me" params:nil];
    return resultMethod;
}

@end
