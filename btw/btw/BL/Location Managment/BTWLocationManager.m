//
//  BTWLocationManager.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWLocationManager.h"
#import "BTWNetworkHelper.h"

#import "SVGeocoder.h"
#import "SPGooglePlacesAutocomplete.h"

static CLLocationDirection const kDistanceFilter = 100.0f;
static CGFloat const kAddressSearchRadius = 10000.0f;
static NSString *const kAddressSearchRegionID = @"kAddressSearchRegionID";

@interface BTWLocationManager () <CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) SPGooglePlacesAutocompleteQuery *placesQuery;
@end

@implementation BTWLocationManager

#pragma mark - Initializaton
static BTWLocationManager *_sharedInstance = nil;
+ (BTWLocationManager *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [BTWLocationManager new];
    });
    return _sharedInstance;
}

- (id)init
{
    if (self = [super init]) {
        self.locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kDistanceFilter;
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined &&
            [_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] &&
            [_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [_locationManager performSelector:@selector(requestAlwaysAuthorization)];
            [_locationManager performSelector:@selector(requestWhenInUseAuthorization)];
        } else {
            [self initializeLocationManager];
        }
        
        self.placesQuery = [[SPGooglePlacesAutocompleteQuery alloc] initWithApiKey:kGoogleMapsAppID];
        _placesQuery.radius = kAddressSearchRadius;
    }
    return self;
}

- (void)initializeLocationManager
{
    [_locationManager startUpdatingLocation];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidSetInitialLocationNotification object:nil];
}

#pragma mark - Location
- (void)updateLocation
{
    [_locationManager stopUpdatingLocation];
    [_locationManager startUpdatingLocation];
}

#pragma mark - Location manager delegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        [self initializeLocationManager];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [locations firstObject];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidUpdateLocationNotification object:nil];
}

#pragma mark - Geocoding
- (void)geocodeCurrentLocationWithCompletionHandler:(void (^)(BTWPlacemark *, NSError*))completionHandler
{
    if (completionHandler) {
        [completionHandler copy];
        
        [SVGeocoder reverseGeocode:_locationManager.location.coordinate completion:^(NSArray *placemarks, NSHTTPURLResponse *urlResponse, NSError *error)
        {
            BTWPlacemark *resultPlacemark = nil;
            if ([placemarks count] > 0) {
                resultPlacemark = [BTWPlacemark placemarkWithSVPlacemark:[placemarks firstObject] isCurrentLocation:YES];
            }
            completionHandler(resultPlacemark, error);
        }];
    }
}

- (void)geocodeAddress:(NSString *)address withCompletionHandler:(void (^)(NSArray *placemarks, NSError *error))completionHandler
{
    if (completionHandler) {
        [completionHandler copy];

        _placesQuery.location = _locationManager.location.coordinate;
        _placesQuery.input = address;
        [_placesQuery fetchPlaces:^(NSArray *places, NSError *error){
            NSArray *placemarks = [BTWPlacemark placemarksFromArray:places];
            completionHandler(placemarks, error);
        }];
    }
}

@end
