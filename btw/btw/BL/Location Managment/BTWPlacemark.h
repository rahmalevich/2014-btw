//
//  BTWPlacemark.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@class SVPlacemark, SPGooglePlacesAutocompletePlace;
@interface BTWPlacemark : NSObject

@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSString *subtitle;
@property (nonatomic, copy, readonly) NSString *address;
@property (nonatomic, copy, readonly) NSString *country;
@property (nonatomic, copy, readonly) NSString *locality;
@property (nonatomic, copy, readonly) NSString *reference;
@property (nonatomic, readonly) BOOL isCurrentLocation;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

+ (BTWPlacemark *)placemarkWithTitle:(NSString *)title coordinate:(CLLocationCoordinate2D)coordinate;
+ (BTWPlacemark *)placemarkWithSVPlacemark:(SVPlacemark *)placemark;
+ (BTWPlacemark *)placemarkWithSVPlacemark:(SVPlacemark *)placemark isCurrentLocation:(BOOL)isCurrentLocation;
+ (BTWPlacemark *)placemarkWithSPGooglePlacesAutocompletePlace:(SPGooglePlacesAutocompletePlace *)place;
+ (NSArray *)placemarksFromArray:(NSArray *)placemarksArray;

- (void)updateLocationWithCompletionHandler:(BTWErrorBlock)completionBlock;

@end
