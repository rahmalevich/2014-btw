//
//  BTWMapSnapshotter.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 11.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

typedef void(^BTWMapSnapshotterBlock)(UIImage *snapshot, NSError *error);

@interface BTWMapSnapshotter : NSObject

//@property (nonatomic, strong) UIImage *locationPinImage;
//@property (nonatomic, strong) UIImage *startPinImage;
//@property (nonatomic, strong) UIImage *finishPinImage;
//@property (nonatomic, assign) CGSize shapshotSize;

- (void)getSnapshotForLocationCoordinate:(CLLocationCoordinate2D)coordinate withCompletionHandler:(BTWMapSnapshotterBlock)completionHandler;
- (void)getSnapshotForPolyline:(NSString *)polyline withCompletionHandler:(BTWMapSnapshotterBlock)completionHandler;

@end
