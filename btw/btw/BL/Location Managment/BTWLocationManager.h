//
//  BTWLocationManager.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 15.10.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWPlacemark.h"

static NSString * const kLocationManagerDidSetInitialLocationNotification = @"kLocationManagerDidSetInitialLocationNotification";
static NSString * const kLocationManagerDidUpdateLocationNotification = @"kLocationManagerDidUpdateLocationNotification";

@interface BTWLocationManager : NSObject

@property (nonatomic, strong, readonly) CLLocation *currentLocation;

+ (BTWLocationManager *)sharedInstance;
- (void)updateLocation;

- (void)geocodeCurrentLocationWithCompletionHandler:(void(^)(BTWPlacemark *placemark, NSError *error))completionHandler;
- (void)geocodeAddress:(NSString *)address withCompletionHandler:(void(^)(NSArray *placemarks, NSError *error))completionHandler;

@end
