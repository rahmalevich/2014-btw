//
//  BTWPlacemark.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 22.12.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

#import "BTWPlacemark.h"
#import "BTWLocationManager.h"

#import "SVPlacemark.h"
#import "SPGooglePlacesAutocompletePlace.h"

static NSInteger kSubtitleTermsOffset = 2;

@interface BTWPlacemark ()

@property (nonatomic, copy, readwrite) NSString *title;
@property (nonatomic, copy, readwrite) NSString *subtitle;
@property (nonatomic, copy, readwrite) NSString *address;
@property (nonatomic, copy, readwrite) NSString *country;
@property (nonatomic, copy, readwrite) NSString *locality;
@property (nonatomic, copy, readwrite) NSString *reference;
@property (nonatomic, readwrite) BOOL isCurrentLocation;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

@property (nonatomic, strong) id internalPlacemark;

@end

@implementation BTWPlacemark

#pragma mark - Initialization
+ (BTWPlacemark *)placemarkWithTitle:(NSString *)title coordinate:(CLLocationCoordinate2D)coordinate
{
    BTWPlacemark *newPlacemark = [BTWPlacemark new];
    newPlacemark.title = title;
    newPlacemark.coordinate = coordinate;
    return newPlacemark;
}

+ (BTWPlacemark *)placemarkWithSVPlacemark:(SVPlacemark *)svPlacemark
{
    return [self placemarkWithSVPlacemark:svPlacemark isCurrentLocation:NO];
}

+ (BTWPlacemark *)placemarkWithSVPlacemark:(SVPlacemark *)svPlacemark isCurrentLocation:(BOOL)isCurrentLocation
{
    BTWPlacemark *newPlacemark = [BTWPlacemark new];
    newPlacemark.isCurrentLocation = isCurrentLocation;
    newPlacemark.title = [svPlacemark name];
    newPlacemark.subtitle = svPlacemark.formattedAddress;
    newPlacemark.address = svPlacemark.formattedAddress;
    newPlacemark.country = svPlacemark.country;
    newPlacemark.locality = svPlacemark.locality;
    newPlacemark.coordinate = svPlacemark.coordinate;
    newPlacemark.internalPlacemark = svPlacemark;
    return newPlacemark;
}

+ (BTWPlacemark *)placemarkWithSPGooglePlacesAutocompletePlace:(SPGooglePlacesAutocompletePlace *)spPlacemark
{
    BTWPlacemark *newPlacemark = [BTWPlacemark new];
    newPlacemark.title = [spPlacemark.terms count] > 0 ? spPlacemark.terms[0][@"value"] : spPlacemark.name;
    if ([spPlacemark.terms count] > kSubtitleTermsOffset) {
        NSMutableString *subtitle = [NSMutableString string];
        for (NSInteger i = kSubtitleTermsOffset; i < [spPlacemark.terms count]; i++) {
            [subtitle appendFormat:@"%@%@", i == kSubtitleTermsOffset ? @"" : @", ", spPlacemark.terms[i][@"value"]];
        }
        newPlacemark.subtitle = [NSString stringWithString:subtitle];
    }
    newPlacemark.address = spPlacemark.description;
    newPlacemark.reference = spPlacemark.reference;
    newPlacemark.internalPlacemark = spPlacemark;
    return newPlacemark;
}

+ (NSArray *)placemarksFromArray:(NSArray *)placemarksArray
{
    NSMutableArray *resultPlacemarks = [NSMutableArray array];
    for (id placemark in placemarksArray) {
        BTWPlacemark *btwPlacemark = nil;
        if ([placemark isKindOfClass:[SVPlacemark class]]) {
            btwPlacemark = [BTWPlacemark placemarkWithSVPlacemark:placemark];
        } else if ([placemark isKindOfClass:[SPGooglePlacesAutocompletePlace class]]) {
            btwPlacemark = [BTWPlacemark placemarkWithSPGooglePlacesAutocompletePlace:placemark];
        }
        
        if (btwPlacemark) {
            [resultPlacemarks addObject:btwPlacemark];
        }
    }
    
    return [NSArray arrayWithArray:resultPlacemarks];
}

- (id)init
{
    if (self = [super init]) {
        self.coordinate = kCLLocationCoordinate2DInvalid;
        self.isCurrentLocation = NO;
    }
    return self;
}

#pragma mark - Location
- (void)updateLocationWithCompletionHandler:(BTWErrorBlock)completionBlock
{
    if (!CLLocationCoordinate2DIsValid(_coordinate)) {
        if ([_internalPlacemark isKindOfClass:[SPGooglePlacesAutocompletePlace class]]) {
            [completionBlock copy];
            [(SPGooglePlacesAutocompletePlace *)_internalPlacemark resolveToPlacemark:^(SVPlacemark *placemark, NSString *addressString, NSError *error)
            {
                if (!error) {
                    self.coordinate = placemark.coordinate;
                }
                if (completionBlock) {
                    completionBlock(error);
                }
            }];
        } else {
            if (completionBlock) {
                completionBlock([NSError errorWithDomain:kBTWInternalErrorDomain code:BTWInternalErrorCodeUnknown userInfo:nil]);
            }
        }
    } else {
        if (completionBlock) {
            completionBlock(nil);
        }
    }
}

@end
