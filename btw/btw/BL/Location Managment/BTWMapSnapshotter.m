//
//  BTWMapSnapshotter.m
//  btw
//
//  Created by Mikhail Rakhmalevich on 11.02.15.
//  Copyright (c) 2015 BTW. All rights reserved.
//

#import "BTWMapSnapshotter.h"
#import "BTWApiMethod+GoogleServices.h"
#import "BTWNetworkHelper.h"

@interface BTWMapSnapshotter ()
@end

@implementation BTWMapSnapshotter

#pragma mark - Snapshot methods
- (void)getSnapshotForLocationCoordinate:(CLLocationCoordinate2D)coordinate withCompletionHandler:(BTWMapSnapshotterBlock)completionHandler
{
    NSAssert(completionHandler, @"Error! You should set completion handler!");
    
    BTWApiMethod *staticMapMethod = [BTWApiMethod staticMapMethodForCoordinate:coordinate];
    [[BTWNetworkHelper sharedInstance] getImageWithMethod:staticMapMethod callback:^(UIImage *response, NSError *error){
        completionHandler(response, error);
    }];
}

- (void)getSnapshotForPolyline:(NSString *)polyline withCompletionHandler:(BTWMapSnapshotterBlock)completionHandler
{
    NSAssert(completionHandler, @"Error! You should set completion handler!");
    
    BTWApiMethod *staticMapMethod = [BTWApiMethod staticMapMethodForPolyline:polyline];
    [[BTWNetworkHelper sharedInstance] getImageWithMethod:staticMapMethod callback:^(UIImage *response, NSError *error){
        completionHandler(response, error);
    }];
}

@end
