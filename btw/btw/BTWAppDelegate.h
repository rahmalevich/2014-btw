//
//  BTWAppDelegate.h
//  btw
//
//  Created by Mikhail Rakhmalevich on 16.09.14.
//  Copyright (c) 2014 BTW. All rights reserved.
//

@interface BTWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end


@interface BTWApplication : UIApplication

@end
